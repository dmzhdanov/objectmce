# ObjectMCE library 
c++ library with Python interface for performing quantum dynamical calculations using the fluid analogies to quantum mechanics.

## Prerequisites

**Compilation of the ObjectMCE library**

The code compliation was tested with 

- g++ gnu compiler on Linux,
- MSVC 15 2017 and 16 2019 compilers on Windows. 

Since some features of c++17 are used, the earlier versions of MSVC will not suit.

[Python 3](https://www.python.org/downloads/) and [CMake](https://cmake.org) should be present.

The following libraries are required:

- [Eigen c++ library](eigen.tuxfamily.org)

- [Boost library](https://www.boost.org)

- pybind11 (install it by running `python3 -m pip install pybind11` from command line)

None of the first two libraries needs to be compiled - just download and unpack.

NOTE for linux installation: 1. You might need to [update cmake to the latest version](https://askubuntu.com/questions/829310/how-to-upgrade-cmake-in-ubuntu) first, or to remove "-B ./bin_rl" from 6th line of generate_project_release.sh (flag -B is new). 2. As of January 2020, pip installation of pybind11 on Ubuntu 18 is broken (see [here](https://github.com/pybind/pybind11/issues/890#issuecomment-570630304) and [here](https://github.com/craigstar/SophusPy/issues/3)). Use manual installation instead using the following script:

```
git clone https://github.com/pybind/pybind11.git
cd pybind11
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local/
make -j8
make install
```

**Python interface**

1. To work with Python interface, JupyterLab is needed: `python3 -m pip install JupyterLab`
2. Install matplotlib extension: `python3 -m pip install ipympl`
3. Then, install the following extensions for JupyterLab 
(to enable extensions, you need to install [node.js](https://nodejs.org) first):
    * (necessary)
        - @jupyter-widgets/jupyterlab-manager
        - jupyter-matplotlib
    * (recommended)
        - @aquirdturtle/collapsible_headings
        - @ryantam626/jupyterlab_code_formatter

NOTE: You can install extensions from command line by running

        jupyter labextension install @jupyter-widgets/jupyterlab-manager
        jupyter labextension install jupyter-matplotlib  
		etc.



## Installation on Windows machine
1. Open file *CMakeLists.txt* and edit lines 14 and 15 to specify proper paths to Eigen and Boost libraries, as well as to Python installation

2. Assuming Visual Studio 2019, run the batch file *generate_project_release.bat* 
NOTE: In the case of Visual Studio 2017, you need edit *generate_project_release.bat* and uncomment the corresponding line.

3. Run *generate_project_release.bat*. As result, the MSVC solution *ObjectMCE.sln* should be generated in the new folder *bin_r*

4. Build entire solution *ObjectMCE.sln*. As result, the python library *PyMCE.pyd* should be created in *\bin_r\Release* forder.

## Installation on Linux machine
1. Open file *CMakeLists.txt* and edit lines 17-25 to specify proper paths to Eigen and Boost libraries, as well as to pybind11 installation.

2. Run the batch file *generate_project_release.sh*. As result, the folder *bin_rl* should be generated.

3. In command prompt, type `cd ./bin_rl` and then `make`. As result, the python library *libPyMCE.pyd* should be created in *bin_rl/lib* forder.

## Running the example via python interface

1. Go to */Uses/simple_tunneling* folder and open *simple_tunneling.ipynb* in JupyterLab. 

2. Follow the instructions in the file.