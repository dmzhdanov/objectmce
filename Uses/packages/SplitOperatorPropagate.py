import numpy as np

def PropagateHKin(state, delta_x, delta_t, mass=1., hbar=1.):
    p_state = np.fft.ifft(
        state, norm="ortho")
    p_multiplier = np.zeros(state.size)
    mult = -hbar * 2 * np.pi / (state.size * delta_x)
    for i in range(int(round(state.size / 2))):
        p_multiplier[i] = (i * mult)
    for i in range(int(round(state.size / 2)), state.size):
        p_multiplier[i] = (i - state.size) * mult
    return np.fft.fft(
        (np.exp(-1j * (p_multiplier**2) / (2 * mass) * delta_t)) * p_state,
        norm="ortho")


def PropagateHPot(state, potential, delta_t, x0, delta_x):
    return np.fromfunction(
        np.vectorize(
            lambda i: state[int(i)] * np.exp(-1j * delta_t * potential(x0 + i * delta_x))
        ), (state.size, ),
        dtype=int)

def SplitOperatorPropagate(propagation_time, state, potential, x0, delta_x, delta_t, mass = 1, hbar = 1):
    output = np.copy(state)
    t = delta_t
    while (t<propagation_time):
        output=PropagateHKin(output, delta_x, delta_t, mass, hbar)
        output=PropagateHPot(output, potential, delta_t, x0, delta_x)
        t = t + delta_t
    end_delta_t=propagation_time-t+delta_t
    output=PropagateHKin(output, delta_x, end_delta_t, mass, hbar)
    output=PropagateHPot(output, potential, end_delta_t, x0, delta_x)
    return output