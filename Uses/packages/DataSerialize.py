import matplotlib.pyplot as plt
from matplotlib import colors as mcolors
import numpy as np
import inspect # to serialize functions
import sys
#sys.path.insert(0, 'E:/My_documents/c++/CHAMPS/ObjectMCE/ObjectMCE/bin_r/Release') #path to PyMCE.pyd
#sys.path.insert(0, 'E:/My_documents\\c++\\CHAMPS\\ObjectMCE\\ObjectMCE\\bin\\Debug') #path to PyMCE.pyd
import PyMCE
import linecache
#import __builtin__


def ToJSONComplex(z):
    if (isinstance(z, complex)==1):
        return {"real": z.real, "imag": z.imag}
    else:
        output = [];
        for i in range(0,len(z)):
            output.append({"real": z[i].real, "imag": z[i].imag})
        return output


def SerializeState(state):
    N = state.Size()
    K = state.Configuration().NumberOfSpatialDOFs()
    output = []
    for n in range(0, N):
        phase_space_point = []            
        for k in range(0, K):
            phase_space_point.append(
                {
                    "P" : state[n].GetCenter(k).P(),
                    "X" : state[n].GetCenter(k).X()
                }) 
        output.append({
            "multiplier": {
                "amplitude":
                ToJSONComplex(state[n].GetMultiplier().Amplitude()),
                "phase": state[n].GetMultiplier().Phase()
            },
            "phase_space_point": phase_space_point
        })

    return output


def ReconstructState(data, factory):
    N = len(data)
    state = factory.Create(N)
    K = state.Configuration().NumberOfSpatialDOFs()
    for n in range(0, N):
        state[n].GetMultiplier().Amplitude(
            data[n]["multiplier"]["amplitude"]["real"] +
            1j * data[n]["multiplier"]["amplitude"]["imag"])
        state[n].GetMultiplier().Phase(data[n]["multiplier"]["phase"])
        for k in range(0, K):
            state[n].GetCenter(k).X(data[n]["phase_space_point"][k]['X'])
            state[n].GetCenter(k).P(data[n]["phase_space_point"][k]['P'])
    return state


global potential_scale, wavefunction_scale

def SerializeSimulationData(data, properties, save_so_state=False):
    global wavefunction_scale
    global potential_scale
    global p_potential
    #simulation_data.append([time, step_no, wavefunction.Clone(True),so_state.copy()])
    frames_list = []
    for n in range(len(data)):
        frames_list.append({
            "time":
            data[n][0],
            "step_no":
            data[n][1],
            "state":
            SerializeState(data[n][2]),
            "so_state":
            ToJSONComplex(data[n][3]) if save_so_state else []
        })

    try:
        p_potential_str = str(inspect.getsourcelines(p_potential)[0][0]).strip(
            "['\\n']").split(" = ")[1]
    except:
        p_potential_str = "0"
    return {
        "configuration": {
            "number_of_spatial_dofs":
            data[0][2].Configuration().NumberOfSpatialDOFs(),
            "gaussian_width":
            data[0][2].Configuration().GaussianWidth(0)
        },
        "simulation_data": frames_list,
        "properties": properties
    }

global p_potential, potential_scale, wavefunction_scale
p_potential = None
potential_scale = None
wavefunction_scale = None

def ReconstructSimulationData(jsondict):
    global wavefunction_scale
    global potential_scale
    global p_potential
    #simulation_data.append([time, step_no, wavefunction.Clone(True),so_state.copy()])
    factory = PyMCE.WavefunctionFactory(
        1, jsondict["configuration"]["number_of_spatial_dofs"],
        jsondict["configuration"]["gaussian_width"], 500)

    simulation_data = []
    frames_list = jsondict["simulation_data"]
    for n in range(len(frames_list)):
        so_state = np.zeros(len(frames_list[n]["so_state"]), dtype=complex)
        for i in range(so_state.size):
            so_state[i] = frames_list[n]["so_state"][i][
                "real"] + 1j * frames_list[n]["so_state"][i]["imag"]
        simulation_data.append([
            frames_list[n]["time"], frames_list[n]["step_no"],
            ReconstructState(frames_list[n]["state"], factory), so_state
        ])
    wavefunction_scale = jsondict["properties"]["wavefunction_scale"]
    potential_scale = jsondict["properties"]["potential_scale"]
    #__builtins__.potential_scale=potential_scale
    ##### restoring p_potential this vay to recover source code as well (used by inspect.getsourcelines(p_potential))
    ## see https://stackoverflow.com/questions/50822656/source-code-for-an-evaled-dynamically-generated-function-in-python
    filename = '<dynamic-123456>'  # Angle brackets may be required?
    source = "p_potential = " + jsondict["properties"]["p_potential"]
    code = compile(source, filename, 'exec')
    exec(code)
    lines = [line + '\n' for line in source.splitlines()]
    linecache.cache[filename] = (len(source), None, lines, filename)
    ##### end of restoring p_potential
    ##p_potential = eval(jsondict["properties"]["p_potential"]) #restores p_potential but not its source code
    return simulation_data