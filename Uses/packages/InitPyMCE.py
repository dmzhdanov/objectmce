import sys
#sys.path.insert(0, 'E:/My_documents/c++/CHAMPS/ObjectMCE/ObjectMCE/bin_r/Release') #path to PyMCE.pyd
#sys.path.insert(0, 'E:/My_documents\\c++\\CHAMPS\\ObjectMCE\\ObjectMCE\\bin\\Debug') #path to PyMCE.pyd
import PyMCE

operator_factory = PyMCE.OperatorFactory()
X = lambda dof_id = 0: operator_factory.CreateX(dof_id)
P = lambda dof_id = 0: operator_factory.CreateP(dof_id)
Exp = lambda X: operator_factory.CreateExp(X)
Id = lambda dof_id = 0: operator_factory.CreateId(dof_id)

def ASSERT_EQ(value, expected_value):
  if(abs(value-expected_value)<1.e-10):
    print("[OK]")
  else:
    print("[ERROR]: ", value, " != ", expected_value)
	
