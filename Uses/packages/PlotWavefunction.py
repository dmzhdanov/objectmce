import matplotlib.lines as mlines
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors
from matplotlib import patches as mpatches
import numpy as np
import sys
#sys.path.insert(0, 'E:/My_documents/c++/CHAMPS/ObjectMCE/ObjectMCE/bin_r/Release') #path to PyMCE.pyd
#sys.path.insert(0, 'E:/My_documents\\c++\\CHAMPS\\ObjectMCE\\ObjectMCE\\bin\\Debug') #path to PyMCE.pyd
import PyMCE

def PlotWavefunction(axes,
                     wavefunction,
                     color="g",
                     time=0,
                     xlim=[-8, 8],
                     ylim=[-1, 1],
                     potential=None,
                     text='',
                     title='',
                     xlabel='x',
                     ylabel='\u03A8(t); |A_n|(t) [x1000]; P_n(t) [x0.1]',
                     wavefunction_scale=1,
                     so_points=None,
                     so_values=None,
                     circles_scale=[0.5, 0.25]):
    axes.set_ylim([ylim[0], ylim[1]])
    axes.set_aspect(2.)
    points = np.arange(xlim[0], xlim[1], 0.1, dtype=float)
    values = np.full(points.size, 0, dtype=complex)
    centers = np.full(points.size, 0, dtype=complex)
    nulls = np.full(wavefunction.Size(), 0, dtype=float)
    colors = np.full(points.size, '#FF0000')

    axes.clear()
    axes.set_xlim(xlim)
    axes.set_ylim(ylim)

    axes.set_title(title)
    axes.set_xlabel(xlabel)
    axes.set_ylabel(ylabel)

    if (potential != None):
        potential_energies = np.full(points.size, 0, dtype=float)
        for i in range(0, len(potential_energies)):
            potential_energies[i] = potential(points[i])
        p_line_p = axes.plot(
            points,
            potential_energies,
            'brown',
            linewidth=2,
            markersize=3,
            linestyle=':')
        axes.fill_between(points, potential_energies, -1000, color="#DDDDDD")

    if ((so_points is not None) and (so_values is not None)):
        axes.plot(so_points, np.abs(so_values), c='gray', linewidth=2)
        axes.plot(so_points, np.real(so_values), c='gray', ls='--')
        axes.plot(
            so_points, np.imag(so_values), c='gray', ls=':', linewidth=0.5)

    X_es = np.full(wavefunction.Size(), 0, dtype=float)
    P_es = np.full(wavefunction.Size(), 0, dtype=float)
    A_es = np.full(wavefunction.Size(), 0, dtype=float)
    Are_es = np.full(wavefunction.Size(), 0, dtype=float)
    Aim_es = np.full(wavefunction.Size(), 0, dtype=float)
    #axes.lines =[];

    axes.text(
        xlim[0] + 0.2,
        ylim[0] + 0.2, ("time = " + '%.5f' % time) + text,
        fontsize=12)

    for i in range(0, len(values)):
        values[i] = (PyMCE.Value(wavefunction,
                                 [points[i]])) / wavefunction_scale
    for i in range(0, wavefunction.Size()):
        X_es[i] = wavefunction[i].GetCenter(0).X()
        P_es[i] = wavefunction[i].GetCenter(0).P()
        Atmp = wavefunction[i].GetMultiplier().Amplitude() / wavefunction_scale
        A_es[i] = abs(Atmp)
        Are_es[i] = (Atmp.real / (A_es[i]**0.5) * circles_scale[0]
                     if A_es[i] > 0 else 0)
        Aim_es[i] = (Atmp.imag / (A_es[i]**0.5) * circles_scale[1]
                     if A_es[i] > 0 else 0)
        if wavefunction[i].Nature(
        ) == PyMCE.BasisFunctionNature.LameBurghardtishche:
            colors[i] = 'yellow'  #'#ffffcc' #yellow
        elif (wavefunction[i].Nature() == PyMCE.BasisFunctionNature.
              Garashchuchishche):
            colors[i] = '#ff8c00'  #'#fff2e6' #orange
        elif (wavefunction[i].Nature() == PyMCE.BasisFunctionNature.
              Garahardtishche):
            colors[i] = '#ff0000'  #red
        elif (wavefunction[i].Nature() == PyMCE.BasisFunctionNature.
              ShaloBohmishche):
            colors[i] = 'cyan'  #'#e0ffff' #light cyan
        elif (wavefunction[i].Nature() == PyMCE.BasisFunctionNature.
              ShaloBohmishche_v2):
            colors[i] = '#aaaaff'  #light blue
        elif (wavefunction[i].Nature() == PyMCE.BasisFunctionNature.
              Shalashilishche):
            colors[i] = 'gray'
        elif (wavefunction[i].Nature() == PyMCE.BasisFunctionNature.
              QuasiProbabilishche):
            colors[i] = '#71B971'  #dark green
        elif (wavefunction[i].Nature() == PyMCE.BasisFunctionNature.
              QuasiProbabilishche_v4):
            colors[i] = '#FFC0CB'  #pink
    p_line_r = axes.plot(
        points,
        np.real(values),
        color + '',
        linewidth=0.5,
        markersize=3,
        linestyle='--')
    p_line_i = axes.plot(
        points,
        np.imag(values),
        color + 'x',
        linewidth=0.5,
        markersize=3,
        linestyle='dotted')
    p_line_a = axes.plot(points, np.abs(values), color + '-', linewidth=2)
    #p_centers = axes.scatter(X_es, nulls, marker='o', c='gray', s=1000 * A_es)
    #p_centers = axes.scatter(X_es, nulls, facecolors='none', edgecolors=colors, s=1000 * A_es)
    for xi, yi, ri, ci, dxi, dyi in zip(X_es, nulls, A_es, colors, Are_es, Aim_es):
        circle = mpatches.Ellipse((xi, yi),
                             (ri**0.5) * circles_scale[0]*2,
                             (ri**0.5) * circles_scale[1]*2,
                             color=ci,
                             linewidth=0.5,
                             fill=False)
        axes.add_artist(circle)
        axes.plot([xi, xi + dxi], [0, dyi],
                  color=colors[i],
                  linewidth=1)

    for i in range(0, wavefunction.Size()):
        axes.arrow(
            X_es[i], 0, 0, P_es[i] / 10, head_width=0.09,
            head_length=0.09)  #, length_includes_head = True)
            
def PlotWavefunction2(axes,
                      wavefunction,
                      color="g",
                      xlim=[-8, 8],
                      ylim=[-1, 1],
                      potential=None,
                      parameters_names = [r"$~~~~~t{=}$",r"$~~~~\#{=}$",r"$\langle \hat H \rangle{=}$"],
                      parameters_values =["0","0","0"],
                      parameters_offset =[-8 + 0.1, -1+0.03],
                      parameters_shift=[3,0],
                      title='',
                      xlabel=r"$x$",
                      ylabel="",#r"$|\psi|,\Re[\psi],\Im[\psi]$",
                      wavefunction_scale=1,
                      so_points=None,
                      so_values=None,
                      circles_scale=[0.5, 0.25],
                      momentum_scale = 1/10,
                      primary_color = None,
                      secondary_color = "red",
                      secondary_offset = 0
                     ):

    points = np.arange(xlim[0], xlim[1], 0.1, dtype=float)
    values = np.full(points.size, 0, dtype=complex)
    centers = np.full(points.size, 0, dtype=complex)
    nulls = np.full(wavefunction.Size(), 0, dtype=float)
    colors = np.full(points.size, '#FF0000')

    wf_styles = {"abs": [2, "-"], "re": [0.5, "--"], 'im': [0.5, ":"]}
    reference_color = "gray"
    momentum_color = "black"

    if (len(axes.artists) > 0):
        axes.artists.clear()
        axes.lines.clear()
        axes.texts.clear()
    else:
        axes.clear()
        axes.set_xlim(xlim)
        axes.set_ylim(ylim)
        axes.set_aspect(2.)
        axes.set_adjustable('box',share=True)
        ax_r =plt.gcf().add_axes(axes.get_position())
        #ax_r = axes.twinx()
        ax_r.set_ylabel(r"$\bar p_k$ ", color=momentum_color)
        ax_r.set_xlim(xlim)
        ax_r.set_ylim([ylim[0]/momentum_scale,ylim[1]/momentum_scale])
        ax_r.set_facecolor("None")
        ax_r.set_aspect(2.*momentum_scale)
        ax_r.yaxis.set_label_position("right")
        ax_r.yaxis.tick_right()
        ax_r.xaxis.labelpad = 0
        ax_r.yaxis.labelpad = 0        

        axes.set_title(title)
        axes.set_xlabel(xlabel)
        axes.set_ylabel(ylabel)
        axes.yaxis.label.set_color(color)
        axes.tick_params(axis='y', colors=color)
        axes.xaxis.labelpad = 0
        axes.yaxis.labelpad = 0

        if (potential != None):
            potential_energies = np.full(points.size, 0, dtype=float)
            for i in range(0, len(potential_energies)):
                potential_energies[i] = potential(points[i])


        axes.fill_between(points,
                              potential_energies,
                              -1000,
                              color="#DDDDDD")
        legend_elements = [
            mlines.Line2D([0], [0],
                                    c=color,
                                    ls=wf_styles["abs"][1],
                                    linewidth=wf_styles["abs"][0],
                                    label=r'$|\psi|$'),
            mlines.Line2D([0], [0],
                                    c=color,
                                    ls=wf_styles["re"][1],
                                    linewidth=wf_styles["re"][0],
                                    label=r'$\Re[\psi]$'),
            mlines.Line2D([0], [0],
                                    c=color,
                                    ls=wf_styles["im"][1],
                                    linewidth=wf_styles["im"][0],
                                    label=r'$\Im[\psi]$')
        ]
        axes.legend(handles=legend_elements,
                    loc=[0, 0.60],
                    labelspacing=0,
                    handlelength=1.,
                    frameon=False)
           
        par_offset =parameters_offset.copy()
        for param in parameters_names:
            ax_r.text(par_offset[0],
                      par_offset[1]/momentum_scale, param)
            par_offset[0] = par_offset[0] +parameters_shift[0]
            par_offset[1] = par_offset[1] +parameters_shift[1]/momentum_scale

    artists_list = [];
    if ((so_points is not None) and (so_values is not None)):
        artists_list.append(axes.plot(so_points,
                  np.abs(so_values),
                  c=reference_color,
                  linewidth=wf_styles["abs"][0])[0])
        artists_list.append(axes.plot(so_points,
                  np.real(so_values),
                  c=reference_color,
                  ls=wf_styles["re"][1],
                  linewidth=wf_styles["re"][0])[0])
        artists_list.append(axes.plot(so_points,
                  np.imag(so_values),
                  c=reference_color,
                  ls=wf_styles["im"][1],
                  linewidth=wf_styles["im"][0])[0])

    X_es = np.full(wavefunction.Size(), 0, dtype=float)
    P_es = np.full(wavefunction.Size(), 0, dtype=float)
    A_es = np.full(wavefunction.Size(), 0, dtype=float)
    Are_es = np.full(wavefunction.Size(), 0, dtype=float)
    Aim_es = np.full(wavefunction.Size(), 0, dtype=float)

    par_offset =parameters_offset.copy()
    for param in parameters_values:
        axes.text(par_offset[0]+1,
                  par_offset[1], param)
        par_offset[0] = par_offset[0] +parameters_shift[0]
        par_offset[1] = par_offset[1] +parameters_shift[1]

    for i in range(0, len(values)):
        values[i] = (PyMCE.Value(wavefunction,
                                 [points[i]])) / wavefunction_scale
    for i in range(0, wavefunction.Size()):
        X_es[i] = wavefunction[i].GetCenter(0).X()
        P_es[i] = wavefunction[i].GetCenter(0).P()
        Atmp = wavefunction[i].GetMultiplier().Amplitude() / wavefunction_scale
        A_es[i] = abs(Atmp)
        Are_es[i] = (Atmp.real / (A_es[i]**0.5) *
                     circles_scale[0] if A_es[i] > 0 else 0)
        Aim_es[i] = (Atmp.imag / (A_es[i]**0.5) *
                     circles_scale[1] if A_es[i] > 0 else 0)
        if (primary_color is not None):
            colors[i] = primary_color if i>=secondary_offset else secondary_color
        else:     
            if wavefunction[i].Nature(
            ) == PyMCE.BasisFunctionNature.LameBurghardtishche:
                colors[i] = 'yellow'  #'#ffffcc' #yellow
            elif (wavefunction[i].Nature() ==
                  PyMCE.BasisFunctionNature.Garashchuchishche):
                colors[i] = '#ff8c00'  #'#fff2e6' #orange
            elif (wavefunction[i].Nature() ==
                  PyMCE.BasisFunctionNature.Garahardtishche):
                colors[i] = '#ff0000'  #red
            elif (wavefunction[i].Nature() ==
                  PyMCE.BasisFunctionNature.ShaloBohmishche):
                colors[i] = 'cyan'  #'#e0ffff' #light cyan
            elif (wavefunction[i].Nature() ==
                  PyMCE.BasisFunctionNature.ShaloBohmishche_v2):
                colors[i] = '#aaaaff'  #light blue
            elif (wavefunction[i].Nature() ==
                  PyMCE.BasisFunctionNature.Shalashilishche):
                colors[i] = 'gray'
            elif (wavefunction[i].Nature() ==
                  PyMCE.BasisFunctionNature.QuasiProbabilishche):
                colors[i] = '#71B971'  #dark green
            elif (wavefunction[i].Nature() ==
                  PyMCE.BasisFunctionNature.QuasiProbabilishche_v4):
                colors[i] = '#FFC0CB'  #pink
    p_line_r, = axes.plot(points,
                          np.real(values),
                          color + '',
                          markersize=3,
                          ls=wf_styles["re"][1],
                          linewidth=wf_styles["re"][0])
    p_line_i, = axes.plot(
        points,
        np.imag(values),
        color,  # + 'x',
        markersize=3,
        ls=wf_styles["im"][1],
        linewidth=wf_styles["im"][0])
    p_line_a, = axes.plot(points,
                          np.abs(values),
                          color + '-',
                          ls=wf_styles["abs"][1],
                          linewidth=wf_styles["abs"][0])
    #p_centers = axes.scatter(X_es, nulls, marker='o', c='gray', s=1000 * A_es)
    #p_centers = axes.scatter(X_es, nulls, facecolors='none', edgecolors=colors, s=1000 * A_es)
    for xi, yi, ri, ci, dxi, dyi in zip(X_es, nulls, A_es, colors, Are_es,
                                        Aim_es):
        circle = mpatches.Ellipse((xi, yi), (ri**0.5) * circles_scale[0] * 2,
                                  (ri**0.5) * circles_scale[1] * 2,
                                  color=ci,
                                  linewidth=0.5,
                                  fill=False)
        axes.add_artist(circle)
        artists_list.append(circle)
        artists_list.append(axes.plot(
            [xi, xi + dxi],
            [0, dyi],
            color=ci,  #colors[i],
            linewidth=1)[0])

    for i in range(0, wavefunction.Size()):
        axes.arrow(X_es[i],
                   0,
                   0,
                   P_es[i] * momentum_scale,
                   color = momentum_color,
                   head_width=0.09,
                   head_length=0.09)  #, length_includes_head = True)
    artists_list = artists_list + [p_line_r,p_line_i,p_line_a]
    return artists_list