from IPython.display import display
import ipywidgets as widgets
import time
import sys
#sys.path.insert(0, 'E:/My_documents/c++/CHAMPS/ObjectMCE/ObjectMCE/bin_r/Release') #path to PyMCE.pyd
#sys.path.insert(0, 'E:/My_documents\\c++\\CHAMPS\\ObjectMCE\\ObjectMCE\\bin\\Debug') #path to PyMCE.pyd
import PyMCE


class StopButton:
    stop = True
    button = None
    caption = ""
    caption_stopped = ""

    def __init__(self, caption="Click to stop", caption_stopped="Stopped..."):
        self.stop = False
        self.caption = caption
        self.caption_stopped = caption_stopped
        self.button = widgets.Button(description=caption)
        self.button.on_click(self.on_button_clicked.__get__(self, StopButton))
        display(self.button)
    
    def __del__(self):
        self.close();
        del self.button;

    def on_button_clicked(self, button):
        self.button.description = self.caption_stopped
        self.stop = True

    def close(self):
        if self.button is None :
            raise MemoryError("StopButton already closed.")
        self.button.close()
        self.caption = None
        self.caption_stopped = None;