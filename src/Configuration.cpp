#include "stdafx.h"

#include "Configuration.h"
#include "Wavefunction/EhrenfestBasisFunction.h"
#include "Wavefunction/Wavefunction.h"

namespace mce {

Configuration::Configuration(size_t num_pess, size_t num_spatial_dofs,
                             double gaussian_width,
                             double gaussian_overlap_cutoff)
    : IConfiguration(), num_pess_(num_pess),
      num_spatial_dofs_(num_spatial_dofs), gaussian_width_(gaussian_width),
      gaussian_overlap_cutoff_(gaussian_overlap_cutoff){};

// Configuration::Configuration()
//    : IConfiguration(), num_pess_(0), num_spatial_dofs_(1),
//    gaussian_width_(1),
//      gaussian_overlap_cutoff_(5) {}

Configuration::~Configuration() {}

void Configuration::CWiseAbs(
    std::shared_ptr<IBasisFunction> basis_function) const {
  if (dynamic_cast<EhrenfestBasisFunction *>(basis_function.get())) {
    auto &ehrenfest_basis =
        *dynamic_cast<EhrenfestBasisFunction *>(basis_function.get());
    for (auto &electronic_wavefunction :
         ehrenfest_basis.electronic_wavefunction_) {
      electronic_wavefunction.Phase(std::abs(electronic_wavefunction.Phase()));
      electronic_wavefunction.Amplitude(
          std::abs(electronic_wavefunction.Amplitude()));
    };
    ehrenfest_basis.GetMultiplier().Amplitude(
        std::abs(ehrenfest_basis.GetMultiplier().Amplitude()));
	ehrenfest_basis.GetMultiplier().Phase(
		std::abs(ehrenfest_basis.GetMultiplier().Phase()));
    for (auto &phase_point : ehrenfest_basis.gaussian_center_) {
      phase_point.X(std::abs(phase_point.X()));
	  phase_point.P(std::abs(phase_point.P()));
    };
  } else {
    QI_Error::RaiseError(
        "[ERROR] - Configuration::CWiseAbs - branch not implemented.");
  }
}

double
Configuration::InfinityNorm(const mce::Wavefunction &wavefunction) const {
  double return_value = 0;
  for (auto const &basis_function : wavefunction) {
    if (!dynamic_cast<EhrenfestBasisFunction *>(basis_function.get())) {
      QI_Error::RaiseError(
          "[ERROR] - Configuration::InfinityNorm - branch not implemented.");
    }
    auto &ehrenfest_basis =
        *dynamic_cast<EhrenfestBasisFunction *>(basis_function.get());
    for (auto &electronic_wavefunction :
         ehrenfest_basis.electronic_wavefunction_) {
      return_value = std::max(return_value, electronic_wavefunction.Phase());
      return_value =
          std::max(return_value, std::abs(electronic_wavefunction.Amplitude()));
    };
    for (auto &phase_point : ehrenfest_basis.gaussian_center_) {
      return_value = std::max(return_value, phase_point.X());
      return_value = std::max(return_value, phase_point.P());
    };
	return_value =
		std::max(return_value, std::abs(ehrenfest_basis.GetMultiplier().Amplitude()));
	return_value =
		std::max(return_value, std::abs(ehrenfest_basis.GetMultiplier().Phase()));
  }
  return return_value;
};

} // namespace mce
