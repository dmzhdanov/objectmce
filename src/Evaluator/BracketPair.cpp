#include "stdafx.h"

#include "Algorithms/CCSMatrixElements.h"
#include "Commons/QI_Error.h"
#include "Evaluator/BracketPair.h"

namespace mce {

// BracketPair::BracketPair() {}

BracketPair::~BracketPair() {}

BracketPair::BracketPair(const BracketPair &other) {
  bra_ = other.bra_.ShallowCopy();
  ket_ = other.ket_.ShallowCopy();
  evaluator_strategy_ = other.evaluator_strategy_;
};

BracketPair::BracketPair(BracketPair &&other) {
  bra_ = other.bra_.ShallowCopy();
  ket_ = other.ket_.ShallowCopy();
  evaluator_strategy_ = other.evaluator_strategy_;
};

BracketPair::BracketPair(const Wavefunction &bra) : BracketPair(bra, bra){};

BracketPair::BracketPair(const Wavefunction &bra, const Wavefunction &ket, const std::shared_ptr<IEvaluatorStrategy> evaluator_strategy)
	: bra_(bra.ShallowCopy()), ket_(ket.ShallowCopy()), evaluator_strategy_(evaluator_strategy){
	if (bra_.Configuration().NumberOfSpatialDOFs() !=
		ket_.Configuration().NumberOfSpatialDOFs()) {
		QI_Error::RaiseError(
			"[ERROR] BracketPair::BracketPair - Incompatible configurations.");
	};
}

BracketPair::BracketPair(const Wavefunction &bra, const std::shared_ptr<IEvaluatorStrategy> evaluator_strategy) : BracketPair(bra, bra, evaluator_strategy)
{
};

BracketPair::BracketPair(const Wavefunction &bra, const Wavefunction &ket) : BracketPair(bra, ket, nullptr)
{
}

bool BracketPair::IsOverlapping(size_t bra_idx, size_t ket_idx) const {
  for (size_t i = 0; i < bra_.Configuration().NumberOfSpatialDOFs(); i++) {
    double max_distance =
        std::max(bra_.Configuration().GaussianWidth(i) *
                     bra_.Configuration().GaussianOverlapCutoff(i),
                 ket_.Configuration().GaussianWidth(i) *
                     ket_.Configuration().GaussianOverlapCutoff(i));
    if (std::abs(bra_[bra_idx]->GetCenter(i).X() -
                 ket_[ket_idx]->GetCenter(i).X()) > max_distance) {
      return false;
    };
  }
  return true;
}

bool BracketPair::IsSymmetric() const { return Ket().data_ == Bra().data_; }

std::complex<double> BracketPair::Overlap(size_t bra_idx, size_t ket_idx,
                                          SpatialDOF_id dof_id) const {
  return math::CCSOverlap(
      bra_[bra_idx]->GetCenter(dof_id), ket_[ket_idx]->GetCenter(dof_id),
      bra_.Configuration().GaussianQuadraticCoefficient(dof_id),
      ket_.Configuration().GaussianQuadraticCoefficient(dof_id));
}

//std::complex<double> BracketPair::Overlap(size_t bra_idx,
//                                          size_t ket_idx) const {
//  const Eigen::Index &NumberOfDOFs =
//      (Eigen::Index)bra_.Configuration().NumberOfSpatialDOFs();
//  std::complex<double> overlap = 1;
//  for (Eigen::Index dof_id = 0; dof_id < NumberOfDOFs; dof_id++) {
//    overlap *= Overlap(bra_idx, ket_idx, dof_id);
//  }
//  return overlap;
//}

} // namespace mce
