﻿#include "stdafx.h"

#include "Evaluator/EvaluatorEntry_CCS_Gaussian.h"

#include "Algorithms/CCSMatrixElements.h"
#include "Algorithms/Utilities.h"
#include "Evaluator/EvaluatorEntry_CCS_Gaussian_Implementation.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityFlow_CCS.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityGauge_CCS.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityOperator_CCS.h"

#define MCE_LOCAL_EVALUATORTYPE                                                \
  EvaluatorEntry<OperatorEntryGaussian<MCE_POLICY(CCS)>>

namespace mce {

MCE_LOCAL_EVALUATORTYPE::EvaluatorEntry(std::any initializer_list)
    : EvaluatorEntry(std::make_from_tuple<EvaluatorEntry>(std::move(
          std::any_cast<
              std::tuple<const std::shared_ptr<IOperatorEntry<PolicyType>>,
                         const BracketPair &, const SpatialDOF_id>>(
              initializer_list)))){};

MCE_LOCAL_EVALUATORTYPE::EvaluatorEntry(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id) {
  // default strategy
  if (!bracket_pair.Strategy()) {
    evaluation_strategy_ = std::make_shared<
        mce::internal::GaussianEvaluatorEntryCCSDefaultImplementation>(
        entry, bracket_pair, dof_id);
    return;
  }
  // quasiprobability flow
  {
    const auto strategy = std::dynamic_pointer_cast<
        evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)>>(
        bracket_pair.Strategy());
    if (strategy) {
      if (strategy->FlowType() ==
          evaluator_strategy::QuasiprobabilityFlowType::Position) {
        evaluation_strategy_ = std::make_shared<
            mce::internal::
                GaussianEvaluatorEntryCCSPositionQuasiprobabilityFlowImplementation>(
            entry, bracket_pair, dof_id);
      } else if (strategy->FlowType() ==
                 evaluator_strategy::QuasiprobabilityFlowType::Momentum) {
        evaluation_strategy_ = std::make_shared<
            mce::internal::
                GaussianEvaluatorEntryCCSMomentumQuasiprobabilityFlowImplementation>(
            entry, bracket_pair, dof_id);
      } else {
        QI_Error::RaiseError("[ERROR] "
                             "EvaluatorEntry<OperatorEntryGaussian<MCE_POLICY("
                             "CCS)>>::EvaluatorEntry - unknown flow type.");
      }
      return;
    }
  }
  // quasiprobability gauge
  {
	  const auto strategy = std::dynamic_pointer_cast<
		  evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(CCS)>>(
			  bracket_pair.Strategy());
	  if (strategy) {
		  if (strategy->FlowType() ==
			  evaluator_strategy::QuasiprobabilityGaugeType::Position) {
			  evaluation_strategy_ = std::make_shared<
				  mce::internal::
				  GaussianEvaluatorEntryCCSPositionQuasiprobabilityGaugeImplementation>(
					  entry, bracket_pair, dof_id);
		  }
		  else if (strategy->FlowType() ==
			  evaluator_strategy::QuasiprobabilityGaugeType::Momentum) {
			  evaluation_strategy_ = std::make_shared<
				  mce::internal::
				  GaussianEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation>(
					  entry, bracket_pair, dof_id);
		  }
		  else {
			  QI_Error::RaiseError("[ERROR] "
				  "EvaluatorEntry<OperatorEntryGaussian<MCE_POLICY("
				  "CCS)>>::EvaluatorEntry - unknown flow type.");
		  }
		  return;
	  }
  }
  // quasiprobability operator
  {
	  const auto strategy = std::dynamic_pointer_cast<
		  evaluator_strategy::QuaisiprobabilityOperatorStrategy<MCE_POLICY(CCS)>>(
			  bracket_pair.Strategy());
	  if (strategy) {
		  evaluation_strategy_ = std::make_shared<
			  mce::internal::
			  GaussianEvaluatorEntryCCSQuasiprobabilityOperatorImplementation>(
				  entry, bracket_pair, dof_id);
		  return;
	  }
  }
  // otherwise
  {
    QI_Error::RaiseError("[ERROR] "
                         "EvaluatorEntry<OperatorEntryGaussian<MCE_POLICY("
                         "CCS)>>::EvaluatorEntry - unknown strategy.");
  }
}

MCE_LOCAL_EVALUATORTYPE::EvaluatorEntry(const MCE_LOCAL_EVALUATORTYPE &other)
    : evaluation_strategy_(other.evaluation_strategy_->Clone()){};

MCE_LOCAL_EVALUATORTYPE::~EvaluatorEntry() {}

std::complex<double>
MCE_LOCAL_EVALUATORTYPE::Evaluate(const BracketPair &bracket_pair,
                                  size_t bra_idx, size_t ket_idx,
                                  SpatialDOF_id dof_id) const {

  return evaluation_strategy_->Evaluate(bracket_pair, bra_idx, ket_idx, dof_id);
}

// std::complex<double>
// MCE_LOCAL_EVALUATORTYPE::Evaluate(const BracketPair &bracket_pair,
//                                  size_t bra_idx, size_t ket_idx,
//                                  SpatialDOF_id dof_id) const {
//// the commented code is correct but does not give any precision gains
//// if (entry_->taylor_coefficients_.rows() == 1) {
// //   auto res = SymmetricEvaluate(bracket_pair, bra_idx, ket_idx, dof_id);
//	//return res;
//	////std::cout << "\n1 **" << res;
// // };
//
//	 if (creation_annihilation_matrix_.size() == 0)
//	 {
//		 return 0;
//	 }
//
//  VectorXc_r bra_z_vector =
//      utilities::PowerSeries(
//          creation_annihilation_matrix_.cols(),
//          std::conj(math::CCSEigenvalue(
//              bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
//              bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(
//                  dof_id))))
//          .transpose();
//
//  CanonicPair effective_ket_center =
//      bracket_pair.Ket()[ket_idx]->GetCenter(dof_id);
//  double effective_ket_quadratic_coefficient =
//      bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id);
//
//  auto multiplier_tuple = math::CCSGaussianMultiplyInPlace2(
//      entry_->exp_taylor_coefficients_, effective_ket_center,
//      effective_ket_quadratic_coefficient);
//
//  Eigen::VectorXcd ket_z_vector = utilities::PowerSeries(
//      creation_annihilation_matrix_.rows(),
//      math::CCSEigenvalue(effective_ket_center,
//                          effective_ket_quadratic_coefficient));
//
//  auto effective_overlap = math::CCSOverlap(
//      bracket_pair.Bra()[bra_idx]->GetCenter(dof_id), //
//      effective_ket_center,
//      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id),
//      effective_ket_quadratic_coefficient, std::get<1>(multiplier_tuple));
//
//  auto res = std::get<0>(multiplier_tuple) * effective_overlap *
//         (bra_z_vector * (creation_annihilation_matrix_ * ket_z_vector))(0,
//         0);
//  //std::cout << "\n2 **" << res;
//  return res;
//}

// std::complex<double>
// MCE_LOCAL_EVALUATORTYPE::SymmetricEvaluate(const BracketPair &bracket_pair,
//                                           size_t bra_idx, size_t ket_idx,
//                                           SpatialDOF_id dof_id) const {
//  auto half_exp_taylor_coefficients = entry_->exp_taylor_coefficients_;
//
//  CanonicPair effective_ket_center =
//      bracket_pair.Ket()[ket_idx]->GetCenter(dof_id);
//  double effective_ket_quadratic_coefficient =
//      bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id);
//  // auto multiplier_ket_tuple = math::CCSGaussianMultiplyInPlace2(
//  //    half_exp_taylor_coefficients, effective_ket_center,
//  //    effective_ket_quadratic_coefficient);
//  auto multiplier_ket_tuple =
//      std::make_tuple<std::complex<double>, std::complex<double>>(1, 0);
//  VectorXc_c ket_z_vector = utilities::PowerSeries(
//      creation_annihilation_matrix_.rows(),
//      math::CCSEigenvalue(effective_ket_center,
//                          effective_ket_quadratic_coefficient));
//
//  half_exp_taylor_coefficients = half_exp_taylor_coefficients.conjugate();
//
//  CanonicPair effective_bra_center =
//      bracket_pair.Bra()[bra_idx]->GetCenter(dof_id);
//  double effective_bra_quadratic_coefficient =
//      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id);
//  auto multiplier_bra_tuple = math::CCSGaussianMultiplyInPlace2(
//      half_exp_taylor_coefficients, effective_bra_center,
//      effective_bra_quadratic_coefficient);
//  // auto multiplier_bra_tuple = std::make_tuple<std::complex<double>,
//  // std::complex<double>>(1, 0);
//
//  VectorXc_r bra_z_vector =
//      utilities::PowerSeries(
//          creation_annihilation_matrix_.cols(),
//          std::conj(math::CCSEigenvalue(effective_bra_center,
//                                        effective_bra_quadratic_coefficient)))
//          .transpose();
//
//  auto creation_annihilation_matrix = math::GetCreationAnnihilationMatrix(
//      entry_->taylor_coefficients_, effective_bra_quadratic_coefficient,
//      effective_ket_quadratic_coefficient);
//
//  auto effective_overlap = math::CCSOverlap(
//      effective_bra_center, //
//      effective_ket_center, effective_bra_quadratic_coefficient,
//      effective_ket_quadratic_coefficient,
//      std::get<1>(multiplier_ket_tuple) +
//          std::conj(std::get<1>(multiplier_bra_tuple)));
//
//  return std::get<0>(multiplier_ket_tuple) *
//         std::conj(std::get<0>(multiplier_bra_tuple)) * effective_overlap *
//         (bra_z_vector * (creation_annihilation_matrix * ket_z_vector))(0, 0);
//}

MCE_LOCAL_EVALUATORTYPE::EntryType MCE_LOCAL_EVALUATORTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_EVALUATORTYPE>(*this);
  return clone;
};

} // namespace mce

#undef MCE_LOCAL_EVALUATORTYPE
