#include "stdafx.h"

#include "Evaluator/EvaluatorStrategyQuasiprobabilityGauge_CCS.h"

#define MCE_LOCAL_EVALUATORSTRATEGY                                            \
  QuaisiprobabilityGaugeStrategy<MCE_POLICY(CCS)>

#define MCE_LOCAL_EVALUATORSTRATEGYNAME QuaisiprobabilityGaugeStrategy

namespace mce {
namespace evaluator_strategy {

MCE_LOCAL_EVALUATORSTRATEGY::MCE_LOCAL_EVALUATORSTRATEGYNAME(
    double mean_momentum, double mean_momentum_x_derivative,
    const QuasiprobabilityGaugeType flow_type, const CanonicPair &width_factors,
    size_t center_basis_function_id, unsigned approximation_order)
    : kernel_data_(std::make_shared<QuasiprobabilityGaugeKernelData>()) {
  kernel_data_->width_factors_ = width_factors;
  FlowType(flow_type);
  CenterBasisFunction(center_basis_function_id);
  // DofId(dof_id);
  ApproximationOrder(approximation_order);
  MeanMomentum(mean_momentum);
  MeanMomentumXDerivative(mean_momentum_x_derivative);
};

MCE_LOCAL_EVALUATORSTRATEGY::MCE_LOCAL_EVALUATORSTRATEGYNAME(
    const MCE_LOCAL_EVALUATORSTRATEGY &other)
    : kernel_data_(other.kernel_data_){};

IEvaluatorStrategy::EntryType MCE_LOCAL_EVALUATORSTRATEGY::ShallowCopy() const {
  auto copy = std::make_shared<MCE_LOCAL_EVALUATORSTRATEGY>();
  copy->kernel_data_ = kernel_data_;
  return copy;
};

IEvaluatorStrategy::EntryType MCE_LOCAL_EVALUATORSTRATEGY::Clone() const {
  auto copy = std::make_shared<MCE_LOCAL_EVALUATORSTRATEGY>();
  *(copy->kernel_data_) = *kernel_data_;
  return copy;
};

size_t MCE_LOCAL_EVALUATORSTRATEGY::CenterBasisFunction(
    size_t center_basis_function_id) {
  kernel_data_->center_basis_function_id_ = center_basis_function_id;
  return kernel_data_->center_basis_function_id_;
};

// SpatialDOF_id MCE_LOCAL_EVALUATORSTRATEGY::DofId(const SpatialDOF_id dof_id)
// { 	kernel_data_->dof_id_ = dof_id; 	return kernel_data_->dof_id_;
//};

QuasiprobabilityGaugeType MCE_LOCAL_EVALUATORSTRATEGY::FlowType(
    const QuasiprobabilityGaugeType flow_type) {
  kernel_data_->flow_type_ = flow_type;
  return kernel_data_->flow_type_;
};

double MCE_LOCAL_EVALUATORSTRATEGY::MeanMomentum(double mean_momentum) {
  kernel_data_->mean_momentum_ = mean_momentum;
  return kernel_data_->mean_momentum_;
};
double MCE_LOCAL_EVALUATORSTRATEGY::MeanMomentumXDerivative(
    double mean_momentum_x_derivative) {
  kernel_data_->mean_momentum_x_derivative_ = mean_momentum_x_derivative;
  return kernel_data_->mean_momentum_x_derivative_;
};

unsigned
MCE_LOCAL_EVALUATORSTRATEGY::ApproximationOrder(unsigned approximation_order) {
  kernel_data_->approximation_order_ = approximation_order;
  return kernel_data_->approximation_order_;
}

size_t MCE_LOCAL_EVALUATORSTRATEGY::CenterBasisFunction() const {
  return kernel_data_->center_basis_function_id_;
};

// SpatialDOF_id MCE_LOCAL_EVALUATORSTRATEGY::DofId() const {
//	return kernel_data_->dof_id_;
//};

QuasiprobabilityGaugeType MCE_LOCAL_EVALUATORSTRATEGY::FlowType() const {
  return kernel_data_->flow_type_;
};

double MCE_LOCAL_EVALUATORSTRATEGY::MeanMomentum() const {
  return kernel_data_->mean_momentum_;
};

double MCE_LOCAL_EVALUATORSTRATEGY::MeanMomentumXDerivative() const {
  return kernel_data_->mean_momentum_x_derivative_;
};

CanonicPair MCE_LOCAL_EVALUATORSTRATEGY::WidthFactors() const {
  return kernel_data_->width_factors_;
};

unsigned MCE_LOCAL_EVALUATORSTRATEGY::ApproximationOrder() const {
  return kernel_data_->approximation_order_;
}

CanonicPair MCE_LOCAL_EVALUATORSTRATEGY::GetGaussianQuadraticPXCoefficients(
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id) {
  if (kernel_data_ == nullptr) {
    QI_Error::RaiseError(
        "[ERROR] "
        "QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)>::"
        "GetGaussianQuadraticPXCoefficients - kernel_data_ = nullptr.");
  }
  CanonicPair alpha_pair;
  // for Wigner space projection of basis function with gaussian coefficient
  // alpha ~ exp(-2*alpha*(x-x_0)^2-(p-p_0)^2/(2*alpha*hbar^2)) hence, we are
  // defining alpha_eff = 2*alpha
  auto alpha_eff_times_2 =
      (bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id) +
       bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id));
  alpha_pair.P(1. /
               (alpha_eff_times_2 * std::pow(kHBar * WidthFactors().P(), 2)));
  alpha_pair.X(alpha_eff_times_2 / std::pow(WidthFactors().X(), 2));
  return alpha_pair;
};

} // namespace evaluator_strategy
} // namespace mce
