#include "stdafx.h"

#include "Evaluator/EvaluatorEntry_CCS_Implicit_Implementation.h"

#include "Algorithms/CCSMatrixElements.h"
#include "Algorithms/QuasiprobabilityAuxiliaries.h"
#include "Algorithms/QuasiprobabilityGaugeAuxiliaries.h"
#include "Algorithms/Utilities.h"
#include "Evaluator/EvaluatorEntry_CCS_Implicit.h"

namespace mce {
namespace internal {
// ################################
// ################################
// ################################
// ################################
#define MCE_LOCAL_IMPLEMENTERTYPE ImplicitEvaluatorEntryCCSDefaultImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSDefaultImplementation

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id) {}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  return bracket_pair.Overlap(bra_idx, ket_idx, dof_id);
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

  // ################################
  // ################################
  // ################################
  // ################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  ImplicitEvaluatorEntryCCSPositionQuasiprobabilityFlowImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSPositionQuasiprobabilityFlowImplementation

MatrixXc_r MCE_LOCAL_IMPLEMENTERTYPE::InitializeIdentityMatrix() {
  MatrixXc_r m(1, 1);
  m << 1.;
  return m;
};
MatrixXc_r MCE_LOCAL_IMPLEMENTERTYPE::identity_matrix_(
    MCE_LOCAL_IMPLEMENTERTYPE::InitializeIdentityMatrix());
VectorXc_r MCE_LOCAL_IMPLEMENTERTYPE::zero_vector_(0);

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : strategy_(std::dynamic_pointer_cast<
                evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(
                    CCS)>>(bracket_pair.Strategy())),
      husimi_alpha_pair_(
          strategy_->GetGaussianQuadraticPXCoefficients(bracket_pair, dof_id)) {
}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : strategy_(other.strategy_),
      husimi_alpha_pair_(other.husimi_alpha_pair_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  return quasiprobability_auxiliaries::GaussianWeightedPositionFlow(
      bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(dof_id),
      husimi_alpha_pair_, bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id),
      bracket_pair.Ket()[bra_idx]->GetCenter(dof_id),
      bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id),
      identity_matrix_, zero_vector_);
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

  // ################################
  // ################################
  // ################################
  // ################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityFlowImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityFlowImplementation

MatrixXc_r MCE_LOCAL_IMPLEMENTERTYPE::InitializeIdentityMatrix() {
  MatrixXc_r m(1, 1);
  m << 1.;
  return m;
};
MatrixXc_r MCE_LOCAL_IMPLEMENTERTYPE::identity_matrix_(
    MCE_LOCAL_IMPLEMENTERTYPE::InitializeIdentityMatrix());
VectorXc_r MCE_LOCAL_IMPLEMENTERTYPE::zero_vector_(0);

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : strategy_(std::dynamic_pointer_cast<
                evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(
                    CCS)>>(bracket_pair.Strategy())),
      husimi_alpha_pair_(
          strategy_->GetGaussianQuadraticPXCoefficients(bracket_pair, dof_id)) {
}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : strategy_(other.strategy_),
      husimi_alpha_pair_(other.husimi_alpha_pair_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  if (strategy_->DofId() == dof_id) {
    return quasiprobability_auxiliaries::GaussianWeightedMomentumFlow(
        bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(dof_id),
        husimi_alpha_pair_, bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
        bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id),
        bracket_pair.Ket()[ket_idx]->GetCenter(dof_id),
        bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id),
        identity_matrix_, zero_vector_);
  }

  return quasiprobability_auxiliaries::
      ComplementaryGaussianWeightedMomentumFlow(
          bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(
              dof_id),
          husimi_alpha_pair_, bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
          bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(
              dof_id),
          bracket_pair.Ket()[ket_idx]->GetCenter(dof_id),
          bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(
              dof_id),
          identity_matrix_, zero_vector_);
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

  // ######################################################################
  // ######################################################################
  // ######################################################################
  // #################     GAUGE FLOW TRANSFORMATIONS    ##################
  // ######################################################################
  // ######################################################################
  // ######################################################################

  // ################################
  // ################################
  // ################################
  // ################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : strategy_(std::dynamic_pointer_cast<
                evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(
                    CCS)>>(bracket_pair.Strategy())),
      husimi_alpha_pair_(
          strategy_->GetGaussianQuadraticPXCoefficients(bracket_pair, dof_id)) {
}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : strategy_(other.strategy_),
      husimi_alpha_pair_(other.husimi_alpha_pair_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  if (std::abs(bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(
                   dof_id) -
               bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(
                   dof_id)) > kTolerance) {
    QI_Error::RaiseError(
        "[ERROR] "
        "ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation"
        "::Evaluate - bra and ket have different Gaussian widths (alpha "
        "coefficients). Method works only if they are the same.");
  }

  return quasiprobability_gauge_auxiliaries::HusimiFunctionMatrixElement(
      bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(dof_id),
      bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
      bracket_pair.Ket()[ket_idx]->GetCenter(dof_id), husimi_alpha_pair_,
      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id));
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

  // ################################
  // ################################
  // ################################
  // ################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  ImplicitEvaluatorEntryCCSPositionQuasiprobabilityGaugeImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSPositionQuasiprobabilityGaugeImplementation

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : strategy_(std::dynamic_pointer_cast<
                evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(
                    CCS)>>(bracket_pair.Strategy())),
      husimi_alpha_pair_(
          strategy_->GetGaussianQuadraticPXCoefficients(bracket_pair, dof_id)) {
}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : strategy_(other.strategy_),
      husimi_alpha_pair_(other.husimi_alpha_pair_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  if (std::abs(bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(
                   dof_id) -
               bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(
                   dof_id)) > kTolerance) {
    QI_Error::RaiseError(
        "[ERROR] "
        "ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation"
        "::Evaluate - bra and ket have different Gaussian widths (alpha "
        "coefficients). Method works only if they are the same.");
  }

  return quasiprobability_gauge_auxiliaries::HusimiFunctionMatrixElement(
      bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(dof_id),
      bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
      bracket_pair.Ket()[ket_idx]->GetCenter(dof_id), husimi_alpha_pair_,
      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id));
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

  // ######################################################################
  // ######################################################################
  // ######################################################################
  // #################     QUASIPROBABILITY OPERATOR     ##################
  // ######################################################################
  // ######################################################################
  // ######################################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  ImplicitEvaluatorEntryCCSQuasiprobabilityOperatorImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSQuasiprobabilityOperatorImplementation

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : strategy_(std::dynamic_pointer_cast<
                evaluator_strategy::QuaisiprobabilityOperatorStrategy<MCE_POLICY(
                    CCS)>>(bracket_pair.Strategy())),
      husimi_alpha_pair_(
          strategy_->GetGaussianQuadraticPXCoefficients(bracket_pair, dof_id)) {
}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : strategy_(other.strategy_),
      husimi_alpha_pair_(other.husimi_alpha_pair_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  if (std::abs(bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(
                   dof_id) -
               bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(
                   dof_id)) > kTolerance) {
    QI_Error::RaiseError(
        "[ERROR] "
        "ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityOperatorImplementation"
        "::Evaluate - bra and ket have different Gaussian widths (alpha "
        "coefficients). Method works only if they are the same (can be fixed).");
  }

  return quasiprobability_gauge_auxiliaries::HusimiFunctionMatrixElement(
      bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(dof_id),
      bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
      bracket_pair.Ket()[ket_idx]->GetCenter(dof_id), husimi_alpha_pair_,
      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id));
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

} // namespace internal
} // namespace mce
