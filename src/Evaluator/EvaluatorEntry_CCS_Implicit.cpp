#include "stdafx.h"

#include "Evaluator/EvaluatorEntry_CCS_Implicit.h"

#include "Algorithms/CCSMatrixElements.h"
#include "Evaluator/EvaluatorEntry_CCS_Implicit_Implementation.h"

#define MCE_LOCAL_EVALUATORTYPE                                                \
  EvaluatorEntry<OperatorEntryImplicit<MCE_POLICY(CCS)>>

namespace mce {

MCE_LOCAL_EVALUATORTYPE::~EvaluatorEntry() {}

MCE_LOCAL_EVALUATORTYPE::EvaluatorEntry(
    const std::shared_ptr<IOperatorEntry<PolicyType>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id) {
  if (!bracket_pair.Strategy()) {
    evaluation_strategy_ = std::make_shared<
        mce::internal::ImplicitEvaluatorEntryCCSDefaultImplementation>(
        entry, bracket_pair, dof_id);
    return;
  }
  // quasiprobability flow
  {
    const auto strategy = std::dynamic_pointer_cast<
        evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)>>(
        bracket_pair.Strategy());
    if (strategy) {
      if (strategy->FlowType() ==
          evaluator_strategy::QuasiprobabilityFlowType::Position) {
        evaluation_strategy_ = std::make_shared<
            mce::internal::
                ImplicitEvaluatorEntryCCSPositionQuasiprobabilityFlowImplementation>(
            entry, bracket_pair, dof_id);
      } else if (strategy->FlowType() ==
                 evaluator_strategy::QuasiprobabilityFlowType::Momentum) {
        evaluation_strategy_ = std::make_shared<
            mce::internal::
                ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityFlowImplementation>(
            entry, bracket_pair, dof_id);
      } else {
        QI_Error::RaiseError(
            "[ERROR] "
            "EvaluatorEntry<OperatorEntryImplicit<MCE_POLICY("
            "CCS)>>::EvaluatorEntry - unknown quasiprobability flow type.");
      }
    }
    return;
  }
  // quasiprobability gauge
  {
    const auto strategy = std::dynamic_pointer_cast<
        evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(CCS)>>(
        bracket_pair.Strategy());
    if (strategy) {
      if (strategy->FlowType() ==
          evaluator_strategy::QuasiprobabilityGaugeType::Position) {
        evaluation_strategy_ = std::make_shared<
            mce::internal::
                ImplicitEvaluatorEntryCCSPositionQuasiprobabilityGaugeImplementation>(
            entry, bracket_pair, dof_id);
      } else if (strategy->FlowType() ==
                 evaluator_strategy::QuasiprobabilityGaugeType::Momentum) {
        evaluation_strategy_ = std::make_shared<
            mce::internal::
                ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation>(
            entry, bracket_pair, dof_id);
      } else {
        QI_Error::RaiseError(
            "[ERROR] "
            "EvaluatorEntry<OperatorEntryImplicit<MCE_POLICY("
            "CCS)>>::EvaluatorEntry - unknown gauge flow type.");
      }
    }
    return;
  }
  // quasiprobability operator
  {
	  const auto strategy = std::dynamic_pointer_cast<
		  evaluator_strategy::QuaisiprobabilityOperatorStrategy<MCE_POLICY(CCS)>>(
			  bracket_pair.Strategy());
	  if (strategy) {
		  evaluation_strategy_ = std::make_shared<
			  mce::internal::
			  ImplicitEvaluatorEntryCCSQuasiprobabilityOperatorImplementation>(
				  entry, bracket_pair, dof_id);
		  return;
	  }
  }
  // otherwise
  {
    QI_Error::RaiseError("[ERROR] "
                         "EvaluatorEntry<OperatorEntryImplicit<MCE_POLICY("
                         "CCS)>>::EvaluatorEntry - unknown strategy.");
  }
}

MCE_LOCAL_EVALUATORTYPE::EvaluatorEntry(const MCE_LOCAL_EVALUATORTYPE &other)
    : evaluation_strategy_(other.evaluation_strategy_->Clone()) {}

std::complex<double>
MCE_LOCAL_EVALUATORTYPE::Evaluate(const BracketPair &bracket_pair,
                                  size_t bra_idx, size_t ket_idx,
                                  SpatialDOF_id dof_id) const {
  return evaluation_strategy_->Evaluate(bracket_pair, bra_idx, ket_idx, dof_id);
  // return bracket_pair.Overlap(bra_idx, ket_idx, dof_id);
}

MCE_LOCAL_EVALUATORTYPE::EntryType MCE_LOCAL_EVALUATORTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_EVALUATORTYPE>(*this);
  return clone;
};

} // namespace mce

#undef MCE_LOCAL_EVALUATORTYPE
