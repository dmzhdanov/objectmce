#include "stdafx.h"

#include "Evaluator/EvaluatorEntry_CCS_Sum.h"

#define MCE_LOCAL_EVALUATORTYPE                                                \
  EvaluatorEntry<OperatorEntrySum<MCE_POLICY(CCS)>>

namespace mce {

MCE_LOCAL_EVALUATORTYPE::~EvaluatorEntry() {}

MCE_LOCAL_EVALUATORTYPE::EvaluatorEntry(
    const std::shared_ptr<IOperatorEntry<PolicyType>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : entry_(
          std::dynamic_pointer_cast<OperatorEntrySum<MCE_POLICY(CCS)>>(entry)) {
  summands_.clear();
  summands_.reserve(entry_->summands_.size());
  for (auto &summand : entry_->summands_) {
    summands_.push_back(summand->Evaluator());
  }
}

MCE_LOCAL_EVALUATORTYPE::EvaluatorEntry(const MCE_LOCAL_EVALUATORTYPE &other)
    : entry_(other.entry_), summands_() {
  for (auto &summand : other.summands_) {
    summands_.push_back((*summand).Clone());
  }
}

std::complex<double>
MCE_LOCAL_EVALUATORTYPE::Evaluate(const BracketPair &bracket_pair,
                                  size_t bra_idx, size_t ket_idx,
                                  SpatialDOF_id dof_id) const {
  std::complex<double> result = 0;
  for (auto &summand : summands_) {
    result += summand->Evaluate(bracket_pair, bra_idx, ket_idx, dof_id);
  };
  return result;
}

MCE_LOCAL_EVALUATORTYPE::EntryType MCE_LOCAL_EVALUATORTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_EVALUATORTYPE>(*this);
  return clone;
};

} // namespace mce

#undef MCE_LOCAL_EVALUATORTYPE
