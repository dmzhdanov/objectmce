#include "stdafx.h"

#include "Evaluator/Evaluator_CCS.h"
#include "Evaluator/IEvaluatorEntry_CCS.h"

namespace mce {
Evaluator<MCE_POLICY(CCS)>::Evaluator(const OperatorBaseType &operator_base,
                                      const BracketPair &bracket_pair)
    : EvaluatorAlgorithmicCore<Evaluator<MCE_POLICY(CCS)>, MCE_POLICY(CCS)>(
          operator_base,
          [&](const std::pair<const SpatialDOF_id, OperatorEntryType>
                  operator_entry)
              -> std::pair<const SpatialDOF_id, EvaluatorEntryType> {
            EvaluatorEntryType entry;
            try {
              entry = operator_entry.second->Evaluator(
                  IEvaluatorEntry<PolicyType>::ParameterPack(
                      operator_entry.second, bracket_pair,
                      operator_entry.first));
            } catch (...) {
              QI_Error::RaiseError(
                  "[ERROR] Bad any_cast in Evaluator constructor...");
            }
            return std::move(std::pair<const SpatialDOF_id, EvaluatorEntryType>(
                operator_entry.first, entry));
          }),
      implicit_evaluator_entry_(
          std::make_shared<
              EvaluatorEntry<OperatorEntryImplicit<MCE_POLICY(CCS)>>>(
              IEvaluatorEntry<PolicyType>::ParameterPack(nullptr, bracket_pair,
                                                         0))),
      bracket_pair_(bracket_pair) {}

std::complex<double>
Evaluator<MCE_POLICY(CCS)>::operator()(size_t bra_idx, size_t ket_idx) const {
  size_t number_of_dofs =
      bracket_pair_.Bra().Configuration().NumberOfSpatialDOFs();
  return Traverse<std::complex<double>>(
      [&](const std::pair<const SpatialDOF_id, EvaluatorEntryType> &entry) {
        return entry.second->Evaluate(bracket_pair_, bra_idx, ket_idx,
                                      entry.first);
      },
      [&](SpatialDOF_id dof_id) {
		  return implicit_evaluator_entry_->Evaluate(bracket_pair_, bra_idx, ket_idx,
			  dof_id);//bracket_pair_.Overlap(bra_idx, ket_idx, dof_id);
      },
      number_of_dofs);
}

std::complex<double> Evaluator<MCE_POLICY(CCS)>::Mean() const {
  std::complex<double> mean = 0;
  auto bra_max = bracket_pair_.Bra().Size();
  auto ket_max = bracket_pair_.Ket().Size();
  for (size_t i = 0; i < bra_max; i++) {
    for (size_t j = 0; j < ket_max; j++) {
      mean += std::conj(bracket_pair_.Bra()[i]->GetMultiplier().Value()) *
              (*this)(i, j) * bracket_pair_.Ket()[j]->GetMultiplier().Value();
    }
  }
  return mean;
};

Evaluator<MCE_POLICY(CCS)>::~Evaluator() {}

} // namespace mce
