#include "stdafx.h"

#include "Evaluator/EvaluatorEntry_CCS_Gaussian_Implementation.h"

#include "Algorithms/CCSMatrixElements.h"
#include "Algorithms/QuasiprobabilityAuxiliaries.h"
#include "Algorithms/QuasiprobabilityGaugeAuxiliaries.h"
#include "Algorithms/Utilities.h"
#include "Evaluator/EvaluatorEntry_CCS_Gaussian.h"

namespace mce {
namespace internal {
// ################################
// ################################
// ################################
// ################################
#define MCE_LOCAL_IMPLEMENTERTYPE GaussianEvaluatorEntryCCSDefaultImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSDefaultImplementation

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : entry_(std::dynamic_pointer_cast<OperatorEntryGaussian<MCE_POLICY(CCS)>>(
          entry)),
      creation_annihilation_matrix_(math::GetCreationAnnihilationMatrix(
          entry_->taylor_coefficients_, // ???!!! is entry initialized by
                                        // previous line or we should again use
                                        // dynamic_pointer_cast?
          bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(
              dof_id),
          bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(
              dof_id) -
              (entry_->exp_taylor_coefficients_.size() < 3
                   ? 0.
                   : entry_->exp_taylor_coefficients_[2]
                         .real()))) // ???!!! tricky place: difference can be
                                    // zero or even negative even though the
                                    // matrix elements are non-inifinite are
                                    // well-defined! Think about better
                                    // algorithm...
{
  bra_z_vectors_.resize(bracket_pair.Bra().Size());
  ket_storage_.resize(bracket_pair.Ket().Size());
}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : entry_(other.entry_),
      creation_annihilation_matrix_(other.creation_annihilation_matrix_),
      bra_z_vectors_(other.bra_z_vectors_), ket_storage_(other.ket_storage_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  if (creation_annihilation_matrix_.size() == 0) {
    return 0;
  }

  if (!bra_z_vectors_[bra_idx]) {
    bra_z_vectors_[bra_idx] = std::make_shared<VectorXc_r>(
        utilities::PowerSeries(
            creation_annihilation_matrix_.cols(),
            std::conj(math::CCSEigenvalue(
                bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
                bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(
                    dof_id))))
            .transpose());
  }
  if (!ket_storage_[ket_idx]) {
    CanonicPair effective_ket_center =
        bracket_pair.Ket()[ket_idx]->GetCenter(dof_id);
    double effective_ket_quadratic_coefficient =
        bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id);
    auto ket_multiplier_tuple = math::CCSGaussianMultiplyInPlace2(
        entry_->exp_taylor_coefficients_, effective_ket_center,
        effective_ket_quadratic_coefficient);
    ket_storage_[ket_idx] = std::make_shared<PrecalcualtedStorage>(
        effective_ket_center, ket_multiplier_tuple,
        utilities::PowerSeries(
            creation_annihilation_matrix_.rows(),
            math::CCSEigenvalue(effective_ket_center,
                                effective_ket_quadratic_coefficient)));
  }

  const auto &ket_data = *ket_storage_[ket_idx];

  auto effective_ket_quadratic_coefficient =
      bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id) -
      (entry_->exp_taylor_coefficients_.size() < 3
           ? 0.
           : entry_->exp_taylor_coefficients_[2].real());

  auto effective_overlap = math::CCSOverlap(
      bracket_pair.Bra()[bra_idx]->GetCenter(dof_id), //
      ket_data.center,
      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id),
      effective_ket_quadratic_coefficient,
      std::get<1>(ket_storage_[ket_idx]->multiplier_tuple));

  auto return_value =
      std::get<0>(ket_data.multiplier_tuple) * effective_overlap *
      ((*bra_z_vectors_[bra_idx]) *
       (creation_annihilation_matrix_ * (ket_data.z_vector)))(0, 0);

  return return_value;
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

  // ################################
  // ################################
  // ################################
  // ################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  GaussianEvaluatorEntryCCSPositionQuasiprobabilityFlowImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSPositionQuasiprobabilityFlowImplementation

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : entry_(std::dynamic_pointer_cast<OperatorEntryGaussian<MCE_POLICY(CCS)>>(
          entry)),
      strategy_(std::dynamic_pointer_cast<
                evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(
                    CCS)>>(bracket_pair.Strategy())),
      husimi_alpha_pair_(
          strategy_->GetGaussianQuadraticPXCoefficients(bracket_pair, dof_id)) {
}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : entry_(other.entry_), strategy_(other.strategy_),
      husimi_alpha_pair_(other.husimi_alpha_pair_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  return quasiprobability_auxiliaries::GaussianWeightedPositionFlow(
      bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(dof_id),
      husimi_alpha_pair_, bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id),
      bracket_pair.Ket()[ket_idx]->GetCenter(dof_id),
      bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id),
      entry_->taylor_coefficients_, entry_->exp_taylor_coefficients_);
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

  // ################################
  // ################################
  // ################################
  // ################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  GaussianEvaluatorEntryCCSMomentumQuasiprobabilityFlowImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSMomentumQuasiprobabilityFlowImplementation

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : entry_(std::dynamic_pointer_cast<OperatorEntryGaussian<MCE_POLICY(CCS)>>(
          entry)),
      strategy_(std::dynamic_pointer_cast<
                evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(
                    CCS)>>(bracket_pair.Strategy())),
      husimi_alpha_pair_(
          strategy_->GetGaussianQuadraticPXCoefficients(bracket_pair, dof_id)) {
}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : entry_(other.entry_), strategy_(other.strategy_),
      husimi_alpha_pair_(other.husimi_alpha_pair_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  if (strategy_->DofId() == dof_id) {
    return quasiprobability_auxiliaries::GaussianWeightedMomentumFlow(
        bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(dof_id),
        husimi_alpha_pair_, bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
        bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id),
        bracket_pair.Ket()[ket_idx]->GetCenter(dof_id),
        bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id),
        entry_->taylor_coefficients_, entry_->exp_taylor_coefficients_);
  }
  return quasiprobability_auxiliaries::
      ComplementaryGaussianWeightedMomentumFlow(
          bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(
              dof_id),
          husimi_alpha_pair_, bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
          bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(
              dof_id),
          bracket_pair.Ket()[ket_idx]->GetCenter(dof_id),
          bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(
              dof_id),
          entry_->taylor_coefficients_, entry_->exp_taylor_coefficients_);
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

  // ######################################################################
  // ######################################################################
  // ######################################################################
  // #################     GAUGE FLOW TRANSFORMATIONS    ##################
  // ######################################################################
  // ######################################################################
  // ######################################################################

  // ################################
  // ################################
  // ################################
  // ################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  GaussianEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : entry_(std::dynamic_pointer_cast<OperatorEntryGaussian<MCE_POLICY(CCS)>>(
          entry)),
      strategy_(std::dynamic_pointer_cast<
                evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(
                    CCS)>>(bracket_pair.Strategy())),
      husimi_alpha_pair_(
          strategy_->GetGaussianQuadraticPXCoefficients(bracket_pair, dof_id)) {
}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : entry_(other.entry_), strategy_(other.strategy_),
      husimi_alpha_pair_(other.husimi_alpha_pair_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  return quasiprobability_gauge_auxiliaries::GaugeMomentumFlowMatrixElement(
      entry_->taylor_coefficients_, entry_->exp_taylor_coefficients_,
      strategy_->MeanMomentum(), strategy_->MeanMomentumXDerivative(),
      bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(dof_id),
      bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id),
      bracket_pair.Ket()[ket_idx]->GetCenter(dof_id),
      bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id),
      husimi_alpha_pair_, strategy_->ApproximationOrder());
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

  // ################################
  // ################################
  // ################################
  // ################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  GaussianEvaluatorEntryCCSPositionQuasiprobabilityGaugeImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSPositionQuasiprobabilityGaugeImplementation

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : entry_(std::dynamic_pointer_cast<OperatorEntryGaussian<MCE_POLICY(CCS)>>(
          entry)),
      strategy_(std::dynamic_pointer_cast<
                evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(
                    CCS)>>(bracket_pair.Strategy())),
      husimi_alpha_pair_(
          strategy_->GetGaussianQuadraticPXCoefficients(bracket_pair, dof_id)) {
}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : entry_(other.entry_), strategy_(other.strategy_),
      husimi_alpha_pair_(other.husimi_alpha_pair_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  return quasiprobability_gauge_auxiliaries::GaugePositionFlowMatrixElement(
      entry_->taylor_coefficients_, entry_->exp_taylor_coefficients_,
      strategy_->MeanMomentum(),
      bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(dof_id),
      bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id),
      bracket_pair.Ket()[ket_idx]->GetCenter(dof_id),
      bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id),
      husimi_alpha_pair_, strategy_->ApproximationOrder());
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

  // ######################################################################
  // ######################################################################
  // ######################################################################
  // #################     QUASIPROBABILITY OPERATOR     ##################
  // ######################################################################
  // ######################################################################
  // ######################################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  GaussianEvaluatorEntryCCSQuasiprobabilityOperatorImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSQuasiprobabilityOperatorImplementation

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
    const BracketPair &bracket_pair, const SpatialDOF_id dof_id)
    : entry_(std::dynamic_pointer_cast<OperatorEntryGaussian<MCE_POLICY(CCS)>>(
          entry)),
      strategy_(std::dynamic_pointer_cast<
                evaluator_strategy::QuaisiprobabilityOperatorStrategy<
                    MCE_POLICY(CCS)>>(bracket_pair.Strategy())),
      husimi_alpha_pair_(
          strategy_->GetGaussianQuadraticPXCoefficients(bracket_pair, dof_id)) {

}

MCE_LOCAL_IMPLEMENTERTYPE::MCE_LOCAL_IMPLEMENTERTYPENAME(
    const MCE_LOCAL_IMPLEMENTERTYPE &other)
    : entry_(other.entry_), strategy_(other.strategy_),
      husimi_alpha_pair_(other.husimi_alpha_pair_){};

MCE_LOCAL_IMPLEMENTERTYPE::~MCE_LOCAL_IMPLEMENTERTYPENAME(){};

std::complex<double>
MCE_LOCAL_IMPLEMENTERTYPE::Evaluate(const BracketPair &bracket_pair,
                                    size_t bra_idx, size_t ket_idx,
                                    SpatialDOF_id dof_id) const {
  return quasiprobability_auxiliaries::RightHusimiOperatorMatrixElement(
      entry_->taylor_coefficients_, entry_->exp_taylor_coefficients_,
      bracket_pair.Bra()[strategy_->CenterBasisFunction()]->GetCenter(dof_id),
      bracket_pair.Bra()[bra_idx]->GetCenter(dof_id),
      bracket_pair.Bra().Configuration().GaussianQuadraticCoefficient(dof_id),
      bracket_pair.Ket()[ket_idx]->GetCenter(dof_id),
      bracket_pair.Ket().Configuration().GaussianQuadraticCoefficient(dof_id),
      husimi_alpha_pair_);
};

IEvaluatorEntryCCSImplementation::EntryType
MCE_LOCAL_IMPLEMENTERTYPE::Clone() const {
  auto clone = std::make_shared<MCE_LOCAL_IMPLEMENTERTYPE>(*this);
  return clone;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

// ################################
// ################################
// ################################
// ################################

} // namespace internal
} // namespace mce
