#include "stdafx.h"

#include "Algorithms/Utilities.h"

namespace mce {
namespace utilities {

MatrixXc_r DiagonalMatrix(ptrdiff_t index_of_diagonal, size_t size,
                          size_t size2) {
  size2 = (size2 == 0) ? size : size2;
  MatrixXc_r diagonal_matrix(size, size);
  diagonal_matrix.setZero();
  diagonal_matrix.diagonal(index_of_diagonal)
      .setConstant(std::complex<double>(1));
  return diagonal_matrix;
};

MatrixXc_r RangeMatrix(ptrdiff_t index_of_diagonal, size_t size, size_t size2) {
  size2 = (size2 == 0) ? size : size2;
  MatrixXc_r range_matrix(size, size2);
  range_matrix.setZero();

  auto diagonal_length = range_matrix.diagonal(index_of_diagonal).size();
  range_matrix.diagonal(index_of_diagonal) =
      Eigen::VectorXd(diagonal_length)
          .setLinSpaced(int(diagonal_length), int(1), int(diagonal_length)) // ???!!! eigen generates warnings without this typecasts
          .cast<std::complex<double>>();
  return range_matrix;
};

MatrixXc_r TaylorXDifferenciate(const MatrixXc_r &taylor_coefficients) {
  if ((taylor_coefficients.size() == 0) || (taylor_coefficients.cols() < 2)) {
    return MatrixXc_r(taylor_coefficients.rows(),
                      std::max(taylor_coefficients.cols() - 1, Eigen::Index(0)))
        .setZero();
  };
  MatrixXc_r diff =
      RangeMatrix(-1, taylor_coefficients.cols(),
                  std::max(Eigen::Index(0), taylor_coefficients.cols() - 1));
  MatrixXc_r output = taylor_coefficients * diff;
  return output;
};

MatrixXc_r TaylorXMultiply(const MatrixXc_r &left, const VectorXc_r &right) {
  MatrixXc_r output(
      left.rows(),
      std::max(Eigen::Index(0),
               left.cols() + std::max(right.cols() - 1, Eigen::Index(0))));
  output.setZero();
  for (MatrixXc_r::Index i = 0; i < right.cols(); ++i) {
    output.block(0, i, left.rows(), left.cols()) += left * right(i);
  }
  return output;
};

MatrixXc_r TaylorXAdd(const MatrixXc_r &left, const MatrixXc_r &right) {
  MatrixXc_r output(std::max(left.rows(), right.rows()),
                    std::max(left.cols(), right.cols()));
  output.setZero();
  output.block(0, 0, left.rows(), left.cols()) += left;
  output.block(0, 0, right.rows(), right.cols()) += right;
  return output;
};

bool CheckEqual(const MatrixXc_r &left, const MatrixXc_r &right,
                double tolerance) {
  if (left.size() == right.size()) {
    if (left.size() == 0) {
      return true;
    }
    return left.isApprox(right, kTolerance);
  }
  return false;
}

} // namespace utilities
}; // namespace mce
