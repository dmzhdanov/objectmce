#include "stdafx.h"

#include <boost/math/constants/constants.hpp>

#include "Algorithms/CCSMatrixElements.h"
#include "Algorithms/WavefunctionUtilities.h"

namespace mce {
namespace ccs_utilities {

void Project(Wavefunction &target, const Wavefunction &source,
             const Operator<PolicyType> &kernel, bool fast, double epsilon_reg) {
  auto copy_t = target;

  auto A_evaluator = kernel.Evaluate({target, target});
  auto b_evaluator = kernel.Evaluate({target, source});

  MatrixXc_r A(target.Size(), target.Size());
  for (size_t i = 0, nRows = A.rows(), nCols = A.cols(); i < nRows; ++i) {
    for (size_t j = 0; j < nCols; ++j) {
      A(i, j) = A_evaluator(i, j);
    }
    A(i, i) += epsilon_reg;
  }

  VectorXc_c b(target.Size());
  b.setZero();
  for (size_t i = 0, nRows = b.rows(), jMax = source.Size(); i < nRows; ++i) {
    for (size_t j = 0; j < jMax; ++j) {
      b(i) += b_evaluator(i, j) * source[j]->GetMultiplier().Value();
    }
  }

  // fast algorithm
  // https://eigen.tuxfamily.org/dox/group__TutorialLinearAlgebra.html
  // slow algorithm:
  // https://eigen.tuxfamily.org/dox/group__LeastSquares.html

  using namespace Eigen;
  VectorXc_c coeffs =
      fast ? A.colPivHouseholderQr().solve(b) //  A.llt().solve(b)
           : (VectorXc_c)A.bdcSvd(ComputeThinU | ComputeThinV).solve(b);
  for (size_t i = 0, nRows = coeffs.rows(); i < nRows; ++i) {
    target[i]->GetMultiplier().Amplitude(coeffs(i) /* - epsilon_reg*/);
    target[i]->GetMultiplier().Phase(0);
  }
}

void Filter(Wavefunction &target, double tolerance) {
  size_t i = 0;
  while (i < target.Size()) {
    if (std::abs(target[i]->GetMultiplier().Amplitude()) < tolerance) {
      target.Erase(i);
    } else {
      i++;
    }
  }
}

std::complex<double> Value(const Wavefunction &target,
                           const VectorXc_r &point) {
  std::complex<double> value = 0;
  auto number_of_spatial_dofs = target.Configuration().NumberOfSpatialDOFs();
  if (number_of_spatial_dofs != point.size()) {
    QI_Error::RaiseError(
        "[ERROR] Value - point's size != number of spatial dofs: " +
        std::to_string(point.size()) +
        " != " + std::to_string(number_of_spatial_dofs));
  }
  for (const auto &gaussian : target) {
    for (SpatialDOF_id dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
      auto &center = gaussian->GetCenter(dof_id);
      auto dx = point[dof_id] - center.X();
      std::complex<double> alpha =
          target.Configuration().GaussianQuadraticCoefficient(dof_id);
      value +=
          gaussian->GetMultiplier().Value() *
          std::exp(-alpha * dx * dx + I * center.P() * dx) *
          std::pow(2. * alpha / boost::math::constants::pi<double>(), 0.25);
    }
  };
  return value;
};

} // namespace ccs_utilities
} // namespace mce
