#include "stdafx.h"

#include "Algorithms/CCSPropagator.h"

#include "Algorithms/WavefunctionOdeintSupport.h"
#include "Wavefunction/Wavefunction.h"

#include <boost/numeric/odeint/algebra/vector_space_algebra.hpp> // all algebras.
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp> // the integrate routines.
#include <boost/numeric/odeint/integrate/integrate_const.hpp> // the integrate routines.
#include <boost/numeric/odeint/stepper/runge_kutta_dopri5.hpp> // the include path for all steppers, XYZ is a placeholder for a stepper.

//#include <boost/numeric/odeint.hpp>
//namespace std{
//    mce::Wavefunction abs(const mce::Wavefunction &state) {
//    mce::Wavefunction state_abs = state;
//    for (auto &basis_function : state_abs) {
//        state_abs.Configuration().CWiseAbs(basis_function);
//    }
//    return state_abs;
//    }
//}

namespace mce {
namespace algorithm {

CCSPropagator::CCSPropagator() : werewolf_policy_(nullptr),adaptive_flag_(true){};
CCSPropagator::~CCSPropagator(){};

CCSPropagator &
CCSPropagator::SetWerewolfPolicy(WerewolfPolicyType werewolf_policy) {
  werewolf_policy_ = werewolf_policy;
  return *this;
}

CCSPropagator& CCSPropagator::SetAdaptiveFlag(bool adaptive_flag){
	adaptive_flag_ = adaptive_flag;
	return *this;
}

void CCSPropagator::Integrate(const ISystem &system, Wavefunction &wavefunction,
                              double start_time, double end_time,
                              double delta_t, double absolute_error_tolerance,
                              double relative_error_tolerance) {
  std::function<bool(Wavefunction &, const double, const size_t)> obs =
      [this](Wavefunction const &x, const double t, const size_t step_number) {
        std::cout << "\n*** " << t;
        return true;
      };
  ObservedIntegrate(system, wavefunction, start_time, end_time, delta_t, obs, 0,
                    absolute_error_tolerance, relative_error_tolerance);
};

void CCSPropagator::ObservedIntegrate(
    const ISystem &system, Wavefunction &wavefunction, double start_time,
    double end_time, double delta_t,
    const std::function<bool(Wavefunction &, const double, const size_t)>
        &observer,
    double observation_interval, double absolute_error_tolerance,
    double relative_error_tolerance) {

  class UserTriggeredException : public std::exception {};

  using namespace boost::numeric::odeint;

  using StepperType = runge_kutta_dopri5<Wavefunction, double, Wavefunction,
                                         double, vector_space_algebra>;

  std::function<void(const Wavefunction, Wavefunction &, const double)>
      f_system = [&system](const Wavefunction wf, Wavefunction &der,
                           const double time) { return system(wf, der, time); };

  TimeVar clock_time;
  utilities::MeasureTime(&clock_time, true);

  size_t step_number = 0;
  std::function<void(Wavefunction &, const double)> timely_observer =
      [&step_number, &clock_time, &observation_interval, &observer, &end_time,
       &start_time, this](Wavefunction &wavefunction, const double time) {
        if (werewolf_policy_ != nullptr) {
          werewolf_policy_(wavefunction, time);
        }
        auto time_diff = utilities::MeasureTime(&clock_time, false);
        if ((std::abs(time - start_time) < kTolerance) ||
            (std::abs(end_time - time) < kTolerance) ||
            (time_diff >= (decltype(time_diff))observation_interval)) {
          utilities::MeasureTime(&clock_time, true);

          bool continue_flag = observer(wavefunction, time, step_number);

          if (!continue_flag)
            throw UserTriggeredException();
        }
        step_number++;
      };

  // size_t steps = integrate_const(StepperType(), f_system, wavefunction,
  //                               start_time, end_time, delta_t, obs);

  //#ifdef PYTHON_BINDINGS
  //  pybind11::gil_scoped_release release;
  //#endif

  try {
	  if (adaptive_flag_)
	  {
		  size_t steps = integrate_adaptive(
			  make_controlled<StepperType>(absolute_error_tolerance,
				  relative_error_tolerance),
			  f_system, wavefunction, start_time, end_time, delta_t, timely_observer);
	  }
	  else
	  {
		  size_t steps = integrate_const(
			  StepperType(),
			  f_system, wavefunction, start_time, end_time, delta_t, timely_observer);
	  }
  } catch (const UserTriggeredException &) {
    std::cout << "\n[EXCEPTION] Integration aborted by user.";
    // QI_Error::RaiseError("[EXCEPTION] CCSPropagator::ObservedIntegrate -
    // Integration aborted by user.");
  };

  //#ifdef PYTHON_BINDINGS
  //  pybind11::gil_scoped_acquire acquire;
  //#endif
};
} // namespace algorithm
} // namespace mce
