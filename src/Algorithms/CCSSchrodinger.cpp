#include "stdafx.h"

#include <Eigen/Eigenvalues>

#include "Algorithms/CCSSchrodinger.h"
#include "Algorithms/CCSSystemUtilities.h"

namespace mce {
namespace algorithm {

CCSSchrodingerSystem::CCSSchrodingerSystem(
    const Operator<PolicyType> &hamiltonian,
    const Operator<PolicyType> &linear_solve_kernel,
    OperatorFactory<PolicyType> &operator_factory)
    : hamiltonian_(hamiltonian), linear_solve_kernel_(linear_solve_kernel),
      operator_factory_(operator_factory), amplitude_max_regularizer_(1.e-7){};

void CCSSchrodingerSystem::operator()(const Wavefunction &state,
                                     Wavefunction &der_t_state,
                                     const double t) const {

  size_t max_retries = 20;
  VectorXc_c coeffs; // amplitude coefficients to be calculated;
  double linear_solve_error;
  amplitude_max_regularizer_ /=
      (amplitude_max_regularizer_ > 1.e-7 ? 4. : 2.); // mutable variable
  size_t attempts_count = 0;
  double state_norm =
      operator_factory_.CreateId(0).Evaluate({state}).Mean().real();

  do {
    {
      int i = 0;
      // if (attempts_count > 0)
      //{
      // std::cout << "\n t = " << t << ", H = " <<
      // hamiltonian_.Evaluate({state}).Mean()/ state_norm << "\n";
      //}
      while (i++ < attempts_count) {
        std::cout << "*";
      }
    }
    amplitude_max_regularizer_ *= 2; // mutable variable
    attempts_count++;
    if (der_t_state.IsNull()) {
      der_t_state = state.Clone(false);
    }
    der_t_state.SetZero();
    using namespace ccs_system_utilities;
    auto H_der = Derivative(hamiltonian_, operator_factory_).Evaluate({state});
    auto number_of_spatial_dofs = state.Configuration().NumberOfSpatialDOFs();

    // calculating centers shifts dP/dt and dX/dt according to Hamilton
    // equations and the phase time derivative "as is normally done" (c) D.V.
    // Makhov et al. / Chemical Physics 493 (2017) 200�218
    for (size_t n = 0; n < state.Size(); n++) {
      double der_t_phase = 0;
      for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
        der_t_state[n]->GetCenter(dof_id).P(
            -H_der.DiffCenterX(dof_id, n)
                 .real()); // ???!!! taking real part! double-check in tests
                           // that result is real
        der_t_state[n]->GetCenter(dof_id).X(
            H_der.DiffCenterP(dof_id, n)
                .real()); // ???!!! taking real part! double-check in tests that
                          // result is real
        der_t_phase += state[n]->GetCenter(dof_id).P() *
                       der_t_state[n]->GetCenter(dof_id).X();
      }
      der_t_state[n]->GetMultiplier().Phase(der_t_phase - H_der(n, n).real());
    }
    // updating multiplier

    // ???!!! Unfortunately, self adjoint Eigen solver seems to not work with
    // row-major matrices
    // Eigen::MatrixXcd
    MatrixXc_r A(state.Size(), state.Size());
    VectorXc_c b(state.Size());

    // Finding the remaining amplitude variations as a solution of linear system
    // A.x=b initializing A
    auto A_evaluator = linear_solve_kernel_.Evaluate({state});
    for (size_t i = 0, nRows = A.rows(), nCols = A.cols(); i < nRows; ++i) {
      for (size_t j = 0; j < nCols; ++j) {
        A(i, j) =
            A_evaluator(i, j) * std::exp(I * state[j]->GetMultiplier().Phase());
      }
      A(i, i) += amplitude_max_regularizer_;
    }

    // initializing b
    auto Id_der =
        Derivative(linear_solve_kernel_, operator_factory_).Evaluate({state});
    auto H = (linear_solve_kernel_ * hamiltonian_)
                 .Evaluate({state}); // ???!!! Optimize by defining the
                                     // corresponding class variable!
    for (size_t m = 0; m < state.Size(); m++) {
      std::complex<double> pder_t_ampl_H = 0;
      for (size_t n = 0; n < state.Size(); n++) {
        std::complex<double> pder_t_ampl = 0;
        for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
          pder_t_ampl += der_t_state[n]->GetCenter(dof_id).X() *
                             Id_der.DiffKetCenterX(dof_id, m, n) +
                         der_t_state[n]->GetCenter(dof_id).P() *
                             Id_der.DiffKetCenterP(dof_id, m, n);
        }
        pder_t_ampl +=
            I / kHBar * der_t_state[n]->GetMultiplier().Phase() * Id_der(m, n);

        pder_t_ampl_H += -(H(m, n) * I / kHBar + pder_t_ampl) *
                         state[n]->GetMultiplier().Value();
      }
      b(m) = pder_t_ampl_H;
    }

    using namespace Eigen;
    coeffs = // A.llt().solve(b);// Doesn't work in most cases.
        A.colPivHouseholderQr().solve(b);
    VectorXc_c v = (A * coeffs - b);
    linear_solve_error = std::abs((v.transpose() * v)(0, 0));
  } while ((linear_solve_error > kTolerance) && (attempts_count < max_retries));

  if (attempts_count >= max_retries) {
    std::cout << "\n[INFO] Max retries limit of " << max_retries
              << " exceeded. Linear solve error is still " << linear_solve_error
              << ". Continuing anyway...\n";
  };

  for (size_t m = 0; m < state.Size(); m++) {
    der_t_state[m]->GetMultiplier().Amplitude(coeffs(m));
  }

  auto self_projection =
      std::abs(ccs_system_utilities::WavefunctionDerivativeSelfProjection(
                   state, der_t_state, operator_factory_)
                   .real());
  if (self_projection > 1.e-5) {
    std::cout << "\n Look at this beautiful self-projection: "
              << self_projection;
  }
}

} // namespace algorithm
} // namespace mce
