#include "stdafx.h"

#include "Algorithms/QuasiprobabilityAuxiliaries.h"

#include "Algorithms/CCSMatrixElements.h"
#include "Algorithms/Utilities.h"

#include "Commons/Faddeeva.hh"
#include <boost/math/constants/constants.hpp>
#include <math.h>

namespace mce {
namespace quasiprobability_auxiliaries {

// interatively calculates the integrals of form
// $I_n=\int_{-\infty}^{\infty}x^n erf(a x + b) \exp(-\alpha x^2 + \beta x) dx$
// where p ranges from 0 to p_max
// result is written to the 0-th argument
void ErfGaussIntegral(
    VectorXc_c &output, std::complex<double> a, std::complex<double> b,
    double alpha, std::complex<double> beta, int p_max,
    std::complex<double> exponential_regularizing_multiplier) {
  if (output.size() < p_max + 1) {
    output.resize(p_max + 1);
  };
  auto erf = [](std::complex<double> arg) -> std::complex<double> {
    return Faddeeva::erf(arg);
  };
  using Vector = std::vector<std::complex<double>>;
  auto SetZero = [](Vector &v) { std::fill(v.begin(), v.end(), 0); };

  double pi = boost::math::constants::pi<double>();
  std::complex<double> A = a / std::sqrt(2. * alpha);
  std::complex<double> &B = b;
  std::complex<double> mu = beta / std::sqrt(2. * alpha);
  std::complex<double> Z = 1. + 2. * A * A;
  double alpha_mult = std::pow(2. * alpha, -0.5);
  std::complex<double> R1 =
      std::sqrt(2 * pi) *
      std::exp(mu * mu / 2. + exponential_regularizing_multiplier) *
      erf((A * mu + B) / std::sqrt(Z));
  std::complex<double> R2 =
      std::sqrt(2 * pi) *
      std::exp(mu * mu / 2. + exponential_regularizing_multiplier -
               std::pow(A * mu + B, 2) / Z);

  Vector P(p_max + 2); // change to +2 to use primitve version
  Vector Q(p_max + 2);
  Vector Q_new(p_max + 2);
  Vector P_new(p_max + 2);
  SetZero(P);
  SetZero(Q);
  SetZero(Q_new);
  SetZero(P_new);
  P[0] = alpha_mult;
  Vector muPowers(p_max + 1);
  muPowers[0] = 1;
  for (int i = 1; i <= p_max; i++) {
    muPowers[i] = muPowers[i - 1] * mu;
  }

  // std::complex<double> Z1 = 2. * A * B;
  // std::complex<double> Z2 = 2. * A / std::sqrt(pi * Z) * R2;
  // output(0) = muPowers[0] * P[0] * R1;
  // for (int i = 1; i <= p_max; i++) {
  //  P_new[0] = alpha_mult * (P[1]);
  //  Q_new[0] = alpha_mult * (Q[1] - Z1 * Q[0] / Z + Z2 * P[0]);
  //  output(i) = muPowers[0] * (P_new[0] * R1 + Q_new[0]);
  //  for (int j = 1; j < i; j++) {
  //    P_new[j] = (i - j) % 2 == 1
  //                   ? 0
  //                   : alpha_mult * (double(j + 1) * P[j + 1] + P[j - 1]);
  //    Q_new[j] = alpha_mult * (double(j + 1) * Q[j + 1] +
  //                             (Q[j - 1] - Z1 * Q[j]) / Z + Z2 * P[j]);
  //    output(i) += muPowers[j] * (P_new[j] * R1 + Q_new[j]);
  //  }
  //  P_new[i] = alpha_mult * P[i - 1];
  //  output(i) += muPowers[i] * (P_new[i] * R1);
  //  std::swap(Q_new, Q);
  //  std::swap(P_new, P);
  //}

  std::complex<double> Z1 = 2. * A * B;
  std::complex<double> Z2 = 2. * A / std::sqrt(pi * Z) * R2;
  output(0) = muPowers[0] * P[0] * R1;
  for (int i = 1; i <= p_max; i++) {
    P_new[i] = alpha_mult * P[i - 1];
    output(i) = muPowers[i] * (P_new[i] * R1);
    for (int j = i - 1; j > 0; j--) {
      P_new[j] = 0;
      Q_new[j] = alpha_mult * (double(j + 1) * Q[j + 1] +
                               (Q[j - 1] - Z1 * Q[j]) / Z + Z2 * P[j]);
      output(i) += muPowers[j] * Q_new[j];
      if (--j == 0)
        break;
      P_new[j] = alpha_mult * (double(j + 1) * P[j + 1] + P[j - 1]);
      Q_new[j] =
          alpha_mult * (double(j + 1) * Q[j + 1] + (Q[j - 1] - Z1 * Q[j]) / Z);
      output(i) += muPowers[j] * (P_new[j] * R1 + Q_new[j]);
    }
    P_new[0] = alpha_mult * (P[1]);
    Q_new[0] = alpha_mult * (Q[1] - Z1 * Q[0] / Z + Z2 * P[0]);
    output(i) += muPowers[0] * (P_new[0] * R1 + Q_new[0]);
    std::swap(Q_new, Q);
    std::swap(P_new, P);
  }
}

std::tuple<std::complex<double>, double, double, double>
JIntegralCoefficients(double P, double X, double a, double b, double p2,
                      double x2, double alpha2) {
  double pi = boost::math::constants::pi<double>();

  std::complex<double> mu =
      2 * kHBar * pi *
      std::exp(((-I) * a * b * kHBar * (a * p2 + 2. * alpha2 * (P + 2. * p2)) *
                    (-X + x2) -
                I * a * p2 * (-X + x2) / kHBar -
                (2. * I) * a * alpha2 * (a + 4. * alpha2) * P * (-X + x2) *
                    std::pow(b, 2) * std::pow(kHBar, 3) -
                a * b * std::pow(-P + p2, 2) -
                alpha2 * b * std::pow(-P + p2, 2) -
                a * alpha2 * std::pow(-X + x2, 2) -
                a * alpha2 * b * std::pow(kHBar, 2) *
                    (b * std::pow(-P + p2, 2) +
                     (a + 4. * alpha2) * std::pow(-X + x2, 2))) /
               (a + alpha2 + a * alpha2 * b * std::pow(kHBar, 2)) /
               (1 + (a + 4. * alpha2) * b * std::pow(kHBar, 2))) *
      std::pow((a + alpha2 + a * alpha2 * b * std::pow(kHBar, 2)) *
                   (1 + (a + 4. * alpha2) * b * std::pow(kHBar, 2)) / alpha2,
               -0.25);

  double p =
      (p2 + b * (2 * a * P + 4 * alpha2 * P - a * p2) * std::pow(kHBar, 2)) /
      (1 + (a + 4 * alpha2) * b * std::pow(kHBar, 2));

  double x = (alpha2 * x2 +
              a * (X - alpha2 * b * (-2 * X + x2) * std::pow(kHBar, 2))) /
             (a + alpha2 + a * alpha2 * b * std::pow(kHBar, 2));

  double alpha = kHBar * (a + alpha2 + a * alpha2 * b * std::pow(kHBar, 2)) /
                 (kHBar + (a + 4 * alpha2) * b * std::pow(kHBar, 3));

  return std::tuple<std::complex<double>, double, double, double>(mu, p, x,
                                                                  alpha);
}

std::tuple<std::complex<double>, CanonicPair, double>
JIntegralCoefficients(const CanonicPair &kernel_center,
                      const CanonicPair &kernel_width,
                      const CanonicPair &ket_center, double ket_alpha) {
  auto result = JIntegralCoefficients(
      kernel_center.P(), kernel_center.X(), kernel_width.X(), kernel_width.P(),
      ket_center.P(), ket_center.X(), ket_alpha);
  CanonicPair center;
  center.P(std::get<1>(result));
  center.X(std::get<2>(result));
  return std::tuple<std::complex<double>, CanonicPair, double>(
      std::get<0>(result), center, std::get<3>(result));
};

std::complex<double> GaussianWeightedMomentumFlow(
    double P, double X, double a, double b, double p1, double x1, double alpha1,
    double p2, double x2, double alpha2, const MatrixXc_r &taylor_coefficients,
    const VectorXc_r &exp_taylor_coefficients) {

  if (taylor_coefficients.size() == 0) {
    return 0;
  }

  if (exp_taylor_coefficients.size() > 3) {
    QI_Error::RaiseError("[ERROR] GaussianWeightedMomentumFlow - length of "
                         "exp_taylor_coefficients should not exceed 3.");
  }

  if ((exp_taylor_coefficients.size() == 3) &&
      std::abs(exp_taylor_coefficients[2].imag()) > kTolerance) {
    QI_Error::RaiseError(
        "[ERROR] GaussianWeightedMomentumFlow - exp_taylor_coefficients[2] "
        "should not contain imaginary part.");
  }

  std::complex<double> c0[2], c1[2];

  c0[0] = x1 * (-(alpha1 * x1) + I * p1 / kHBar) -
          std::pow(kHBar, -1) * (I * p2 * x2 + a * kHBar * std::pow(X, 2) +
                                 alpha2 * kHBar * std::pow(x2, 2));
  c0[1] = 2. * a * X + 2. * alpha1 * x1 + 2. * alpha2 * x2 - I * p1 / kHBar +
          I * p2 / kHBar;

  double c02minus = a + alpha1 + alpha2;
  c1[0] = a * X + 2 * alpha2 * x2 + I * (-P + p2) / kHBar;
  c1[1] = -a - 2 * alpha2;
  std::complex<double> c20InvSqrt =
      2 * std::sqrt(b * std::pow(kHBar, 2) /
                    (1 + (a + 4 * alpha2) * b * std::pow(kHBar, 2)));

  for (size_t i = 0;
       i < std::min((size_t)exp_taylor_coefficients.size(), (size_t)2); i++) {
    c0[i] += exp_taylor_coefficients[i];
  }
  if (exp_taylor_coefficients.size() == 3) {
    c02minus -= exp_taylor_coefficients[2].real();
  }
  // std::cout << "\nc0: " << c0[0] << ", " << c0[1] << ", " << -c02minus;
  // std::cout << "\nc1: " << c1[0] << ", " << c1[1];
  // std::cout << "\nc20InvSqrt: " << c20InvSqrt << "\n";

  VectorXc_c output;
  ErfGaussIntegral(output, 0.5 * I * c1[1] * c20InvSqrt,
                   0.5 * I * c1[0] * c20InvSqrt, c02minus, c0[1],
                   std::max((int)taylor_coefficients.cols() - 1, 0), c0[0]);

  std::complex<double> mult = (-I) * std::sqrt(2. / b) *
                              std::pow(alpha1 * alpha2, 0.25) /
                              kHBar; // *std::exp(c0[0]);

  // std::cout << "mult: " << mult << "\n";

  return (taylor_coefficients.row(0) * output)(0, 0) * mult;
}

std::complex<double>
GaussianWeightedMomentumFlow(const CanonicPair &kernel_center,
                             const CanonicPair &kernel_width,
                             const CanonicPair &bra_center, double bra_alpha,
                             const CanonicPair &ket_center, double ket_alpha,
                             const MatrixXc_r &taylor_coefficients,
                             const VectorXc_r &exp_taylor_coefficients) {
  return GaussianWeightedMomentumFlow(
      kernel_center.P(), kernel_center.X(), kernel_width.X(), kernel_width.P(),
      bra_center.P(), bra_center.X(), bra_alpha, ket_center.P(), ket_center.X(),
      ket_alpha, taylor_coefficients, exp_taylor_coefficients);
};

std::complex<double> ComplementaryGaussianWeightedMomentumFlow(
    const CanonicPair &kernel_center, const CanonicPair &kernel_width,
    const CanonicPair &bra_center, double bra_alpha,
    const CanonicPair &ket_center, double ket_alpha,
    const MatrixXc_r &taylor_coefficients,
    const VectorXc_r &exp_taylor_coefficients) {
  if ((taylor_coefficients.rows() == 0) || (taylor_coefficients.rows() == 0)) {
    return 0;
  }

  if (taylor_coefficients.rows() > 1) {
    QI_Error::RaiseError("[ERROR] ComplementaryGaussianWeightedMomentumFlow - "
                         "taylor_coefficients"
                         "should be one-row matrix. Actual dimensions are (" +
                         std::to_string(taylor_coefficients.rows()) + " x " +
                         std::to_string(taylor_coefficients.cols()) + ")");
  }

  auto ket_j_tuple =
      JIntegralCoefficients(kernel_center, kernel_width, ket_center, ket_alpha);

  double pi = boost::math::constants::pi<double>();

  return 1. / (2. * pi * kHBar) * std::get<0>(ket_j_tuple) *
         math::GetGaussianMatrixElement(
             bra_center, bra_alpha, std::get<1>(ket_j_tuple),
             std::get<2>(ket_j_tuple), taylor_coefficients,
             exp_taylor_coefficients);
}

std::complex<double>
GaussianWeightedPositionFlow(const CanonicPair &kernel_center,
                             const CanonicPair &kernel_width,
                             const CanonicPair &bra_center, double bra_alpha,
                             const CanonicPair &ket_center, double ket_alpha,
                             const MatrixXc_r &taylor_coefficients,
                             const VectorXc_r &exp_taylor_coefficients) {

  if ((taylor_coefficients.rows() == 0) || (taylor_coefficients.rows() == 0)) {
    return 0;
  }
  if (exp_taylor_coefficients.size() > 0) {
    if (taylor_coefficients.cwiseAbs().maxCoeff() > kTolerance) {
      QI_Error::RaiseError(
          "[ERROR] GaussianWeightedPositionFlow - exp_taylor_coefficients"
          "must be empty.");
    } else {
      return 0.;
    }
  }
  if ((taylor_coefficients.cols() != 1) || (taylor_coefficients.rows() > 2)) {
    QI_Error::RaiseError(
        "[ERROR] GaussianWeightedPositionFlow - taylor_coefficients"
        "should be (2 x 1) matrix. Actual dimensions are (" +
        std::to_string(taylor_coefficients.rows()) + " x " +
        std::to_string(taylor_coefficients.cols()) + ")");
  }

  auto ket_j_tuple =
      JIntegralCoefficients(kernel_center, kernel_width, ket_center, ket_alpha);

  double pi = boost::math::constants::pi<double>();

  MatrixXc_r actual_taylor_coefficients(1, taylor_coefficients.rows());
  actual_taylor_coefficients(0, 0) = taylor_coefficients(0, 0);
  if (taylor_coefficients.rows() > 1) {
    actual_taylor_coefficients(0, 0) +=
        taylor_coefficients(1, 0) * kernel_center.P();
    actual_taylor_coefficients(0, 1) =
        taylor_coefficients(1, 0) * I / (kernel_width.P() * kHBar);
  }

  return 1. / (2. * pi * kHBar) * std::get<0>(ket_j_tuple) *
         math::GetGaussianMatrixElement(
             bra_center, bra_alpha, std::get<1>(ket_j_tuple),
             std::get<2>(ket_j_tuple), actual_taylor_coefficients,
             exp_taylor_coefficients);
}

HusimiFunctionMatrixElement::HusimiFunctionMatrixElement(
    const CanonicPair &phase_space_point, const CanonicPair &bra,
    double bra_alpha, const CanonicPair &ket, double ket_alpha,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab)
    : phase_space_point_(phase_space_point), bra_(bra), ket_(ket),
      kernel_gaussian_quadratic_coefficients_ab_(
          kernel_gaussian_quadratic_coefficients_ab),
      bra_alpha_(bra_alpha), ket_alpha_(ket_alpha) {
  std::complex<double> P2 = ket.P();
  std::complex<double> X2 = ket.X();
  std::complex<double> P1 = bra.P();
  std::complex<double> X1 = bra.X();
  auto &alpha1 = bra_alpha;
  auto &alpha2 = ket_alpha;
  auto b = kernel_gaussian_quadratic_coefficients_ab.P();
  auto a = kernel_gaussian_quadratic_coefficients_ab.X();

  nnom1_ = a + alpha1 + alpha2;
  ddenom_ = nnom1_ + b * (4 * alpha1 * alpha2 + a * (alpha1 + alpha2)) *
                         std::pow(kHBar, 2);
  A_ =
      -((a * (alpha1 + alpha2 + 4 * b * alpha1 * alpha2 * std::pow(kHBar, 2))) /
        ddenom_);
  B_ = -((b * nnom1_) / ddenom_);
  C_ = (-2 * a * b * I * (alpha1 - alpha2) * kHBar) / ddenom_;
  PP_ = (P2 * alpha1 + P1 * alpha2 +
         2. * I * (X1 - X2) * alpha1 * alpha2 * kHBar) /
        (alpha1 + alpha2);
  XX_ = (-(I * (P1 - P2)) + 2. * (X1 * alpha1 + X2 * alpha2) * kHBar) /
        (2. * (alpha1 + alpha2) * kHBar);
  D_ = (I * (P1 * X1 + PP_ * (X1 - X2) - P2 * X2 - (P1 - P2) * XX_)) /
       (2. * kHBar);
}

std::complex<double> HusimiFunctionMatrixElement::Value() {
  auto &alpha1 = bra_alpha_;
  auto &alpha2 = ket_alpha_;
  auto P0 = phase_space_point_.P();
  auto X0 = phase_space_point_.X();
  auto b = kernel_gaussian_quadratic_coefficients_ab_.P();
  auto a = kernel_gaussian_quadratic_coefficients_ab_.X();
  double constexpr pi = boost::math::constants::pi<double>();

  return 1. / pi * std::pow(alpha1 * alpha2, 0.25) *
         std::sqrt((2 * a * b) / ddenom_) *
         std::exp(D_ + B_ * std::pow(-PP_ + P0, 2) +
                  C_ * (-PP_ + P0) * (-XX_ + X0) + A_ * std::pow(-XX_ + X0, 2));
}

std::complex<double> HusimiFunctionMatrixElement::RightPolynomialOperatorValue(
    const MatrixXc_r &taylor_coefficients) {
  auto &alpha1 = bra_alpha_;
  auto &alpha2 = ket_alpha_;
  auto P0 = phase_space_point_.P();
  auto X0 = phase_space_point_.X();
  auto b = kernel_gaussian_quadratic_coefficients_ab_.P();
  auto a = kernel_gaussian_quadratic_coefficients_ab_.X();
  std::complex<double> P2 = ket_.P();
  std::complex<double> X2 = ket_.X();

  std::complex<double> mmult =
      XX_ + ((PP_ - P0) * (C_ - 4. * B_ * I * alpha1 * kHBar) +
             2. * (XX_ - X0) * (A_ - I * C_ * alpha1 * kHBar)) /
                (2. * (alpha1 + alpha2));
  std::complex<double> CC =
      -(I * std::sqrt(1. + b * (a + 4. * alpha1) * std::pow(kHBar, 2))) /
      (2. * std::sqrt(ddenom_));

  Eigen::Index dim = taylor_coefficients.size() + 1;
  std::vector<std::complex<double>> series(dim), tmp(dim),
      tmp_for_momentum_derivative(dim);
  Eigen::Map<Eigen::RowVectorXcd> series_map(series.data(), dim);
  Eigen::Map<Eigen::RowVectorXcd> tmp_map(tmp.data(), dim);
  series_map.setZero();
  tmp_map.setZero();
  if (taylor_coefficients.rows() == 0) {
    return 0;
  }

  const std::complex<double> c0 = (P2 - 2. * I * alpha2 * X2 * kHBar);
  const std::complex<double> c1 = 2. * I * alpha2 * kHBar;

  auto MomentumDerivative = [c0, c1, &tmp_for_momentum_derivative](
                                std::vector<std::complex<double>> &output,
                                Eigen::Index len) {
    auto &input = tmp_for_momentum_derivative;
    std::copy(output.begin(), output.begin() + len + 1, input.begin());
    std::transform(input.begin(), input.begin() + len, output.begin(),
                   [c0](std::complex<double> &val) { return val * c0; });
    output[len] = 0;
    std::transform(
        input.begin(), input.begin() + len, output.begin() + 1, output.begin() + 1,
        [c1](std::complex<double> &val_in, std::complex<double> &val_out) {
          return val_out += val_in * c1;
        });
    for (Eigen::Index i = 1; i < len; i++) {
      output[i - 1] += -I * kHBar * double(i) * input[i];
    }
  };

  series_map.head(taylor_coefficients.cols()) = taylor_coefficients.row(0);
  for (Eigen::Index row_idx = 1; row_idx < taylor_coefficients.rows();
       row_idx++) {
    tmp_map.head(taylor_coefficients.cols()) = taylor_coefficients.row(row_idx);
    for (int k = 0; k < row_idx; k++) {
      MomentumDerivative(tmp, taylor_coefficients.cols() + k);
    }
    series_map.head(taylor_coefficients.cols() + row_idx) +=
        tmp_map.head(taylor_coefficients.cols() + row_idx);
  }

  tmp_map.setZero();
  std::complex<double> CCincr = CC;
  for (unsigned i = 1; i < dim; i++) {
	  series[i] *= CCincr;
	  CCincr *= CC;
  }

  mce::utilities::HermiteSeries(series.begin(), series.end(), tmp.begin());
  std::complex<double> mult_incr = 1;

  std::complex<double> output = 0;
  for (unsigned i = 0; i < dim; i++) {
	  output += tmp[i] * mult_incr;
	  mult_incr *= mmult/2./CC;
  }

  output *= Value();
  return output; 
}

std::complex<double> RightHusimiOperatorMatrixElement(
    const MatrixXc_r &taylor_coefficients,
    const VectorXc_r &exp_taylor_coefficients,
    const CanonicPair &phase_space_point, const CanonicPair &bra,
    double bra_alpha, const CanonicPair &ket, double ket_alpha,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab) {

  double ket_alpha_eff = ket_alpha;
  CanonicPair ket_eff = ket;

  std::complex<double> prefactor = math::CCSGaussianMultiplyInPlace(
      exp_taylor_coefficients, ket_eff, ket_alpha_eff);

  HusimiFunctionMatrixElement husimi_function_matrix_element(
      phase_space_point, bra, bra_alpha, ket_eff, ket_alpha_eff,
      kernel_gaussian_quadratic_coefficients_ab);
  return prefactor*husimi_function_matrix_element.RightPolynomialOperatorValue(taylor_coefficients);
}

} // namespace quasiprobability_auxiliaries
} // namespace mce
