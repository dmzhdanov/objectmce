#include "Algorithms/Conversions.h"

namespace mce {
namespace utilities {
PhaseSpacePoint
PhaseSpacePointFromMatrix(const Eigen::Ref<const MatrixXd_r> &matrix) {
  if (matrix.cols() != 2) {
    QI_Error::RaiseError(
        "[Error] PhaseSpacePointFromMatrix - matrix must have 2 cols. " +
        std::to_string(matrix.cols()) + "-cols matrix is given.");
  }
  PhaseSpacePoint point(matrix.rows());
  for (Eigen::Index i = 0; i < matrix.rows(); i++) {
    point[i].X(matrix(i, 0));
    point[i].P(matrix(i, 1));
  }
  return point;
}
} // namespace utilities
} // namespace mce
