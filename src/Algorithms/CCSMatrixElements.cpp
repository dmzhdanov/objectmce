#include "stdafx.h"

#include <cmath>

#include "Algorithms/CCSMatrixElements.h"
#include "Algorithms/Utilities.h"
#include "Commons/QI_Error.h"

namespace mce {
namespace math {

std::complex<double>
CCSEigenvalue(const CanonicPair &ket,
              const double gaussian_quadratic_coefficient_ket) {
  const double sqrt_a = std::sqrt(gaussian_quadratic_coefficient_ket);
  return sqrt_a * ket.X() + I * ket.P() / (2 * kHBar * sqrt_a);
}

std::complex<double> CCSOverlap(const CanonicPair &bra, const CanonicPair ket,
                                const double gaussian_quadratic_coefficient_bra,
                                const double gaussian_quadratic_coefficient_ket,
                                std::complex<double> optional_exponent) {
  const double &a1 = gaussian_quadratic_coefficient_bra;
  const double &a2 = gaussian_quadratic_coefficient_ket;
  auto a = a1 + a2;
  auto dX = bra.X() - ket.X();
  auto dP = bra.P() - ket.P();
  auto exponent = -(-4. * I * dX * (a1 * ket.P() + a2 * bra.P()) / kHBar +
                    dP * dP / (kHBar * kHBar) + 4 * a1 * a2 * dX * dX) /
                      (4. * a) +
                  optional_exponent;
  return std::sqrt(2. / a) * std::pow(a1 * a2, 0.25) * std::exp(exponent);
}

// calculates overlap matrix element $\scpr{bra}{ket}$ assuming that the
// gaussian widths given by 3rd and 4th parameters
std::complex<double> CCSOverlap(
    const std::complex<double> &z_bra_conj, const std::complex<double> &z_ket,
    const double gaussian_quadratic_coefficient_bra,
    const double gaussian_quadratic_coefficient_ket,
    std::complex<double> optional_exponent /*for regularization purposes*/) {
  const double &a1 = gaussian_quadratic_coefficient_bra;
  const double &a2 = gaussian_quadratic_coefficient_ket;
  auto a = a1 + a2;
  auto d = 2 * std::sqrt(a1 * a2) / a;
  auto &z1$ = z_bra_conj;
  auto &z2 = z_ket;
  auto exponent = (-z1$.imag() * z1$.imag() - z2.imag() * z2.imag()) -
                  (a1 * z1$ * z1$ + a2 * z2 * z2) / a + d * z1$ * z2;
  return std::sqrt(d) * std::exp(exponent + optional_exponent);
}

std::tuple<std::complex<double>, std::complex<double>>
CCSGaussianMultiplyInPlace2(const VectorXc_r &exp_taylor_coefficients,
                            CanonicPair &state,
                            double &ket_quadratic_coefficient) {
  if (exp_taylor_coefficients.size() == 0) {
    return std::tuple<std::complex<double>, std::complex<double>>(1., 0);
  }
  CanonicPair output;
  double output_gaussian_width;
  std::complex<double> multiplier, exponent;

  if (exp_taylor_coefficients.size() > 3) {
    QI_Error::RaiseError("[ERROR] math::CCSGaussianMultiply - Taylor "
                         "coefficients of order>2 are not supported.");
  }
  if (exp_taylor_coefficients.size() == 3 &&
      std::abs(exp_taylor_coefficients[2].imag()) > kTolerance) {
    QI_Error::RaiseError(
        "[ERROR] math::CCSGaussianMultiply - complex quadratic Taylor "
        "coefficient in exponent is not supported.");
  }

  double b1i =
      (exp_taylor_coefficients.size() < 2 ? 0.
                                          : exp_taylor_coefficients[1].imag());
  double b1r =
      (exp_taylor_coefficients.size() < 2 ? 0.
                                          : exp_taylor_coefficients[1].real());
  double b2 =
      (exp_taylor_coefficients.size() < 3 ? 0.
                                          : exp_taylor_coefficients[2].real());
  double &a = ket_quadratic_coefficient;

  output.P(b1i * kHBar + state.P());
  output.X((b1r + 2. * a * state.X()) / (2. * (a - b2)));
  output_gaussian_width = a - b2;

  double &a2 = output_gaussian_width;

  multiplier = std::pow(a / a2, 0.25);
  exponent = (output.X() * (b1r / 2. + I * b1i) +
              (b1r / 2 + b2 * state.X()) *
                  (a * state.X() + I * state.P() / kHBar) / a2) +
             exp_taylor_coefficients(0, 0);

  state = output;
  ket_quadratic_coefficient = output_gaussian_width;
  return std::tuple(multiplier, exponent);
};

std::complex<double>
CCSGaussianMultiplyInPlace(const VectorXc_r &exp_taylor_coefficients,
                           CanonicPair &state,
                           double &gaussian_quadratic_coefficient) {
  auto tuple = CCSGaussianMultiplyInPlace2(exp_taylor_coefficients, state,
                                           gaussian_quadratic_coefficient);
  auto &multiplier = std::get<0>(tuple);
  auto &exponent = std::get<1>(tuple);
  return multiplier * std::exp(exponent);
};

MatrixXc_r_sp
GetCreationAnnihilationMatrix(const MatrixXc_r &taylor_coefficients,
                              const double gaussian_quadratic_coefficient_bra,
                              const double gaussian_quadratic_coefficient_ket) {

  if (taylor_coefficients.size() == 0) {
    return MatrixXc_r_sp();
  }
  size_t dim = taylor_coefficients.cols() + taylor_coefficients.rows() - 1;
  MatrixXc_r output(dim, dim);
  output.setZero();

  auto &a1 = gaussian_quadratic_coefficient_bra;
  auto &a2 = gaussian_quadratic_coefficient_ket;
  auto a = a1 + a2;

  auto D = utilities::DiagonalMatrix(-1, dim);
  auto Dtr = utilities::DiagonalMatrix(1, dim);
  auto R = utilities::RangeMatrix(-1, dim);

  auto Pleft = 2. * I * kHBar * (std::sqrt(a1) * a2 / a * D);
  auto Pright = I * kHBar * std::sqrt(a2) * (R - 2. * a1 / a * Dtr);

  auto Xleft = std::sqrt(a1) / a * D;
  auto Xright = (R + 2. * a2 / a * Dtr) / (2. * std::sqrt(a2));

  MatrixXc_r p_powers(dim, dim);
  MatrixXc_r px_powers(dim, dim);
  for (Eigen::Index i = 0; i < taylor_coefficients.rows(); i++) {
    if (i == 0) {
      p_powers.setZero();
      p_powers(0, 0) = 1;
    } else {
      p_powers = (Pleft * p_powers + p_powers * Pright).eval();
    }
    for (Eigen::Index j = 0; j < taylor_coefficients.cols(); j++) {
      if (j == 0) {
        px_powers = p_powers;
      } else {
        px_powers = (Xleft * px_powers + px_powers * Xright).eval();
      }
      output += taylor_coefficients(i, j) * px_powers;
    }
  }
  // removing zero columns at the end
  utilities::ChopToFitNonZeros(output);

  // size_t rows = dim;
  // while (rows > 0 && output.row(rows - 1).cwiseAbs().maxCoeff() < kTolerance)
  //  rows--;
  // size_t cols = dim;
  // while (cols > 0 && output.col(cols - 1).cwiseAbs().maxCoeff() < kTolerance)
  //  cols--;
  // return output.block(0, 0, rows, cols).sparseView(1., kTolerance);

  return output.sparseView(1., kTolerance);
}

std::complex<double>
GetGaussianMatrixElement(const CanonicPair &center_bra, double alpha_bra,
                         const CanonicPair &center_ket, double alpha_ket,
                         const MatrixXc_r &taylor_coefficients,
                         const VectorXc_r &exp_taylor_coefficients) {
  // auto effective_ket_quadratic_coefficient =
  //    alpha_ket - (exp_taylor_coefficients.size() < 3
  //                     ? 0.
  //                     : exp_taylor_coefficients[2].real());

  CanonicPair effective_ket_center = center_ket;
  double effective_ket_quadratic_coefficient = alpha_ket;
  auto ket_multiplier_tuple = math::CCSGaussianMultiplyInPlace2(
      exp_taylor_coefficients, effective_ket_center,
      effective_ket_quadratic_coefficient);

  auto creation_annihilation_matrix = math::GetCreationAnnihilationMatrix(
      taylor_coefficients, alpha_bra,
      effective_ket_quadratic_coefficient); // ???!!! tricky place: difference
                                            // can be zero or even negative even
                                            // though the matrix elements are
                                            // non-inifinite are well-defined!
                                            // Think about better algorithm...

  if (creation_annihilation_matrix.size() == 0) {
    return 0;
  }

  VectorXc_r bra_z_vector =
      utilities::PowerSeries(
          creation_annihilation_matrix.cols(),
          std::conj(math::CCSEigenvalue(center_bra, alpha_bra)))
          .transpose();

  VectorXc_c ket_z_vector = utilities::PowerSeries(
      creation_annihilation_matrix.rows(),
      math::CCSEigenvalue(effective_ket_center,
                          effective_ket_quadratic_coefficient));

  auto effective_overlap = math::CCSOverlap(center_bra, //
                                            effective_ket_center, alpha_bra,
                                            effective_ket_quadratic_coefficient,
                                            std::get<1>(ket_multiplier_tuple));

  auto return_value =
      std::get<0>(ket_multiplier_tuple) * effective_overlap *
      ((bra_z_vector) * (creation_annihilation_matrix * (ket_z_vector)))(0, 0);

  return return_value;
} // namespace math

} // namespace math
} // namespace mce
