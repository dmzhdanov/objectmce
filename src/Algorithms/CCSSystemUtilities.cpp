#include "stdafx.h"

#include "Algorithms/CCSSystemUtilities.h"

namespace mce {
namespace ccs_system_utilities {

DerivativeCreator::DerivativeCreator(
    const OperatorType &observable,
    const OperatorFactory<PolicyType> &operator_factory)
    : observable_(observable), operator_factory_(operator_factory){};

void DerivativeCreator::InitializeXTimesObservable(
    const IConfiguration &configuration) {
  for (size_t i = x_times_observable_.size();
       i < configuration.NumberOfSpatialDOFs(); i++) {
    x_times_observable_.push_back(operator_factory_.CreateX(i) * observable_);
  }
};

void DerivativeCreator::InitializeObservableTimesX(
    const IConfiguration &configuration) {
  for (size_t i = observable_times_x_.size();
       i < configuration.NumberOfSpatialDOFs(); i++) {
    observable_times_x_.push_back(observable_ * operator_factory_.CreateX(i));
  }
};

DerivativeEvaluator::DerivativeEvaluator(
    std::shared_ptr<DerivativeCreator> creator, const BracketPair &bracket_pair)
    : creator_(creator), bracket_pair_(bracket_pair),
      observable_(std::make_shared<EvaluatorType>(
          creator->observable_.Evaluate(bracket_pair))){};

void DerivativeEvaluator::InitializeXTimesObservable() {
  const auto &configuration = bracket_pair_.Bra().Configuration();
  creator_->InitializeXTimesObservable(configuration);
  for (size_t i = 0; i < configuration.NumberOfSpatialDOFs(); i++) {
    x_times_observable_.push_back(std::make_shared<EvaluatorType>(
        creator_->x_times_observable_[i].Evaluate(bracket_pair_)));
  }
};

void DerivativeEvaluator::InitializeObservableTimesX() {
  const auto &configuration = bracket_pair_.Bra().Configuration();
  creator_->InitializeObservableTimesX(configuration);
  for (size_t i = 0; i < configuration.NumberOfSpatialDOFs(); i++) {
    observable_times_x_.push_back(std::make_shared<EvaluatorType>(
        creator_->observable_times_x_[i].Evaluate(bracket_pair_)));
  }
};

std::complex<double> DerivativeEvaluator::operator()(size_t bra_idx,
                                                     size_t ket_idx) {
  return (*observable_)(bra_idx, ket_idx);
};

std::complex<double> DerivativeEvaluator::DiffCenterX(SpatialDOF_id dof_id,
                                                      size_t ket_idx) {
  if (!bracket_pair_.IsSymmetric()) {
    QI_Error::RaiseError("[ERROR] DerivativeEvaluator::dX - works only "
                         "for symmetric brackets.");
  }
  InitializeObservableTimesX();
  InitializeXTimesObservable();
  const auto &alpha =
      bracket_pair_.Ket().Configuration().GaussianQuadraticCoefficient(dof_id);
  const auto &canonic = bracket_pair_.Ket()[ket_idx]->GetCenter(dof_id);
  return ((*(observable_times_x_[dof_id]))(ket_idx, ket_idx) +
          (*(x_times_observable_[dof_id]))(ket_idx, ket_idx)) *
             2. * alpha -
         (*observable_)(ket_idx, ket_idx) *
             (4 * alpha *
              canonic.X()); // ???!!! strongly requires optimizaion (store and
                            // evaluate operator observable_times_x_ +
                            // x_times_observable_ instead of two evaluators)
};

std::complex<double> DerivativeEvaluator::DiffCenterP(SpatialDOF_id dof_id,
                                                      size_t ket_idx) {
  if (!bracket_pair_.IsSymmetric()) {
    QI_Error::RaiseError("[ERROR] DerivativeEvaluator::dX - works only "
                         "for symmetric brackets.");
  }
  InitializeObservableTimesX();
  InitializeXTimesObservable();
  return ((*(observable_times_x_[dof_id]))(ket_idx, ket_idx) -
          (*(x_times_observable_[dof_id]))(ket_idx, ket_idx)) *
         I / kHBar; // ???!!! strongly requires optimizaion (store and evaluate
                    // operator observable_times_x_ - x_times_observable_
                    // instead of two evaluators)
};

std::complex<double> DerivativeEvaluator::DiffKetCenterX(SpatialDOF_id dof_id,
                                                         size_t bra_idx,
                                                         size_t ket_idx) {
  InitializeObservableTimesX();
  const auto &alpha =
      bracket_pair_.Ket().Configuration().GaussianQuadraticCoefficient(dof_id);
  const auto &canonic = bracket_pair_.Ket()[ket_idx]->GetCenter(dof_id);
  return (*(observable_times_x_[dof_id]))(bra_idx, ket_idx) * 2. * alpha -
         (*observable_)(bra_idx, ket_idx) *
             (2 * alpha * canonic.X() + I * canonic.P() / kHBar);
};

std::complex<double> DerivativeEvaluator::DiffKetCenterP(SpatialDOF_id dof_id,
                                                         size_t bra_idx,
                                                         size_t ket_idx) {
  InitializeObservableTimesX();
  const auto &canonic = bracket_pair_.Ket()[ket_idx]->GetCenter(dof_id);
  return ((*(observable_times_x_[dof_id]))(bra_idx, ket_idx) -
          (*observable_)(bra_idx, ket_idx) * canonic.X()) *
         I / kHBar;
};

Derivative::Derivative(const OperatorType &observable,
                       const OperatorFactory<PolicyType> &operator_factory)
    : creator_(
          std::make_shared<DerivativeCreator>(observable, operator_factory)){};

DerivativeEvaluator Derivative::Evaluate(const BracketPair &bracket_pair) {
  return DerivativeEvaluator(creator_, bracket_pair);
}

std::complex<double> WavefunctionDerivativeSelfProjection(
    const Wavefunction &state, const Wavefunction &der_t_state,
    OperatorFactory<PolicyType> operator_factory_) {
  using namespace ccs_system_utilities;
  auto Id_der = Derivative(operator_factory_.CreateId(0), operator_factory_)
                    .Evaluate({state});
  auto number_of_spatial_dofs = state.Configuration().NumberOfSpatialDOFs();

  std::complex<double> result = 0;

  for (size_t m = 0; m < state.Size(); m++) {
    for (size_t n = 0; n < state.Size(); n++) {
      std::complex<double> pder_t_ampl = 0;
      for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
        pder_t_ampl += der_t_state[n]->GetCenter(dof_id).X() *
                           Id_der.DiffKetCenterX(dof_id, m, n) +
                       der_t_state[n]->GetCenter(dof_id).P() *
                           Id_der.DiffKetCenterP(dof_id, m, n);
      }
      pder_t_ampl +=
          I / kHBar * der_t_state[n]->GetMultiplier().Phase() * Id_der(m, n);

      result +=
          std::conj(state[m]->GetMultiplier().Value()) *
          (state[n]->GetMultiplier().Value() * (pder_t_ampl) +
           Id_der(m, n) *
               std::exp(I * (state[n]->GetMultiplier().Phase())) *
               der_t_state[n]->GetMultiplier().Amplitude());
    }
  }
  return result;
}

std::complex<double> Covariance(const OperatorType &left_operator, const OperatorType &right_operator, const OperatorType &identity_operator, const BracketPair &bracket_pair, bool normalize)
{
	std::complex<double> overlap = identity_operator.Evaluate(bracket_pair).Mean();
	OperatorType product_operator = left_operator * right_operator;
	return (product_operator.Evaluate(bracket_pair).Mean()*overlap - left_operator.Evaluate(bracket_pair).Mean()*right_operator.Evaluate(bracket_pair).Mean())/(normalize ? overlap*overlap : std::pow(overlap/std::abs(overlap),2));
}

} // namespace ccs_system_utilities
} // namespace mce
