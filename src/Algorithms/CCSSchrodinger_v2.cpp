#include "stdafx.h"

#include <Eigen/Eigenvalues>

#include "Algorithms/CCSSchrodinger_v2.h"
#include "Algorithms/CCSSystemUtilities.h"

namespace mce {
namespace algorithm {

CCSSchrodingerSystem_v2::CCSSchrodingerSystem_v2(
    const Operator<PolicyType> &hamiltonian,
    const Operator<PolicyType> &linear_solve_kernel,
    OperatorFactory<PolicyType> &operator_factory)
    : hamiltonian_(hamiltonian), linear_solve_kernel_(linear_solve_kernel),
      operator_factory_(operator_factory), amplitude_max_regularizer_(1.e-6){};

void CCSSchrodingerSystem_v2::operator()(const Wavefunction &state,
                                        Wavefunction &der_t_state,
                                        const double t) const {

  size_t max_retries = 20;
  VectorXd_c coeffs; // amplitude coefficients to be calculated;
  double linear_solve_error;
  amplitude_max_regularizer_ /=
      (amplitude_max_regularizer_ > 1.e-7 ? 4. : 2.); // mutable variable
  size_t attempts_count = 0;
  double state_norm =
      operator_factory_.CreateId(0).Evaluate({state}).Mean().real();

  auto P = [](const Wavefunction &state, size_t id, SpatialDOF_id dof_id) {
    return state[id]->GetCenter(dof_id).P();
  };

  auto X = [](const Wavefunction &state, size_t id, SpatialDOF_id dof_id) {
    return state[id]->GetCenter(dof_id).X();
  };

  auto Phase = [](const Wavefunction &state, size_t id) {
    return state[id]->GetMultiplier().Phase() +
           std::arg(state[id]->GetMultiplier().Amplitude());
  };

  auto Amplitude = [](const Wavefunction &state, size_t id) {
    return std::abs(state[id]->GetMultiplier().Amplitude());
  };

  auto CplxAmplitude = [](const Wavefunction &state, size_t id) {
    return state[id]->GetMultiplier().Amplitude();
  };

  auto SlowPhase = [](const Wavefunction &state, size_t id) {
    return std::arg(state[id]->GetMultiplier().Amplitude());
  };

  auto FastPhase = [](const Wavefunction &state, size_t id) {
    return state[id]->GetMultiplier().Phase();
  };

  auto alpha = [&state](SpatialDOF_id dof_id) {
    return state.Configuration().GaussianQuadraticCoefficient(dof_id);
  };

  der_t_state.SetZero();

  auto eta_ket_op = [&state, &P, &X, &alpha, &der_t_state,
                     this](size_t j) -> Operator<PolicyType> {
    auto output = hamiltonian_ * (-I / kHBar);
    for (size_t dof_id_j = 0;
         dof_id_j < state.Configuration().NumberOfSpatialDOFs(); dof_id_j++) {
      output += (I / kHBar) * P(state, j, dof_id_j) *
                X(der_t_state, j, dof_id_j) *
                operator_factory_.CreateId(dof_id_j);
      output -= 2. * X(der_t_state, j, dof_id_j) * alpha(dof_id_j) *
                (operator_factory_.CreateX(dof_id_j) -
                 X(state, j, dof_id_j) * operator_factory_.CreateId(dof_id_j));
      output -= P(der_t_state, j, dof_id_j) *
                (operator_factory_.CreateX(dof_id_j) -
                 X(state, j, dof_id_j) * operator_factory_.CreateId(dof_id_j)) *
                (I / kHBar);
    }
    return output;
  };

  do {
    {
      int i = 0;
      while (i++ < attempts_count) {
        std::cout << "*";
      }
    }
    amplitude_max_regularizer_ *= 2.; // mutable variable
    attempts_count++;
    if (der_t_state.IsNull()) {
      der_t_state = state.Clone(false);
    }
    der_t_state.SetZero();

    using namespace ccs_system_utilities;
    auto H_der = Derivative(hamiltonian_, operator_factory_).Evaluate({state});
    auto number_of_spatial_dofs = state.Configuration().NumberOfSpatialDOFs();

    // calculating centers shifts dP/dt and dX/dt according to Hamilton
    // equations and the phase time derivative "as is normally done" (c) D.V.
    // Makhov et al. / Chemical Physics 493 (2017) 200�218

    for (size_t n = 0; n < state.Size(); n++) {
      double der_t_phase = 0;
      for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
        der_t_state[n]->GetCenter(dof_id).P(
            -H_der.DiffCenterX(dof_id, n)
                 .real()); // ???!!! taking real part! double-check in tests
                           // that result is real
        der_t_state[n]->GetCenter(dof_id).X(
            H_der.DiffCenterP(dof_id, n)
                .real()); // ???!!! taking real part! double-check in tests that
                          // result is real
        der_t_phase += state[n]->GetCenter(dof_id).P() *
                       der_t_state[n]->GetCenter(dof_id).X();
      }
      der_t_state[n]->GetMultiplier().Phase(der_t_phase - H_der(n, n).real());
    }
    // updating multiplier

    // ???!!! Unfortunately, self adjoint Eigen solver seems to not work with
    // row-major matrices
    // Eigen::MatrixXcd

    MatrixXd_r A(state.Size() * (2), state.Size() * (2));
    VectorXd_c b(state.Size() * (2));

    // Finding the remaining amplitude variations as a solution of linear system
    // A.x=b initializing A
    auto A11 = A.block(0, 0, state.Size(), state.Size());
    auto A33 = A.block(state.Size(), state.Size(), state.Size(), state.Size());
    auto A13 = A.block(0, state.Size(), state.Size(), state.Size());

    auto A_evaluator = linear_solve_kernel_.Evaluate({state});
    for (size_t i = 0, state_size = state.Size(); i < state_size; ++i) {
      for (size_t j = 0; j < state_size; ++j) {
        std::complex<double> chi_ij =
            A_evaluator(i, j) *
            std::exp(I * (Phase(state, j) - Phase(state, i)));
        A11(i, j) = chi_ij.real();
        A33(i, j) = chi_ij.real() * Amplitude(state, i) * Amplitude(state, j);
        A13(i, j) = (I * chi_ij).real() * Amplitude(state, j);
      }
      A11(i, i) += amplitude_max_regularizer_;
      A33(i, i) += amplitude_max_regularizer_;
    }
    A.block(state.Size(), 0, state.Size(), state.Size()) = A13.transpose();

    //std::cout << "\n!\n" << A << "\n!\n";

    // initializing b
    auto b1 = b.segment(0, state.Size());
    auto b3 = b.segment(state.Size(), state.Size());

    auto Id_der =
        Derivative(linear_solve_kernel_, operator_factory_).Evaluate({state});
    auto H = (linear_solve_kernel_ * hamiltonian_)
                 .Evaluate({state}); // ???!!! Optimize by defining the
                                     // corresponding class variable!

    auto chi_i_eta_j = [&](size_t i, size_t j) {
      auto result = -I / kHBar * H(i, j);
      for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
        result -=
            P(der_t_state, j, dof_id) * Id_der.DiffKetCenterP(dof_id, i, j);
        result -=
            X(der_t_state, j, dof_id) * Id_der.DiffKetCenterX(dof_id, i, j);
      }
      // return result;
      // auto result2 = eta_ket_op(j).Evaluate({state})(i,j);
	  //std::cout << "\n*** " << result; << " | " << result2;
      return result;
    };

    for (size_t m = 0, state_size = state.Size(); m < state_size; m++) {
      b1(m) = 0;
      b3(m) = 0;
      for (size_t n = 0; n < state_size; n++) {
        std::complex<double> b_stub =
            std::exp(-I * (Phase(state, m) - Phase(state, n))) *
            (I * Id_der(m, n) * FastPhase(der_t_state, n) - chi_i_eta_j(m, n));
        b1(m) -= Amplitude(state, n) * b_stub.real();
        b3(m) -=
            Amplitude(state, m) * Amplitude(state, n) * (-I * b_stub).real();
      }
    }

    //std::cout << "\n!b:\n" << b << "\n!\n";

    using namespace Eigen;
    coeffs = // A.llt().solve(b);// Doesn't work in most cases.
        A.colPivHouseholderQr().solve(b);
    VectorXc_c v = (A * coeffs - b);
    linear_solve_error = std::abs((v.transpose() * v)(0, 0));
  } while ((linear_solve_error > kTolerance) && (attempts_count < max_retries));

  if (attempts_count >= max_retries) {
    std::cout << "\n[INFO] Max retries limit of " << max_retries
              << " exceeded. Linear solve error is still " << linear_solve_error
              << ". Continuing anyway...\n";
  };

  const auto &d_ampl = coeffs.segment(0, state.Size());
  const auto &d_slow_phase = coeffs.segment(state.Size(), state.Size());

  for (size_t m = 0; m < state.Size(); m++) {
    der_t_state[m]->GetMultiplier().Amplitude(
        I * d_slow_phase(m) * CplxAmplitude(state, m) +
        d_ampl(m) * std::exp(I * SlowPhase(state, m)));
  }

  auto self_projection =
      std::abs(ccs_system_utilities::WavefunctionDerivativeSelfProjection(
                   state, der_t_state, operator_factory_)
                   .real());
  if (self_projection > 1.e-5) {
    std::cout << "\n Look at this beautiful self-projection: "
              << self_projection;
  }
}

} // namespace algorithm
} // namespace mce
