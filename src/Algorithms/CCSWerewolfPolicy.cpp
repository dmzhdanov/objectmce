#include "stdafx.h"

#include "Algorithms/CCSWerewolfPolicy.h"
#include "Algorithms/CCSPilotWaveUtilities.h"
#include "Algorithms/CCSSystemUtilities.h"

namespace mce {
namespace algorithm {

CCSWerewolfPolicy::CCSWerewolfPolicy(const CCSSchrodingerWerewolfSystem &system)
    : to_garashchuchishche_threshold_(0.25 /*kTolerance*/),
      hamiltonian_(system.hamiltonian_),
      operator_factory_(system.operator_factory_),
      smoothing_scale_(system.smoothing_scale_) {}

CCSWerewolfPolicy::~CCSWerewolfPolicy(){};

CCSWerewolfPolicy &
CCSWerewolfPolicy::SetToGarashchuchishcheThreshold(double threshold) {
  to_garashchuchishche_threshold_ = threshold;
  return *this;
};

void CCSWerewolfPolicy::operator()(const Wavefunction &state, const double t) {
  auto H_der = ccs_system_utilities::Derivative(hamiltonian_, operator_factory_)
                   .Evaluate({state});
  for (size_t n = 0; n < state.Size(); n++) {
    auto number_of_spatial_dofs = state.Configuration().NumberOfSpatialDOFs();
    auto smoothing_kernel_op = ccs_system_utilities::GetPilotSmoothingKernel(
        state, n, operator_factory_, smoothing_scale_);
    for (SpatialDOF_id dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
      double momentum_classical = state[n]->GetCenter(dof_id).P();
      double momentum_pilot = ccs_system_utilities::GetPilotWaveMomentum(
          state, n, operator_factory_.CreateP(dof_id),
          smoothing_kernel_op).real();
      bool become_garashchuchishche = abs(momentum_pilot - momentum_classical)
                                      /**(momentum_pilot > 0 ? 1 : -1)*/
                                      > to_garashchuchishche_threshold_;
      state[n]->Nature(become_garashchuchishche
                           ? BasisFunctionNature::Garashchuchishche
                           : BasisFunctionNature::Garahardtishche);
    }
  }
};

} // namespace algorithm
} // namespace mce
