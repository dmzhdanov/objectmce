#include "stdafx.h"

#include "Algorithms/QuasiprobabilityGaugeAuxiliaries.h"

#include "Commons/Faddeeva.hh"
#include <boost/math/constants/constants.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <math.h>

namespace mce {
namespace quasiprobability_gauge_auxiliaries {

std::complex<double>
HusimiFunctionMatrixElement(const CanonicPair &phase_space_point,
                            const CanonicPair &bra, const CanonicPair &ket,
                            const CanonicPair
                                &kernel_gaussian_quadratic_coefficients_ab,
                            double wavefunction_gaussian_quadratic_coefficient /*must be the same for bra and ket*/) {
  using Complex = std::complex<double>;
  auto X1 = bra.X();
  auto P1 = bra.P();
  auto X2 = ket.X();
  auto P2 = ket.P();
  auto X0 = phase_space_point.X();
  auto P0 = phase_space_point.P();
  auto a = kernel_gaussian_quadratic_coefficients_ab.X();
  auto b = kernel_gaussian_quadratic_coefficients_ab.P();
  auto &alpha = wavefunction_gaussian_quadratic_coefficient;
  double pi = boost::math::constants::pi<double>();
  auto X12 = (X1 + X2 - (I * (P1 - P2)) / (2. * alpha * kHBar)) / 2.;
  auto P12 = (P1 + P2 + 2. * I * (X1 - X2) * alpha * kHBar) / 2.;

  Complex result =
      ((std::sqrt(2) *
        std::exp((-2. * a * std::pow(-X12 + X0, 2) * alpha) / (a + 2 * alpha) +
                 std::pow(P12, 2) / (2. * alpha * std::pow(kHBar, 2)) -
                 (std::pow(P1, 2) + std::pow(P2, 2)) /
                     (4. * alpha * std::pow(kHBar, 2)) -
                 (b * std::pow(-P12 + P0, 2)) /
                     (1 + 2. * b * alpha * std::pow(kHBar, 2))) *
        std::sqrt(
            (a * b * alpha) /
            ((a + 2. * alpha) * (1 + 2. * b * alpha * std::pow(kHBar, 2))))) /
       pi);

  return result;
}

std::complex<double>
HusimiFunctionXDerivativeMatrixElement(const CanonicPair &phase_space_point,
                                       const CanonicPair &bra,
                                       const CanonicPair &ket,
                                       const CanonicPair &
                                           kernel_gaussian_quadratic_coefficients_ab,
                                       double
                                           wavefunction_gaussian_quadratic_coefficient /*must be the same for bra and ket*/) {
  using Complex = std::complex<double>;
  auto X1 = bra.X();
  auto P1 = bra.P();
  auto X2 = ket.X();
  auto P2 = ket.P();
  auto X0 = phase_space_point.X();
  auto P0 = phase_space_point.P();
  auto a = kernel_gaussian_quadratic_coefficients_ab.X();
  auto b = kernel_gaussian_quadratic_coefficients_ab.P();
  auto &alpha = wavefunction_gaussian_quadratic_coefficient;
  double pi = boost::math::constants::pi<double>();
  auto X12 = (X1 + X2 - (I * (P1 - P2)) / (2. * alpha * kHBar)) / 2.;
  auto P12 = (P1 + P2 + 2. * I * (X1 - X2) * alpha * kHBar) / 2.;

  return ((-4 * a * (-X12 + X0) * alpha) / (a + 2 * alpha)) *
         HusimiFunctionMatrixElement(
             phase_space_point, bra, ket,
             kernel_gaussian_quadratic_coefficients_ab,
             wavefunction_gaussian_quadratic_coefficient);
}

std::complex<double>
HusimiFunctionPDerivativeMatrixElement(const CanonicPair &phase_space_point,
                                       const CanonicPair &bra,
                                       const CanonicPair &ket,
                                       const CanonicPair &
                                           kernel_gaussian_quadratic_coefficients_ab,
                                       double
                                           wavefunction_gaussian_quadratic_coefficient /*must be the same for bra and ket*/) {
  using Complex = std::complex<double>;
  auto X1 = bra.X();
  auto P1 = bra.P();
  auto X2 = ket.X();
  auto P2 = ket.P();
  auto X0 = phase_space_point.X();
  auto P0 = phase_space_point.P();
  auto a = kernel_gaussian_quadratic_coefficients_ab.X();
  auto b = kernel_gaussian_quadratic_coefficients_ab.P();
  auto &alpha = wavefunction_gaussian_quadratic_coefficient;
  double pi = boost::math::constants::pi<double>();
  auto X12 = (X1 + X2 - (I * (P1 - P2)) / (2. * alpha * kHBar)) / 2.;
  auto P12 = (P1 + P2 + 2. * I * (X1 - X2) * alpha * kHBar) / 2.;

  return ((-2 * b * (-P12 + P0)) / (1 + 2 * b * alpha * std::pow(kHBar, 2))) *
         HusimiFunctionMatrixElement(
             phase_space_point, bra, ket,
             kernel_gaussian_quadratic_coefficients_ab,
             wavefunction_gaussian_quadratic_coefficient);
}

std::complex<double> JBG2FunctionMatrixElement(
    const CanonicPair &phase_space_point, const CanonicPair &bra,
    const CanonicPair &ket,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab,
    double wavefunction_gaussian_quadratic_coefficient /*must be the same for
                                                          bra and ket*/
    ,
    unsigned int approximation_order /*default -1 -exact*/) {
  auto X1 = bra.X();
  auto P1 = bra.P();
  auto X2 = ket.X();
  auto P2 = ket.P();
  // auto X0 = phase_space_point.X();
  auto P0 = phase_space_point.P();
  // auto a = kernel_gaussian_quadratic_coefficients_ab.X();
  auto b = kernel_gaussian_quadratic_coefficients_ab.P();
  auto &alpha = wavefunction_gaussian_quadratic_coefficient;
  double pi = boost::math::constants::pi<double>();
  auto P12r = P0 - (P1 + P2) / 2.;
  auto P12c = I * (-X1 + X2) * alpha * kHBar;
  auto P12 = (P1 + P2 + 2. * I * (X1 - X2) * alpha * kHBar) / 2.;
  auto scale = std::sqrt(b / (1 + 2 * b * alpha * std::pow(kHBar, 2)));
  // auto Erf = [](std::complex<double> arg) -> std::complex<double> {
  //  return Faddeeva::erf(arg);
  //};
  auto ErfNorm = [](std::complex<double> arg) -> std::complex<double> {
    return -1. + Faddeeva::erf(arg);
  };
  auto IncompleteGamma = [](unsigned int a, double z) -> double {
    return boost::math::tgamma(a, z);
  };
  auto Factorial = [](unsigned int a) -> std::complex<double> {
    return boost::math::unchecked_factorial<double>(a);
  };

  if ((approximation_order == -1) || (approximation_order == 0)) {
    return -(std::exp(std::pow(scale, 2) * std::pow(-P12 + P0, 2)) *
             std::sqrt(pi) * ErfNorm(scale * (P12 - P0))) /
           (2. * scale);
  } else if (approximation_order == 1) {
    return -(std::exp(P12c * (P12c + 2 * P12r) * std::pow(scale, 2)) *
             std::sqrt(pi) * (1 + std::pow(P12r, 2) * std::pow(scale, 2)) *
             ErfNorm((-P12c - P12r) * scale)) /
           (2. * scale);
  } else {
    auto &order = approximation_order;
    auto z = P12r + P12c;
    return (std::exp(std::pow(scale, 2) * std::pow(z, 2)) * std::sqrt(pi) *
            (-ErfNorm(-scale * z)) *
            IncompleteGamma(1 + order,
                            -(std::pow(P12c, 2).real() * std::pow(scale, 2)))) /
           (2. * scale * Factorial(order));
    // return -(std::exp(std::pow(P12r, 2) * std::pow(scale, 2) +
    //                  P12c * (P12c + 2 * P12r) * std::pow(scale, 2)) *
    //         std::sqrt(pi) * Erf((-P12c - P12r) * scale) *
    //         IncompleteGamma(1 + order,
    //                         std::pow(P12r, 2) * std::pow(scale, 2))) /
    //       (2. * scale * Factorial(order));
  }
};

std::complex<double> JBG2FunctionPDerivativeMatrixElement(
    const CanonicPair &phase_space_point, const CanonicPair &bra,
    const CanonicPair &ket,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab,
    double wavefunction_gaussian_quadratic_coefficient /*must be the same for
                                                          bra and ket*/
    ,
    unsigned int approximation_order /*default -1 -exact*/) {

  auto X1 = bra.X();
  auto P1 = bra.P();
  auto X2 = ket.X();
  auto P2 = ket.P();
  // auto X0 = phase_space_point.X();
  auto P0 = phase_space_point.P();
  // auto a = kernel_gaussian_quadratic_coefficients_ab.X();
  auto b = kernel_gaussian_quadratic_coefficients_ab.P();
  auto &alpha = wavefunction_gaussian_quadratic_coefficient;
  double pi = boost::math::constants::pi<double>();
  auto P12r = P0 - (P1 + P2) / 2.;
  auto P12c = I * (-X1 + X2) * alpha * kHBar;
  auto P12 = (P1 + P2 + 2. * I * (X1 - X2) * alpha * kHBar) / 2.;
  auto scale = std::sqrt(b / (1 + 2 * b * alpha * std::pow(kHBar, 2)));
  // auto Erf = [](std::complex<double> arg) -> std::complex<double> {
  //  return Faddeeva::erf(arg);
  //};
  auto ErfNorm = [](std::complex<double> arg) -> std::complex<double> {
    return -1. + Faddeeva::erf(arg);
  };
  auto IncompleteGamma = [](unsigned int a, double z) -> double {
    return boost::math::tgamma(a, z);
  };
  auto Factorial = [](unsigned int a) -> std::complex<double> {
    return boost::math::unchecked_factorial<double>(a);
  };

  if ((approximation_order == -1) || (approximation_order == 0)) {
    return 1. + std::exp(std::pow(scale, 2) * std::pow(P12 - P0, 2)) *
                    std::sqrt(pi) * scale * (P12 - P0) *
                    ErfNorm(scale * (P12 - P0));
  } else if (approximation_order == 1) {
    return (1 + std::pow(P12r, 2) * std::pow(scale, 2) +
            std::exp(std::pow(P12c + P12r, 2) * std::pow(scale, 2)) *
                std::sqrt(pi) * scale *
                (P12c + P12r + P12c * std::pow(P12r, 2) * std::pow(scale, 2)) *
                (-ErfNorm(-(P12c + P12r) * scale))) /
           std::exp(std::pow(P12r, 2) * std::pow(scale, 2));
  } else {
    auto &order = approximation_order;
    auto z = P12r + P12c;
    return ((1. + std::exp(std::pow(scale, 2) * std::pow(z, 2)) *
                      std::sqrt(pi) * scale * z * (-ErfNorm(-scale * z))) *
            IncompleteGamma(1 + order,
                            -(std::pow(P12c, 2).real() * std::pow(scale, 2)))) /
           Factorial(order);
    // return (std::pow(std::pow(P12r, 2) * std::pow(scale, 2), order) *
    //        (1. + std::exp(std::pow(P12c + P12r, 2) * std::pow(scale, 2)) *
    //                  P12c * std::sqrt(pi) * scale *
    //                  Erf((P12c + P12r) * scale))) /
    //           (std::exp(std::pow(P12r, 2) * std::pow(scale, 2)) *
    //            Factorial(order)) +
    //       ((1. + std::exp(std::pow(P12c + P12r, 2) * std::pow(scale, 2)) *
    //                  (P12c + P12r) * std::sqrt(pi) * scale *
    //                  Erf((P12c + P12r) * scale)) *
    //        IncompleteGamma(order, std::pow(P12r, 2) * std::pow(scale, 2))) /
    //           Factorial(order - 1);
  }
};

std::complex<double> GaugeMomentumFlowMatrixElement(
    double inverse_mass, double p_mean, double p_mean_x_derivative,
    const CanonicPair &phase_space_point, const CanonicPair &bra,
    const CanonicPair &ket,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab,
    double wavefunction_gaussian_quadratic_coefficient /*must be the same for
                                                          bra and ket*/
    ,
    unsigned int approximation_order) {
  using Complex = std::complex<double>;
  auto X1 = bra.X();
  auto P1 = bra.P();
  auto X2 = ket.X();
  auto P2 = ket.P();
  auto &alpha = wavefunction_gaussian_quadratic_coefficient;

  auto P12 = (P1 + P2 + 2. * I * (X1 - X2) * alpha * kHBar) / 2.;

  auto Q = HusimiFunctionMatrixElement(
      phase_space_point, bra, ket, kernel_gaussian_quadratic_coefficients_ab,
      wavefunction_gaussian_quadratic_coefficient);
  auto Q_x_der = HusimiFunctionXDerivativeMatrixElement(
      phase_space_point, bra, ket, kernel_gaussian_quadratic_coefficients_ab,
      wavefunction_gaussian_quadratic_coefficient);

  auto jbg2 = JBG2FunctionMatrixElement(
      phase_space_point, bra, ket, kernel_gaussian_quadratic_coefficients_ab,
      wavefunction_gaussian_quadratic_coefficient,approximation_order);

  // m_i_n_u_s potential x-derivative!
  return -inverse_mass *
         (p_mean_x_derivative * jbg2 * Q +
          ((p_mean - P12) * jbg2 + alpha * kHBar * kHBar) * Q_x_der);
}

std::complex<double> GaugePositionFlowMatrixElement(
    double inverse_mass, double p_mean, const CanonicPair &phase_space_point,
    const CanonicPair &bra, const CanonicPair &ket,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab,
    double wavefunction_gaussian_quadratic_coefficient /*must be the same for
                                                          bra and ket*/
    ,
    unsigned int approximation_order) {
  using Complex = std::complex<double>;
  auto X1 = bra.X();
  auto P1 = bra.P();
  auto X2 = ket.X();
  auto P2 = ket.P();
  auto &alpha = wavefunction_gaussian_quadratic_coefficient;
  auto P12 = (P1 + P2 + 2. * I * (X1 - X2) * alpha * kHBar) / 2.;

  // auto a = kernel_gaussian_quadratic_coefficients_ab.X();
  // auto b = kernel_gaussian_quadratic_coefficients_ab.P();
  // auto P0 = phase_space_point.P();

  auto Q = HusimiFunctionMatrixElement(
      phase_space_point, bra, ket, kernel_gaussian_quadratic_coefficients_ab,
      wavefunction_gaussian_quadratic_coefficient);

  if ((approximation_order == -1) || (approximation_order == 0))
  {
	  auto P0 = phase_space_point.P();
	  auto b = kernel_gaussian_quadratic_coefficients_ab.P();
	  auto non_regularized_position_flow =
		  inverse_mass *
		  (p_mean - ((P12 + 2 * b * P0 * alpha * std::pow(kHBar, 2)) /
		  (1 + 2 * b * alpha * std::pow(kHBar, 2)))) * Q;
	  return non_regularized_position_flow;
  }

  auto Q_p_der = HusimiFunctionPDerivativeMatrixElement(
	  phase_space_point, bra, ket, kernel_gaussian_quadratic_coefficients_ab,
	  wavefunction_gaussian_quadratic_coefficient);

  auto jbg2 = JBG2FunctionMatrixElement(
	  phase_space_point, bra, ket, kernel_gaussian_quadratic_coefficients_ab,
	  wavefunction_gaussian_quadratic_coefficient,approximation_order);
  auto jbg2_p_der = JBG2FunctionPDerivativeMatrixElement(
	  phase_space_point, bra, ket, kernel_gaussian_quadratic_coefficients_ab,
	  wavefunction_gaussian_quadratic_coefficient,approximation_order);

  auto general_position_flow =
      inverse_mass *
      ((p_mean - P12) * jbg2_p_der * Q +
       ((p_mean - P12) * jbg2 + alpha * kHBar * kHBar) * Q_p_der);

  // std::cout << "[" << non_regularized_position_flow << "<>" <<
  // general_position_flow << "]\n";

  return general_position_flow;
}

std::complex<double> GaugeMomentumFlowMatrixElement(
    const MatrixXc_r &taylor_coefficients,
    const MatrixXc_r &exp_taylor_coefficients, double p_mean,
    double p_mean_x_derivative, const CanonicPair &phase_space_point,
    const CanonicPair &bra,
    double alpha_bra, /*must be the same for bra and ket*/
    const CanonicPair &ket,
    double alpha_ket, /*must be the same for bra and ket*/
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab,
    unsigned int approximation_order) {
  if (taylor_coefficients.size() == 0)
    return 0;

  if ((taylor_coefficients.cols() > 1) || (taylor_coefficients.rows() > 2) ||
      (exp_taylor_coefficients.size() != 0)) {
    QI_Error::RaiseError(
        "[ERROR] "
        "GaugeMomentumFlowMatrixElement - taylor_coefficients "
        "should be (1x0...2) matrix and exp_taylor_coefficients should be "
        "empty. Actually: (" +
        std::to_string(taylor_coefficients.rows()) + "," +
        std::to_string(taylor_coefficients.cols()) + ") and " +
        std::to_string(exp_taylor_coefficients.size()));
  }

  if (std::abs(alpha_bra - alpha_ket) > kTolerance) {
    QI_Error::RaiseError("[ERROR] "
                         "GaugeMomentumFlowMatrixElement - bra and ket "
                         "Gaussian widths must be equal.");
  }

  std::complex<double> result = 0;
  if (std::abs(taylor_coefficients(0, 0)) > kTolerance) {
    result += taylor_coefficients(0, 0) *
              HusimiFunctionMatrixElement(
                  phase_space_point, bra, ket,
                  kernel_gaussian_quadratic_coefficients_ab,
                  alpha_bra /*must be the same for bra and ket*/);
  }

  if (taylor_coefficients.rows() == 2) {
    if (std::abs(result) > kTolerance) {
      QI_Error::RaiseError("[ERROR] "
                           "GaugeMomentumFlowMatrixElement - strange "
                           "combination of constant and momentum.");
    }
    result += GaugeMomentumFlowMatrixElement(
        taylor_coefficients(1, 0).real(), p_mean, p_mean_x_derivative,
        phase_space_point, bra, ket, kernel_gaussian_quadratic_coefficients_ab,
        alpha_bra /*must be the same for bra and ket*/, approximation_order);
  }

  return result;
}

std::complex<double> GaugePositionFlowMatrixElement(
    const MatrixXc_r &taylor_coefficients,
    const MatrixXc_r &exp_taylor_coefficients, double p_mean,
    const CanonicPair &phase_space_point, const CanonicPair &bra,
    double alpha_bra /*gaussian quadratic coefficient, must be the same for bra
                                            and ket*/
    ,
    const CanonicPair &ket,
    double alpha_ket /*gaussian quadratic coefficient, must be the same for bra
                                            and ket*/
    ,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab,
    unsigned int approximation_order) {
  if (taylor_coefficients.size() == 0)
    return 0;

  if ((taylor_coefficients.cols() > 1) || (taylor_coefficients.rows() > 2) ||
      (exp_taylor_coefficients.size() != 0)) {
    QI_Error::RaiseError(
        "[ERROR] "
        "GaugePositionFlowMatrixElement - taylor_coefficients "
        "should be (1x0...2) matrix and exp_taylor_coefficients should be "
        "empty. Actually: (" +
        std::to_string(taylor_coefficients.rows()) + "," +
        std::to_string(taylor_coefficients.cols()) + ") and " +
        std::to_string(exp_taylor_coefficients.size()));
  }

  if (std::abs(alpha_bra - alpha_ket) > kTolerance) {
    QI_Error::RaiseError("[ERROR] "
                         "GaugePositionFlowMatrixElement - bra and ket "
                         "Gaussian widths must be equal.");
  }

  std::complex<double> result = 0;
  if (std::abs(taylor_coefficients(0, 0)) > kTolerance) {
    result += taylor_coefficients(0, 0) *
              HusimiFunctionMatrixElement(
                  phase_space_point, bra, ket,
                  kernel_gaussian_quadratic_coefficients_ab, alpha_bra);
  }
  if (taylor_coefficients.rows() == 2) {
    if (std::abs(result) > kTolerance) {
      QI_Error::RaiseError("[ERROR] "
                           "GaugePositionFlowMatrixElement - strange "
                           "combination of constant and momentum.");
    }
    result += GaugePositionFlowMatrixElement(
        taylor_coefficients(1, 0).real(), p_mean, phase_space_point, bra, ket,
        kernel_gaussian_quadratic_coefficients_ab, alpha_bra,
        approximation_order);
  }

  return result;
}

} // namespace quasiprobability_gauge_auxiliaries
} // namespace mce
