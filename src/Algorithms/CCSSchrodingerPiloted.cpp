#include "stdafx.h"

#include <Eigen/Eigenvalues>

#include "Algorithms/CCSSchrodingerPiloted.h"
#include "Algorithms/CCSSystemUtilities.h"
#include <Eigen/StdVector>

namespace mce {
namespace algorithm {

CCSSchrodingerSystemPiloted::CCSSchrodingerSystemPiloted(
    const Operator<PolicyType> &hamiltonian,
    const Operator<PolicyType> &linear_solve_kernel,
    OperatorFactory<PolicyType> &operator_factory)
    : hamiltonian_(hamiltonian), linear_solve_kernel_(linear_solve_kernel),
      operator_factory_(operator_factory), amplitude_max_regularizer_(1.e-7),
      smoothing_scale_(1.){};

void CCSSchrodingerSystemPiloted::operator()(const Wavefunction &state,
                                            Wavefunction &der_t_state,
                                            const double t) const {

  size_t max_retries = 20;
  VectorXd_c coeffs; // amplitude coefficients to be calculated;
  double linear_solve_error;
  amplitude_max_regularizer_ /=
      (amplitude_max_regularizer_ > 1.e-7 ? 4. : 2.); // mutable variable
  size_t attempts_count = 0;
  double state_norm =
      operator_factory_.CreateId(0).Evaluate({state}).Mean().real();

  auto P = [](const Wavefunction &state, size_t id, SpatialDOF_id dof_id) {
    return state[id]->GetCenter(dof_id).P();
  };
  auto X = [](const Wavefunction &state, size_t id, SpatialDOF_id dof_id) {
    return state[id]->GetCenter(dof_id).X();
  };

  auto X_op = [this](SpatialDOF_id dof_id) {
    return operator_factory_.CreateX(dof_id);
  };

  auto Phase = [](const Wavefunction &state, size_t id) {
    return state[id]->GetMultiplier().Phase() +
           std::arg(state[id]->GetMultiplier().Amplitude());
  };

  auto SlowPhase = [](const Wavefunction &state, size_t id) {
    return std::arg(state[id]->GetMultiplier().Amplitude());
  };

  auto FastPhase = [](const Wavefunction &state, size_t id) {
    return state[id]->GetMultiplier().Phase();
  };

  auto Amplitude = [](const Wavefunction &state, size_t id) {
    return std::abs(state[id]->GetMultiplier().Amplitude());
  };

  auto CplxAmplitude = [](const Wavefunction &state, size_t id) {
    return state[id]->GetMultiplier().Amplitude();
  };

  auto alpha = [&state](SpatialDOF_id dof_id) {
    return state.Configuration().GaussianQuadraticCoefficient(dof_id);
  };

  auto xi_ket_op = [&state, &X,
                    this](size_t j,
                          SpatialDOF_id dof_id_j) -> Operator<PolicyType> {
    // represents \pder{state_j}{PCenter_{dof_id_j}}
    return (operator_factory_.CreateX(dof_id_j) -
            X(state, j, dof_id_j) * operator_factory_.CreateId(dof_id_j)) *
           (I / kHBar);
  };

  auto xi_bra_op = [&state, &X,
                    this](size_t i,
                          SpatialDOF_id dof_id_i) -> Operator<PolicyType> {
    // represents \conj{\pder{state_i}{PCenter_{dof_id_i}}}
    return -(operator_factory_.CreateX(dof_id_i) -
             X(state, i, dof_id_i) * operator_factory_.CreateId(dof_id_i)) *
           (I / kHBar);
  };

  auto number_of_spatial_dofs = state.Configuration().NumberOfSpatialDOFs();

  do {
    {
      int i = 0;
      while (i++ < attempts_count) {
        std::cout << "*";
      }
    }
    amplitude_max_regularizer_ *= 2; // mutable variable
    attempts_count++;
    if (der_t_state.IsNull()) {
      der_t_state = state.Clone(false);
    }
    der_t_state.SetZero();

    using namespace ccs_system_utilities;
    auto H_der = Derivative(hamiltonian_, operator_factory_).Evaluate({state});

    // calculating centers shifts dP/dt and dX/dt according to Hamilton
    // equations and the phase time derivative "as is normally done" (c) D.V.
    // Makhov et al. / Chemical Physics 493 (2017) 200�218

    for (size_t n = 0; n < state.Size(); n++) {
      double der_t_phase = 0;
      auto smoothing_kernel_op = GetPilotSmoothingKernel(state, n);

      for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
        // der_t_state[n]->GetCenter(dof_id).P(
        //	-H_der.DiffCenterX(dof_id, n)
        //	.real()); // ???!!! taking real part! double-check in tests
        //			  // that result is real
        // der_t_state[n]->GetCenter(dof_id).X(
        //    H_der.DiffCenterP(dof_id, n)
        //        .real()); // ???!!! taking real part! double-check in tests
        //        that
        //                  // result is real

        auto momentum_op =
            I / kHBar *
            (hamiltonian_ * X_op(dof_id) -
             X_op(dof_id) *
                 hamiltonian_); // ???!!! will throw for complex hamiltonians!

        double pilot_momentum =
            (smoothing_kernel_op * momentum_op)
                .Evaluate({state})
                .Mean()
                .real() /
            smoothing_kernel_op.Evaluate({state}).Mean().real();

		// std::cout << "\n*** " << pilot_momentum << " | " << smoothing_kernel_op.Evaluate({ state }).Mean().real();;

        der_t_state[n]->GetCenter(dof_id).X(
            pilot_momentum); // ???!!! Works only for unit mass!!!

        der_t_phase += state[n]->GetCenter(dof_id).P() *
                       der_t_state[n]->GetCenter(dof_id).X();
      }
      der_t_state[n]->GetMultiplier().Phase(der_t_phase - H_der(n, n).real());
    }

    auto eta_ket_op = [&state, &P, &X, &alpha, &der_t_state,
                       this](size_t j) -> Operator<PolicyType> {
      auto output = hamiltonian_ * (-I / kHBar);
      for (size_t dof_id_j = 0;
           dof_id_j < state.Configuration().NumberOfSpatialDOFs(); dof_id_j++) {
        output += (I / kHBar) * P(state, j, dof_id_j) *
                  X(der_t_state, j, dof_id_j) *
                  operator_factory_.CreateId(dof_id_j);
        output -=
            2. * X(der_t_state, j, dof_id_j) * alpha(dof_id_j) *
            (operator_factory_.CreateX(dof_id_j) -
             X(state, j, dof_id_j) * operator_factory_.CreateId(dof_id_j));
        // output -= P(der_t_state, j, dof_id_j)
        // *(operator_factory_.CreateX(dof_id_j) - 	X(state, j, dof_id_j) *
        // operator_factory_.CreateId(dof_id_j)) * 	(I / kHBar);
      }
      return output;
    };

    // updating multiplier

    // ???!!! Unfortunately, self adjoint Eigen solver seems to not work with
    // row-major matrices
    // Eigen::MatrixXcd

    using MatrixType = MatrixXd_c; // MatrixXd_r
    using BlockType = MatrixType::BlockXpr;
    using VectorBlockType = std::vector<std::shared_ptr<BlockType>>;
    MatrixType A(state.Size() * (2 + number_of_spatial_dofs),
                 state.Size() * (2 + number_of_spatial_dofs));
    VectorXd_c b(state.Size() * (2 + number_of_spatial_dofs));

    // Finding the remaining amplitude variations as a solution of linear system
    // A.x=b initializing A

    auto A11 = A.block(0, 0, state.Size(), state.Size());
    auto A33 = A.block((number_of_spatial_dofs + 1) * state.Size(),
                       (number_of_spatial_dofs + 1) * state.Size(),
                       state.Size(), state.Size());
    auto A13 = A.block(0, (number_of_spatial_dofs + 1) * state.Size(),
                       state.Size(), state.Size());

    VectorBlockType A12(number_of_spatial_dofs);
    VectorBlockType A32(number_of_spatial_dofs);
    for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
      A12[dof_id] = std::make_shared<BlockType>(
          A.block(0, (dof_id + 1) * state.Size(), state.Size(), state.Size()));
      A32[dof_id] = std::make_shared<BlockType>(
          A.block((number_of_spatial_dofs + 1) * state.Size(),
                  (dof_id + 1) * state.Size(), state.Size(), state.Size()));
    };

    auto A_evaluator = linear_solve_kernel_.Evaluate({state});
    auto Id_der =
        Derivative(linear_solve_kernel_, operator_factory_).Evaluate({state});
    auto H = (linear_solve_kernel_ * hamiltonian_)
                 .Evaluate({state}); // ???!!! Optimize by defining the
                                     // corresponding class variable!
    auto H_der2 =
        Derivative(hamiltonian_ * linear_solve_kernel_, operator_factory_)
            .Evaluate({state}); // ???!!! is multiplication order correct?

    for (size_t i = 0, state_size = state.Size(); i < state_size; ++i) {
      for (size_t j = 0; j < state_size; ++j) {
        std::complex<double> chi_ij =
            A_evaluator(i, j) *
            std::exp(I * (Phase(state, j) - Phase(state, i)));
        A11(i, j) = chi_ij.real();
        A33(i, j) = chi_ij.real() * Amplitude(state, i) * Amplitude(state, j);
        A13(i, j) = (I * chi_ij).real() * Amplitude(state, j);

        for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
          auto xi_ij = Id_der.DiffKetCenterP(dof_id, i, j) *
                       std::exp(I * (Phase(state, j) - Phase(state, i)));
          (*A12[dof_id])(i, j) = xi_ij.real() * Amplitude(state, j);
          (*A32[dof_id])(i, j) =
              (-I * xi_ij).real() * Amplitude(state, i) * Amplitude(state, j);
        }
      }
    }
    A.block(state.Size() * (number_of_spatial_dofs + 1), 0, state.Size(),
            state.Size()) = A13.transpose();

    A.block(state.Size(), 0, state.Size() * (number_of_spatial_dofs),
            state.Size()) =
        A.block(0, state.Size(), state.Size(),
                state.Size() * (number_of_spatial_dofs))
            .transpose();

    A.block(state.Size(), (number_of_spatial_dofs + 1) * state.Size(),
            state.Size() * (number_of_spatial_dofs), state.Size()) =
        A.block((number_of_spatial_dofs + 1) * state.Size(), state.Size(),
                state.Size(), state.Size() * (number_of_spatial_dofs))
            .transpose();

    for (size_t dof_id_i = 0; dof_id_i < number_of_spatial_dofs; dof_id_i++) {
      for (size_t dof_id_j = 0; dof_id_j < number_of_spatial_dofs; dof_id_j++) {
        auto A22_ij =
            A.block(state.Size() * (1 + dof_id_i),
                    state.Size() * (1 + dof_id_j), state.Size(), state.Size());
        for (size_t i = 0, state_size = state.Size(); i < state_size; ++i) {
          for (size_t j = 0; j < state_size; ++j) {
            auto scpr_xi = Amplitude(state, i) * Amplitude(state, j) *
                           (std::exp(I * (Phase(state, j) - Phase(state, i)))) *
                           (xi_bra_op(i, dof_id_i) * linear_solve_kernel_ *
                            xi_ket_op(j, dof_id_j))
                               .Evaluate({state})(i, j);
            A22_ij(i, j) = scpr_xi.real();
          }
        }
      }
    }

    // regularizing A
    for (Eigen::Index i = 0; i < A.cols(); i++) {
      A(i, i) += amplitude_max_regularizer_;
    }

    // std::cout << "\n!\n" << A << "\n!\n";

    // initializing b1 and b3
    auto chi_i_xi_j = [&](SpatialDOF_id dof_id, size_t i, size_t j) {
      return Id_der.DiffKetCenterP(dof_id, i, j);
    };

    // initializing b
    auto b1 = b.segment(0, state.Size());
    auto b3 =
        b.segment((number_of_spatial_dofs + 1) * state.Size(), state.Size());

    auto chi_i_eta_j = [&](size_t i, size_t j) {
      auto result = -I / kHBar * H(i, j);
      for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
        // result -=
        //    P(der_t_state, j, dof_id) * Id_der.DiffKetCenterP(dof_id, i, j);
        result -=
            X(der_t_state, j, dof_id) * Id_der.DiffKetCenterX(dof_id, i, j);
      }
      return result;
    };

    for (size_t m = 0, state_size = state.Size(); m < state_size; m++) {
      b1(m) = 0;
      b3(m) = 0;
      for (size_t n = 0; n < state_size; n++) {
        std::complex<double> b_stub =
            std::exp(-I * (Phase(state, m) - Phase(state, n))) *
            (I * Id_der(m, n) * FastPhase(der_t_state, n) -
             chi_i_eta_j(m, n)); // was simply phase
        b1(m) -= Amplitude(state, n) * b_stub.real();
        b3(m) -=
            Amplitude(state, m) * Amplitude(state, n) * (-I * b_stub).real();
      }
    }

    // initializing b2
    for (size_t dof_id_i = 0; dof_id_i < number_of_spatial_dofs; dof_id_i++) {
      auto b2_i = b.segment(state.Size() * (1 + dof_id_i), state.Size());
      for (size_t i = 0, state_size = state.Size(); i < state_size; ++i) {
        b2_i(i) = 0;
        for (size_t j = 0; j < state_size; ++j) {
          auto eta_op_i =
              (xi_bra_op(i, dof_id_i) * linear_solve_kernel_ *
               (-eta_ket_op(j) + I * operator_factory_.CreateId(dof_id_i) *
                                     der_t_state[j]->GetMultiplier().Phase()))
                  .Evaluate({state})(i, j);
          b2_i(i) +=
              -Amplitude(state, i) *
              Amplitude(state, j) * // ??? unclear why it is "-" here
              (std::exp(I * (Phase(state, j) - Phase(state, i))) * eta_op_i)
                  .real();
        }
      }
    }

    // std::cout << "\n!b\n" << b << "\n!\n";

    using namespace Eigen;
    coeffs = // A.llt().solve(b);// Doesn't work in most cases.
        A.colPivHouseholderQr().solve(b);
    VectorXc_c v = (A * coeffs - b);
    linear_solve_error = std::abs((v.transpose() * v)(0, 0));
  } while ((linear_solve_error > kTolerance) && (attempts_count < max_retries));

  // std::cout << "\n!coeffs:\n" << coeffs << "\n!\n";

  if (attempts_count >= max_retries) {
    std::cout << "\n[INFO] Max retries limit of " << max_retries
              << " exceeded. Linear solve error is still " << linear_solve_error
              << ". Continuing anyway...\n";
  };

  const auto &d_ampl = coeffs.segment(0, state.Size());
  const auto &d_slow_phase =
      coeffs.segment(state.Size() * (number_of_spatial_dofs + 1), state.Size());

  for (size_t m = 0; m < state.Size(); m++) {
    der_t_state[m]->GetMultiplier().Amplitude(
        I * d_slow_phase(m) * CplxAmplitude(state, m) +
        d_ampl(m) * std::exp(I * SlowPhase(state, m)));
  }

  for (size_t dof_id_i = 0; dof_id_i < number_of_spatial_dofs; dof_id_i++) {
    auto der_momenta_i =
        coeffs.segment(state.Size() * (1 + dof_id_i), state.Size());
    for (size_t m = 0; m < state.Size(); m++) {
      der_t_state[m]->GetCenter(dof_id_i).P(der_momenta_i(m));
    }
  }

  auto self_projection =
      std::abs(ccs_system_utilities::WavefunctionDerivativeSelfProjection(
                   state, der_t_state, operator_factory_)
                   .real());
  if (self_projection > 1.e-3) {
    std::cout << "\n Look at this beautiful self-projection: "
              << self_projection;
  }
}

void CCSSchrodingerSystemPiloted::SetSmoothingScale(double smoothing_scale) {
  smoothing_scale_ = smoothing_scale;
}

Operator<CCSSchrodingerSystemPiloted::PolicyType>
CCSSchrodingerSystemPiloted::GetPilotSmoothingKernel(const Wavefunction &state,
                                                    size_t basis_id) const {
  auto number_of_spatial_dofs = state.Configuration().NumberOfSpatialDOFs();
  auto alpha_scaled = [&state, this](SpatialDOF_id dof_id) {
    return state.Configuration().GaussianQuadraticCoefficient(dof_id) /
           smoothing_scale_ / smoothing_scale_;
  };
  auto X = [&state, &basis_id, this](SpatialDOF_id dof_id) {
    return state[basis_id]->GetCenter(dof_id).X() *
           operator_factory_.CreateId(dof_id);
  };
  auto X_op = [this](SpatialDOF_id dof_id) {
    return operator_factory_.CreateX(dof_id);
  };

  auto Multiplier = [&state](size_t basis_id) {
    return state[basis_id]->GetMultiplier().Value();
  };

  auto exp_op = [&](SpatialDOF_id dof_id) {
    return operator_factory_.CreateExp(-alpha_scaled(dof_id) *
                                       (X_op(dof_id) - X(dof_id)) *
                                       (X_op(dof_id) - X(dof_id)));
  };
  if (number_of_spatial_dofs < 1) {
    QI_Error::RaiseError(
        "[ERROR] CCSSchrodingerSystemPiloted::_GetPilotWaveMomentum - "
        "number_of_spatial_dofs < 1");
  };

  auto smoothing_kernel_op = exp_op(0);
  for (size_t dof_id_i = 1; dof_id_i < number_of_spatial_dofs; dof_id_i++) {
    smoothing_kernel_op *= exp_op(dof_id_i);
  }
  return smoothing_kernel_op;
};

} // namespace algorithm

} // namespace mce
