#include "stdafx.h"

#include "Algorithms/CCSQuasiprobabilityFlowBundle.h"

namespace mce {
namespace quasiprobability_auxiliaries {

CCSQuasiprobabilityFlowBundle::CCSQuasiprobabilityFlowBundle(
    const OperatorFactory<PolicyType> &operator_factory,
    const OperatorType &hamiltonian, const SpatialDOF_id number_of_dofs,
    const CanonicPair &width_factors)
    : operator_factory_(operator_factory), number_of_dofs_(number_of_dofs),
      auxiliary_harmonic_potentials_(number_of_dofs),
      velocities_(number_of_dofs), momenta_(number_of_dofs),
      strategy_(std::make_shared<EvaluatorStrategyType>(
          evaluator_strategy::QuasiprobabilityFlowType::Momentum,
          width_factors)),
      identity_(std::make_shared<OperatorType>(operator_factory_.CreateId(0))) {
  potential_ = std::make_shared<OperatorType>(hamiltonian);
  for (SpatialDOF_id dof_id = 0; dof_id < number_of_dofs; dof_id++) {
    auto X = operator_factory_.CreateX(dof_id);
    auto P = operator_factory_.CreateP(dof_id);
    auxiliary_harmonic_potentials_[dof_id] =
        std::make_shared<OperatorType>((-0.5) * X * X);
    velocities_[dof_id] = std::make_shared<OperatorType>(
        (1. / I / kHBar) * (X * hamiltonian - hamiltonian * X));
    momenta_[dof_id] = std::make_shared<OperatorType>(P);
    (*potential_) -= 0.5 * P * (*velocities_[dof_id]);
  }
};

void CCSQuasiprobabilityFlowBundle::Initialize(Wavefunction wavefunction) {
  using namespace evaluator_strategy;
  strategy_->FlowType(QuasiprobabilityFlowType::Momentum);
  potential_ev_ = std::make_shared<EvaluatorType>(
      potential_->Evaluate({wavefunction, strategy_->ShallowCopy()}));
  auxiliary_harmonic_potentials_ev_.resize(
      auxiliary_harmonic_potentials_.size());
  for (SpatialDOF_id dof_id = 0; dof_id < velocities_.size(); dof_id++) {
    auxiliary_harmonic_potentials_ev_[dof_id] = std::make_shared<EvaluatorType>(
        auxiliary_harmonic_potentials_[dof_id]->Evaluate(
            {wavefunction, strategy_->ShallowCopy()}));
  }

  strategy_->FlowType(QuasiprobabilityFlowType::Position);
  velocities_ev_.resize(velocities_.size());
  momenta_ev_.resize(velocities_.size());
  norm_ev_ = std::make_shared<EvaluatorType>(
      identity_->Evaluate({wavefunction, strategy_->ShallowCopy()}));
  for (SpatialDOF_id dof_id = 0; dof_id < velocities_.size(); dof_id++) {
    velocities_ev_[dof_id] =
        std::make_shared<EvaluatorType>(velocities_[dof_id]->Evaluate(
            {wavefunction, strategy_->ShallowCopy()}));
    momenta_ev_[dof_id] = std::make_shared<EvaluatorType>(
        momenta_[dof_id]->Evaluate({wavefunction, strategy_->ShallowCopy()}));
  };
}

void CCSQuasiprobabilityFlowBundle::Reset() {
  potential_ev_ = 0;
  velocities_ev_.resize(0);
  momenta_ev_.resize(0);
  norm_ev_ = 0;
}

bool CCSQuasiprobabilityFlowBundle::IsInitialized() {
  return !(potential_ev_ == 0);
}

double CCSQuasiprobabilityFlowBundle::EvaluatePositionFlow(
    size_t center_basis_function_id, SpatialDOF_id dof_id) {
  strategy_->CenterBasisFunction(center_basis_function_id);
  strategy_->DofId(dof_id);
  auto norm = norm_ev_->Mean().real();
  auto position_flow = velocities_ev_[dof_id]->Mean().real();
  return (position_flow / norm);
};

double CCSQuasiprobabilityFlowBundle::EvaluateEffectiveMomentum(
    size_t center_basis_function_id, SpatialDOF_id dof_id) {
  strategy_->CenterBasisFunction(center_basis_function_id);
  strategy_->DofId(dof_id);
  auto norm = norm_ev_->Mean().real();
  return (momenta_ev_[dof_id]->Mean().real() / norm_ev_->Mean().real());
};

double CCSQuasiprobabilityFlowBundle::EvaluateEffectivePosition(
	size_t center_basis_function_id, SpatialDOF_id dof_id) {
	strategy_->CenterBasisFunction(center_basis_function_id);
	strategy_->DofId(dof_id);
	auto norm = norm_ev_->Mean().real();
	return (auxiliary_harmonic_potentials_ev_[dof_id]->Mean().real() / norm_ev_->Mean().real());
};

double CCSQuasiprobabilityFlowBundle::EvaluateMomentumFlow(
    size_t center_basis_function_id, SpatialDOF_id dof_id) {
  strategy_->CenterBasisFunction(center_basis_function_id);
  strategy_->DofId(dof_id);
  auto norm = norm_ev_->Mean().real();
  auto momentum_flow = potential_ev_->Mean().real();
  return (momentum_flow / norm);
};

} // namespace quasiprobability_auxiliaries
} // namespace mce
