#include "Algorithms/CCSPilotWaveUtilities.h"

namespace mce {
namespace ccs_system_utilities {

std::complex<double>
GetPilotWaveMomentum(Wavefunction state, size_t basis_idx,
                     const Operator<MCE_POLICY(CCS)> &momentum_operator,
                     const Operator<MCE_POLICY(CCS)> &smoothing_kernel) {
  std::complex<double> pilot_momentum =
      (smoothing_kernel * momentum_operator).Evaluate({state}).Mean().real() /
      smoothing_kernel.Evaluate({state}).Mean();

  return pilot_momentum;
}

std::complex<double>
GetPilotWaveMomentumSquared(Wavefunction state, size_t basis_idx,
                            const Operator<MCE_POLICY(CCS)> &momentum_operator,
                            const Operator<MCE_POLICY(CCS)> &smoothing_kernel) {
  std::complex<double> pilot_momentum_squared =
      0.5 *
      (smoothing_kernel * momentum_operator * momentum_operator +
       momentum_operator * momentum_operator * smoothing_kernel)
          .Evaluate({state})
          .Mean() /
      smoothing_kernel.Evaluate({state}).Mean().real();

  return pilot_momentum_squared;
}

std::complex<double> GetLocalAverageTimeDerivative(
    Wavefunction state, const Operator<MCE_POLICY(CCS)> &hamiltonian,
    const Operator<MCE_POLICY(CCS)> &observable,
    const Operator<MCE_POLICY(CCS)> &smoothing_kernel) {
  auto area = smoothing_kernel.Evaluate({state}).Mean();
  std::complex<double> observable_derivative =
      (-I / kHBar) *
      ((smoothing_kernel * observable * hamiltonian -
        hamiltonian * smoothing_kernel * observable)
               .Evaluate({state})
               .Mean() /
           area -
       (smoothing_kernel * observable).Evaluate({state}).Mean() / area / area *
           (smoothing_kernel * hamiltonian - hamiltonian * smoothing_kernel)
               .Evaluate({state})
               .Mean());
  return observable_derivative;
}

std::complex<double> GetPilotWaveMomentumDerivative(
    Wavefunction state, size_t basis_idx,
    const Operator<MCE_POLICY(CCS)> &hamiltonian,
    const Operator<MCE_POLICY(CCS)> &momentum_operator,
    const Operator<MCE_POLICY(CCS)> &smoothing_kernel) {
  auto momentum_acceleration_op =
      I / kHBar *
      (hamiltonian * momentum_operator -
       momentum_operator * hamiltonian); // ???!!! will throw for
                                         // complex hamiltonians!
  std::complex<double> pilot_momentum_acceleration =
      (smoothing_kernel * momentum_acceleration_op).Evaluate({state}).Mean() /
      smoothing_kernel.Evaluate({state}).Mean().real();

  return pilot_momentum_acceleration;
}

std::complex<double> GetMaterialTimeDerivative(
    Wavefunction state, const Operator<MCE_POLICY(CCS)> &hamiltonian,
    const Operator<MCE_POLICY(CCS)> &observable,
    const Operator<MCE_POLICY(CCS)> &momentum_operator,
    const Operator<MCE_POLICY(CCS)> &smoothing_kernel, double velocity) {
  auto ker_obs = smoothing_kernel * observable;
  auto evaluator =
      (ker_obs * hamiltonian - hamiltonian * ker_obs) * (1. / (I * kHBar)) -
      (velocity / (I * kHBar)) *
          (smoothing_kernel * momentum_operator -
           momentum_operator * smoothing_kernel) *
          observable;
  return evaluator.Evaluate({state}).Mean();
};

std::pair<double, double>
GetApproximateMaterialTimeDerivativeOfSquaredPilotWaveMomentum(
    Wavefunction state, const Operator<MCE_POLICY(CCS)> &hamiltonian,
    const Operator<MCE_POLICY(CCS)> &momentum_operator,
    const Operator<MCE_POLICY(CCS)> &identity_operator,
    const Operator<MCE_POLICY(CCS)> &smoothing_kernel, double velocity) {
  auto momentum_squared = momentum_operator * momentum_operator;
  auto numerator_der =
      GetMaterialTimeDerivative(state, hamiltonian, momentum_squared,
                                momentum_operator, smoothing_kernel, velocity).real();
  auto denominator_der =
      GetMaterialTimeDerivative(state, hamiltonian, identity_operator,
                                momentum_operator, smoothing_kernel, velocity).real();
  if (isnan(numerator_der))
	  std::cout << "<n_nan>";
  if (isnan(denominator_der))
	  std::cout << "<d_nan>";
  auto numerator =
      (smoothing_kernel * momentum_squared).Evaluate({state}).Mean().real();
  auto denominator = smoothing_kernel.Evaluate({state}).Mean().real();
  double squared_pilot_wave_momentum = (numerator / denominator);
  double squared_pilot_wave_momentum_material_derivative =
      (numerator_der / denominator - (squared_pilot_wave_momentum) *
                                         (denominator_der /
                                         denominator));
  return std::pair<double, double>(
      squared_pilot_wave_momentum,
      squared_pilot_wave_momentum_material_derivative);
}

std::complex<double>
GetPilotWaveVelocity(Wavefunction state, size_t basis_idx,
                     const Operator<MCE_POLICY(CCS)> &hamiltonian,
                     const Operator<MCE_POLICY(CCS)> &position_operator,
                     const Operator<MCE_POLICY(CCS)> &smoothing_kernel) {
  auto velocity_op = I / kHBar *
                     (hamiltonian * position_operator -
                      position_operator * hamiltonian); // ???!!! will throw for
                                                        // complex hamiltonians!
  std::complex<double> pilot_velocity =
      (smoothing_kernel * velocity_op).Evaluate({state}).Mean() /
      smoothing_kernel.Evaluate({state}).Mean().real();

  return pilot_velocity;
}

Operator<MCE_POLICY(CCS)>
PilotWaveQuantumForceEvaluator::Der(SpatialDOF_id dof_id,
                                    const Operator<MCE_POLICY(CCS)> &O) const {
  auto P_op = operator_factory_.CreateP(dof_id);
  return -I / kHBar *
         (P_op * O - O * P_op); // "-" stems from replacement in
                                // differentiation: \pder{\exp(-\beta(\hat
                                // x-q)^2)}{q} = - \pder{\exp(-\beta(\hat
                                // x-q)^2)}{\hat x}
}

Operator<MCE_POLICY(CCS)>
PilotWaveQuantumForceEvaluator::Der2(SpatialDOF_id dof_id,
                                     const Operator<MCE_POLICY(CCS)> &O) const {
  auto X_op = operator_factory_.CreateX(dof_id);
  auto V_op = I / kHBar * (hamiltonian_ * X_op - X_op * hamiltonian_);
  return -I / kHBar * (V_op * O - O * V_op);
}

PilotWaveQuantumForceEvaluator::PilotWaveQuantumForceEvaluator(
    Wavefunction state, size_t basis_idx,
    const Operator<MCE_POLICY(CCS)> &hamiltonian,
    const OperatorFactory<MCE_POLICY(CCS)> &operator_factory,
    const Operator<MCE_POLICY(CCS)> &smoothing_kernel)
    : state_(state.ShallowCopy()), basis_idx_(basis_idx),
      hamiltonian_(hamiltonian), operator_factory_(operator_factory),
      smoothing_kernel_(smoothing_kernel) {

  auto number_of_spatial_dofs = state.Configuration().NumberOfSpatialDOFs();

  sigma_ = std::make_unique<Operator<MCE_POLICY(CCS)>>(
      kHBar * kHBar / 4. * Der2(0, Der(0, smoothing_kernel)));
  Operator<MCE_POLICY(CCS)> s2 = kHBar / 2. * Der2(0, smoothing_kernel);
  s1_ = std::make_unique<Operator<MCE_POLICY(CCS)>>(kHBar / 2. *
                                                    Der(0, smoothing_kernel));

  for (SpatialDOF_id dof_id = 1; dof_id < number_of_spatial_dofs; dof_id++) {
    *sigma_ += kHBar * kHBar / 2. * Der2(dof_id, Der(dof_id, smoothing_kernel));
    s2 += kHBar / 2. * Der2(dof_id, smoothing_kernel);
    *s1_ += kHBar / 2. * Der(dof_id, smoothing_kernel);
  }

  _sigma_ = sigma_->Evaluate({state}).Mean().real();
  _s1_ = s1_->Evaluate({state}).Mean().real();
  _s2_ = s2.Evaluate({state}).Mean().real();
  _K_ = smoothing_kernel.Evaluate({state}).Mean().real();
};

double PilotWaveQuantumForceEvaluator::operator()(SpatialDOF_id dof_id) const {
  auto _d_s1 = Der(dof_id, *s1_).Evaluate({state_}).Mean().real();
  auto _d_sigma = Der(dof_id, *sigma_).Evaluate({state_}).Mean().real();
  auto _d_K = Der(dof_id, smoothing_kernel_).Evaluate({state_}).Mean().real();

  double der_t_P_bohm =
      (_d_sigma / _K_ - (_sigma_ * _d_K + _d_s1 * _s2_) / (_K_ * _K_) +
        _d_K * _s1_ * _s2_ / (_K_ * _K_ * _K_));
  return der_t_P_bohm;
}

Operator<MCE_POLICY(CCS)> GetPilotSmoothingKernel(
    const Wavefunction &state, size_t basis_id,
    const OperatorFactory<MCE_POLICY(CCS)> &operator_factory,
    double smoothing_scale) {
  auto number_of_spatial_dofs = state.Configuration().NumberOfSpatialDOFs();
  auto alpha_scaled = [&state, &smoothing_scale](SpatialDOF_id dof_id) {
    return state.Configuration().GaussianQuadraticCoefficient(dof_id) /
           smoothing_scale / smoothing_scale;
  };

  auto X = [&state, &basis_id, &operator_factory](SpatialDOF_id dof_id) {
    return state[basis_id]->GetCenter(dof_id).X() *
           operator_factory.CreateId(dof_id);
  };
  auto X_op = [&operator_factory](SpatialDOF_id dof_id) {
    return operator_factory.CreateX(dof_id);
  };

  auto Multiplier = [&state](size_t basis_id) {
    return state[basis_id]->GetMultiplier().Value();
  };

  auto exp_op = [&](SpatialDOF_id dof_id) {
    return operator_factory.CreateExp(-alpha_scaled(dof_id) *
                                      (X_op(dof_id) - X(dof_id)) *
                                      (X_op(dof_id) - X(dof_id)));
  };
  if (number_of_spatial_dofs < 1) {
    QI_Error::RaiseError("[ERROR] GetPilotWaveMomentum - "
                         "number_of_spatial_dofs < 1");
  };

  auto smoothing_kernel_op = exp_op(0);
  for (size_t dof_id_i = 1; dof_id_i < number_of_spatial_dofs; dof_id_i++) {
    smoothing_kernel_op *= exp_op(dof_id_i);
  }
  return smoothing_kernel_op;
};
} // namespace ccs_system_utilities
} // namespace mce
