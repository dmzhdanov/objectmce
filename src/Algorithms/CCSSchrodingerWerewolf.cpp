#include "stdafx.h"

#include <Eigen/Eigenvalues>
#include <Eigen/StdVector>

#include "Algorithms/CCSMatrixElements.h"
#include "Algorithms/CCSPilotWaveUtilities.h"
#include "Algorithms/CCSSchrodingerWerewolf.h"
#include "Algorithms/CCSSystemUtilities.h"
#include "Algorithms/QuasiprobabilityGaugeAuxiliaries.h"
#include "Algorithms/WavefunctionUtilities.h"
#include "Commons/Faddeeva.hh"

namespace mce {
namespace algorithm {

CCSSchrodingerWerewolfSystem::CCSSchrodingerWerewolfSystem(
    const Operator<PolicyType> &hamiltonian,
    const Operator<PolicyType> &linear_solve_kernel,
    OperatorFactory<PolicyType> &operator_factory)
    : hamiltonian_(hamiltonian), linear_solve_kernel_(linear_solve_kernel),
      operator_factory_(operator_factory), momentum_mma_regularizer_(1),
      amplitude_mma_regularizer_(1.), amplitude_max_regularizer_(1.e-7),
      reprojection_regularizer_(-1),
      reference_amplitude_max_regularizer_(1.e-7), smoothing_scale_(1.),
      regularize_phase_scale_(1.), husimi_width_factors_(CanonicPair(1, 1)),
      gauge_approximation_order_(-1){};

void CCSSchrodingerWerewolfSystem::operator()(const Wavefunction &state,
                                              Wavefunction &der_t_state,
                                              const double t) const {

  // **************** index specification begin
  class MomentumIndex {
  public:
    MomentumIndex(SpatialDOF_id dof_id, size_t basis_id)
        : dof_id_(dof_id), basis_id_(basis_id) {}
    bool operator<(const MomentumIndex &other) const {
      return ((dof_id_ < other.dof_id_) ||
              ((dof_id_ == other.dof_id_) && (basis_id_ < other.basis_id_)));
    }
    SpatialDOF_id DofId() const { return dof_id_; }
    size_t BasisId() const { return basis_id_; }

  public:
    SpatialDOF_id dof_id_;
    size_t basis_id_;
  };
  std::set<MomentumIndex> variational_momenta;
  auto number_of_spatial_dofs = state.Configuration().NumberOfSpatialDOFs();
  auto AddToIndex = [&variational_momenta,
                     &number_of_spatial_dofs](size_t basis_id) {
    for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
      variational_momenta.insert({dof_id, basis_id});
    }
  };
  // **************** index specification end
  bool mma_flag = true;
  size_t max_retries = 1; // 20;
  VectorXd_c coeffs;      // amplitude coefficients to be calculated;
  double linear_solve_error;
  amplitude_max_regularizer_ /=
      (amplitude_max_regularizer_ > reference_amplitude_max_regularizer_
           ? 4.
           : 2.); // mutable variable
  size_t attempts_count = 0;
  double state_norm =
      operator_factory_.CreateId(0).Evaluate({state}).Mean().real();

  auto CheckNan = [](double x, std::string msg) {
    if (isnan(x)) {
      std::cout << "[ERROR]: nan variable " << msg << ".\n";
      std::flush(std::cout);
      return true;
    }
    return false;
  };

  auto P = [](const Wavefunction &state, size_t id, SpatialDOF_id dof_id) {
    return state[id]->GetCenter(dof_id).P();
  };

  auto X = [](const Wavefunction &state, size_t id, SpatialDOF_id dof_id) {
    return state[id]->GetCenter(dof_id).X();
  };

  auto X_op = [this](SpatialDOF_id dof_id) {
    return operator_factory_.CreateX(dof_id);
  };

  auto P_op = [this](SpatialDOF_id dof_id) {
    return operator_factory_.CreateP(dof_id);
  };

  auto Id_op = [this](SpatialDOF_id dof_id) {
    return operator_factory_.CreateId(dof_id);
  };

  auto Phase = [](const Wavefunction &state, size_t id) {
    return state[id]->GetMultiplier().Phase() +
           std::arg(state[id]->GetMultiplier().Amplitude());
  };

  auto SlowPhase = [](const Wavefunction &state, size_t id) {
    return std::arg(state[id]->GetMultiplier().Amplitude());
  };

  auto FastPhase = [](const Wavefunction &state, size_t id) {
    return state[id]->GetMultiplier().Phase();
  };

  auto Amplitude = [](const Wavefunction &state, size_t id) {
    return std::abs(state[id]->GetMultiplier().Amplitude());
  };

  auto CplxAmplitude = [](const Wavefunction &state, size_t id) {
    return state[id]->GetMultiplier().Amplitude();
  };

  auto alpha = [&state](SpatialDOF_id dof_id) {
    return state.Configuration().GaussianQuadraticCoefficient(dof_id);
  };

  auto xi_ket_op = [&state, &X,
                    this](size_t j,
                          SpatialDOF_id dof_id_j) -> Operator<PolicyType> {
    // represents \pder{state_j}{PCenter_{dof_id_j}}
    return (operator_factory_.CreateX(dof_id_j) -
            X(state, j, dof_id_j) * operator_factory_.CreateId(dof_id_j)) *
           (I / kHBar);
  };

  auto xi_bra_op = [&state, &X,
                    this](size_t i,
                          SpatialDOF_id dof_id_i) -> Operator<PolicyType> {
    // represents \conj{\pder{state_i}{PCenter_{dof_id_i}}}
    return -(operator_factory_.CreateX(dof_id_i) -
             X(state, i, dof_id_i) * operator_factory_.CreateId(dof_id_i)) *
           (I / kHBar);
  };

  using namespace ccs_system_utilities;
  auto H_der = Derivative(hamiltonian_, operator_factory_).Evaluate({state});

  auto Id_der_k =
      Derivative(linear_solve_kernel_, operator_factory_).Evaluate({state});
  auto H_k = (linear_solve_kernel_ * hamiltonian_)
                 .Evaluate({state}); // ???!!! Optimize by defining the
                                     // corresponding class variable!

  bool uninitialized_quasiprobability_bundle = true;
  bool uninitialized_quasiprobability_gauge_bundle = true;

  if (state.CheckForNans()) {
    QI_Error::RaiseError("[ERROR] CCSSchrodingerWerewolfSystem::operator() - "
                         "nans found in input state.");
  }

  do {
    variational_momenta.clear();
    {
      int i = 0;
      while (i++ < attempts_count) {
        std::cout << "*";
      }
    }
    amplitude_max_regularizer_ *= 2; // mutable variable
    attempts_count++;
    if (der_t_state.IsNull()) {
      der_t_state = state.Clone(false);
    }
    der_t_state.SetZero();

    //#pragma omp parallel for
    for (int n = 0; n < (int)state.Size(); n++) {
      switch (state[n]->nature_) {
      case BasisFunctionNature::Shalashilishche:

        // *********** SHALASHILISHE BEGIN ********************
        {
          // calculating centers shifts dP/dt and dX/dt according to Hamilton
          // equations and the phase time derivative "as is normally done" (c)
          // D.V. Makhov et al. / Chemical Physics 493 (2017) 200�218
          double der_t_phase = 0;
          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
            der_t_state[n]->GetCenter(dof_id).P(
                -H_der.DiffCenterX(dof_id, n)
                     .real()); // ???!!! taking real part! double-check in tests
                               // that result is real

            der_t_state[n]->GetCenter(dof_id).X(
                H_der.DiffCenterP(dof_id, n)
                    .real()); // ???!!! taking real part! double-check in tests
                              // that result is real

            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }

          if (mma_flag) {
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                state[n]->GetMultiplier().Amplitude() *
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
          }

          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);
        }
        // *********** SHALASHILISHE END   ********************

        break;
      case BasisFunctionNature::QuasiProbabilishche:

        // *********** QUASIPROBABILISHCHE BEGIN ********************
        {
          if (quasiprobability_bundle_ == 0) {
            quasiprobability_bundle_ = std::make_shared<
                quasiprobability_auxiliaries::CCSQuasiprobabilityFlowBundle>(
                operator_factory_, hamiltonian_,
                state.Configuration().NumberOfSpatialDOFs(),
                husimi_width_factors_);
          }
          if (uninitialized_quasiprobability_bundle) {
            (*quasiprobability_bundle_).Initialize(state);
            uninitialized_quasiprobability_bundle = false;
          }
          double der_t_phase = 0;
          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
            der_t_state[n]
                ->GetCenter(dof_id)
                .P((*quasiprobability_bundle_).EvaluateMomentumFlow(n, dof_id) /* + momentum_mma_regularizer_ * ((*quasiprobability_bundle_).EvaluateEffectiveMomentum(n, dof_id)-state[n]->GetCenter(dof_id).P())*/);

            der_t_state[n]
                ->GetCenter(dof_id)
                .X(/*state[n]->GetCenter(dof_id).P()*/
                   (*quasiprobability_bundle_).EvaluatePositionFlow(n, dof_id));

            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }

          /*              der_t_state[n]->GetCenter(dof_id).P(
          momentum_mma_regularizer_ *
                  std::pow(pilot_momentum - state[n]->GetCenter(dof_id).P(),
                          3));*/

          if (mma_flag) {
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                state[n]->GetMultiplier().Amplitude() *
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
          }

          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);
        }
        // *********** QUASIPROBABILISHCHE END ********************
        break;

      case BasisFunctionNature::QuasiProbabilishche_v2:
        // *********** QUASIPROBABILISHCHE_V2 BEGIN ********************
        //{
        //  AddToIndex(n);
        //  if (quasiprobability_bundle_ == 0) {
        //    quasiprobability_bundle_ = std::make_shared<
        //        quasiprobability_auxiliaries::CCSQuasiprobabilityFlowBundle>(
        //        operator_factory_, hamiltonian_,
        //        state.Configuration().NumberOfSpatialDOFs(),
        //        husimi_width_factors_);
        //  }
        //  if (uninitialized_quasiprobability_bundle) {
        //    (*quasiprobability_bundle_).Initialize(state);
        //    uninitialized_quasiprobability_bundle = false;
        //  }
        //  double der_t_phase = 0;
        //  for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {

        //    auto position_velocity =
        //        (*quasiprobability_bundle_).EvaluatePositionFlow(n, dof_id);
        //    der_t_state[n]->GetCenter(dof_id).X(position_velocity);

        //    auto effective_momentum = (*quasiprobability_bundle_)
        //                                  .EvaluateEffectiveMomentum(n,
        //                                  dof_id);
        //    der_t_state[n]->GetCenter(dof_id).P(
        //        momentum_mma_regularizer_ *
        //        std::pow(effective_momentum - state[n]->GetCenter(dof_id).P(),
        //                 3));

        //    der_t_phase += state[n]->GetCenter(dof_id).P() *
        //                   der_t_state[n]->GetCenter(dof_id).X();
        //  }

        //  /*              der_t_state[n]->GetCenter(dof_id).P(
        //  momentum_mma_regularizer_ *
        //                  std::pow(pilot_momentum -
        //  state[n]->GetCenter(dof_id).P(), 3));*/

        //  if (mma_flag) {
        //    der_t_state[n]->GetMultiplier().Amplitude(
        //        -amplitude_mma_regularizer_ *
        //        state[n]->GetMultiplier().Amplitude() *
        //        std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
        //  }

        //  der_t_state[n]->GetMultiplier().Phase(
        //      (der_t_phase - H_der(n, n).real()) / kHBar);
        //}
        // *********** QUASIPROBABILISHCHE_V2 END ********************

        {
          if (quasiprobability_bundle_ == 0) {
            quasiprobability_bundle_ = std::make_shared<
                quasiprobability_auxiliaries::CCSQuasiprobabilityFlowBundle>(
                operator_factory_, hamiltonian_,
                state.Configuration().NumberOfSpatialDOFs(),
                husimi_width_factors_);
          }
          if (uninitialized_quasiprobability_bundle) {
            (*quasiprobability_bundle_).Initialize(state);
            uninitialized_quasiprobability_bundle = false;
          }
          if (quasiprobability_gauge_bundle_ == 0) {
            quasiprobability_gauge_bundle_ =
                std::make_shared<quasiprobability_gauge_auxiliaries::
                                     CCSQuasiprobabilityGaugeBundle>(
                    operator_factory_, hamiltonian_,
                    state.Configuration().NumberOfSpatialDOFs(),
                    smoothing_scale_, husimi_width_factors_,
                    gauge_approximation_order_);
          }
          if (uninitialized_quasiprobability_gauge_bundle) {
            (*quasiprobability_gauge_bundle_).Initialize(state);
            uninitialized_quasiprobability_gauge_bundle = false;
          }
          double der_t_phase = 0;
          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {

            auto kernel_state = Wavefunction(state.configuration_);
            kernel_state.PushBack(state[n]->Clone(true));
            kernel_state[0]->GetMultiplier().Amplitude(1.);

            //auto momentum_flow_approx =
            //    (((hamiltonian_ * P_op(dof_id) - P_op(dof_id) * hamiltonian_) *
            //      (I / kHBar))
            //         .Evaluate({kernel_state, state})
            //         .Mean() /
            //     Id_op(dof_id).Evaluate({kernel_state, state}).Mean())
            //        .real();

            auto strategy = std::make_shared<
                evaluator_strategy::QuaisiprobabilityOperatorStrategy<
                    MCE_POLICY(CCS)>>(husimi_width_factors_, n, dof_id);

   //         auto force_m_op =
   //             (hamiltonian_ * P_op(dof_id) - P_op(dof_id) * hamiltonian_) *
   //             (I / kHBar);

   //         auto check1 =
   //             force_m_op.Evaluate({state, state, strategy}).Mean().real();
   //         auto check2 =
   //             (Id_op(dof_id).Evaluate({state, kernel_state}).Mean() *
   //              force_m_op.Evaluate({kernel_state, state}).Mean())
   //                 .real()/(2 * 3.141592653589793);

			//std::cout << "\nchk: " << check1 << " <> " << check2 << "\n";

            auto momentum_flow_approx =
                ((((hamiltonian_ * P_op(dof_id) - P_op(dof_id) * hamiltonian_) *
                   (I / kHBar))
                      .Evaluate({state, state, strategy})
                      .Mean()
                      .real()) /
                 Id_op(dof_id).Evaluate({state, state, strategy}).Mean())
                    .real();

            //std::cout << "\n mf: " << momentum_flow_approx << " <> "
            //          << momentum_flow_approx2;

            // auto momentum_flow_alt =
            //    (*quasiprobability_bundle_).EvaluateMomentumFlow(n, dof_id);
            // auto momentum_flow = -H_der.DiffCenterX(dof_id, n).real();
            auto momentum_gauge = (*quasiprobability_gauge_bundle_)
                                      .EvaluateMomentumFlow(n, dof_id);
            der_t_state[n]->GetCenter(dof_id).P(momentum_flow_approx +
                                                momentum_gauge);
            // std::cout << "{" << momentum_flow << ", " << momentum_flow_alt
            //          << ", " << momentum_flow_approx << "}\n";

            // auto position_flow = H_der.DiffCenterP(dof_id, n).real();

            //auto position_flow_alt =
            //    (*quasiprobability_bundle_).EvaluatePositionFlow(n, dof_id);

            auto position_flow_alt =
                ((((hamiltonian_ * X_op(dof_id) - X_op(dof_id) * hamiltonian_) *
                   (I / kHBar))
                      .Evaluate({state, state, strategy})
                      .Mean()
                      .real()) /
                 Id_op(dof_id).Evaluate({state, state, strategy}).Mean())
                    .real();
            //std::cout << "\n pf: " << position_flow_alt << " <> "
            //          << position_flow_alt2;

            auto position_gauge = (*quasiprobability_gauge_bundle_)
                                      .EvaluatePositionFlow(n, dof_id);
            der_t_state[n]->GetCenter(dof_id).X(position_flow_alt +
                                                position_gauge);
            // std::cout << "{" << state[n]->GetCenter(dof_id).P() << ", "
            //          << position_flow << ", " << position_flow_alt << ", "
            //          << position_flow_approx << ", " << position_gauge
            //          << "}\n";

            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }

          if (mma_flag) {
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                state[n]->GetMultiplier().Amplitude() *
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
          }

          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);
        }
        break;

      case BasisFunctionNature::QuasiProbabilishche_v3:
        // *********** QUASIPROBABILISHCHE_V3 BEGIN ********************
        {

          if (quasiprobability_bundle_ == 0) {
            quasiprobability_bundle_ = std::make_shared<
                quasiprobability_auxiliaries::CCSQuasiprobabilityFlowBundle>(
                operator_factory_, hamiltonian_,
                state.Configuration().NumberOfSpatialDOFs(),
                husimi_width_factors_);
          }
          if (quasiprobability_gauge_bundle_ == 0) {
            quasiprobability_gauge_bundle_ =
                std::make_shared<quasiprobability_gauge_auxiliaries::
                                     CCSQuasiprobabilityGaugeBundle>(
                    operator_factory_, hamiltonian_,
                    state.Configuration().NumberOfSpatialDOFs(),
                    smoothing_scale_, husimi_width_factors_,
                    gauge_approximation_order_);
          }
          if (uninitialized_quasiprobability_bundle) {
            (*quasiprobability_bundle_).Initialize(state);
            uninitialized_quasiprobability_bundle = false;
          }
          if (uninitialized_quasiprobability_gauge_bundle) {
            (*quasiprobability_gauge_bundle_).Initialize(state);
            uninitialized_quasiprobability_gauge_bundle = false;
          }
          double der_t_phase = 0;
          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {

            auto momentum_flow =
                (*quasiprobability_bundle_).EvaluateMomentumFlow(n, dof_id);
            auto momentum_gauge = (*quasiprobability_gauge_bundle_)
                                      .EvaluateMomentumFlow(n, dof_id);
            der_t_state[n]->GetCenter(dof_id).P(momentum_flow + momentum_gauge);

            auto position_flow =
                (*quasiprobability_bundle_).EvaluatePositionFlow(n, dof_id);
            auto position_gauge = (*quasiprobability_gauge_bundle_)
                                      .EvaluatePositionFlow(n, dof_id);
            der_t_state[n]->GetCenter(dof_id).X(position_flow + position_gauge);

            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }

          if (mma_flag) {
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                state[n]->GetMultiplier().Amplitude() *
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
          }

          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);
        }
        // *********** QUASIPROBABILISHCHE_V3 END ********************
        break;
      case BasisFunctionNature::QuasiProbabilishche_v4:
        // *********** QUASIPROBABILISHCHE_V4 BEGIN ********************
        {
          // algorithm works only for default husimi factors (input is ignored)
          // CanonicPair default_husimi_width_factors(1, 1);

          if (quasiprobability_bundle_ == 0) {
            quasiprobability_bundle_ = std::make_shared<
                quasiprobability_auxiliaries::CCSQuasiprobabilityFlowBundle>(
                operator_factory_, hamiltonian_,
                state.Configuration().NumberOfSpatialDOFs(),
                husimi_width_factors_);
          }
          if (uninitialized_quasiprobability_bundle) {
            (*quasiprobability_bundle_).Initialize(state);
            uninitialized_quasiprobability_bundle = false;
          }
          double der_t_phase = 0;
          double amplitude_regularizer_adjuster = 0;

          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
            // Strictly valid only for 1D case!
            if (number_of_spatial_dofs > 1) {
              QI_Error::RaiseError(
                  "[ERROR] CCSSchrodingerWerewolfSystem::operator() - "
                  "QuasiProbabilishche_v4 is implemented only for 1D case.");
            }

            auto kernel_state = Wavefunction(state.configuration_);
            kernel_state.PushBack(state[n]->Clone(true));
            kernel_state[0]->GetMultiplier().Amplitude(1.);

            Eigen::Matrix2d m, M, C;
            Eigen::Vector2d v, V, x, J, CoM;
            double p_mean, x_mean;
            double weight;

            m.setZero();
            v.setZero();
            p_mean = 0;
            x_mean = 0;
            weight = 0;

            auto PlacementFactor = [&](double x) {
              if (x < 1) {
                QI_Error::RaiseError(
                    "[ERROR] CCSSchrodingerWerewolf::operator() - Husimi "
                    "factors should be > 1 for Quasiprobabilishche_v4.");
              }
              return std::log(x * x) / std::sqrt(2.);
            };

            auto alpha =
                state.Configuration().GaussianQuadraticCoefficient(dof_id);
            CanonicPair kernel_state_wigner_px_alphas(
                1. / (2. * alpha * kHBar * kHBar), 2 * alpha);

            // Eigen::Matrix<double, 4, 1> p_shifts, x_shifts, xp_weights;
            // p_shifts << -1, 1, -1, 1;
            // x_shifts << -1, -1, 1, 1;
            // xp_weights << 10, 10, 10, 10;
            // auto xp_width_factors = husimi_width_factors_;
            // p_shifts *= 1. / std::sqrt(kernel_state_wigner_px_alphas.P()) *
            // PlacementFactor(xp_width_factors.P());
            // x_shifts *= 1. / std::sqrt(kernel_state_wigner_px_alphas.X()) *
            // PlacementFactor(xp_width_factors.X());

            // for fixed husimi width factors 1.1 and 1.1
            Eigen::Matrix<double, 9, 1> p_shifts, x_shifts, xp_weights;
            p_shifts << -0.5348862153476973, 0., 0.5348862153476973,
                -0.5348862153476973, 0., 0.5348862153476973,
                -0.5348862153476973, 0., 0.5348862153476973;
            x_shifts << -0.5348862153476973, -0.5348862153476973,
                -0.5348862153476973, 0., 0., 0., 0.5348862153476973,
                0.5348862153476973, 0.5348862153476973;
            xp_weights << 1., 3.485789440372107, 1., 3.485789440372107,
                12.150728022609686, 3.485789440372107, 1., 3.485789440372107,
                1.;
            p_shifts *= 1. / std::sqrt(kernel_state_wigner_px_alphas.P());
            x_shifts *= 1. / std::sqrt(kernel_state_wigner_px_alphas.X());

            // std::cout << alpha << "{" << p_shifts(0) << "," << x_shifts(0) <<
            // "}\n.";

            for (Eigen::Index i = 0; i < p_shifts.size(); i++) {
              kernel_state[0]->GetCenter(dof_id).P(
                  state[n]->GetCenter(dof_id).P() + p_shifts(i));
              kernel_state[0]->GetCenter(dof_id).X(
                  state[n]->GetCenter(dof_id).X() + x_shifts(i));
              m(0, 0) +=
                  xp_weights(i) * ccs_system_utilities::Covariance(
                                      X_op(dof_id), P_op(dof_id), Id_op(dof_id),
                                      {kernel_state, state}, false)
                                      .imag();
              m(0, 1) += -xp_weights(i) * ccs_system_utilities::Covariance(
                                              P_op(dof_id), P_op(dof_id),
                                              Id_op(dof_id),
                                              {kernel_state, state}, false)
                                              .imag();
              m(1, 0) +=
                  xp_weights(i) * ccs_system_utilities::Covariance(
                                      X_op(dof_id), X_op(dof_id), Id_op(dof_id),
                                      {kernel_state, state}, false)
                                      .imag();
              m(1, 1) += -xp_weights(i) * ccs_system_utilities::Covariance(
                                              P_op(dof_id), X_op(dof_id),
                                              Id_op(dof_id),
                                              {kernel_state, state}, false)
                                              .imag();

              v(0) +=
                  xp_weights(i) * ccs_system_utilities::Covariance(
                                      P_op(dof_id), hamiltonian_, Id_op(dof_id),
                                      {kernel_state, state}, false)
                                      .imag();
              v(1) +=
                  xp_weights(i) * ccs_system_utilities::Covariance(
                                      X_op(dof_id), hamiltonian_, Id_op(dof_id),
                                      {kernel_state, state}, false)
                                      .imag();

              std::complex<double> overlap =
                  Id_op(dof_id).Evaluate({kernel_state, state}).Mean();

              p_mean += xp_weights(i) *
                        (P_op(dof_id).Evaluate({kernel_state, state}).Mean() *
                         std::conj(overlap))
                            .real();
              x_mean += xp_weights(i) *
                        (X_op(dof_id).Evaluate({kernel_state, state}).Mean() *
                         std::conj(overlap))
                            .real();
              weight += xp_weights(i) * std::pow(std::abs(overlap), 2);
            }

            C.setIdentity();

            m = m / kHBar / weight;
            v = v / kHBar / weight;
            p_mean /= weight;
            x_mean /= weight;
            weight /= xp_weights.sum();

            CheckNan(weight, "weight");
            CheckNan(p_mean, "p_mean");
            CheckNan(x_mean, "p_mean");
            if ((m.array().isNaN().maxCoeff()) ||
                (v.array().isNaN().maxCoeff())) {
              std::cout << "[ERROR] m or v contain nans.\n";
            }

            double state_weight =
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2);

            CoM(0) = p_mean - state[n]->GetCenter(dof_id).P();
            CoM(1) = x_mean - state[n]->GetCenter(dof_id).X();

            // version 1
            J(0) = (*quasiprobability_bundle_).EvaluateMomentumFlow(n, dof_id);
            J(1) = (*quasiprobability_bundle_).EvaluatePositionFlow(n, dof_id);
            //       if (std::abs(overlap*overlap) > 0.01 * state_weight) {
            double flow_regularizer = std::exp(-100 * std::abs(weight) /
                                               (state_weight + 0.0000000001));
            // version 1
            x = v + m * J;

            // std::cout << n << ": p_mean=" << p_mean << ", j_p=" << J(0) <<
            // "x_mean=" << x_mean << ", j_x=" << J(1) << ".\n";

            // version 2
            // m = m - C;
            // M = m.transpose() * m;
            // V = -m.transpose() * v;
            // auto cond = M.trace(); // std::abs(V(0)) + std::abs(V(1));
            // M = M + 0.005 * cond * C;
            // x = M.inverse() * V;

            amplitude_regularizer_adjuster += flow_regularizer;
            if (flow_regularizer > 0.1) {
              std::cout.precision(std::numeric_limits<double>::max_digits10);
              std::cout << "[INFO] Flow regularizer is " << flow_regularizer
                        << " for n = " << n << ", t = " << t
                        << " with weight=" << state_weight
                        << ", dP_com = " << CoM(0) << ", dX_com = " << CoM(1)
                        << ", dP = " << x(0) << "dX = " << x(1)
                        << "\n. dp[0] = "
                        << der_t_state[0]->GetCenter(dof_id).P()
                        << ", dx[0] = " << der_t_state[0]->GetCenter(dof_id).X()
                        << ".\n";
            }

            // std::cout << "{" << n << ": " << x(0) << ", " << x(1) << "}";
            der_t_state[n]->GetCenter(dof_id).P(x(0));
            der_t_state[n]->GetCenter(dof_id).X(x(1));

            bool nan_flag =
                CheckNan(der_t_state[n]->GetCenter(dof_id).P(), "P") ||
                CheckNan(der_t_state[n]->GetCenter(dof_id).X(), "X");

            if (nan_flag) {
              std::cout << "<<[ERROR] nan detected for n = " << n << ".\n";
              std::cout << "<<[ERROR] nan detected for n = " << n
                        << ": <I>_state = "
                        << Id_op(dof_id).Evaluate({state}).Mean()
                        << ", : <I>_kernel_state = "
                        << Id_op(dof_id).Evaluate({kernel_state}).Mean()
                        << ".\n";
              std::flush(std::cout);
              der_t_state[n]->GetCenter(dof_id).X(0);
              der_t_state[n]->GetCenter(dof_id).P(0);
            }

            /*auto a = ccs_system_utilities::Covariance(
                         X_op(dof_id), P_op(dof_id), Id_op(dof_id),
                         {kernel_state, state})
                         .imag()-kHBar;
            auto b = -ccs_system_utilities::Covariance(
                          P_op(dof_id), P_op(dof_id), Id_op(dof_id),
                          {kernel_state, state})
                          .imag();
            auto c = ccs_system_utilities::Covariance(
                         X_op(dof_id), X_op(dof_id), Id_op(dof_id),
                         {kernel_state, state})
                         .imag();
            auto d = -ccs_system_utilities::Covariance(
                          P_op(dof_id), X_op(dof_id), Id_op(dof_id),
                          {kernel_state, state})
                          .imag() - kHBar;
            auto r = (a * d - b * c);

            // auto overlap = Id_op(dof_id).Evaluate({kernel_state,
            // state}).Mean();

            auto position_velocity =
                0.//(*quasiprobability_bundle_).EvaluatePositionFlow(n, dof_id)
                                ;

            double momentum_velocity =
                0.//(*quasiprobability_bundle_).EvaluateMomentumFlow(n, dof_id)
                                ;
            double pder_t_p = ccs_system_utilities::Covariance(
                                P_op(dof_id), hamiltonian_, Id_op(dof_id),
                                {kernel_state, state})
                                .imag();
            auto p_rhs = kHBar * momentum_velocity - pder_t_p;

            double pder_t_x = ccs_system_utilities::Covariance(
                                X_op(dof_id), hamiltonian_, Id_op(dof_id),
                                {kernel_state, state})
                                .imag();
            auto x_rhs = kHBar * position_velocity - pder_t_x;

            auto dp = (d * p_rhs - b * x_rhs) / r;
            auto dx = (-c * p_rhs + a * x_rhs) / r;

                        //std::cout << n << ": p : {" << momentum_velocity <<
            "," << pder_t_p << "}\n";
                        //std::cout << n << ": x : {" << position_velocity <<
            "," << pder_t_x << "}\n";

            //if (std::abs(dp - momentum_velocity) > 30) {
            //  //std::cout << "{" << n << ", " << dp << ", " <<
            momentum_velocity
            //  //          << "}";
            //  dp = momentum_velocity;
            //}
            //if (std::abs(dx - position_velocity) > 30) {
            //  //std::cout << "{" << n << ", " << dx << ", " <<
            position_velocity
            //  //          << "}";
            //  dx = position_velocity;
            //}

            der_t_state[n]->GetCenter(dof_id).P(dp);
            der_t_state[n]->GetCenter(dof_id).X(dx);

            // std::cout << "{" << der_t_state[n]->GetCenter(dof_id).P() << ", "
            // << der_t_state[n]->GetCenter(dof_id).X() << "}";

            bool nan_flag =
                CheckNan(a, "a") || CheckNan(b, "b") || CheckNan(c, "c") ||
                CheckNan(d, "d") || CheckNan(p_rhs, "p_hrs") ||
                CheckNan(x_rhs, "x_hrs") ||
                CheckNan(position_velocity, "v_x") ||
                CheckNan(momentum_velocity, "v_p") ||
                CheckNan(der_t_state[n]->GetCenter(dof_id).P(), "P") ||
                CheckNan(der_t_state[n]->GetCenter(dof_id).X(), "X");

            if (nan_flag) {
              std::cout << "<<[ERROR] nan detected for n = " << n
                        << ": num(P)=" << (d * p_rhs - b * x_rhs)
                        << ", num(X)=" << (-c * p_rhs + a * x_rhs)
                        << ", r=" << r << ".\n";
              std::cout << "<<[ERROR] nan detected for n = " << n
                        << ", a=" << a
                        << ", b=" << b << ", c=" << c << ", d=" << d << ".\n";
              std::cout << "<<[ERROR] nan detected for n = " << n
                        << ": <I>_state = "
                        << Id_op(dof_id).Evaluate({state}).Mean()
                        << ", : <I>_kernel_state = "
                        << Id_op(dof_id).Evaluate({kernel_state}).Mean()
                        << ".\n";
              std::flush(std::cout);
              der_t_state[n]->GetCenter(dof_id).X(position_velocity);
              der_t_state[n]->GetCenter(dof_id).P(momentum_velocity);
            }*/

            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }

          if (mma_flag) {
            auto amplitude = state[n]->GetMultiplier().Amplitude();
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                (std::abs(amplitude * amplitude) +
                 10000 * amplitude_regularizer_adjuster) *
                amplitude);
          }

          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);

          if (CheckNan(der_t_state[n]->GetMultiplier().Phase(), "phase")) {
            std::cout << "<<[ERROR] nan (phase) detected for n = " << n
                      << ".\n";
            std::flush(std::cout);
          }
          if (CheckNan(der_t_state[n]->GetMultiplier().Amplitude().imag(),
                       "amplitude(r)") ||
              CheckNan(der_t_state[n]->GetMultiplier().Amplitude().real(),
                       "amplitude(i)")) {
            std::cout << "<<[ERROR] nan (amplitude) detected for n = " << n
                      << ".\n";
            std::flush(std::cout);
          }
        }
        // *********** QUASIPROBABILISHCHE_V4 END ********************

        break;

      case BasisFunctionNature::LameBurghardtishche:

        // *********** LameBurghardtishche BEGIN ********************
        {
          double der_t_phase = 0;
          AddToIndex(n);
          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
            if (mma_flag) {
              // *** introducing MMA for momentum:
              der_t_state[n]->GetCenter(dof_id).P(
                  -H_der.DiffCenterX(dof_id, n)
                       .real()); // ???!!! taking real part! double-check in
                                 // tests that result is real
            }

            der_t_state[n]->GetCenter(dof_id).X(
                H_der.DiffCenterP(dof_id, n)
                    .real()); // ???!!! taking real part! double-check in tests
                              // that result is real
            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }

          if (mma_flag) {
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                state[n]->GetMultiplier().Amplitude() *
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
          }

          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);
        }
        // *********** LameBurghardtishche END   ********************

        break;
      case BasisFunctionNature::Garashchuchishche:

        // *********** Garashchuchishche BEGIN ********************
        {
          AddToIndex(n);
          double der_t_phase = 0;
          auto smoothing_kernel_op =
              ccs_system_utilities::GetPilotSmoothingKernel(
                  state, n, operator_factory_, smoothing_scale_);

          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
            double pilot_momentum =
                ccs_system_utilities::GetPilotWaveMomentum(
                    state, n, P_op(dof_id), smoothing_kernel_op)
                    .real();

            double pilot_velocity =
                ccs_system_utilities::GetPilotWaveVelocity(
                    state, n, hamiltonian_, X_op(dof_id), smoothing_kernel_op)
                    .real();

            if (mma_flag) {
              // *** introducing MMA for momentum:
              der_t_state[n]->GetCenter(dof_id).P(
                  momentum_mma_regularizer_ *
                  std::pow(pilot_momentum - state[n]->GetCenter(dof_id).P(),
                           3));
            }

            der_t_state[n]->GetCenter(dof_id).X(pilot_velocity);

            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }
          if (mma_flag) {
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                state[n]->GetMultiplier().Amplitude() *
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
          }
          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);
        }
        // *********** Garashchuchishche END   ********************

        break;

      case BasisFunctionNature::Garahardtishche:

        // *********** Garahardtishche BEGIN ********************
        {
          double der_t_phase = 0;
          AddToIndex(n);
          auto smoothing_kernel_op =
              ccs_system_utilities::GetPilotSmoothingKernel(
                  state, n, operator_factory_, smoothing_scale_);
          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
            if (mma_flag) {
              // *** introducing MMA for momentum:
              double pilot_momentum =
                  ccs_system_utilities::GetPilotWaveMomentum(
                      state, n, P_op(dof_id), smoothing_kernel_op)
                      .real();
              der_t_state[n]->GetCenter(dof_id).P(
                  momentum_mma_regularizer_ *
                  std::pow(pilot_momentum - state[n]->GetCenter(dof_id).P(),
                           3));
            }

            der_t_state[n]->GetCenter(dof_id).X(
                H_der.DiffCenterP(dof_id, n)
                    .real()); // ???!!! taking real part! double-check in tests
                              // that result is real
            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }

          if (mma_flag) {
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                state[n]->GetMultiplier().Amplitude() *
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
          }

          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);
        }
        // *********** Garahardtishche END   ********************

        break;

      case BasisFunctionNature::ShaloBohmishche:
        // *********** ShaloBohmishche BEGIN ********************
        {
          auto smoothing_kernel_op =
              ccs_system_utilities::GetPilotSmoothingKernel(
                  state, n, operator_factory_, smoothing_scale_);

          // auto smoothing_kernel_op = GetPilotSmoothingKernel(state, n);
          ccs_system_utilities::PilotWaveQuantumForceEvaluator
              quantum_force_evaluator(state, n, hamiltonian_, operator_factory_,
                                      smoothing_kernel_op);
          double der_t_phase = 0;
          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
            auto der_t_P_bohm = quantum_force_evaluator(dof_id);

            der_t_state[n]->GetCenter(dof_id).P(
                -H_der.DiffCenterX(dof_id, n).real()         // classical force
                + (isnan(der_t_P_bohm) ? 0 : der_t_P_bohm)); // quantum force
            der_t_state[n]->GetCenter(dof_id).X(
                H_der.DiffCenterP(dof_id, n)
                    .real()); // ???!!! taking real part! double-check in tests
                              // that result is real
            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }

          if (mma_flag) {
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                state[n]->GetMultiplier().Amplitude() *
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
          }

          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);
        }
        // *********** ShaloBohmishche END ********************
        break;
      case BasisFunctionNature::BlueShaloBohmishche:
        // *********** BlueShaloBohmishche BEGIN ********************
        {
          auto smoothing_kernel_op =
              ccs_system_utilities::GetPilotSmoothingKernel(
                  state, n, operator_factory_, smoothing_scale_);
          double der_t_phase = 0;
          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
            double pilot_momentum =
                ccs_system_utilities::GetPilotWaveMomentum(
                    state, n, P_op(dof_id), smoothing_kernel_op)
                    .real();

            double pilot_velocity =
                ccs_system_utilities::GetPilotWaveVelocity(
                    state, n, hamiltonian_, X_op(dof_id), smoothing_kernel_op)
                    .real();

            der_t_state[n]->GetCenter(dof_id).P(
                momentum_mma_regularizer_ *
                std::pow(pilot_momentum - state[n]->GetCenter(dof_id).P(), 1));
            // der_t_state[n]->GetCenter(dof_id).X(
            //	H_der.DiffCenterP(dof_id, n)
            //	.real());
            der_t_state[n]->GetCenter(dof_id).X(pilot_velocity);
            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }

          if (mma_flag) {
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                state[n]->GetMultiplier().Amplitude() *
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
          }

          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);
        }
        // *********** BlueShaloBohmishche END ********************
        break;
      case BasisFunctionNature::ShaloBohmishche_v2:
        // *********** ShaloBohmishche_v2 BEGIN ********************
        {
          double der_t_phase = 0;
          auto smoothing_kernel_op =
              ccs_system_utilities::GetPilotSmoothingKernel(
                  state, n, operator_factory_, smoothing_scale_);
          // auto square_momentum_kernel_op =
          // ccs_system_utilities::GetPilotSmoothingKernel(
          //  state, n, operator_factory_, 1); // lets try wider kernel with
          //  smoothing scale = 1
          ccs_system_utilities::PilotWaveQuantumForceEvaluator
              quantum_force_evaluator(state, n, hamiltonian_, operator_factory_,
                                      smoothing_kernel_op);

          for (size_t dof_id = 0; dof_id < number_of_spatial_dofs; dof_id++) {
            std::complex<double> pilot_momentum =
                ccs_system_utilities::GetPilotWaveMomentum(
                    state, n, P_op(dof_id), smoothing_kernel_op);

            std::complex<double> pilot_velocity =
                ccs_system_utilities::GetPilotWaveVelocity(
                    state, n, hamiltonian_, X_op(dof_id), smoothing_kernel_op);

            auto squared_momentum_der =
                GetApproximateMaterialTimeDerivativeOfSquaredPilotWaveMomentum(
                    state, hamiltonian_, P_op(dof_id), Id_op(dof_id),
                    smoothing_kernel_op, pilot_velocity.real());

            double pilot_momentum_squared = squared_momentum_der.first;
            double pilot_momentum_squared_der = squared_momentum_der.second;

            auto alpha = state.Configuration().GaussianQuadraticCoefficient();
            auto k_alpha = alpha / smoothing_scale_ / smoothing_scale_;
            auto effective_momentum_square =
                // std::max(0.,
                (pilot_momentum_squared - 2 * kHBar * kHBar * alpha *
                                              (alpha + k_alpha) /
                                              (2. * alpha + k_alpha)
                 // kHBar * kHBar * alpha
                );
            auto effective_momentum_amplitude =
                std::sqrt(std::abs(effective_momentum_square));

            // auto effective_momentum_derivative =
            //    (effective_momentum_square * (n % 2) > 0 ? 1 : -1) *
            //    pilot_momentum_squared_der / /*effective_momentum_amplitude*/
            //    std::sqrt(std::abs(pilot_momentum_squared)) / 2.;

            der_t_state[n]->GetCenter(dof_id).P((
                momentum_mma_regularizer_ *
                std::pow(((n % 2) > 0 ? 1 : -1) * effective_momentum_amplitude -
                             state[n]->GetCenter(dof_id).P(),
                         1))); // quantum damper to averaged squared momentum

            der_t_state[n]->GetCenter(dof_id).X(pilot_velocity.real());
            der_t_phase += state[n]->GetCenter(dof_id).P() *
                           der_t_state[n]->GetCenter(dof_id).X();
          }

          if (mma_flag) {
            der_t_state[n]->GetMultiplier().Amplitude(
                -amplitude_mma_regularizer_ *
                state[n]->GetMultiplier().Amplitude() *
                std::pow(std::abs(state[n]->GetMultiplier().Amplitude()), 2));
          }

          der_t_state[n]->GetMultiplier().Phase(
              (der_t_phase - H_der(n, n).real()) / kHBar);
        }
        // *********** ShaloBohmishche_v2 END ********************
        break;

      default:
        QI_Error::RaiseError(
            "[ERROR] CCSSchrodingerWerewolfSystem::operator() - basis function "
            "#" +
            std::to_string(n) +
            ": the beast of given nature is not supported yet.");
      }
    }

    if (der_t_state.CheckForNans()) {
      QI_Error::RaiseError(
          "[ERROR] CCSSchrodingerWerewolfSystem::operator() - nans found in "
          "der_t_state after intitial initialization.");
    }

    auto eta_ket_op = [&mma_flag, &state, &P, &X, &Amplitude, &Id_op, &X_op,
                       &P_op, &alpha, &der_t_state, &variational_momenta,
                       this](size_t j) -> Operator<PolicyType> {
      auto output = hamiltonian_ * (-I / kHBar);
      for (size_t dof_id_j = 0;
           dof_id_j < state.Configuration().NumberOfSpatialDOFs(); dof_id_j++) {
        output += (I / kHBar) * P(state, j, dof_id_j) *
                  X(der_t_state, j, dof_id_j) * Id_op(dof_id_j);
        output -= 2. * X(der_t_state, j, dof_id_j) * alpha(dof_id_j) *
                  (X_op(dof_id_j) - X(state, j, dof_id_j) * Id_op(dof_id_j));

        if (mma_flag || (variational_momenta.find({dof_id_j, j}) ==
                         variational_momenta.end())) {
          output -= P(der_t_state, j, dof_id_j) *
                    (operator_factory_.CreateX(dof_id_j) -
                     X(state, j, dof_id_j) * Id_op(dof_id_j)) *
                    (I / kHBar);
        }
      }

      return output;
    };

    // updating multiplier

    // ???!!! Unfortunately, self adjoint Eigen solver seems to not work with
    // row-major matrices
    // Eigen::MatrixXcd

    using MatrixType = MatrixXd_c; // MatrixXd_r
    using BlockType = MatrixType::BlockXpr;
    using VectorBlockType = std::vector<std::shared_ptr<BlockType>>;
    MatrixType A(state.Size() * 2 + variational_momenta.size(),
                 state.Size() * 2 + variational_momenta.size());
    VectorXd_c b(state.Size() * 2 + variational_momenta.size());

    // Finding the remaining amplitude variations as a solution of linear system
    // A.x=b initializing A

    size_t slicing_offsets[] = {
        0, state.Size(), (size_t)(b.size()) - state.Size(), (size_t)b.size()};
    auto ABlock = [&A, &slicing_offsets](int i, int j) {
      return A.block(slicing_offsets[i - 1], slicing_offsets[j - 1],
                     slicing_offsets[i] - slicing_offsets[i - 1],
                     slicing_offsets[j] - slicing_offsets[j - 1]);
    };
    auto bBlock = [&b, &slicing_offsets](int i) {
      return b.segment(slicing_offsets[i - 1],
                       slicing_offsets[i] - slicing_offsets[i - 1]);
    };

    auto A11 = ABlock(1, 1);
    auto A33 = ABlock(3, 3);
    auto A13 = ABlock(1, 3);

    auto A12 = ABlock(1, 2);
    auto A32 = ABlock(3, 2);

    auto A_evaluator = linear_solve_kernel_.Evaluate({state});

    for (size_t i = 0, state_size = state.Size(); i < state_size; ++i) {
      for (size_t j = 0; j < state_size; ++j) {
        std::complex<double> chi_ij =
            A_evaluator(i, j) *
            std::exp(I * (Phase(state, j) - Phase(state, i)));
        A11(i, j) = chi_ij.real();
        A33(i, j) = chi_ij.real() * Amplitude(state, i) * Amplitude(state, j);
        A13(i, j) = (I * chi_ij).real() * Amplitude(state, j);
      }

      size_t idx_j2 = 0;
      for (auto &p_it : variational_momenta) {
        const auto &j = p_it.BasisId();
        const auto &dof_id_j = p_it.DofId();
        auto xi_ij = Id_der_k.DiffKetCenterP(dof_id_j, i, j) *
                     std::exp(I * (Phase(state, j) - Phase(state, i)));
        // setting A12 matrix element
        A12(i, idx_j2) = xi_ij.real() * Amplitude(state, j);
        // setting A32 matrix element
        A32(i, idx_j2) =
            (-I * xi_ij).real() * Amplitude(state, i) * Amplitude(state, j);
        idx_j2++;
      }
    }

    ABlock(3, 1) = A13.transpose();

    if (variational_momenta.size() > 0) {
      ABlock(2, 1) = A12.transpose();
      ABlock(2, 3) = A32.transpose();

      auto A22 = ABlock(2, 2);
      size_t idx_i = 0;
      for (auto &p_it_i : variational_momenta) {
        size_t idx_j = 0;
        const auto &i = p_it_i.BasisId();
        const auto &dof_id_i = p_it_i.DofId();
        for (auto &p_it_j : variational_momenta) {
          const auto &j = p_it_j.BasisId();
          const auto &dof_id_j = p_it_j.DofId();
          auto scpr_xi = Amplitude(state, i) * Amplitude(state, j) *
                         (std::exp(I * (Phase(state, j) - Phase(state, i)))) *
                         (xi_bra_op(i, dof_id_i) * linear_solve_kernel_ *
                          xi_ket_op(j, dof_id_j))
                             .Evaluate({state})(i, j);
          A22(idx_i, idx_j) = scpr_xi.real();
          idx_j++;
        }
        idx_i++;
      }
    }

    // regularizing A
    for (Eigen::Index i = 0; i < A.cols() - (Eigen::Index)state.Size(); i++) {
      A(i, i) += amplitude_max_regularizer_;
    }
    for (Eigen::Index i = A.cols() - (Eigen::Index)state.Size(); i < A.cols();
         i++) {
      A(i, i) += amplitude_max_regularizer_ * regularize_phase_scale_;
    }

    // std::cout << "\n!\n" << A << "\n!\n";

    // initializing b1 and b3
    auto chi_i_xi_j = [&](SpatialDOF_id dof_id, size_t i, size_t j) {
      return Id_der_k.DiffKetCenterP(dof_id, i, j);
    };

    // initializing b
    auto b1 = bBlock(1);
    auto b3 = bBlock(3);

    auto chi_i_eta_j = [&](size_t i, size_t j) {
      auto result = -I / kHBar * H_k(i, j);
      for (size_t dof_id_j = 0; dof_id_j < number_of_spatial_dofs; dof_id_j++) {

        if (mma_flag || (variational_momenta.find({dof_id_j, j}) ==
                         variational_momenta.end())) {
          result -= P(der_t_state, j, dof_id_j) *
                    Id_der_k.DiffKetCenterP(dof_id_j, i, j);
        }
        result -= X(der_t_state, j, dof_id_j) *
                  Id_der_k.DiffKetCenterX(dof_id_j, i, j);
      }
      return result;
    };

    for (size_t m = 0, state_size = state.Size(); m < state_size; m++) {
      b1(m) = 0;
      b3(m) = 0;
      for (size_t n = 0; n < state_size; n++) {
        std::complex<double> b_stub =
            std::exp(-I * (Phase(state, m) - Phase(state, n))) *
            ((+(mma_flag ? -Amplitude(der_t_state, n) : 0.) +
              Amplitude(state, n) * I * FastPhase(der_t_state, n)) *
                 Id_der_k(m, n) -
             Amplitude(state, n) * chi_i_eta_j(m, n));
        b1(m) -= b_stub.real();
        b3(m) -= Amplitude(state, m) * (-I * b_stub).real();
      }
    }

    // initializing b2
    auto b2 = bBlock(2);
    {
      size_t idx_i = 0;
      for (auto &p_it_i : variational_momenta) {
        b2(idx_i) = 0;
        const auto &i = p_it_i.BasisId();
        const auto &dof_id_i = p_it_i.DofId();
        for (size_t j = 0, state_size = state.Size(); j < state_size; ++j) {
          auto eta_op_i =
              (xi_bra_op(i, dof_id_i) * linear_solve_kernel_ *
               (-eta_ket_op(j) * Amplitude(state, j) +
                Id_op(dof_id_i) *
                    (Amplitude(state, j) * I * FastPhase(der_t_state, j) +
                     (mma_flag ? -Amplitude(der_t_state, j) : 0.))))
                  .Evaluate({state})(i, j);

          b2(idx_i) -=
              Amplitude(state, i) *
              (std::exp(I * (Phase(state, j) - Phase(state, i))) * eta_op_i)
                  .real();
        }
        idx_i++;
      }
    }

    // std::cout << "\n!b:\n" << b << "\n!\n";

    using namespace Eigen;

    if ((A.array().isNaN().maxCoeff()) || (b.array().isNaN().maxCoeff())) {
      std::cout << "[INFO] Linear system contains nans.\n";
    }

    coeffs = // A.llt().solve(b);// Doesn't work in most cases.
        A.colPivHouseholderQr().solve(b);

    if (coeffs.array().isNaN().maxCoeff()) {
      size_t nan_count = 0;
      for (Eigen::Index i = 0; i < coeffs.size(); i++) {
        if (std::isnan(coeffs(i))) {
          coeffs(i) = 0;
          nan_count++;
        }
      }
      std::cout << "[INFO] Linear solver produced " << nan_count << " nans.\n";
      std::flush(std::cout);
    }
    VectorXc_c v = (A * coeffs - b);
    linear_solve_error = std::abs((v.transpose() * v)(0, 0));
  } while ((linear_solve_error > kTolerance) && (attempts_count < max_retries));

  // std::cout << "\n!coeffs:\n" << coeffs << "\n!\n";

  if ((max_retries > 1) && (attempts_count >= max_retries)) {
    std::cout << "\n[INFO] Max retries limit of " << max_retries
              << " exceeded. Linear solve error is still " << linear_solve_error
              << ". Continuing anyway...\n";
  };

  const auto &d_ampl = coeffs.segment(0, state.Size());
  const auto &d_variational_momenta =
      coeffs.segment(state.Size(), variational_momenta.size());
  const auto &d_slow_phase =
      coeffs.segment(state.Size() + variational_momenta.size(), state.Size());

  for (size_t m = 0; m < state.Size(); m++) {
    auto der_t_mma_ampl = der_t_state[m]->GetMultiplier().Amplitude();
    auto der_t_ampl = I * d_slow_phase(m) * CplxAmplitude(state, m) +
                      (d_ampl(m)) * std::exp(I * SlowPhase(state, m));

    if (CheckNan(der_t_ampl.real(), "der_t_ampl.real") ||
        CheckNan(der_t_ampl.real(), "der_t_ampl.real")) {
      std::cout << "[INFO] - error in slow amplitude for n = " << m << ": "
                << "var-l phase = " << d_slow_phase(m)
                << ", var-l amplitude = " << d_ampl(m)
                << ", amplitude = " << CplxAmplitude(state, m)
                << ", slow phase = " << SlowPhase(state, m) << ".\n";
    }

    der_t_state[m]->GetMultiplier().Amplitude(
        der_t_ampl + der_t_mma_ampl /*** this term is MMA for amplitude*/);
  }

  {
    size_t idx_i = 0;
    for (auto &p_it_i : variational_momenta) {
      auto &der_t_psp_ref =
          der_t_state[p_it_i.BasisId()]->GetCenter(p_it_i.DofId());
      der_t_psp_ref.P(/*** this term is MMA for momentum*/ (
                          mma_flag ? der_t_psp_ref.P() : 0) +
                      d_variational_momenta(idx_i));
      idx_i++;
    }
  }

  if (der_t_state.CheckForNans()) {
    QI_Error::RaiseError("[ERROR] CCSSchrodingerWerewolfSystem::operator() - "
                         "nans found in der_t_state before self-projection.");
  }

  if (reprojection_regularizer_ > 0) {
    auto state_copy = state.Clone(false);
    ccs_utilities::Project(state_copy, state, Id_op(0), true,
                           reprojection_regularizer_);
    auto norm = (Id_op(0).Evaluate({state, state_copy}).Mean() /
                 Id_op(0).Evaluate(state).Mean());

    for (size_t i = 0; i < state.Size(); i++) {
      der_t_state[i]->GetMultiplier().Amplitude(
          der_t_state[i]->GetMultiplier().Amplitude() +
          (state_copy[i]->GetMultiplier().Value() -
           norm * state[i]->GetMultiplier().Value()) *
              std::exp(-I * state[i]->GetMultiplier().Phase()));
    }
  }

  auto self_projection =
      std::abs(ccs_system_utilities::WavefunctionDerivativeSelfProjection(
                   state, der_t_state, operator_factory_)
                   .real());

  if (self_projection > 1.e-4) {
    std::cout << "*";
    // std::cout << "\n Look at this beautiful self-projection: "
    //          << self_projection;
  }

  auto normalizer = 1. / state_norm * self_projection;

  for (size_t m = 0, state_size = state.Size(); m < state_size; m++) {
    auto new_value = der_t_state[m]->GetMultiplier().Amplitude() -
                     normalizer * state[m]->GetMultiplier().Amplitude();
    der_t_state[m]->GetMultiplier().Amplitude(new_value);
  }

  if (der_t_state.CheckForNans()) {
    QI_Error::RaiseError("[ERROR] CCSSchrodingerWerewolfSystem::operator() - "
                         "nans found in der_t_state after self-projection.");
  }

  // auto self_projection2 =
  //    std::abs(ccs_system_utilities::WavefunctionDerivativeSelfProjection(
  //                 state, der_t_state, operator_factory_)
  //                 .real());
}

void CCSSchrodingerWerewolfSystem::SetSmoothingScale(double smoothing_scale) {
  smoothing_scale_ = smoothing_scale;
}

void CCSSchrodingerWerewolfSystem::SetRegularizer(
    double reference_amplitude_max_regularizer) {
  reference_amplitude_max_regularizer_ = reference_amplitude_max_regularizer_;
  amplitude_max_regularizer_ = reference_amplitude_max_regularizer_;
};

// Operator<CCSSchrodingerWerewolfSystem::PolicyType>
// CCSSchrodingerWerewolfSystem::GetPilotSmoothingKernel(
//    const Wavefunction &state, size_t basis_id,
//    double optional_additional_scale) const {
//  auto number_of_spatial_dofs = state.Configuration().NumberOfSpatialDOFs();
//  auto alpha_scaled = [&optional_additional_scale, &state,
//                       this](SpatialDOF_id dof_id) {
//    return optional_additional_scale *
//           state.Configuration().GaussianQuadraticCoefficient(dof_id) /
//           smoothing_scale_ / smoothing_scale_;
//  };
//
//  auto X = [&state, &basis_id, this](SpatialDOF_id dof_id) {
//    return state[basis_id]->GetCenter(dof_id).X() *
//           operator_factory_.CreateId(dof_id);
//  };
//  auto X_op = [this](SpatialDOF_id dof_id) {
//    return operator_factory_.CreateX(dof_id);
//  };
//
//  auto Multiplier = [&state](size_t basis_id) {
//    return state[basis_id]->GetMultiplier().Value();
//  };
//
//  auto exp_op = [&](SpatialDOF_id dof_id) {
//    return operator_factory_.CreateExp(-alpha_scaled(dof_id) *
//                                       (X_op(dof_id) - X(dof_id)) *
//                                       (X_op(dof_id) - X(dof_id)));
//  };
//  if (number_of_spatial_dofs < 1) {
//    QI_Error::RaiseError(
//        "[ERROR] CCSSchrodingerSystemPiloted::_GetPilotWaveMomentum - "
//        "number_of_spatial_dofs < 1");
//  };
//
//  auto smoothing_kernel_op = exp_op(0);
//  for (size_t dof_id_i = 1; dof_id_i < number_of_spatial_dofs; dof_id_i++) {
//    smoothing_kernel_op *= exp_op(dof_id_i);
//  }
//  return smoothing_kernel_op;
//};

} // namespace algorithm
} // namespace mce
