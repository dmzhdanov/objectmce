#include "stdafx.h"

#include "Algorithms/CCSPilotWaveUtilities.h"
#include "Algorithms/CCSQuasiprobabilityGaugeBundle.h"

namespace mce {
namespace quasiprobability_gauge_auxiliaries {

CCSQuasiprobabilityGaugeBundle::CCSQuasiprobabilityGaugeBundle(
    const OperatorFactory<PolicyType> &operator_factory,
    const OperatorType &hamiltonian, const SpatialDOF_id number_of_dofs,
    double pilot_smoothing_scale, const CanonicPair &width_factors,
    unsigned approximation_order)
    : operator_factory_(operator_factory), number_of_dofs_(number_of_dofs),
      pilot_smoothing_scale_(pilot_smoothing_scale),
      velocities_(number_of_dofs), momenta_(number_of_dofs),
      identity_(std::make_shared<OperatorType>(operator_factory_.CreateId(0))),
      strategy_(std::make_shared<EvaluatorStrategyType>(
          0, 0, // mean momentum and mean momentum derivative
          evaluator_strategy::QuasiprobabilityGaugeType::Momentum,
          width_factors, 0 /*center_basis_function_id*/, approximation_order)) {
  for (SpatialDOF_id dof_id = 0; dof_id < number_of_dofs; dof_id++) {
    auto X = operator_factory_.CreateX(dof_id);
    auto P = operator_factory_.CreateP(dof_id);
    momenta_[dof_id] = std::make_shared<OperatorType>(P);
    velocities_[dof_id] = std::make_shared<OperatorType>(
        (1. / I / kHBar) * (X * hamiltonian - hamiltonian * X));
  }
};

void CCSQuasiprobabilityGaugeBundle::Initialize(Wavefunction wavefunction) {
  using namespace evaluator_strategy;
  wavefunction_ = std::move(wavefunction.ShallowCopy());
  strategy_->FlowType(QuasiprobabilityGaugeType::Momentum);
  velocities_mom_ev_.resize(velocities_.size());
  for (SpatialDOF_id dof_id = 0; dof_id < velocities_.size(); dof_id++) {
    velocities_mom_ev_[dof_id] =
        std::make_shared<EvaluatorType>(velocities_[dof_id]->Evaluate(
            {wavefunction, strategy_->ShallowCopy()}));
  }

  strategy_->FlowType(QuasiprobabilityGaugeType::Position);
  norm_ev_ = std::make_shared<EvaluatorType>(
      identity_->Evaluate({wavefunction, strategy_->ShallowCopy()}));
  velocities_pos_ev_.resize(velocities_.size());
  for (SpatialDOF_id dof_id = 0; dof_id < velocities_.size(); dof_id++) {
    velocities_pos_ev_[dof_id] =
        std::make_shared<EvaluatorType>(velocities_[dof_id]->Evaluate(
            {wavefunction, strategy_->ShallowCopy()}));
  };
}

void CCSQuasiprobabilityGaugeBundle::Reset() {
  velocities_mom_ev_.resize(0);
  velocities_pos_ev_.resize(0);
  norm_ev_ = 0;
}

bool CCSQuasiprobabilityGaugeBundle::IsInitialized() {
  return !(norm_ev_ == 0);
}

double CCSQuasiprobabilityGaugeBundle::EvaluatePositionFlow(
    size_t center_basis_function_id, SpatialDOF_id dof_id) {

  auto local_kernel_op = ccs_system_utilities::GetPilotSmoothingKernel(
      wavefunction_, center_basis_function_id, operator_factory_,
      pilot_smoothing_scale_);
  OperatorType &P = (*momenta_[dof_id]);
  OperatorType local_momentum_op = local_kernel_op * P;
  auto local_norm = local_kernel_op.Evaluate({wavefunction_}).Mean();
  auto local_momentum =
      (local_momentum_op.Evaluate({wavefunction_}).Mean() / local_norm).real();

  strategy_->CenterBasisFunction(center_basis_function_id);
  //???!!!??? remove zero!!!
  strategy_->MeanMomentum(local_momentum);

  auto norm = norm_ev_->Mean().real();
  auto position_flow = velocities_pos_ev_[dof_id]->Mean().real();
  return (position_flow / norm);
};

// double CCSQuasiprobabilityGaugeBundle::EvaluatePositionFlowTest(
//	size_t center_basis_function_id, SpatialDOF_id dof_id) {
//
//	strategy_->CenterBasisFunction(center_basis_function_id);
//	//???!!!??? remove zero!!!
//	strategy_->MeanMomentum(0.);
//
//	auto norm = norm_ev_->Mean().real();
//	auto position_flow = velocities_pos_ev_[dof_id]->Mean().real();
//	return (position_flow / norm);
//};

double CCSQuasiprobabilityGaugeBundle::EvaluateMomentumFlow(
    size_t center_basis_function_id, SpatialDOF_id dof_id) {

  auto local_kernel_op = ccs_system_utilities::GetPilotSmoothingKernel(
      wavefunction_, center_basis_function_id, operator_factory_,
      pilot_smoothing_scale_);
  OperatorType &P = (*momenta_[dof_id]);
  OperatorType local_momentum_op = local_kernel_op * P;
  OperatorType local_norm_x_derivative_op =
      (1. / (I * kHBar)) * (P * local_kernel_op - local_kernel_op * P);
  OperatorType local_momentum_x_derivative_op = local_norm_x_derivative_op * P;
  auto local_norm = local_kernel_op.Evaluate({wavefunction_}).Mean();
  auto local_momentum =
      (local_momentum_op.Evaluate({wavefunction_}).Mean() / local_norm).real();
  auto local_momentum_x_derivative =
      ((local_momentum_x_derivative_op.Evaluate({wavefunction_}).Mean().real() -
        local_momentum * local_norm_x_derivative_op.Evaluate({wavefunction_})
                             .Mean()
                             .real()) /
       local_norm)
          .real();

  strategy_->CenterBasisFunction(center_basis_function_id);
  strategy_->MeanMomentum(local_momentum);
  strategy_->MeanMomentumXDerivative(local_momentum_x_derivative);

  // std::cout << "n=" << center_basis_function_id << ": lm=" << local_momentum
  // << ", lmd=" << local_momentum_x_derivative << "\n";

  auto norm = norm_ev_->Mean().real();
  auto momentum_flow = velocities_mom_ev_[dof_id]->Mean().real();
  return (momentum_flow / norm);
};

double CCSQuasiprobabilityGaugeBundle::EvaluateHusimiFunction(
    size_t center_basis_function_id) {
  strategy_->CenterBasisFunction(center_basis_function_id);
  return norm_ev_->Mean().real();
}

} // namespace quasiprobability_gauge_auxiliaries
} // namespace mce
