#include "stdafx.h"

#include <boost/math/special_functions/erf.hpp>
#include <math.h>
#include <random>

#include "Algorithms/Conversions.h"
#include "Configuration.h"
#include "Wavefunction/CanonicPair.h"
#include "Wavefunction/EhrenfestBasisFunction.h"
#include "WavefunctionFactory.h"

namespace mce {
WavefunctionFactory::WavefunctionFactory(size_t num_pess,
                                         size_t num_spatial_dofs,
                                         double gaussian_width,
                                         double gaussian_overlap_cutoff)
    : configuration_(std::make_shared<Configuration>(num_pess, num_spatial_dofs,
                                                     gaussian_width,
                                                     gaussian_overlap_cutoff)) {
}

WavefunctionFactory::~WavefunctionFactory() {}

Wavefunction WavefunctionFactory::Create(size_t size) {
  return Wavefunction(configuration_, size);
};

Wavefunction WavefunctionFactory::CreateUniformSwarm(
    const PES_id pes_id, const PhaseSpacePoint &center,
    const PhaseSpacePoint &spread, const PhaseSpacePoint &step_size) {
  //???!!! no checks for negative spread and step size!

  // center.Print(std::cout);
  // spread.Print(std::cout);
  // step_size.Print(std::cout);

  size_t number_of_dofs = configuration_->NumberOfSpatialDOFs();
  if ((number_of_dofs != center.NumberOfDOFs()) ||
      (number_of_dofs != spread.NumberOfDOFs()) ||
      (number_of_dofs != step_size.NumberOfDOFs())) {
    QI_Error::RaiseError(
        "[ERROR] WavefunctionBasisFactory::CreateUniformSwarm - "
        "inconsistent input spatiad dimensions: configuration requires " +
        std::to_string(number_of_dofs) + "dimensions. Found: <" +
        std::to_string(center.NumberOfDOFs()) + " | " +
        std::to_string(spread.NumberOfDOFs()) + " | " +
        std::to_string(step_size.NumberOfDOFs()) + ">.");
  }
  if (/*pes_id != 0 && */ (configuration_->NumberOfPESs() <= pes_id)) {
    QI_Error::RaiseError(
        "[ERROR] WavefunctionBasisFactory::CreateUniformSwarm - "
        " PES_id = " +
        std::to_string(pes_id) + " >= configuration's NumberOfPESs().");
  }
  Wavefunction wavefunction(configuration_);
  ElectronicWavefunction electronic_wavefunction(
      configuration_->NumberOfPESs());
  for (auto &coefficient : electronic_wavefunction) {
    coefficient.SetZero();
  }
  electronic_wavefunction[pes_id].amplitude_ = 1;

  PhaseSpacePoint min_bound(number_of_dofs);
  PhaseSpacePoint current_point(number_of_dofs);
  for (size_t i = 0; i < number_of_dofs; ++i) {
    min_bound[i].P(center[i].P() -
                   std::floor(2 * spread[i].P() / step_size[i].P()) *
                       step_size[i].P() / 2);
    min_bound[i].X(center[i].X() -
                   std::floor(2 * spread[i].X() / step_size[i].X()) *
                       step_size[i].X() / 2);
  }
  if (number_of_dofs == 0) {
    return wavefunction;
  }
  Eigen::Index j = number_of_dofs - 1;
  current_point[j].X(min_bound[j].X());
  do {
    while (current_point[j].X() <= center[j].X() + spread[j].X() + kTolerance) {
      {
        Eigen::Index i = number_of_dofs - 1;
        current_point[i].P(min_bound[i].P());
        while (i >= 0) {
          while (current_point[i].P() <
                 center[i].P() + spread[i].P() + kTolerance) {
            wavefunction.PushBack(std::make_shared<EhrenfestBasisFunction>(
                current_point, electronic_wavefunction));
            current_point[i].P(current_point[i].P() + step_size[i].P());
          }
          while ((i >= 0) && ((current_point[i].p_ += step_size[i].P()) >=
                              center[i].P() + spread[i].P() + kTolerance)) {
            current_point[i].P(min_bound[i].P());
            --i;
          };
        }
      }
      current_point[j].x_ += step_size[j].X();
    }
    while (((current_point[j].x_ += step_size[j].X()) >=
            center[j].X() + spread[j].X() + kTolerance) &&
           (--j >= 0)) {
      current_point[j].X(min_bound[j].X());
    };
  } while (j > 0);
  return wavefunction;
};

Wavefunction WavefunctionFactory::CreateUniformSwarm2(
    const PES_id pes_id, const Eigen::Ref<const MatrixXd_r> &center,
    const Eigen::Ref<const MatrixXd_r> &spread,
    const Eigen::Ref<const MatrixXd_r> &step_size) {
  using namespace utilities;
  return CreateUniformSwarm(pes_id, PhaseSpacePointFromMatrix(center),
                            PhaseSpacePointFromMatrix(spread),
                            PhaseSpacePointFromMatrix(step_size));
}

Wavefunction WavefunctionFactory::CreateRandomSwarm(
    const PES_id pes_id, const PhaseSpacePoint &center,
    const PhaseSpacePoint &spread, size_t basis_size, unsigned long random_seed) {
  std::uniform_real_distribution<double> unif(-1, 1);
  std::default_random_engine re;
  if (random_seed != 0) {
    re.seed(random_seed);
  }
  double a_random_double = unif(re);
  auto get_rand = [&unif, &re]() {
    return boost::math::erf_inv(unif(re)) * std::sqrt(2.);
  };

  size_t number_of_dofs = configuration_->NumberOfSpatialDOFs();
  if ((number_of_dofs != center.NumberOfDOFs()) ||
      (number_of_dofs != spread.NumberOfDOFs())) {
    QI_Error::RaiseError(
        "[ERROR] WavefunctionBasisFactory::CreateUniformSwarm - "
        "inconsistent input spatiad dimensions: configuration requires " +
        std::to_string(number_of_dofs) + "dimensions. Found: <" +
        std::to_string(center.NumberOfDOFs()) + " | " +
        std::to_string(spread.NumberOfDOFs()) + ">.");
  }
  if (/*pes_id != 0 && */ (configuration_->NumberOfPESs() <= pes_id)) {
    QI_Error::RaiseError(
        "[ERROR] WavefunctionBasisFactory::CreateUniformSwarm - "
        " PES_id = " +
        std::to_string(pes_id) + " >= configuration's NumberOfPESs().");
  }

  Wavefunction wavefunction(configuration_);
  ElectronicWavefunction electronic_wavefunction(
      configuration_->NumberOfPESs());
  PhaseSpacePoint current_point(number_of_dofs);
  for (size_t n = 0; n < basis_size; n++) {
    for (size_t i = 0; i < number_of_dofs; ++i) {
      current_point[i].P(center[i].P() + get_rand() * spread[i].P());
      current_point[i].X(center[i].X() + get_rand() * spread[i].X());
    }
    wavefunction.PushBack(std::make_shared<EhrenfestBasisFunction>(
        current_point, electronic_wavefunction));
  }
  return wavefunction;
}

Wavefunction WavefunctionFactory::CreateRandomSwarm2(
    const PES_id pes_id, const Eigen::Ref<const MatrixXd_r> &center,
    const Eigen::Ref<const MatrixXd_r> &spread, size_t basis_size, unsigned long random_seed) {
  using namespace utilities;
  return CreateRandomSwarm(pes_id, PhaseSpacePointFromMatrix(center),
                           PhaseSpacePointFromMatrix(spread), basis_size,random_seed);
}

} // namespace mce
