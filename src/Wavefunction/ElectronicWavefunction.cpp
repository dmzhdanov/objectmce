#include "stdafx.h"

#include "Wavefunction/ElectronicWavefunction.h"

namespace mce {
// ElectronicWavefunction::ElectronicWavefunction(size_t num_states)
//    : coefficients_(num_states){};

size_t ElectronicWavefunction::NumberOfPESs() const {
  return coefficients_.size();
};

ElectronicWavefunction &ElectronicWavefunction::SetZero() {
  for (auto &amplitude_phase_pair : coefficients_) {
    amplitude_phase_pair.SetZero();
  }
  return *this;
}

AmplitudePhasePair &ElectronicWavefunction::operator[](PES_id id) {
  return const_cast<AmplitudePhasePair &>(
      const_cast<ElectronicWavefunction const &>(*this)[id]);
};

const AmplitudePhasePair &ElectronicWavefunction::operator[](PES_id id) const {
  try {
    return coefficients_.at(id);
  } catch (const std::out_of_range &) {
    QI_Error::RaiseError("[ERROR] AmplitudePhasePair::operator[] - index " +
                         std::to_string(id) + " is out of bounds (" +
                         std::to_string(coefficients_.size()) + ")");
    return coefficients_.at(id);
  }
}

bool ElectronicWavefunction::CheckForNans() const 
{
	for (auto &point : coefficients_) {
		if (point.CheckForNans()) { return true; };
	};
	return false;
};

bool ElectronicWavefunction::IsNull() const { return NumberOfPESs() == 0; };

void ElectronicWavefunction::Initialize(size_t number_of_pess) {
  if (IsNull()) {
    coefficients_.resize(number_of_pess);
  } else {
    QI_Error::RaiseError("[ERROR] ElectronicWavefunction::Initialize - "
                         " already initialized. ");
  }
};

void ElectronicWavefunction::CheckCompatibilityWith(
    const ElectronicWavefunction &other) {
  if (IsNull()) {
    coefficients_.resize(other.NumberOfPESs());
  }
  if (NumberOfPESs() != other.NumberOfPESs()) {
    QI_Error::RaiseError("[ERROR] ElectronicWavefunction::CheckCompatibility - "
                         "dimensionality mismatch: " +
                         std::to_string(NumberOfPESs()) +
                         " != " + std::to_string(other.NumberOfPESs()));
  }
};

ElectronicWavefunction &
ElectronicWavefunction::CWisePlusEqual(const ElectronicWavefunction &other) {
  CheckCompatibilityWith(other);
  for (size_t i = 0; i < NumberOfPESs(); ++i) {
    (*this)[i].CWisePlusEqual(other[i]);
  }
  return *this;
};
ElectronicWavefunction &
ElectronicWavefunction::CWiseMultiplyEqual(const double multiplier) {
  for (auto &point : coefficients_) {
    point.CWiseMultiplyEqual(multiplier);
  };
  return *this;
};

ElectronicWavefunction &ElectronicWavefunction::CWiseDivideEqual(
    const ElectronicWavefunction &denominator) {
  if (!IsNull()) {
    CheckCompatibilityWith(denominator);
    for (size_t i = 0; i < NumberOfPESs(); ++i) {
      operator[](i).CWiseDivideEqual(denominator[i]);
    };
  }
  return *this;
};

ElectronicWavefunction &
ElectronicWavefunction::CWisePlusEqual(const double epsilon) {
  for (auto &point : coefficients_) {
    point.CWisePlusEqual(epsilon);
  };
  return *this;
};

// ElectronicWavefunction CWiseDivide(const ElectronicWavefunction &numerator,
//                                   const ElectronicWavefunction &denominator)
//                                   {
//  if (numerator.IsNull()) {
//    return ElectronicWavefunction();
//  }
//  ElectronicWavefunction division;
//  division.CheckCompatibilityWith(numerator);
//  division.CheckCompatibilityWith(denominator);
//  for (size_t i = 0; i < division.NumberOfPESs(); ++i) {
//    division[i] = CWiseDivide(numerator[i], denominator[i]);
//  };
//  return division;
//};
} // namespace mce
