#include "stdafx.h"

#include "Wavefunction/CanonicPair.h"

namespace mce {

CanonicPair::CanonicPair() : x_(0), p_(0) {}
CanonicPair::CanonicPair(double p, double x) : p_(p), x_(x) {}

bool CanonicPair::CheckForNans() const { return std::isnan(p_) || std::isnan(x_); }

CanonicPair &CanonicPair::operator+=(const CanonicPair &other) {
  x_ += other.X();
  p_ += other.P();
  return *this;
}

CanonicPair &CanonicPair::operator*=(const double multiplier) {
  x_ *= multiplier;
  p_ *= multiplier;
  return *this;
}

CanonicPair &CanonicPair::operator/=(const CanonicPair &other) {
  x_ /= other.X();
  p_ /= other.P();
  return *this;
}

CanonicPair &CanonicPair::CWisePlusEqual(const double epsilon) {
  x_ += epsilon;
  p_ += epsilon;
  return *this;
};

CanonicPair operator/(const CanonicPair &numerator,
                      const CanonicPair &denominator) {
  CanonicPair division;
  division.x_ = numerator.X() / denominator.X();
  division.p_ = numerator.P() / denominator.P();
  return division;
};

void CanonicPair::Print(std::ostream &stream) const {
  stream << "CanonicPair[" << X() << "," << P() << "]";
};

} // namespace mce
