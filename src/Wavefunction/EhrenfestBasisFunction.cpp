#include "stdafx.h"

#include "Wavefunction/EhrenfestBasisFunction.h"

namespace mce {

EhrenfestBasisFunction::~EhrenfestBasisFunction() {}

std::shared_ptr<IBasisFunction> EhrenfestBasisFunction::Clone(bool copy_data) {
  if (copy_data) {
    return std::make_shared<EhrenfestBasisFunction>(*this);
  } else {
    auto clone = std::make_shared<EhrenfestBasisFunction>();
    clone->CheckCompatibilityWith(*this);
    return clone;
  }
};

EhrenfestBasisFunction::EhrenfestBasisFunction(
    const mce::EhrenfestBasisFunction &other) {
  gaussian_center_ = other.gaussian_center_;
  electronic_wavefunction_ = other.electronic_wavefunction_;
  multiplier_ = other.multiplier_;
  nature_ = other.nature_;
};

bool EhrenfestBasisFunction::IsNull() const {
  return (gaussian_center_.NumberOfDOFs() == 0) &&
         (electronic_wavefunction_.NumberOfPESs() == 0);
};

void EhrenfestBasisFunction::CheckCompatibilityWith(
    const IBasisFunction &other) {
  if (IsNull()) {
    gaussian_center_.Initialize(other.gaussian_center_.NumberOfDOFs());
    electronic_wavefunction_.Initialize(
        other.electronic_wavefunction_.NumberOfPESs());
  }
  gaussian_center_.CheckCompatibilityWith(other.gaussian_center_);
  electronic_wavefunction_.CheckCompatibilityWith(
      other.electronic_wavefunction_);
};

bool EhrenfestBasisFunction::CheckForNans() const {
  bool result = gaussian_center_.CheckForNans() ||
                electronic_wavefunction_.CheckForNans() ||
                multiplier_.CheckForNans();
  if (result) {
    std::cout << "[INFO] EhrenfestBasisFunction::CheckForNans() - nan found in "
              << (gaussian_center_.CheckForNans()
                      ? "gaussian center"
                      : (electronic_wavefunction_.CheckForNans()
                             ? "electronic wavefunction"
                             : "multiplier"))
              << ".\n";
    std::flush(std::cout);
  };
  return result;
};

IBasisFunction &EhrenfestBasisFunction::SetZero() {
  gaussian_center_.SetZero();
  electronic_wavefunction_.SetZero();
  multiplier_.SetZero();
  return *this;
};

// algebra
IBasisFunction &
EhrenfestBasisFunction::CWisePlusEqual(const IBasisFunction &other) {
  CheckCompatibilityWith(other);
  gaussian_center_.CWisePlusEqual(other.gaussian_center_);
  electronic_wavefunction_.CWisePlusEqual(other.electronic_wavefunction_);
  multiplier_.CWisePlusEqual(other.multiplier_);
  return *this;
};

IBasisFunction &
EhrenfestBasisFunction::CWiseMultiplyEqual(const double multiplier) {
  gaussian_center_.CWiseMultiplyEqual(multiplier);
  electronic_wavefunction_.CWiseMultiplyEqual(multiplier);
  multiplier_.CWiseMultiplyEqual(multiplier);
  return *this;
};

IBasisFunction &
EhrenfestBasisFunction::CWiseDivideEqual(const IBasisFunction &denominator) {
  if (typeid(denominator).hash_code() !=
      typeid(EhrenfestBasisFunction).hash_code()) {
    QI_Error::RaiseError(
        std::string("[ERROR] CWiseDivide - not implemented for type \"") +
        typeid(denominator).name() + "\"");
  }
  CheckCompatibilityWith(denominator);
  gaussian_center_.CWiseDivideEqual(denominator.gaussian_center_);
  electronic_wavefunction_.CWiseDivideEqual(
      denominator.electronic_wavefunction_);
  multiplier_.CWiseDivideEqual(denominator.multiplier_);
  return *this;
}

IBasisFunction &EhrenfestBasisFunction::CWisePlusEqual(const double epsilon) {
  gaussian_center_.CWisePlusEqual(epsilon);
  electronic_wavefunction_.CWisePlusEqual(epsilon);
  multiplier_.CWisePlusEqual(epsilon);
  return *this;
};

} // namespace mce
