#include "stdafx.h"

#include "Definitions.h"
#include "Wavefunction/AmplitudePhasePair.h"

namespace mce {

AmplitudePhasePair::AmplitudePhasePair(std::complex<double> amplitude,
                                       double phase)
    : amplitude_(amplitude), phase_(phase) {}

AmplitudePhasePair::~AmplitudePhasePair() {}

bool AmplitudePhasePair::CheckForNans() const {
  auto result = std::isnan(amplitude_.imag()) ||
                std::isnan(amplitude_.real()) || std::isnan(phase_);
  if (result) {
    std::cout << "[INFO] AmplitudePhasePair::CheckForNans - nan found in " << (std::isnan(phase_) ? "phase" : "amplitude")<< ".\n";
  }
  return result;
}

AmplitudePhasePair &
AmplitudePhasePair::CWisePlusEqual(const AmplitudePhasePair &other) {
  amplitude_ += other.Amplitude();
  phase_ += other.Phase();
  return *this;
}

AmplitudePhasePair &
AmplitudePhasePair::CWiseMultiplyEqual(const double multiplier) {
  amplitude_ *= multiplier;
  phase_ *= multiplier;
  return *this;
}

AmplitudePhasePair &
AmplitudePhasePair::CWiseDivideEqual(const AmplitudePhasePair &denominator) {
  amplitude_ = Amplitude() / denominator.Amplitude();
  phase_ = Phase() / denominator.Phase();
  return *this;
}

AmplitudePhasePair &AmplitudePhasePair::CWisePlusEqual(const double epsilon) {
  amplitude_ += epsilon;
  phase_ += epsilon;
  return *this;
};

std::complex<double> AmplitudePhasePair::Value() {
  return amplitude_ * std::exp(I * phase_);
};

// AmplitudePhasePair CWiseDivide(const AmplitudePhasePair &numerator,
//                               const AmplitudePhasePair &denominator) {
//  AmplitudePhasePair division;
//  division.amplitude_ = numerator.AmplitudeRef() / denominator.AmplitudeRef();
//  division.phase_ = numerator.PhaseRef() / denominator.PhaseRef();
//  return division;
//}

} // namespace mce
