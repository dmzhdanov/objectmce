#include "stdafx.h"

#include "Configuration.h"
#include "Wavefunction/Wavefunction.h"

#include "Algorithms/Utilities.h"

namespace mce {
Wavefunction::Wavefunction(std::shared_ptr<IConfiguration> configuration /*=
                               std::make_shared<mce::Configuration>()*/
                           ,
                           size_t basis_size)
    : configuration_(configuration), data_(std::make_shared<ContainerType>()) {
  for (size_t i = 0; i < basis_size; ++i) {
    PushBack(std::make_shared<EhrenfestBasisFunction>(
        configuration_->NumberOfSpatialDOFs(), configuration_->NumberOfPESs()));
  }
};

Wavefunction::Wavefunction(const Wavefunction &other) { *this = other; }

Wavefunction::~Wavefunction() {}

Wavefunction &Wavefunction::operator=(const Wavefunction &other) {
  configuration_ = other.configuration_;
  auto other_data = other.data_;
  data_ = std::make_shared<ContainerType>();
  std::transform(
      other_data->cbegin(), other_data->cend(),
      std::inserter(Data(), Data().begin()),
      [&](const auto &multiplicand) { return multiplicand->Clone(); });
  return *this;
}

Wavefunction Wavefunction::Clone(bool copy_data) const {
  Wavefunction clone(configuration_);
  std::transform(
      Data().cbegin(), Data().cend(),
      std::inserter(clone.Data(), clone.Data().begin()),
      [&](const auto &multiplicand) { return multiplicand->Clone(copy_data); });
  return clone;
}

Wavefunction Wavefunction::ShallowCopy() const {
  Wavefunction shallow_copy(configuration_);
  shallow_copy.data_ = data_;
  return shallow_copy;
}

std::shared_ptr<IBasisFunction> Wavefunction::Erase(size_t index) {
  auto basis_function = Data()[index];
  Data().erase(Data().begin() + index);
  return basis_function;
}

void Wavefunction::PushBack(
    const std::shared_ptr<IBasisFunction> basis_function) {
  if (basis_function == nullptr) {
    QI_Error::RaiseError(
        "[ERROR] Wavefunction::PushBack - attempt to push nullptr.");
  }
  Data().push_back(basis_function);
};

const std::shared_ptr<IBasisFunction> &
    Wavefunction::operator[](size_t index) const {
  try {
    return Data().at(index);
  } catch (const std::out_of_range &) {
    QI_Error::RaiseError("[ERROR] Wavefunction::operator[] - index " +
                         std::to_string(index) + " is out of bounds (" +
                         std::to_string(Data().size()) + ")");
    return Data().at(index);
  }
}

std::shared_ptr<IBasisFunction> &Wavefunction::operator[](size_t index) {
  return const_cast<std::shared_ptr<IBasisFunction> &>(
      const_cast<Wavefunction const &>(*this)[index]);
};

Wavefunction &Wavefunction::operator<<(Wavefunction &other) {
  if (other.configuration_ != configuration_) {
    QI_Error::RaiseError(
        "[ERROR] Wavefunction::operator<< - incompatible configurations.");
  }
  if (!utilities::CheckNoDuplicates(*((*this).data_), *(other.data_))) {
    QI_Error::RaiseError("[ERROR] Wavefunction::operator<< - Destination and "
                         "copied wavefunction have identical entries. Consider "
                         "making deep copy of rhs before merging.");
  }
  data_->insert(data_->end(), other.begin(), other.end());
  other.data_->clear();
  return *this;
}

bool Wavefunction::CheckForNans() const {
  int i = 0;
  for (auto &basis_function : *data_) {
    if (basis_function->CheckForNans()) {
      std::cout << "[INFO] Wavefunction::CheckForNans - nan for n=" << i
                << ".\n";
      std::flush(std::cout);
      return true;
    }
    i++;
  }
  return false;
};

Wavefunction &Wavefunction::SetZero() {
  for (auto &entry : Data()) {
    entry->SetZero();
  }
  return *this;
}

Wavefunction &Wavefunction::SetNature(const BasisFunctionNature &nature) {
  for (auto &entry : Data()) {
    entry->Nature(nature);
  }
  return *this;
}

void Wavefunction::CheckCompatibilityWith(const Wavefunction &other) {
  if (other.configuration_ != configuration_) {
    QI_Error::RaiseError("[ERROR] Wavefunction::CheckCompatibilityWith "
                         "- incompatible configurations.");
  }
  if (other.Data().size() != Data().size()) {
    QI_Error::RaiseError("[ERROR] Wavefunction::CheckCompatibilityWith "
                         "- incompatible data sizes");
  }
};

Wavefunction &Wavefunction::operator+=(const Wavefunction &other) {
  if (IsNull()) {
    *this = other;
  } else if (!other.IsNull()) {
    CheckCompatibilityWith(other);
    for (size_t i = 0; i < Data().size(); ++i) {
      Data()[i]->CWisePlusEqual(*(other.Data()[i]));
    }
  }
  return *this;
}

Wavefunction &Wavefunction::operator-=(const Wavefunction &other) {
  // ???!!! very primitive and inefficient way to implement an operation. To be
  // redone in the future...
  auto other_copy = other;
  other_copy *= -1.;
  (*this) += other_copy;
  return *this;
}

Wavefunction &Wavefunction::operator*=(const double multiplier) {
  if (!IsNull()) {
    for (auto &basis_function : Data()) {
      basis_function->CWiseMultiplyEqual(multiplier);
    }
  }
  return *this;
}

Wavefunction &Wavefunction::operator/=(const Wavefunction &other) {
  if (!IsNull()) {
    if (!other.IsNull()) {
      CheckCompatibilityWith(other);
      for (size_t i = 0; i < Data().size(); ++i) {
        Data()[i]->CWiseDivideEqual(*(other.Data()[i]));
      }
    } else {
      QI_Error::RaiseError(
          "[ERROR] - Wavefunction::operator/= - division by null.");
    }
  }
  return *this;
}

Wavefunction &Wavefunction::operator+=(const double epsilon) {
  if (!IsNull()) {
    for (auto &basis_function : Data()) {
      basis_function->CWisePlusEqual(epsilon);
    }
  } else {
    QI_Error::RaiseError(
        "[ERROR] - Wavefunction::operator+=(const double epsilon) - Attempt to "
        "add to null wavefunction is illegal.");
  }
  return *this;
}

} // namespace mce


