#include "stdafx.h"

#include "Wavefunction/PhaseSpacePoint.h"

namespace mce {

PhaseSpacePoint::~PhaseSpacePoint() {}

PhaseSpacePoint::PhaseSpacePoint(size_t num_dof) : phase_variables_(num_dof){};

CanonicPair &PhaseSpacePoint::operator[](const SpatialDOF_id dof_no) {
  return const_cast<CanonicPair &>(
      const_cast<PhaseSpacePoint const &>(*this)[dof_no]);
};

const CanonicPair &
    PhaseSpacePoint::operator[](const SpatialDOF_id dof_no) const {
  try {
    return phase_variables_.at(dof_no);
  } catch (const std::out_of_range &) {
    QI_Error::RaiseError("[ERROR] PhaseSpacePoint::operator[] - index " +
                         std::to_string(dof_no) + " is out of bounds (" +
                         std::to_string(phase_variables_.size()) + ")");
    return phase_variables_.at(dof_no);
  }
};

void PhaseSpacePoint::Initialize(size_t number_of_pess) {
  if (IsNull()) {
    phase_variables_.resize(number_of_pess);
  } else {
    QI_Error::RaiseError("[ERROR] PhaseSpacePoint::Initialize - "
                         " already initialized. ");
  }
};

size_t PhaseSpacePoint::NumberOfDOFs() const { return phase_variables_.size(); }

bool PhaseSpacePoint::IsNull() const { return NumberOfDOFs() == 0; }
void PhaseSpacePoint::CheckCompatibilityWith(const PhaseSpacePoint &other) {
  if (IsNull()) {
    phase_variables_.resize(other.NumberOfDOFs());
  }
  if (NumberOfDOFs() != other.NumberOfDOFs()) {
    QI_Error::RaiseError("[ERROR] PhaseSpacePoint::CheckCompatibility - "
                         "dimensionality mismatch: " +
                         std::to_string(NumberOfDOFs()) +
                         " != " + std::to_string(other.NumberOfDOFs()));
  }
}

PhaseSpacePoint &PhaseSpacePoint::SetZero() {
  for (auto &canonic_pair : phase_variables_) {
    canonic_pair.SetZero();
  }
  return *this;
}

bool PhaseSpacePoint::CheckForNans() const {
  for (auto &point : phase_variables_) {
    if (point.CheckForNans()) {
		std::cout << "[INFO] PhaseSpacePoint::CheckForNans - nan found.\n";
      return true;
    }
  };
  return false;
}

PhaseSpacePoint &PhaseSpacePoint::CWisePlusEqual(const PhaseSpacePoint &other) {
  CheckCompatibilityWith(other);
  for (size_t i = 0; i < NumberOfDOFs(); ++i) {
    (*this)[i] += other[i];
  }
  return *this;
};
PhaseSpacePoint &PhaseSpacePoint::CWiseMultiplyEqual(const double multiplier) {
  for (auto &point : phase_variables_) {
    point *= multiplier;
  }
  return *this;
};

PhaseSpacePoint &
PhaseSpacePoint::CWiseDivideEqual(const PhaseSpacePoint &denominator) {
  if (denominator.IsNull()) {
    QI_Error::RaiseError(
        "[ERROR] PhaseSpacePoint::CWiseDivideEqual - division by null.");
    return *this;
  }
  CheckCompatibilityWith(denominator);
  for (size_t i = 0; i < NumberOfDOFs(); ++i) {
    operator[](i) /= denominator[i];
  }
  return *this;
};

void PhaseSpacePoint::Print(std::ostream &stream) const {
  std::string comma = "";
  stream << "PhaseSpacePoint[";
  for (const auto &point : phase_variables_) {
    stream << comma;
    point.Print(stream);
    comma = ", ";
  }
  stream << "]";
};

PhaseSpacePoint &PhaseSpacePoint::CWisePlusEqual(const double epsilon) {
  for (auto &point : phase_variables_) {
    point.CWisePlusEqual(epsilon);
  }
  return *this;
};

} // namespace mce
