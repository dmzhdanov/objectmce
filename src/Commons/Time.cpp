#include "stdafx.h"

#include "Commons/Time.h"

namespace mce {
namespace utilities {
long long MeasureTime(TimeVar *_time, bool reset) {
  if (reset) {
    TimeVar time_old = *_time;
    *_time = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds>(*_time -
                                                                 time_old)
        .count();
  }
  return std::chrono::duration_cast<std::chrono::microseconds>(
             std::chrono::high_resolution_clock::now() - *_time)
      .count();
};

} // namespace utilities
} // namespace mce
