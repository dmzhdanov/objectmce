#include "stdafx.h"

#include "Commons/Text.h"

namespace mce {
	namespace utilities {
		unsigned kPrintPrecision = 7;


		std::string num2mstr(double num, unsigned precision)
		{
			precision = (precision == 0) ? kPrintPrecision : precision;
			int exp = int(std::log10(std::abs(num)));
			double mantissa;
			if (exp < -100000000)
			{
				exp = 0;
				mantissa = 0;
			}
			else
				mantissa = num / std::pow(10, exp);
			std::ostringstream out;
			out << std::setprecision(precision) << mantissa << ((exp == 0) ? "" : ("*^" + std::to_string(exp)));
			return out.str();
		};

		std::string num2mstr(std::complex<double> num, unsigned precision) //std::numeric_limits<double>::digits10 + 1)
		{
			precision = (precision == 0) ? kPrintPrecision : precision;
			if (std::abs(num) < std::pow(10, -(int)precision))
			{
				return "0";
			}
			std::string re = ((std::abs(num.real()) < std::pow(10, -(int)precision)) ? "" : num2mstr(num.real(), precision));
			std::string im = ((std::abs(num.imag()) < std::pow(10, -(int)precision)) ? "" : std::string(num.imag() >= 0 ? "+" : "-") + "I*" + num2mstr(std::abs(num.imag()), precision));
			return (re.size() >0 ? re : "") + (im.size() > 3 ? im : "");
		};
	}
}
