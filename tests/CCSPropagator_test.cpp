//#include "pch.h"
#include "config.h"

#include "gtest/gtest.h"

#include "Algorithms/CCSPropagator.h"
#include "Algorithms/CCSSchrodinger.h"
#include "Algorithms/CCSSchrodingerPiloted.h"
#include "Algorithms/CCSSchrodingerWerewolf.h"
#include "Algorithms/CCSSchrodinger_v2.h"
#include "Algorithms/CCSSchrodinger_v3.h"
#include "Algorithms/CCSWerewolfPolicy.h"
#include "Algorithms/WavefunctionUtilities.h"
#include "CcsPolicy.h"
#include "Configuration.h"
#include "OperatorFactory.h"
#include "WavefunctionFactory.h"

TEST(GTEST_STATE(CCSPropagator_test),
     TestingCCSPropagatorWithCCSSchrodingerSystem) {
  using namespace mce;
  using PolicyType = MCE_POLICY(CCS);

  using namespace mce;
  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  WavefunctionFactory wf(1, 1, 1., 500);
  WavefunctionFactory wf2(1, 1, 1. / 1, 500);
  auto initial_state = wf2.Create(1);
  initial_state[0]->GetCenter(0).X(1.); // 1.3);
  initial_state[0]->GetCenter(0).P(1);
  initial_state[0]->GetMultiplier().Amplitude(1.);
  // initial_state[1]->GetCenter(0).X(0.7);
  // initial_state[1]->GetCenter(0).P(1);
  // initial_state[1]->GetMultiplier().Amplitude(0.5);
  MatrixXd_r swarm_center(1, 2), swarm_spread(1, 2), swarm_step(1, 2);
  swarm_center << 1., 1.;
  swarm_spread << 4. / 2, 4. / 2;
  swarm_step << 0.3, 0.3;
  Wavefunction wavefunction =
      wf.CreateUniformSwarm2(0, swarm_center, swarm_spread, swarm_step);
  ccs_utilities::Project(wavefunction, initial_state, Id(), true);
  ccs_utilities::Filter(wavefunction, 8.e-2);
  std::cout << "\n[OK] Wavefunction of size " << wavefunction.Size()
            << " created.";

  BracketPair bp({wavefunction});
  auto H = 0.5 * X() * X() + 0.5 * P() * P();
  auto kernel1 = Id();
  auto shrodinger_system =
      algorithm::CCSSchrodingerSystem(H, kernel1, operator_factory);

  auto kernel2 = Id(); // +1. * X() * X();// +0.1*Exp(0.1 * X() * X());
  auto shrodinger_system2 =
      algorithm::CCSSchrodingerSystem(H, kernel2, operator_factory);

  algorithm::CCSPropagator propagator;
  std::cout << "\n[OK] Hamiltonian and propagation auxiliaries created.";
  std::cout << "\n[INFO] <I(0)> = " << Id().Evaluate(wavefunction).Mean();
  std::cout << "\n[INFO] <H(0)> = "
            << H.Evaluate(wavefunction).Mean() /
                   Id().Evaluate(wavefunction).Mean();

  auto pder_t_wavefunction = wavefunction.Clone(false);
  shrodinger_system(wavefunction, pder_t_wavefunction, 0.);

  auto pder_t_wavefunction2 = wavefunction.Clone(false);
  shrodinger_system2(wavefunction, pder_t_wavefunction2, 0.);

  std::cout << "\nUsing " << Eigen::nbThreads() << " threads.";

  propagator.Integrate(shrodinger_system2, wavefunction, 0, 1, 0.01);
  std::cout << "\n[INFO] <I(1)> = " << Id().Evaluate(wavefunction).Mean();
  std::cout << "\n[INFO] <H(1)> = "
            << H.Evaluate(wavefunction).Mean() /
                   Id().Evaluate(wavefunction).Mean();
  std::cout << "\n";
  std::cout.flush();
}

TEST(GTEST_STATE(CCSPropagator_test),
     TestingCCSPropagatorWithCCSSchrodingerSystem_v2) {
  using namespace mce;
  using PolicyType = MCE_POLICY(CCS);

  using namespace mce;
  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  WavefunctionFactory wf(1, 1, 1., 500);
  WavefunctionFactory wf2(1, 1, 1. / 1, 500);
  auto initial_state = wf2.Create(1);
  initial_state[0]->GetCenter(0).X(1.); // 1.3);
  initial_state[0]->GetCenter(0).P(1);
  initial_state[0]->GetMultiplier().Amplitude(1.);
  // initial_state[1]->GetCenter(0).X(0.7);
  // initial_state[1]->GetCenter(0).P(1);
  // initial_state[1]->GetMultiplier().Amplitude(0.5);
  MatrixXd_r swarm_center(1, 2), swarm_spread(1, 2), swarm_step(1, 2);
  swarm_center << 1., 1.;
  swarm_spread << 4. / 2, 4. / 2;
  swarm_step << 0.3, 0.3;
  Wavefunction wavefunction =
      wf.CreateUniformSwarm2(0, swarm_center, swarm_spread, swarm_step);
  ccs_utilities::Project(wavefunction, initial_state, Id(), true);
  ccs_utilities::Filter(wavefunction, 8.e-2);
  std::cout << "\n[OK] Wavefunction of size " << wavefunction.Size()
            << " created.";

  BracketPair bp({wavefunction});
  auto H = 0.5 * X() * X() + 0.5 * P() * P();
  auto kernel1 = Id();
  auto shrodinger_system =
      algorithm::CCSSchrodingerSystem(H, kernel1, operator_factory);

  auto kernel2 = Id();
  auto shrodinger_system2 =
      algorithm::CCSSchrodingerSystem_v2(H, kernel2, operator_factory);

  algorithm::CCSPropagator propagator;
  std::cout << "\n[OK] Hamiltonian and propagation auxiliaries created.";
  std::cout << "\n[INFO] <I(0)> = " << Id().Evaluate(wavefunction).Mean();
  std::cout << "\n[INFO] <H(0)> = "
            << H.Evaluate(wavefunction).Mean() /
                   Id().Evaluate(wavefunction).Mean();

  // for (const auto& entry : wavefunction)
  //{
  // std::cout << "\n" << entry->GetCenter(0).P() << " | " <<
  // entry->GetCenter(0).X() << " | " << entry->GetMultiplier().Amplitude() << "
  // | " << entry->GetMultiplier().Phase();
  //}

  auto pder_t_wavefunction = wavefunction.Clone(false);
  shrodinger_system(wavefunction, pder_t_wavefunction, 0.);

  auto pder_t_wavefunction2 = wavefunction.Clone(false);
  shrodinger_system2(wavefunction, pder_t_wavefunction2, 0.);
  // for (size_t i = 0; i < wavefunction.Size(); i++) {
  //  std::cout << "\n"
  //<< wavefunction[i]->GetMultiplier().Amplitude() << " | "<<
  // pder_t_wavefunction[i]->GetMultiplier().Amplitude() << " | "
  //<< pder_t_wavefunction2[i]->GetMultiplier().Amplitude() << " | "
  //<< pder_t_wavefunction2[i]->GetMultiplier().Amplitude()/
  // pder_t_wavefunction[i]->GetMultiplier().Amplitude();
  //}
  // for (const auto& entry : pder_t_wavefunction)
  //{
  // std::cout << "\n" << entry->GetCenter(0).P() << " | " <<
  // entry->GetCenter(0).X() << " | " << entry->GetMultiplier().Amplitude() << "
  // | " << entry->GetMultiplier().Phase();
  //}

  std::cout << "\nUsing " << Eigen::nbThreads() << " threads.";

  propagator.Integrate(shrodinger_system2, wavefunction, 0, 1, 0.01);
  std::cout << "\n[INFO] <I(1)> = " << Id().Evaluate(wavefunction).Mean();
  std::cout << "\n[INFO] <H(1)> = "
            << H.Evaluate(wavefunction).Mean() /
                   Id().Evaluate(wavefunction).Mean();
  std::cout << "\n";
  std::cout.flush();
}

TEST(GTEST_STATE(CCSPropagator_test),
     TestingCCSPropagatorWithCCSSchrodingerWerewolfSystem) {
  using namespace mce;
  using PolicyType = MCE_POLICY(CCS);

  using namespace mce;
  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  WavefunctionFactory wf(1, 1, 1., 50000);
  WavefunctionFactory wf2(1, 1, 1. / 1, 50000);
  auto initial_state = wf2.Create(1);
  initial_state[0]->GetCenter(0).X(1.); // 1.3);
  initial_state[0]->GetCenter(0).P(1);
  initial_state[0]->GetMultiplier().Amplitude(1.);
  MatrixXd_r swarm_center(1, 2), swarm_spread(1, 2), swarm_step(1, 2);
  swarm_center << 1., 1.;
  swarm_spread << 6. / 2, 6. / 2;
  swarm_step << 1., 1.;

  // Wavefunction wavefunction =
  //	wf.CreateUniformSwarm2(0, swarm_center, swarm_spread, swarm_step);
  // ccs_utilities::Project(wavefunction, initial_state, Id(), true, 1.e-8);
  // ccs_utilities::Filter(wavefunction, 1.e-3);

  // Wavefunction wavefunction = wf.Create(1);
  // wavefunction[0]->GetCenter(0).X(1.); // 1.3);
  // wavefunction[0]->GetCenter(0).P(1);
  // wavefunction[0]->GetMultiplier().Amplitude(1.);

  Wavefunction wavefunction = wf.Create(2);
  wavefunction[0]->GetCenter(0).X(1.); // 1.3);
  wavefunction[0]->GetCenter(0).P(1);
  wavefunction[0]->GetMultiplier().Amplitude(1);
  wavefunction[0]->GetMultiplier().Phase(1.5); //(0.);
  wavefunction[1]->GetCenter(0).X(2.);         // 1.3);
  wavefunction[1]->GetCenter(0).P(1);
  wavefunction[1]->GetMultiplier().Amplitude(1.7); //(1.);
  wavefunction[1]->GetMultiplier().Phase(1.5);     //.Phase(0.);

  std::cout << "\n[OK] Wavefunction of size " << wavefunction.Size()
            << " created.";

  std::cout << "\n ***" << Id().Evaluate({wavefunction}).Mean();

  auto pder_t_wavefunction = wavefunction.Clone(false);
  auto pder_t_wavefunction2 = wavefunction.Clone(false);
  auto kernel1 = Id();
  auto kernel2 = Id() + 0. * P() * P();

  auto H = 0.5 * X() * X() + 0.5 * P() * P() + 0.01 * Exp(-X() * X()) -
           0.01 * Exp(-X() * X());

  auto null_test = 0.5 * Exp(-X() * X()) - 0.5 * Exp(-X() * X());
  ASSERT_NEAR(std::abs(null_test.Evaluate({wavefunction}).Mean()), 0,
              kTolerance);
  std::cout << "\nnull test passed...";

  auto shrodinger_system =
      algorithm::CCSSchrodingerSystemPiloted(H, kernel1, operator_factory);
  // auto shrodinger_system =
  // algorithm::CCSSchrodingerSystem_v3(H, kernel1, operator_factory);
  // auto shrodinger_system =
  //  algorithm::CCSSchrodingerSystem_v3(H, kernel1, operator_factory);

  auto shrodinger_system2 =
      algorithm::CCSSchrodingerWerewolfSystem(H, kernel2, operator_factory);
  // wavefunction.SetNature(BasisFunctionNature::Shalashilishche);
  wavefunction.SetNature(BasisFunctionNature::Garashchuchishche);
  // wavefunction.SetNature(BasisFunctionNature::ShaloBohmishche);
  // wavefunction.SetNature(BasisFunctionNature::LameBurghardtishche);

  shrodinger_system(wavefunction, pder_t_wavefunction, 0.);

  std::cout << "\nMethod 1";
  for (size_t i = 0; i < pder_t_wavefunction.Size(); i++) {
    std::cout << "\nA[" << i
              << "] = " << pder_t_wavefunction[i]->GetMultiplier().Amplitude();
    std::cout << "\nj[" << i
              << "] = " << pder_t_wavefunction[i]->GetMultiplier().Phase();
    std::cout << "\nP[" << i
              << "] = " << pder_t_wavefunction[i]->GetCenter(0).P();
    std::cout << "\nX[" << i
              << "] = " << pder_t_wavefunction[i]->GetCenter(0).X();
  }

  shrodinger_system2(wavefunction, pder_t_wavefunction2, 0.);

  // propagator.SetWerewolfPolicy(algorithm::CCSWerewolfPolicy(1., 1.));

  std::cout << "\nMethod 2";
  for (size_t i = 0; i < pder_t_wavefunction2.Size(); i++) {
    std::cout << "\nA[" << i
              << "] = " << pder_t_wavefunction2[i]->GetMultiplier().Amplitude();
    std::cout << "\nj[" << i
              << "] = " << pder_t_wavefunction2[i]->GetMultiplier().Phase();
    std::cout << "\nP[" << i
              << "] = " << pder_t_wavefunction2[i]->GetCenter(0).P();
    std::cout << "\nX[" << i
              << "] = " << pder_t_wavefunction2[i]->GetCenter(0).X();
  }

  // for (size_t i = 0; i < wavefunction.Size(); i++) {
  //  std::cout << "\n"
  //<< wavefunction[i]->GetMultiplier().Amplitude() << " | "<<
  // pder_t_wavefunction[i]->GetMultiplier().Amplitude() << " | "
  //<< pder_t_wavefunction2[i]->GetMultiplier().Amplitude() << " | "
  //<< pder_t_wavefunction2[i]->GetMultiplier().Amplitude()/
  // pder_t_wavefunction[i]->GetMultiplier().Amplitude();
  //}
  // for (const auto& entry : pder_t_wavefunction)
  //{
  // std::cout << "\n" << entry->GetCenter(0).P() << " | " <<
  // entry->GetCenter(0).X() << " | " << entry->GetMultiplier().Amplitude() << "
  // | " << entry->GetMultiplier().Phase();
  //}

  std::cout << "\n[INFO] <I(0)> = " << Id().Evaluate(wavefunction).Mean();
  std::cout << "\n[INFO] <H(0)> = "
            << H.Evaluate(wavefunction).Mean() /
                   Id().Evaluate(wavefunction).Mean();

  std::cout << "\nUsing " << Eigen::nbThreads() << " threads.";

  bool clone_flag = false;
  std::function<bool(Wavefunction &, const double, size_t step_number)>
      observer = [&H, &Id,&wf,&clone_flag](Wavefunction &state, const double time,
                           const size_t step_number) -> bool {
    // std::cout << "\n[INFO] <I(" << time << ")> = " <<
    // Id().Evaluate(state).Mean(); std::cout << "\n[INFO] <H(" << time << ")> =
    // "
    //	<< H.Evaluate(state).Mean() /
    //	Id().Evaluate(state).Mean();
    return true;
  };

  algorithm::CCSPropagator propagator;
  propagator.ObservedIntegrate(shrodinger_system2, wavefunction, 0, 1, 0.01,
                               observer, 0.001);
  // propagator.Integrate(shrodinger_system2, wavefunction, 0, 1, 0.01);
  std::cout << "\n[INFO] <I(1)> = " << Id().Evaluate(wavefunction).Mean();
  std::cout << "\n[INFO] <H(1)> = "
            << H.Evaluate(wavefunction).Mean() /
                   Id().Evaluate(wavefunction).Mean();
  std::cout << "\n";
  std::cout.flush();
}

TEST(GTEST_STATE(CCSPropagator_test), CCSSchrodingerWerewolfSystem_nan_test) {
  using namespace mce;
  using PolicyType = MCE_POLICY(CCS);

  using namespace mce;
  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  WavefunctionFactory wf(1, 1, 1., 50000);

  Wavefunction wavefunction = wf.Create(2);
  // wavefunction[0]->GetCenter(0).X(0.7); // 1.3);
  // wavefunction[0]->GetCenter(0).P(1.5);
  // wavefunction[0]->GetMultiplier().Amplitude(2.+I);
  // wavefunction[0]->GetMultiplier().Phase(0.);
  // wavefunction[1]->GetCenter(0).X(2.); // 1.3);
  // wavefunction[1]->GetCenter(0).P(1);
  // wavefunction[1]->GetMultiplier().Amplitude(4.-I);
  // wavefunction[1]->GetMultiplier().Phase(0.);

  wavefunction[0]->GetCenter(0).X(1); // 1.3);
  wavefunction[0]->GetCenter(0).P(1);
  wavefunction[0]->GetMultiplier().Amplitude(1);
  wavefunction[0]->GetMultiplier().Phase(0.);
  wavefunction[1]->GetCenter(0).X(2.); // 1.3);
  wavefunction[1]->GetCenter(0).P(1);
  wavefunction[1]->GetMultiplier().Amplitude(1);
  wavefunction[1]->GetMultiplier().Phase(0.);

  // std::cout << "\n[OK] Wavefunction of size " << wavefunction.Size()
  //	<< " created.";
  // std::cout << "\n ***" << Id().Evaluate({ wavefunction }).Mean();
  auto H = 0.5 * P() * P();

  auto kernel2 = Id();
  auto shrodinger_system2 =
      algorithm::CCSSchrodingerWerewolfSystem(H, kernel2, operator_factory);
  shrodinger_system2.SetSmoothingScale(0.01);

  algorithm::CCSPropagator propagator;

  auto pder_t_wavefunction2 = wavefunction.Clone(false);
  wavefunction.SetNature(BasisFunctionNature::ShaloBohmishche);
  shrodinger_system2(wavefunction, pder_t_wavefunction2, 0.);

  // propagator.SetWerewolfPolicy(algorithm::CCSWerewolfPolicy(1., 1.));

  std::cout << "\nMethod 2";
  for (size_t i = 0; i < pder_t_wavefunction2.Size(); i++) {
    std::cout << "\nA[" << i
              << "] = " << pder_t_wavefunction2[i]->GetMultiplier().Amplitude();
    std::cout << "\nj[" << i
              << "] = " << pder_t_wavefunction2[i]->GetMultiplier().Phase();
    std::cout << "\nP[" << i
              << "] = " << pder_t_wavefunction2[i]->GetCenter(0).P();
    std::cout << "\nX[" << i
              << "] = " << pder_t_wavefunction2[i]->GetCenter(0).X();
  }

  ASSERT_EQ(1, 1);
}
