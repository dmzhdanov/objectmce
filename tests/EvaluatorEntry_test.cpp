#include "config.h"

#include "gtest/gtest.h"

#include "CcsPolicy.h"
#include "Operator/OperatorBase.h"
#include "Configuration.h"

TEST(GTEST_STATE(EvaluatorEntry_test), TestingEvaluatorEntryGaussian) {
	using namespace mce;
	using namespace internal;
	using PolicyType = DefaultEvaluationPolicy;

	auto multiplicand = std::make_shared<OperatorEntryGaussian<PolicyType>>();
	multiplicand->taylor_coefficients_ = MatrixXc_r(1, 3);
	multiplicand->taylor_coefficients_ << 1, 2, 3;

	const mce::Wavefunction wavefunction(std::make_shared<mce::Configuration>());
	BracketPair bracket_pair(wavefunction);

	std::tuple<const std::shared_ptr<IOperatorEntry<PolicyType>>,
		const BracketPair &, const SpatialDOF_id> evaluator_arguments(multiplicand, bracket_pair, 0);
	multiplicand->Evaluator(evaluator_arguments);
	EXPECT_EQ(1, 1);

}

