#include "config.h"

#include "gtest/gtest.h"

#include "CcsPolicy.h"
#include "Configuration.h"
#include "WavefunctionFactory.h"

/*TEST(GTEST_STATE(Wavefunction_test), TestingCenterWavefunctionAlgebra) {
  std::shared_ptr<mce::IConfiguration> configuration =
      std::make_shared<mce::Configuration>();
  auto basis1 = std::make_shared<mce::Wavefunction>(configuration);
  auto basis2 = std::make_shared<mce::Wavefunction>(configuration);
  auto bf = basis1->PushBack<mce::EhrenfestBasisFunction>();
  // bf->GetPESAmplitude(0).amplitude_ = 1;
  // bf->GetPESAmplitude(0).phase_ = 0.1;
  (*basis1->begin())->GetCenter(0).p = 1;
  (*basis1->begin())->GetCenter(0).x = 1;
  *basis1 += *basis2;
  *basis1 = *basis1 + 3 * *basis1;
  EXPECT_EQ((*basis1->begin())->GetCenter(0).X(), 4.)
      << "Summation/multiplication error.";
  *basis1 = (*basis1) / (*basis1);
  EXPECT_EQ((*basis1->begin())->GetCenter(0).X(), 1.) << "Division error.";
  *basis2 += *basis1 * 2;
  EXPECT_EQ((*basis2->begin())->GetCenter(0).X(), 2.) << "Division error.";
  *basis1 *= 3;
  EXPECT_EQ((*basis1->begin())->GetCenter(0).X(), 3.)
      << "Multiplication by constant error.";
}*/

TEST(GTEST_STATE(Wavefunction_test), TestingSpatialWavefunctionAlgebra) {
  using namespace mce;
  WavefunctionFactory wavefunction_factory(1, 1, 0.7, 5);
  auto ket = wavefunction_factory.Create(1);
  ket[0]->GetCenter(0).X(3);
  ket[0]->GetCenter(0).P(4);
  ket[0]->GetMultiplier().Amplitude(3. - 4. * I);
  ket[0]->GetMultiplier().Phase(5);

  auto ket2 = wavefunction_factory.Create(1);
  ket2[0]->GetCenter(0).X(-3);
  ket2[0]->GetCenter(0).P(-4);
  ket2[0]->GetMultiplier().Amplitude(-3. + 4. * I);
  ket2[0]->GetMultiplier().Phase(-5);

  auto ket3 = ket + ket2;
  EXPECT_NEAR(ket3[0]->GetCenter(0).X(), 0., kTolerance);
  EXPECT_NEAR(ket3[0]->GetCenter(0).P(), 0., kTolerance);
  EXPECT_NEAR(std::abs(ket3[0]->GetMultiplier().Amplitude()), 0., kTolerance);
  EXPECT_NEAR(ket3[0]->GetMultiplier().Phase(), 0., kTolerance);
}

TEST(GTEST_STATE(Wavefunction_test), TestingCopyingAndCloning) {
  using namespace mce;
  WavefunctionFactory wavefunction_factory(1, 1, 0.7, 5);
  auto ket = wavefunction_factory.Create(1);
  auto ket2 = ket;
  ket << ket2;
  ASSERT_EQ(ket.Size(), 2);
}
