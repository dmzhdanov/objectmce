#include "config.h"

#include "gtest/gtest.h"

#include "CcsPolicy.h"
#include "OperatorFactory.h"

TEST(GTEST_STATE(Operator_test2), TestingOperator) {
  using namespace mce;
  using PolicyType = MCE_POLICY(CCS);
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  Operator<PolicyType> test1 = 2. * Exp(X() * X());
  Operator<PolicyType> test2 = P() * P();

  Operator<PolicyType> test = test1 + test2;

  std::cout << "\n-----" << test.OperatorBase().NumberOfSummands() << "\n";

  test.Print(std::cout);

  EXPECT_TRUE(true);
}

TEST(GTEST_STATE(Operator_test), TestingOperator) {
  using namespace mce;
  using PolicyType = MCE_POLICY(CCS);
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  Operator<PolicyType> test = -2.0 * X() * X() + P() * P() + X() * P();

  MatrixXc_r compare(3, 3);
  compare << std::complex<double>(0, 1), 0, -2, 0, 1, 0, 1, 0, 0;

  // test.Print(std::cout);
  // std::cout << "\n\n"
  //          << (((OperatorEntryGaussian *)((*(test.OperatorBase().begin()))
  //                                             ->begin()
  //                                             ->second.get()))
  //                  ->taylor_coefficients_)
  //          << "\n\n";

  EXPECT_TRUE(
      (compare -
       (((OperatorEntryGaussian<PolicyType>
              *)((*(test.OperatorBase().begin()))->begin()->second.get()))
            ->taylor_coefficients_))
          .cwiseAbs()
          .maxCoeff() < 1.0e-8)
      << " operator +-*= algebra leads to incorrect result.";

  Operator exp_test = Exp(-2.0 * X() * X() + (-1) * X() + Id());
  VectorXc_r compare2(3);
  compare2 << 1, -1, -2;

  // std::cout << (((OperatorEntryGaussian
  // *)((*(exp_test.OperatorBase().begin()))->begin()->second.get()))
  // ->exp_taylor_coefficients_);

  EXPECT_TRUE(
      (compare2 -
       (((OperatorEntryGaussian<PolicyType>
              *)((*(exp_test.OperatorBase().begin()))->begin()->second.get()))
            ->exp_taylor_coefficients_))
          .cwiseAbs()
          .maxCoeff() < 1.0e-8)
      << " operator exp algebra leads to incorrect result.";
}
