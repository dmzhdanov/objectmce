#include "config.h"

#include "gtest/gtest.h"

#include "Algorithms/QuasiprobabilityAuxiliaries.h"
#include "Algorithms/Utilities.h"
#include "CcsPolicy.h"
#include "Commons/Faddeeva.hh"

TEST(GTEST_STATE(erf_test), TestingBoostErf) {
  using namespace mce;
  VectorXc_c test(6);
  quasiprobability_auxiliaries::ErfGaussIntegral(test, 3. - 0.2 * I, 4. + I, 5,
                                                 6. - I, 5);
  // for (int i = 0; i <= 5; i++) {
  //  std::cout << "\n<" << std::setprecision(8) << test(i) << ">";
  //}

  auto CP = [&](double p, double x) {
    CanonicPair cp;
    cp.X(x);
    cp.P(p);
    return cp;
  };

  std::complex<double> test_values[] = {
      3.764751377634 - 2.575608123129301 * I,
      2.001284335379158 - 1.921833188502375 * I,
      1.385067735177699 - 1.610792100344268 * I,
      1.070213955923044 - 1.489347889826977 * I,
      0.90871726404288 - 1.48386735682547 * I,
      0.824926692830625 - 1.576932343484168 * I};

  {
    MatrixXc_r taylor_coefficients(1, 3);
    taylor_coefficients << 0., 0., 1.;
    VectorXc_r exp_taylor_coefficients(0);
    // auto momentum_flow =
    //    quasiprobability_auxiliaries::GaussianWeightedMomentumFlow(
    //        1.3, 0.5,   // P, X
    //        1.8, 0.7,   // a, b
    //        2., 1., 1., // p_1, x_1, alpha_1
    //        3., 2., 1., // p_2, x_2, alpha_2
    //        taylor_coefficients, exp_taylor_coefficients);
    auto momentum_flow =
        quasiprobability_auxiliaries::GaussianWeightedMomentumFlow(
            CP(1.3, 0.5),   // P, X
            CP(0.7, 1.8),   // a, b in reverse order
            CP(2., 1.), 1., // p_1, x_1, alpha_1
            CP(3., 2.), 1., // p_2, x_2, alpha_2
            taylor_coefficients, exp_taylor_coefficients);
    ASSERT_NEAR(std::abs(momentum_flow -
                         (0.08207444988060063 - 0.24460131554169048 * I)),
                0., 1.e-15)
        << "momentum flow 1 calculated incorrectly";
    momentum_flow = quasiprobability_auxiliaries::GaussianWeightedMomentumFlow(
        1.3, 0.5, 1.8, 0.7, 2.4, 1.1, 1.2, 3., 2.3, 0.6, taylor_coefficients,
        exp_taylor_coefficients);
    ASSERT_NEAR(std::abs(momentum_flow -
                         (-0.09094038905247497 - 0.2580323540896381 * I)),
                0., 1.e-15)
        << "momentum flow 2 calculated incorrectly";
  }

  {
    MatrixXc_r taylor_coefficients(1, 4);
    taylor_coefficients << 1., 0., 0., 2.;
    VectorXc_r exp_taylor_coefficients(3);
    exp_taylor_coefficients << 2., 3., -1.1;
    auto momentum_flow =
        0.5 *
        (quasiprobability_auxiliaries::GaussianWeightedMomentumFlow(
             1.3, 0.5, 1.8, 0.7, 2.4, 1.1, 1.2, 3., 2.3, 0.6,
             taylor_coefficients, exp_taylor_coefficients) +
         std::conj(quasiprobability_auxiliaries::GaussianWeightedMomentumFlow(
             1.3, 0.5, 1.8, 0.7, 3., 2.3, 0.6, 2.4, 1.1, 1.2,
             taylor_coefficients, exp_taylor_coefficients)));
    ASSERT_NEAR(
        std::abs(momentum_flow - (24.77558688174709 + 3.5963740990084014 * I)),
        0., 1.e-10)
        << "momentum flow 3 calculated incorrectly";
  }

  ASSERT_NEAR(std::abs(Faddeeva::erf(1. + I * 1.) -
                       (1.3161512816979475 + 0.19045346923783468 * I)),
              0., 1.e-15)
      << "erf calculated incorrectly";
  for (int i = 0; i <= 5; i++) {
    ASSERT_NEAR(std::abs(test(i) - test_values[i]), 0., 1.e-14);
  }
}

TEST(GTEST_STATE(ComplementaryMomentumFlow), TestingComplementaryMomentumFlow) {
  using namespace mce;
  auto CP = [&](double p, double x) {
    CanonicPair cp;
    cp.X(x);
    cp.P(p);
    return cp;
  };
  {
    MatrixXc_r taylor_coefficients(1, 3);
    taylor_coefficients << 0., 0., 1.;
    VectorXc_r exp_taylor_coefficients(0);
    auto momentum_flow =
        quasiprobability_auxiliaries::ComplementaryGaussianWeightedMomentumFlow(
            CP(1.3, 0.5),   // P, X
            CP(0.7, 1.8),   // a, b in reverse order
            CP(2., 1.), 1., // p_1, x_1, alpha_1
            CP(3., 2.), 2., // p_2, x_2, alpha_2
            taylor_coefficients, exp_taylor_coefficients);
    ASSERT_NEAR(std::abs(momentum_flow -
                         (-0.037209132300288175 - 0.04776077415825796 * I)),
                0., 1.e-12)
        << "complementary momentum flow 1 calculated incorrectly";
  }
}

TEST(GTEST_STATE(PositionFlow), TestingPositionFlow) {
  using namespace mce;

  auto CP = [&](double p, double x) {
    CanonicPair cp;
    cp.X(x);
    cp.P(p);
    return cp;
  };

  {
    MatrixXc_r taylor_coefficients(2, 1);
    taylor_coefficients << 0., 1.;
    VectorXc_r exp_taylor_coefficients(0);
    auto position_flow =
        quasiprobability_auxiliaries::GaussianWeightedPositionFlow(
            CP(1.3, 0.5),      // P, X
            CP(0.7, 1.8),      // a, b in reverse order
            CP(2.4, 1.1), 1.2, // p_1, x_1, alpha_1
            CP(3., 2.3), 0.6,  // p_2, x_2, alpha_2
            taylor_coefficients, exp_taylor_coefficients);
    ASSERT_NEAR(std::abs(position_flow -
                         (-0.09805734681569647 - 0.08096838784510209 * I)),
                0., 1.e-14)
        << "position flow 1 calculated incorrectly";

    position_flow +=
        std::conj(quasiprobability_auxiliaries::GaussianWeightedPositionFlow(
            CP(1.3, 0.5),      // P, X
            CP(0.7, 1.8),      // a, b in reverse order
            CP(3., 2.3), 0.6,  // p_2, x_2, alpha_2
            CP(2.4, 1.1), 1.2, // p_1, x_1, alpha_1
            taylor_coefficients, exp_taylor_coefficients));

    ASSERT_NEAR(std::abs(position_flow / 2. -
                         (-0.12029139514213434 + 0.0071091829261188885 * I)),
                0., 1.e-14)
        << "position flow 2 calculated incorrectly";
  }

  {
    MatrixXc_r taylor_coefficients(1, 1);
    taylor_coefficients << 2.;
    VectorXc_r exp_taylor_coefficients(0);
    auto position_flow =
        quasiprobability_auxiliaries::GaussianWeightedPositionFlow(
            CP(1.3, 0.5),      // P, X
            CP(0.7, 1.8),      // a, b in reverse order
            CP(2.4, 1.1), 1.2, // p_1, x_1, alpha_1
            CP(3., 2.3), 0.6,  // p_2, x_2, alpha_2
            taylor_coefficients, exp_taylor_coefficients);
    ASSERT_NEAR(std::abs(position_flow - 2. * (-0.06043814340075205 -
                                               0.005659777527643187 * I)),
                0., 1.e-14)
        << "position flow 3 calculated incorrectly";
  }
}

TEST(GTEST_STATE(RightHusimiOperatorMatrixElement),
     testing_RightHusimiOperatorMatrixElement) {
  using namespace mce;
  using namespace quasiprobability_auxiliaries;

  {
	  MatrixXc_r taylor_coefficients(2, 3);
	  taylor_coefficients << 1., 0., 1., 0.1, 0., 0.5;
	  VectorXc_r exp_taylor_coefficients(3);
	  exp_taylor_coefficients << 2, 1. + I, -0.5;
	  CanonicPair phase_space_point(1, 1);

	  auto result = RightHusimiOperatorMatrixElement(
		  taylor_coefficients, exp_taylor_coefficients, phase_space_point, { 2, 2 },
		  1, { 3, 3 }, 2, { 0.5, 0.5 });

	  ASSERT_NEAR(std::abs(result - (0.4865941428398797 + 0.14274684506941077 * I)),
		  0., 1.e-14)
		  << "RightHusimiOperatorMatrixElement(#1) calculated incorrectly";
  }

  {
	  MatrixXc_r taylor_coefficients(0,0);
	  VectorXc_r exp_taylor_coefficients(0);
	  CanonicPair phase_space_point(1, 1);

	  auto result = RightHusimiOperatorMatrixElement(
		  taylor_coefficients, exp_taylor_coefficients, phase_space_point, { 2, 2 },
		  1, { 3, 3 }, 2, { 0.5, 0.5 });

	  ASSERT_NEAR(std::abs(result - (0.)),
		  0., 1.e-14)
		  << "RightHusimiOperatorMatrixElement(#2) calculated incorrectly";
  }

  //std::cout << "*********** result = " << result << " ************";
  //std::vector<std::complex<double>> mults(4), coeffs(4);
  //mults = { 0,0,0,1 };
  //std::fill(coeffs.begin(), coeffs.end(), 0);
  //mce::utilities::HermiteSeries(mults.begin(), mults.end(), coeffs.begin());
  //for (auto c : coeffs)
  //{
	 // std::cout << "coeff = " << c << "\n";
  //}
}
