//#include "pch.h"
#include "config.h"

#include "gtest/gtest.h"

#include "Algorithms/QuasiprobabilityAuxiliaries.h"
#include "CcsPolicy.h"
#include "Configuration.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityFlow_CCS.h"
#include "OperatorFactory.h"
#include "WavefunctionFactory.h"

TEST(GTEST_STATE(Evaluator_test), TestingCCSEvaluator) {
  using namespace mce;
  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  std::shared_ptr<mce::IConfiguration> configuration =
      std::make_shared<mce::Configuration>();
  auto Psi = Wavefunction(configuration, 3);
  Psi[1]->GetCenter(0).X(1.);
  Psi[2]->GetCenter(0).P(1.);

  auto test = [&](const Operator<MCE_POLICY(CCS)> &observable, size_t bra_idx,
                  size_t ket_idx, std::complex<double> expected_value,
                  std::string comment = "") {
    auto evaluator = observable.Evaluate({Psi, Psi});
    ASSERT_NEAR(std::abs(evaluator(bra_idx, ket_idx) - expected_value), 0,
                kTolerance)
        << comment;
  };

  test(Id(), 1, 1, 1., " Id test failed");
  test(X() * X(), 0, 0, 0.5, " X^2 test failed");
  test((X() - Id()) * (X() - Id()), 0, 0, 1.5, " (X-1)^2 test #1 failed");
  test((X() - Id()) * (X() - Id()), 1, 1, 0.5, " (X-1)^2 test #2 failed");
  test(X(), 0, 0, 0, " X test #1 failed");
  test(X(), 1, 1, 1, " X test #2 failed");
  test(P(), 1, 1, 0, " P test #1 failed");
  test(P(), 2, 2, 1, " P test #2 failed");
  test((P() - Id()) * (P() - Id()), 2, 2, 0.5, " (P-1)^2 test #1 failed");
}

TEST(GTEST_STATE(Evaluator_test2), TestingCCSEvaluator) {
  using namespace mce;
  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };
  auto H = 2. * Exp((-1) * X() * X()) + P() * P() + Id();
  WavefunctionFactory wf;

  auto Psi = wf.Create(1);
  auto Psi2 = wf.Create(2);
  auto ev = H.Evaluate({Psi, Psi2});
  auto mean = ev.Mean();
  ASSERT_EQ(1, 1);
  // std::cout << "\n----------" << ev(0, 0) << "\n";
}

TEST(GTEST_STATE(Evaluator_test3), TestingCCSEvaluatorExponents) {
  using namespace mce;
  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };
  // auto H = X() * Exp((-1) * X() * X());

  auto H = Exp((-0.75) * X() * X());
  WavefunctionFactory wf(1, 1, 1, 5);
  WavefunctionFactory wf2(1, 1, 0.7, 5);

  auto Psi = wf.Create(1);
  Psi[0]->GetCenter(0).X(3.);
  Psi[0]->GetCenter(0).P(4.);

  auto Psi2 = wf2.Create(1);
  Psi2[0]->GetCenter(0).X(2.);
  Psi2[0]->GetCenter(0).P(1.);

  auto ev = H.Evaluate({Psi});
  std::cout << "\n----------" << ev(0, 0) << "\n";
  H = Id();
  std::cout << "\n----------" << H.Evaluate({Psi})(0, 0) << "\n";
  std::cout.flush();
}

TEST(GTEST_STATE(Evaluator_test3), TestingMultidimensionalCCSEvaluator) {
  using namespace mce;
  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };
  auto H = X(0) * (-X(1)) + P(0) * P(1) + Id(0);
  WavefunctionFactory wf(1, 2, 1, 5);

  auto Psi = wf.Create(1);
  Psi[0]->GetCenter(0).X(3.);
  Psi[0]->GetCenter(0).P(4.);
  Psi[0]->GetCenter(1).X(5.);
  Psi[0]->GetCenter(1).P(6.);

  auto ev = H.Evaluate({Psi});
  // std::cout << "\n----------" << ev(0, 0) << "\n";
  ASSERT_NEAR(std::abs(ev(0, 0) - (-3. * 5 + 4 * 6 + 1)), 0., kTolerance);
}

TEST(GTEST_STATE(Evaluator_test4), TestingCCSEvaluatorQuasiprobabilityFlow) {
  using namespace mce;
  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };
  // auto H = X() * Exp((-1) * X() * X());

  WavefunctionFactory wf(1, 1, 1, 500000.);
  WavefunctionFactory wf2(1, 1, 1., 500000.);

  auto Psi = wf.Create(1);
  Psi[0]->GetCenter(0).X(2.);
  Psi[0]->GetCenter(0).P(4.);
  Psi[0]->GetMultiplier().Amplitude(1.);

  auto Psi2 = wf2.Create(2);
  Psi2[0]->GetCenter(0).X(82.);
  Psi2[0]->GetCenter(0).P(40.);
  Psi2[0]->GetMultiplier().Amplitude(1.);
  Psi2[1]->GetCenter(0).X(-82.);
  Psi2[1]->GetCenter(0).P(-40.);
  Psi2[1]->GetMultiplier().Amplitude(1.);

  using namespace evaluator_strategy;

  auto V = 0.5 * X() * X();

  {
    QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)> wigner_strategy_momentum(
        QuasiprobabilityFlowType::Momentum);
    wigner_strategy_momentum.DofId(0);
    wigner_strategy_momentum.CenterBasisFunction(0);

    QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)> wigner_strategy_position(
        QuasiprobabilityFlowType::Position);
    wigner_strategy_position.DofId(0);
    wigner_strategy_position.CenterBasisFunction(0);
    {
      auto Jp_ev = V.Evaluate({Psi, wigner_strategy_momentum.ShallowCopy()});
      auto Jx_ev = P().Evaluate({Psi, wigner_strategy_position.ShallowCopy()});
      auto norm = Id().Evaluate({Psi, wigner_strategy_position.ShallowCopy()});
      ASSERT_NEAR(
          std::abs(Jp_ev.Mean() / norm.Mean() + Psi[0]->GetCenter(0).X()), 0.,
          1.e-14);
      ASSERT_NEAR((Jx_ev.Mean() / norm.Mean()).real() -
                      Psi[0]->GetCenter(0).P(),
                  0., 1.e-14);
    }
  }

  {
    QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)> wigner_strategy(
        QuasiprobabilityFlowType::Momentum);
    wigner_strategy.DofId(0);
	wigner_strategy.FlowType(QuasiprobabilityFlowType::Momentum);
	auto Jp_ev = V.Evaluate({ Psi2, wigner_strategy.ShallowCopy() });
	wigner_strategy.FlowType(QuasiprobabilityFlowType::Position);
	auto Jx_ev = P().Evaluate({ Psi2, wigner_strategy.ShallowCopy() });
	auto norm = Id().Evaluate({ Psi2, wigner_strategy.ShallowCopy() });
    for (int i = 0; i < 2; i++) {
      wigner_strategy.CenterBasisFunction(i);
      ASSERT_NEAR(
          std::abs(Jp_ev.Mean() / norm.Mean() + Psi2[i]->GetCenter(0).X()), 0.,
          1.e-12)
          << "Error in evaluating J_p for i=" << i;
      ASSERT_NEAR((Jx_ev.Mean() / norm.Mean()).real() -
                      Psi2[i]->GetCenter(0).P(),
                  0., 1.e-12)
          << "Error in evaluating J_x for i=" << i;
    }
  }

  //{
  //  MatrixXc_r m(1, 3);
  //  m << 0, 0, 0.5;
  //  VectorXc_r v(0);
  //  std::cout << "\n----------2: "
  //            << quasiprobability_auxiliaries::GaussianWeightedMomentumFlow(
  //                   Psi[0]->GetCenter(0),
  //                   CanonicPair(Psi.Configuration().GaussianQuadraticCoefficient(0),
  //                   1./ Psi.Configuration().GaussianQuadraticCoefficient(0)),
  //                   Psi[0]->GetCenter(0),
  //                   Psi.Configuration().GaussianQuadraticCoefficient(0),
  //                   Psi[0]->GetCenter(0),
  //                   Psi.Configuration().GaussianQuadraticCoefficient(0), m,
  //                   v)
  //            << "\n";
  //}
  //{
  // MatrixXc_r m(2, 1);
  // m << 0, 1;
  // VectorXc_r v(0);
  // std::cout << "\n----------3: "
  //  << quasiprobability_auxiliaries::GaussianWeightedPositionFlow(
  //	  Psi[0]->GetCenter(0),
  //CanonicPair(1./Psi.Configuration().GaussianQuadraticCoefficient(0),
  //Psi.Configuration().GaussianQuadraticCoefficient(0)), 	  Psi[0]->GetCenter(0),
  //	  Psi.Configuration().GaussianQuadraticCoefficient(0),
  //	  Psi[0]->GetCenter(0),
  //	  Psi.Configuration().GaussianQuadraticCoefficient(0), m, v)
  //  << "\n";
  //}
}
