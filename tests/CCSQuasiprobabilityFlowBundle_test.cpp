//#include "pch.h"
#include "config.h"

#include "gtest/gtest.h"

#include "Algorithms/CCSQuasiprobabilityFlowBundle.h"
#include "Algorithms/CCSQuasiprobabilityGaugeBundle.h"
#include "Algorithms/QuasiprobabilityGaugeAuxiliaries.h"
#include "Algorithms/WavefunctionUtilities.h"
#include "Definitions.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityFlow_CCS.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityGauge_CCS.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityOperator_CCS.h"
#include "Evaluator/Evaluator_CCS.h"
#include "OperatorFactory.h"
#include "Wavefunction/CanonicPair.h"
#include "WavefunctionFactory.h"

TEST(GTEST_STATE(CCSQuasiprobabilityFlowBundle_test),
     TestingCCSQuasiprobabilityFlowBundle) {
  using namespace mce;
  using namespace evaluator_strategy;
  using namespace quasiprobability_auxiliaries;

  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  auto H = 0.5 * (P() * P() + X() * X());

  WavefunctionFactory wf(1, 1, 1, 500000.);

  {
    auto Psi = wf.Create(1);
    Psi[0]->GetCenter(0).X(2.);
    Psi[0]->GetCenter(0).P(4.);
    Psi[0]->GetMultiplier().Amplitude(1.);

    CCSQuasiprobabilityFlowBundle flow_bundle(operator_factory, H, 1);
    flow_bundle.Initialize(Psi);
    double j_p = flow_bundle.EvaluateMomentumFlow(0, 0);
    double j_x = flow_bundle.EvaluatePositionFlow(0, 0);
    ASSERT_NEAR(j_p + Psi[0]->GetCenter(0).X(), 0., 1.e-12)
        << "Error in evaluating J_p";
    ASSERT_NEAR(j_x - Psi[0]->GetCenter(0).P(), 0., 1.e-12)
        << "Error in evaluating J_x";
  }
  {
    auto Psi = wf.Create(2);
    Psi[0]->GetCenter(0).X(20.);
    Psi[0]->GetCenter(0).P(40.);
    Psi[0]->GetMultiplier().Amplitude(1.);
    Psi[1]->GetCenter(0).X(-20.);
    Psi[1]->GetCenter(0).P(-40.);
    Psi[1]->GetMultiplier().Amplitude(1.);

    CCSQuasiprobabilityFlowBundle flow_bundle(operator_factory, H, 1);
    flow_bundle.Initialize(Psi);
    for (int i = 0; i < 2; i++) {
      double j_p = flow_bundle.EvaluateMomentumFlow(i, 0);
      double j_x = flow_bundle.EvaluatePositionFlow(i, 0);
      ASSERT_NEAR(j_p + Psi[i]->GetCenter(0).X(), 0., 1.e-11)
          << "Error in evaluating J_p for i=" << i;
      ASSERT_NEAR(j_x - Psi[i]->GetCenter(0).P(), 0., 1.e-11)
          << "Error in evaluating J_x for i=" << i;
    }
  }
}

TEST(GTEST_STATE(CCSQuasiprobabilityFlowBundle_testNo2),
     IntegralTestForCCSQuasiprobabilityFlowBundleNo2) {
  using namespace mce;
  using namespace evaluator_strategy;
  using namespace quasiprobability_auxiliaries;

  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  auto H = 0.5 * (P() * P() + X() * X());

  double width_factor = 1.;
  WavefunctionFactory wf(1, 1, 1, 500000.);
  WavefunctionFactory wf2(1, 1, width_factor, 500000.);

  {
    auto Psi = wf.Create(3);
    Psi[0]->GetCenter(0).P(5.);
    Psi[0]->GetCenter(0).X(-1.);
    Psi[0]->GetMultiplier().Amplitude(1. + I);
    Psi[1]->GetCenter(0).P(4.);
    Psi[1]->GetCenter(0).X(-2.);
    Psi[1]->GetMultiplier().Amplitude(2. - 2. * I);
    Psi[2]->GetCenter(0).P(6.);
    Psi[2]->GetCenter(0).X(-3.);
    Psi[2]->GetMultiplier().Amplitude(0.);

    if (true) {
      CCSQuasiprobabilityFlowBundle flow_bundle(
          operator_factory, H, 1, CanonicPair(1. / std::sqrt(2), std::sqrt(2)));
      flow_bundle.Initialize(Psi);
      double j_p = flow_bundle.EvaluateMomentumFlow(2, 0);
      double j_x = flow_bundle.EvaluatePositionFlow(2, 0);
      // std::cout << "\nj_p=" << j_p << ", j_x=" << j_x << "\n";
      ASSERT_NEAR(j_p - 2.520549929488218, 0., 1.e-12)
          << "Error in evaluating J_p...";
      ASSERT_NEAR(j_x - 5.6726463383429895, 0., 1.e-12)
          << "Error in evaluating J_x...";
    }
  }
}

TEST(GTEST_STATE(CCSQuasiprobabilityGaugeBundle_testNo1),
     IntegralTestForCCSQuasiprobabilityGaugeBundleNo1) {
  using namespace mce;
  using namespace evaluator_strategy;
  using namespace quasiprobability_auxiliaries;

  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  auto H = 0.5 * (P() * P() + X() * X());

  double width_factor = 1.;
  WavefunctionFactory wf(1, 1, 1, 500000.);
  Wavefunction Psi1 = wf.Create(3);
  Psi1[0]->GetCenter(0).P(5.);
  Psi1[0]->GetCenter(0).X(-1.);
  Psi1[0]->GetMultiplier().Amplitude(0);
  Psi1[1]->GetCenter(0).P(6.);
  Psi1[1]->GetCenter(0).X(-2.);
  Psi1[1]->GetMultiplier().Amplitude(1. + I);
  Psi1[2]->GetCenter(0).P(7.);
  Psi1[2]->GetCenter(0).X(1.);
  Psi1[2]->GetMultiplier().Amplitude(2);

  Wavefunction Psi2 = wf.Create(1);
  Psi2[0]->GetCenter(0).P(5.);
  Psi2[0]->GetCenter(0).X(-1.);
  Psi2[0]->GetMultiplier().Amplitude(1);

  auto quasiprobability_gauge_bundle_ = std::make_shared<
      quasiprobability_gauge_auxiliaries::CCSQuasiprobabilityGaugeBundle>(
      operator_factory, H, Psi1.Configuration().NumberOfSpatialDOFs(), 0.5,
      CanonicPair(1, 1));
  quasiprobability_gauge_bundle_->Initialize(Psi1);
  std::flush(std::cout);
  auto val1 = quasiprobability_gauge_bundle_->EvaluateHusimiFunction(0);
  auto val2 = std::pow(std::abs(Id(0).Evaluate({Psi1, Psi2}).Mean()), 2);
  // std::cout << "val= " << val1 << "\n";
  // std::cout << "val2= " << val2 << "\n";
  ASSERT_NEAR(val2 / val1 - 2 * 3.141592653589793, 0, 1.e-6)
      << "Error in Husimi function implementation";
  // std::flush(std::cout);

  {
    CanonicPair phase_space_point(4, 3.);
    CanonicPair bra(2.5, 4);
    CanonicPair ket(3, 4.5);
    CanonicPair kernel_gaussian_quadratic_coefficients_ab(2., 0.5);
    double wavefunction_gaussian_quadratic_coefficient = 1;
    for (int i = -1; i < 20; i++) {
      if (i != 0)
        std::cout
            << i << ": "
            << quasiprobability_gauge_auxiliaries::JBG2FunctionMatrixElement(
                   phase_space_point, bra, ket,
                   kernel_gaussian_quadratic_coefficients_ab,
                   wavefunction_gaussian_quadratic_coefficient, i)
            << "\n";
    }
    for (int i = -1; i < 20; i++) {
      if (i != 0)
        std::cout << i << ": "
                  << quasiprobability_gauge_auxiliaries::
                         JBG2FunctionPDerivativeMatrixElement(
                             phase_space_point, bra, ket,
                             kernel_gaussian_quadratic_coefficients_ab,
                             wavefunction_gaussian_quadratic_coefficient, i)
                  << "\n";
    }
  }
}

TEST(GTEST_STATE(CCSQuasiprobabilityGaugeBundle_testNo2),
     IntegralTestForCCSQuasiprobabilityGaugeBundleNo2) {
  using namespace mce;
  using namespace evaluator_strategy;
  using namespace quasiprobability_auxiliaries;

  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  auto H = 0.5 * (P() * P() + X() * X());

  double width_factor = 1.;
  WavefunctionFactory wf(1, 1, 1, 500000.);
  Wavefunction Psi1 = wf.Create(4);
  Psi1[0]->GetCenter(0).P(5.);
  Psi1[0]->GetCenter(0).X(-1.);
  Psi1[0]->GetMultiplier().Amplitude(0);
  Psi1[1]->GetCenter(0).P(1.);
  Psi1[1]->GetCenter(0).X(-1.);
  Psi1[1]->GetMultiplier().Amplitude(0);
  Psi1[2]->GetCenter(0).P(5.);
  Psi1[2]->GetCenter(0).X(1.);
  Psi1[2]->GetMultiplier().Amplitude((2. + I * 1.));
  Psi1[3]->GetCenter(0).P(6.);
  Psi1[3]->GetCenter(0).X(-2.);
  Psi1[3]->GetMultiplier().Amplitude(1. + I);

  // double norm = std::sqrt(Id().Evaluate(Psi1).Mean().real());
  // for (int i=0;i<Psi1.Size();i++)
  // Psi1[i]->GetMultiplier().Amplitude(Psi1[i]->GetMultiplier().Amplitude()/norm);

  Wavefunction Psi2 = wf.Create(1);

  double a = 1.;
  // std::cout << "Enter scale: ";
  // std::cin >> a;
  CanonicPair husimi_width_factors(1, 1);
  CanonicPair husimi_width_factors2(a, a);
  auto quasiprobability_gauge_bundle = std::make_shared<
      quasiprobability_gauge_auxiliaries::CCSQuasiprobabilityGaugeBundle>(
      operator_factory, H, Psi1.Configuration().NumberOfSpatialDOFs(),
      husimi_width_factors2.P() / std::sqrt(2), husimi_width_factors2);
  auto quasiprobability_bundle = std::make_shared<
      quasiprobability_auxiliaries::CCSQuasiprobabilityFlowBundle>(
      operator_factory, H, Psi1.Configuration().NumberOfSpatialDOFs(),
      husimi_width_factors);
  quasiprobability_gauge_bundle->Initialize(Psi1);
  quasiprobability_bundle->Initialize(Psi1);
  std::flush(std::cout);

  auto p_g0 = quasiprobability_gauge_bundle->EvaluatePositionFlow(0, 0);
  auto p_g1 = quasiprobability_gauge_bundle->EvaluatePositionFlow(1, 0);
  auto p_f0 = quasiprobability_bundle->EvaluatePositionFlow(0, 0);
  auto p_f1 = quasiprobability_bundle->EvaluatePositionFlow(1, 0);

  ASSERT_NEAR(p_g0 + p_f0, p_g1 + p_f1, 1.e-12)
      << "Error in evaluating flows...";
}

TEST(GTEST_STATE2(CCSQuasiprobabilityOperator_testNo1),
     IntegralTestForCCSQuasiprobabilityOperatorNo1) {
  using namespace mce;
  using namespace evaluator_strategy;
  using namespace quasiprobability_auxiliaries;

  using PolicyType = DefaultEvaluationPolicy;
  OperatorFactory<PolicyType> operator_factory;
  auto X = [&](SpatialDOF_id id = 0) { return operator_factory.CreateX(id); };
  auto P = [&](SpatialDOF_id id = 0) { return operator_factory.CreateP(id); };
  auto Id = [&](SpatialDOF_id id = 0) { return operator_factory.CreateId(id); };
  auto Exp = [&](const Operator<PolicyType> &exponent) {
    return operator_factory.CreateExp(exponent);
  };

  auto Operator = 0.5 * (P() * P() + X() * X());// +20. * Exp(-0.25 * X() * X());

  double width_factor = 1.;
  WavefunctionFactory wf(1, 1, 0.8, 500000.);
  Wavefunction Psi1 = wf.Create(4);
  Psi1[0]->GetCenter(0).P(2.);
  Psi1[0]->GetCenter(0).X(3.);
  Psi1[0]->GetMultiplier().Amplitude(1);
  Psi1[1]->GetCenter(0).P(1.);
  Psi1[1]->GetCenter(0).X(-1.);
  Psi1[1]->GetMultiplier().Amplitude(0.8 + I * 0.2);
  Psi1[2]->GetCenter(0).P(5.);
  Psi1[2]->GetCenter(0).X(1.);
  Psi1[2]->GetMultiplier().Amplitude((2. + I * 1.));
  Psi1[3]->GetCenter(0).P(6.);
  Psi1[3]->GetCenter(0).X(-2.);
  Psi1[3]->GetMultiplier().Amplitude(1. + I);

  size_t bf_idx = 0;

  auto kernel_state = Wavefunction(Psi1.configuration_); //wf.Create(0);
  kernel_state.PushBack(Psi1[bf_idx]->Clone(true));
  kernel_state[0]->GetMultiplier().Amplitude(1.);

  auto value_to_compare_with = 1. / (2. * 3.141592653589793 * kHBar) *
                               Id().Evaluate({Psi1, kernel_state}).Mean() *
                               (Operator.Evaluate({kernel_state, Psi1}).Mean());

  CanonicPair width_factors(1, 1);

  auto strategy =
      std::make_shared<QuaisiprobabilityOperatorStrategy<MCE_POLICY(CCS)>>(
          width_factors, bf_idx, 0);

  auto value_to_test =
      Operator.Evaluate({Psi1, Psi1, strategy}).Mean(); // .real();

   //std::cout << "\nvalue_to_compare_with=" << value_to_compare_with
   //         << "\nvalue_to_test=        " << value_to_test << "\n";

  ASSERT_NEAR(std::abs(value_to_test - value_to_compare_with), 0., 1.e-12)
      << "Error in evaluating QuasiprobabilityOperator: "
      << "\nvalue_to_compare_with=" << value_to_compare_with
      << "\nvalue_to_test=        " << value_to_test << "\n";
}
