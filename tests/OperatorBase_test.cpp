#include "config.h"

#include "gtest/gtest.h"

#include "CcsPolicy.h"
#include "Operator/OperatorBase.h"

TEST(GTEST_STATE(OperatorBase_test), TestingSpatialProduct) {
  using namespace mce;
  using namespace internal;
  using PolicyType = DefaultEvaluationPolicy;
  if (true) {
    auto pes = std::make_shared<OperatorBase<IOperatorEntry<PolicyType>>>();
    auto product =
        std::make_shared<SpatialProduct<IOperatorEntry<PolicyType>>>();
    auto multiplicand = std::make_shared<OperatorEntryGaussian<PolicyType>>();
    multiplicand->taylor_coefficients_.resize(2, 2);
    multiplicand->taylor_coefficients_ << 0, 1, 1, 0;
    product->AddMultiplicand(0, multiplicand);
    pes->AddSpatialProduct(product);
    //(*pes) *= (*pes);
    (*pes) = (*pes) * (*pes);
    (*pes) = (*pes) + (*pes);
    // std::cout << "---" << ((OperatorEntryGaussian
    // *)((*(pes->begin()))->begin()->second.get()))
    //	->taylor_coefficients_ << "---\n";
    MatrixXc_r expected_output(3, 3);
    expected_output << I, 0, 1, 0, 2, 0, 1, 0, 0;
    expected_output *= 2;
    EXPECT_TRUE((((OperatorEntryGaussian<PolicyType>
                       *)((*(pes->begin()))->begin()->second.get()))
                     ->taylor_coefficients_ -
                 expected_output)
                    .cwiseAbs()
                    .maxCoeff() < 1.0e-10)
        << "Error in pes multiplication #1";
  }

  {
    auto pes = std::make_shared<OperatorBase<IOperatorEntry<PolicyType>>>();
    auto product =
        std::make_shared<SpatialProduct<IOperatorEntry<PolicyType>>>();
    auto multiplicand = std::make_shared<OperatorEntryGaussian<PolicyType>>();
    multiplicand->taylor_coefficients_.resize(2, 1);
    multiplicand->taylor_coefficients_ << 0, 1;
    multiplicand->exp_taylor_coefficients_.resize(1, 2);
    multiplicand->exp_taylor_coefficients_ << 1, 2;
    product->AddMultiplicand(0, multiplicand);
    pes->AddSpatialProduct(product);
    (*pes) *= (*pes);
    // std::cout << ((OperatorEntryGaussian
    // *)((*(pes->begin()))->begin()->second.get()))->exp_taylor_coefficients_;
    MatrixXc_r expected_output(3, 1);
    expected_output << 0, 2. * I, 1;
    EXPECT_TRUE(
        (((OperatorEntryGaussian<PolicyType> *)((*(pes->begin()))->begin()->second.get()))
             ->taylor_coefficients_ -
         expected_output)
            .cwiseAbs()
            .maxCoeff() < 1.0e-10)
        << "Error in pes multiplication #2";
    MatrixXc_r expected_exp_output(1, 2);
    expected_exp_output << 2, 4;
    std::cout.flush();
    EXPECT_TRUE(
        (((OperatorEntryGaussian<PolicyType> *)((*(pes->begin()))->begin()->second.get()))
             ->exp_taylor_coefficients_ -
         expected_exp_output)
            .cwiseAbs()
            .maxCoeff() < 1.0e-10);
  }
  // EXPECT_EQ(1, 1);
  // EXPECT_TRUE(true);
}
