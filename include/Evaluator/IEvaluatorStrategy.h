#pragma once

#include "Definitions.h"

namespace mce {

	class IEvaluatorStrategy {
	public:
		using EntryType =
			std::shared_ptr<IEvaluatorStrategy>;

		IEvaluatorStrategy() {};
		virtual ~IEvaluatorStrategy() = 0; // needs an inline function below to compile
										// properly
		virtual EntryType Clone() const = 0;
	};

	inline IEvaluatorStrategy::~IEvaluatorStrategy() {};

} // namespace mce
