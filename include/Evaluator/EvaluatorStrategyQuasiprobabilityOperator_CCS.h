#pragma once

#include "IEvaluatorStrategy.h"
#include "Wavefunction/CanonicPair.h"
#include "Evaluator/BracketPair.h"

#define MCE_LOCAL_EVALUATORSTRATEGY                                            \
  QuaisiprobabilityOperatorStrategy<MCE_POLICY(CCS)>

#define MCE_LOCAL_EVALUATORSTRATEGYNAME QuaisiprobabilityOperatorStrategy

namespace mce {

namespace evaluator_strategy {

class QuasiprobabilityOperatorKernelData {
public:
  mce::CanonicPair width_factors_;
  size_t center_basis_function_id_;
  SpatialDOF_id dof_id_;
};

template <class PolicyType> class MCE_LOCAL_EVALUATORSTRATEGYNAME;

template <> class MCE_LOCAL_EVALUATORSTRATEGY : public IEvaluatorStrategy {
public:
  MCE_LOCAL_EVALUATORSTRATEGYNAME(
      const CanonicPair &width_factors = CanonicPair(1., 1.),
      size_t center_basis_function_id = 0, const SpatialDOF_id dof_id = 0);
  MCE_LOCAL_EVALUATORSTRATEGYNAME(const MCE_LOCAL_EVALUATORSTRATEGY &other);
  EntryType Clone() const override;
  EntryType ShallowCopy() const;

  size_t CenterBasisFunction(size_t center_basis_function_id);
  SpatialDOF_id DofId(const SpatialDOF_id dof_id);

  size_t CenterBasisFunction() const;
  SpatialDOF_id DofId() const;
  CanonicPair WidthFactors() const;

  CanonicPair GetGaussianQuadraticPXCoefficients(const BracketPair& pair, const SpatialDOF_id dof_id);

private:
  std::shared_ptr<QuasiprobabilityOperatorKernelData> kernel_data_;
};
}; // namespace evaluator_strategy

} // namespace mce

#undef MCE_LOCAL_EVALUATORSTRATEGY
#undef MCE_LOCAL_EVALUATORSTRATEGYNAME
