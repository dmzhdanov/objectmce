#pragma once

#include "Evaluator/BracketPair.h"
#include "Evaluator/IEvaluatorEntry_CCS_Implementation.h"
#include "Operator/OperatorEntryGaussian.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityFlow_CCS.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityGauge_CCS.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityOperator_CCS.h"

namespace mce {
namespace internal {

#define MCE_LOCAL_IMPLEMENTERTYPE GaussianEvaluatorEntryCCSDefaultImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSDefaultImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
  MCE_LOCAL_IMPLEMENTERTYPENAME(
      const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
      const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
  MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE &other);
  ~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
  std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
                                size_t ket_idx,
                                SpatialDOF_id dof_id) const override;
  EntryType Clone() const override;

private:
  struct PrecalcualtedStorage {
    PrecalcualtedStorage(
        const CanonicPair &_center,
        const std::tuple<std::complex<double>, std::complex<double>>
            &_multiplier_tuple,
        VectorXc_c _z_vector)
        : center(_center), multiplier_tuple(_multiplier_tuple),
          z_vector(_z_vector){};
    CanonicPair center;
    std::tuple<std::complex<double>, std::complex<double>> multiplier_tuple;
    VectorXc_c z_vector;
  };

  std::shared_ptr<OperatorEntryGaussian<MCE_POLICY(CCS)>> entry_;
  MatrixXc_r_sp creation_annihilation_matrix_;
  mutable std::vector<std::shared_ptr<VectorXc_r>> bra_z_vectors_;
  mutable std::vector<std::shared_ptr<PrecalcualtedStorage>> ket_storage_;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  GaussianEvaluatorEntryCCSPositionQuasiprobabilityFlowImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSPositionQuasiprobabilityFlowImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
  MCE_LOCAL_IMPLEMENTERTYPENAME(
      const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
      const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
  MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE &other);
  ~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
  std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
                                size_t ket_idx,
                                SpatialDOF_id dof_id) const override;
  EntryType Clone() const override;

private:
  std::shared_ptr<OperatorEntryGaussian<MCE_POLICY(CCS)>> entry_;
  std::shared_ptr<evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)>> strategy_;
  CanonicPair husimi_alpha_pair_;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  GaussianEvaluatorEntryCCSMomentumQuasiprobabilityFlowImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSMomentumQuasiprobabilityFlowImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
  MCE_LOCAL_IMPLEMENTERTYPENAME(
      const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
      const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
  MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE &other);
  ~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
  std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
                                size_t ket_idx,
                                SpatialDOF_id dof_id) const override;
  EntryType Clone() const override;

private:
	std::shared_ptr<OperatorEntryGaussian<MCE_POLICY(CCS)>> entry_;
	std::shared_ptr<evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)>> strategy_;
	CanonicPair husimi_alpha_pair_;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

// ######################################################################
// ######################################################################
// ######################################################################
// #################     GAUGE FLOW TRANSFORMATIONS    ##################
// ######################################################################
// ######################################################################
// ######################################################################

// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  GaussianEvaluatorEntryCCSPositionQuasiprobabilityGaugeImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSPositionQuasiprobabilityGaugeImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
	MCE_LOCAL_IMPLEMENTERTYPENAME(
		const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
		const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
	MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE &other);
	~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
	std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
		size_t ket_idx,
		SpatialDOF_id dof_id) const override;
	EntryType Clone() const override;

private:
	std::shared_ptr<OperatorEntryGaussian<MCE_POLICY(CCS)>> entry_;
	std::shared_ptr<evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(CCS)>> strategy_;
	CanonicPair husimi_alpha_pair_;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  GaussianEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
	MCE_LOCAL_IMPLEMENTERTYPENAME(
		const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
		const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
	MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE &other);
	~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
	std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
		size_t ket_idx,
		SpatialDOF_id dof_id) const override;
	EntryType Clone() const override;

private:
	std::shared_ptr<OperatorEntryGaussian<MCE_POLICY(CCS)>> entry_;
	std::shared_ptr<evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(CCS)>> strategy_;
	CanonicPair husimi_alpha_pair_;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME


// ######################################################################
// ######################################################################
// ######################################################################
// #################     QUASIPROBABILITY OPERATOR     ##################
// ######################################################################
// ######################################################################
// ######################################################################

#define MCE_LOCAL_IMPLEMENTERTYPE GaussianEvaluatorEntryCCSQuasiprobabilityOperatorImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  GaussianEvaluatorEntryCCSQuasiprobabilityOperatorImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
	MCE_LOCAL_IMPLEMENTERTYPENAME(
		const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
		const BracketPair& bracket_pair, const SpatialDOF_id dof_id);
	MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE& other);
	~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
	std::complex<double> Evaluate(const BracketPair& bracket_pair, size_t bra_idx,
		size_t ket_idx,
		SpatialDOF_id dof_id) const override;
	EntryType Clone() const override;

private:
	std::shared_ptr<OperatorEntryGaussian<MCE_POLICY(CCS)>> entry_;
	std::shared_ptr<evaluator_strategy::QuaisiprobabilityOperatorStrategy<MCE_POLICY(CCS)>> strategy_;
	CanonicPair husimi_alpha_pair_;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

}; // namespace internal
} // namespace mce
