#pragma once

#include "Definitions.h"
#include "Operator/IOperatorEntry.h"
#include "BracketPair.h"

namespace mce {
template <class PolicyType> class IEvaluatorEntry;

template <> class IEvaluatorEntry<MCE_POLICY(CCS)> {
public:
  using EntryType =
      std::shared_ptr<IEvaluatorEntry<MCE_POLICY(CCS)>>;
  using PolicyType = MCE_POLICY(CCS); 
  using ParameterPack = std::tuple<const std::shared_ptr<IOperatorEntry<PolicyType>>,
	  const BracketPair &, const SpatialDOF_id >;

  IEvaluatorEntry(){};
  virtual ~IEvaluatorEntry() = 0; // needs an inline function below to compile
                                  // properly
  virtual std::complex<double> Evaluate(const BracketPair &bracket_pair,
                                        size_t bra_idx, size_t ket_idx,
                                        SpatialDOF_id dof_id) const = 0;

  virtual EntryType Clone() const = 0;
};

inline IEvaluatorEntry<
	MCE_POLICY(CCS)>::~IEvaluatorEntry(){};

} // namespace mce
