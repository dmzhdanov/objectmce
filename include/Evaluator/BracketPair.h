#pragma once

//#include <unsupported/Eigen/CXX11/Tensor>

#include "Evaluator/IEvaluatorStrategy.h"
#include "Wavefunction/Wavefunction.h"

namespace mce {

class BracketPair {
public:
  // using OverlapType = Eigen::Tensor<std::complex<double>, 3>;

  //BracketPair();
  BracketPair(const Wavefunction &bra);
  BracketPair(const Wavefunction &bra, const Wavefunction &ket);
  BracketPair(const Wavefunction &bra, const std::shared_ptr<IEvaluatorStrategy> evaluator_strategy);
  BracketPair(const Wavefunction &bra, const Wavefunction &ket, const std::shared_ptr<IEvaluatorStrategy> evaluator_strategy);
  ~BracketPair();
  BracketPair(const BracketPair &other);
  BracketPair(BracketPair &&other);

  bool IsOverlapping(size_t bra_idx, size_t ket_idx) const;
  bool IsSymmetric() const;
  std::complex<double> Overlap(size_t bra_idx, size_t ket_idx, SpatialDOF_id dof_id) const;
  //std::complex<double> Overlap(size_t bra_idx, size_t ket_idx) const;

  inline const Wavefunction& Bra() const { return bra_; }
  inline const Wavefunction& Ket() const { return ket_; }

  inline const std::shared_ptr<IEvaluatorStrategy>& Strategy() const { return evaluator_strategy_; }

private:
  Wavefunction bra_;
  Wavefunction ket_;
  std::shared_ptr<IEvaluatorStrategy> evaluator_strategy_;
};

} // namespace mce
