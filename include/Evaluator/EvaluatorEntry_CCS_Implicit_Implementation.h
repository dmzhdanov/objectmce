#pragma once

#include "Evaluator/BracketPair.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityFlow_CCS.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityGauge_CCS.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityOperator_CCS.h"
#include "Evaluator/IEvaluatorEntry_CCS_Implementation.h"
#include "Operator/OperatorEntryImplicit.h"

namespace mce {
namespace internal {

#define MCE_LOCAL_IMPLEMENTERTYPE ImplicitEvaluatorEntryCCSDefaultImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSDefaultImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
  MCE_LOCAL_IMPLEMENTERTYPENAME(
      const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
      const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
  MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE &other);
  ~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
  std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
                                size_t ket_idx,
                                SpatialDOF_id dof_id) const override;
  EntryType Clone() const override;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  ImplicitEvaluatorEntryCCSPositionQuasiprobabilityFlowImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSPositionQuasiprobabilityFlowImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
  MCE_LOCAL_IMPLEMENTERTYPENAME(
      const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
      const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
  MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE &other);
  ~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
  std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
                                size_t ket_idx,
                                SpatialDOF_id dof_id) const override;
  EntryType Clone() const override;

private:
  std::shared_ptr<
      evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)>>
      strategy_;
  CanonicPair husimi_alpha_pair_;
  static MatrixXc_r identity_matrix_;
  static VectorXc_r zero_vector_;
  static MatrixXc_r InitializeIdentityMatrix();
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityFlowImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityFlowImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
  MCE_LOCAL_IMPLEMENTERTYPENAME(
      const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
      const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
  MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE &other);
  ~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
  std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
                                size_t ket_idx,
                                SpatialDOF_id dof_id) const override;
  EntryType Clone() const override;

private:
  std::shared_ptr<
      evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)>>
      strategy_;
  CanonicPair husimi_alpha_pair_;
  static MatrixXc_r identity_matrix_;
  static VectorXc_r zero_vector_;
  static MatrixXc_r InitializeIdentityMatrix();
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

// ######################################################################
// ######################################################################
// ######################################################################
// #################     GAUGE FLOW TRANSFORMATIONS    ##################
// ######################################################################
// ######################################################################
// ######################################################################

// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  ImplicitEvaluatorEntryCCSPositionQuasiprobabilityGaugeImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSPositionQuasiprobabilityGaugeImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
	MCE_LOCAL_IMPLEMENTERTYPENAME(
		const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
		const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
	MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE &other);
	~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
	std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
		size_t ket_idx,
		SpatialDOF_id dof_id) const override;
	EntryType Clone() const override;

private:
	std::shared_ptr<evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(CCS)>> strategy_;
	CanonicPair husimi_alpha_pair_;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################
// ###########################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSMomentumQuasiprobabilityGaugeImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
	MCE_LOCAL_IMPLEMENTERTYPENAME(
		const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
		const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
	MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE &other);
	~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
	std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
		size_t ket_idx,
		SpatialDOF_id dof_id) const override;
	EntryType Clone() const override;

private:
	std::shared_ptr<evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(CCS)>> strategy_;
	CanonicPair husimi_alpha_pair_;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

// ######################################################################
// ######################################################################
// ######################################################################
// #################     QUASIPROBABILITY OPERATOR     ##################
// ######################################################################
// ######################################################################
// ######################################################################

#define MCE_LOCAL_IMPLEMENTERTYPE                                              \
  ImplicitEvaluatorEntryCCSQuasiprobabilityOperatorImplementation
#define MCE_LOCAL_IMPLEMENTERTYPENAME                                          \
  ImplicitEvaluatorEntryCCSQuasiprobabilityOperatorImplementation

class MCE_LOCAL_IMPLEMENTERTYPE : public IEvaluatorEntryCCSImplementation {
public:
	MCE_LOCAL_IMPLEMENTERTYPENAME(
		const std::shared_ptr<IOperatorEntry<MCE_POLICY(CCS)>> entry,
		const BracketPair& bracket_pair, const SpatialDOF_id dof_id);
	MCE_LOCAL_IMPLEMENTERTYPENAME(const MCE_LOCAL_IMPLEMENTERTYPE& other);
	~MCE_LOCAL_IMPLEMENTERTYPENAME() override;
	std::complex<double> Evaluate(const BracketPair& bracket_pair, size_t bra_idx,
		size_t ket_idx,
		SpatialDOF_id dof_id) const override;
	EntryType Clone() const override;

private:
	std::shared_ptr<evaluator_strategy::QuaisiprobabilityOperatorStrategy<MCE_POLICY(CCS)>> strategy_;
	CanonicPair husimi_alpha_pair_;
};

#undef MCE_LOCAL_IMPLEMENTERTYPE
#undef MCE_LOCAL_IMPLEMENTERTYPENAME

}; // namespace internal
} // namespace mce
