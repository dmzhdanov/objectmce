#pragma once

#include "Evaluator/BracketPair.h"

namespace mce {
namespace internal {

class IEvaluatorEntryCCSImplementation {
public:
  using PolicyType = MCE_POLICY(CCS);
  using EntryType = std::shared_ptr<IEvaluatorEntryCCSImplementation>;
  virtual ~IEvaluatorEntryCCSImplementation() = 0;
  virtual std::complex<double> Evaluate(const BracketPair &bracket_pair,
                                        size_t bra_idx, size_t ket_idx,
                                        SpatialDOF_id dof_id) const = 0;
  virtual EntryType Clone() const = 0;
};
inline IEvaluatorEntryCCSImplementation::~IEvaluatorEntryCCSImplementation(){};

} // namespace internal
} // namespace mce
