#pragma once

#include "Definitions.h"
#include "Operator/OperatorBase.h"

namespace mce {

template <class EvaluatorType, class PolicyType>
class EvaluatorAlgorithmicCore {
public:
  using OperatorEntryType = std::shared_ptr<IOperatorEntry<PolicyType>>;
  using EvaluatorEntryType = std::shared_ptr<IEvaluatorEntry<PolicyType>>;
  using OperatorBaseType = internal::OperatorBase<IOperatorEntry<PolicyType>>;
  using EvaluatorBaseType = internal::OperatorBase<IEvaluatorEntry<PolicyType>>;
  template <class Algorithm>
  EvaluatorAlgorithmicCore(const OperatorBaseType &operator_base,
                           Algorithm algorithm)
      : operator_base_(std::move(operator_base.ShallowCopy())),
        evaluator_base_(std::move(
            operator_base.template CloneTransformed<IEvaluatorEntry<PolicyType> >(
                algorithm))){};
  ~EvaluatorAlgorithmicCore(){};

  EvaluatorAlgorithmicCore(
	  const EvaluatorAlgorithmicCore<EvaluatorType, PolicyType> &other)
	  : evaluator_base_(other.evaluator_base_),
	    operator_base_(std::move(other.operator_base_.ShallowCopy()))
		{};

protected:
  template <class ValueType>
  ValueType
  Traverse(std::function<ValueType(
               const std::pair<const SpatialDOF_id, EvaluatorEntryType> &)>
               entry_functor,
           std::function<ValueType(const SpatialDOF_id)> no_entry_functor,
           const SpatialDOF_id number_of_spatial_dofs) const {
    ValueType output(0);
    for (auto &summand : evaluator_base_) {
      ValueType value_product(1);
      SpatialDOF_id i = 0;
      for (const auto &entry : *summand) {
        while (i < entry.first) {
          value_product *= no_entry_functor(i);
          i++;
        }
        value_product *= entry_functor(entry);
      }
	  while (++i < number_of_spatial_dofs) {
		  value_product *= no_entry_functor(i);
	  }
      output += value_product;
    }
	return output;
  }

protected:
  EvaluatorBaseType evaluator_base_;
  const OperatorBaseType operator_base_;
};

template <class PolicyType> class Evaluator;

} // namespace mce
