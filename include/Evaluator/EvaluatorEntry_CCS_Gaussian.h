#pragma once

#include "IEvaluatorEntry_CCS.h"
#include "Operator/OperatorEntryGaussian.h"

#include "Evaluator/IEvaluatorEntry_CCS_Implementation.h"

namespace mce {

template <>
class EvaluatorEntry<OperatorEntryGaussian<MCE_POLICY(CCS)>>
    : public IEvaluatorEntry<MCE_POLICY(CCS)> {
public:
  using PolicyType = MCE_POLICY(CCS);

  EvaluatorEntry(std::any initializer_list);
  EvaluatorEntry(const std::shared_ptr<IOperatorEntry<PolicyType>> entry,
                 const BracketPair &bracket_pair, const SpatialDOF_id dof_id);
  EvaluatorEntry(const EvaluatorEntry &other);
  ~EvaluatorEntry() override;

  std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
                                size_t ket_idx,
                                SpatialDOF_id dof_id) const override;
  EntryType Clone() const override;

private:
  std::shared_ptr<mce::internal::IEvaluatorEntryCCSImplementation>
      evaluation_strategy_;
};
} // namespace mce
