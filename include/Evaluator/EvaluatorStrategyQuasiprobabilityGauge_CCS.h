#pragma once

#include "Evaluator/BracketPair.h"
#include "IEvaluatorStrategy.h"
#include "Wavefunction/CanonicPair.h"

#define MCE_LOCAL_EVALUATORSTRATEGY                                            \
  QuaisiprobabilityGaugeStrategy<MCE_POLICY(CCS)>

#define MCE_LOCAL_EVALUATORSTRATEGYNAME QuaisiprobabilityGaugeStrategy

namespace mce {

namespace evaluator_strategy {

enum class QuasiprobabilityGaugeType { Momentum, Position };

class QuasiprobabilityGaugeKernelData {
public:
  mce::CanonicPair width_factors_;
  size_t center_basis_function_id_;
  // SpatialDOF_id dof_id_;
  QuasiprobabilityGaugeType flow_type_;
  double mean_momentum_;
  double mean_momentum_x_derivative_;
  unsigned approximation_order_;
};

template <class PolicyType> class MCE_LOCAL_EVALUATORSTRATEGYNAME;

template <> class MCE_LOCAL_EVALUATORSTRATEGY : public IEvaluatorStrategy {
public:
  MCE_LOCAL_EVALUATORSTRATEGYNAME(
      double mean_momentum = 0, double mean_momentum_x_derivative = 0,
      const QuasiprobabilityGaugeType flow_type =
          QuasiprobabilityGaugeType::Position,
      const CanonicPair &width_factors = CanonicPair(1., 1.),
      size_t center_basis_function_id = 0, unsigned approximation_order = 4);
  MCE_LOCAL_EVALUATORSTRATEGYNAME(const MCE_LOCAL_EVALUATORSTRATEGY &other);
  EntryType Clone() const override;
  EntryType ShallowCopy() const;

  size_t CenterBasisFunction(size_t center_basis_function_id);
  SpatialDOF_id DofId(const SpatialDOF_id dof_id);
  QuasiprobabilityGaugeType FlowType(const QuasiprobabilityGaugeType flow_type);
  double MeanMomentum(double mean_momentum);
  double MeanMomentumXDerivative(double mean_momentum_x_derivative);
  unsigned ApproximationOrder(unsigned approximation_order);

  size_t CenterBasisFunction() const;
  // SpatialDOF_id DofId() const;
  QuasiprobabilityGaugeType FlowType() const;
  double MeanMomentum() const;
  double MeanMomentumXDerivative() const;
  CanonicPair WidthFactors() const;
  unsigned ApproximationOrder() const;

  CanonicPair GetGaussianQuadraticPXCoefficients(const BracketPair &pair,
                                                 const SpatialDOF_id dof_id);

private:
  std::shared_ptr<QuasiprobabilityGaugeKernelData> kernel_data_;
};
}; // namespace evaluator_strategy

} // namespace mce

#undef MCE_LOCAL_EVALUATORSTRATEGY
#undef MCE_LOCAL_EVALUATORSTRATEGYNAME
