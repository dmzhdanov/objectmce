#pragma once

#include "BracketPair.h"
#include "EvaluatorAlgorithmicCore.h"
#include "CCSPolicy.h"

namespace mce {
template <>
class Evaluator<MCE_POLICY(CCS)>
    : public EvaluatorAlgorithmicCore<Evaluator<MCE_POLICY(CCS)>,
                                      MCE_POLICY(CCS)> {
public:
  using PolicyType = MCE_POLICY(CCS);
  // constructors
  Evaluator(const OperatorBaseType &operator_base,
            const BracketPair &bracket_pair);
  ~Evaluator();

  // operations
  std::complex<double> operator()(size_t bra_idx, size_t ket_idx) const;
  std::complex<double> Mean() const;

private:
  const BracketPair bracket_pair_;
  std::shared_ptr<EvaluatorEntry<OperatorEntryImplicit<MCE_POLICY(CCS)>>> implicit_evaluator_entry_;
};
} // namespace mce
