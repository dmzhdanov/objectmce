#pragma once

#include "BracketPair.h"
#include "IEvaluatorEntry_CCS.h"
#include "Operator/OperatorEntrySum.h"

#define MCE_LOCAL_EVALUATORTYPE                                                \
  EvaluatorEntry<OperatorEntrySum<MCE_POLICY(CCS)>>

namespace mce {

template <>
class MCE_LOCAL_EVALUATORTYPE
    : public IEvaluatorEntry<PolicyTraits<EvaluationPolicies::CCS>> {
public:
	using PolicyType = MCE_POLICY(CCS);
  ~EvaluatorEntry() override;

  EvaluatorEntry(std::any initializer_list) : EvaluatorEntry(std::make_from_tuple<EvaluatorEntry>(std::move(
	  std::any_cast<std::tuple<const std::shared_ptr<IOperatorEntry<PolicyType>>,
	  const BracketPair &, const SpatialDOF_id>>(initializer_list))))
  {};

  EvaluatorEntry(const std::shared_ptr<IOperatorEntry<PolicyType>> entry,
                    const BracketPair &bracket_pair,
                    const SpatialDOF_id dof_id);

  EvaluatorEntry(const EvaluatorEntry& other);

  std::complex<double> Evaluate(const BracketPair &bracket_pair, size_t bra_idx,
                                size_t ket_idx,
                                SpatialDOF_id dof_id) const override;

  EntryType Clone() const override;

public:
  std::shared_ptr<OperatorEntrySum<PolicyType>> entry_;
  std::vector<EntryType> summands_;
};
} // namespace mce

#undef MCE_LOCAL_EVALUATORTYPE
