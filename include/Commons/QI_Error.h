#ifndef QI_ERROR_H
#define QI_ERROR_H

//#define QI_ERROR_MPI_SUPPORT

#ifdef PYTHON_BINDINGS
#include <pybind11/pybind11.h>
#endif

#include <iostream>
#include <string>
#ifdef QI_ERROR_MPI_SUPPORT
#include "QI_MPI.h"
#endif
#include <Eigen/Dense>
#include <algorithm>
#include <limits>
#include <omp.h>
#include <signal.h>
#include <vector>
//#include "QI_Def.h"

#define QI_ERROR_BUFFER_SIZE 1000

class QI_Error {
public:
  static int death_code;
  // static MatrixXc_r death_matrix;
  class ErrorData {
  public:
    std::vector<std::vector<long>> data;
    ErrorData() {
      data.resize(NOfThreads());
      std::for_each(data.begin(), data.end(), [&](std::vector<long> &x) {
        x.reserve(QI_ERROR_BUFFER_SIZE);
      });
    };
    inline void Push(long val) {
      if (omp_get_thread_num() < NOfThreads() &&
          data[omp_get_thread_num()].size() < QI_ERROR_BUFFER_SIZE)
        data[omp_get_thread_num()].push_back(val);
    };
    inline void Update(long val) {
      if (omp_get_thread_num() < NOfThreads() &&
          data[omp_get_thread_num()].size() > 0)
        data[omp_get_thread_num()].back() = val;
    }
    inline void Pop() {
      if (omp_get_thread_num() < NOfThreads() &&
          data[omp_get_thread_num()].size() > 0)
        data[omp_get_thread_num()].pop_back();
    };
    void Print(std::ostream &s = std::cout) const {
      int stop = NOfThreads();
      size_t n = 0;
      s << "\n[" << NOfThreads() << "]\n";
      while (stop > 0) {
        for (int i = 0; i < NOfThreads(); i++) {
          if (n == data[i].size())
            stop--;
          s << "|"
            << (((n < data[i].size()) ? std::to_string(data[i][n])
                                      : std::string("")) +
                "                    ")
                   .substr(0, 20)
            << std::flush;
        }
        s << "|\n";
        n++;
      };
    };
    int NOfThreads() const { return Eigen::nbThreads(); };
  };

  static ErrorData &_Mem() {
    static ErrorData error_memory;
    return error_memory;
  }
  template <class T> static inline void Push(T val) { _Mem().Push((long)val); };
  template <class T> static inline void Update(T val) {
    _Mem().Update((long)val);
  };
  static inline void Pop() { _Mem().Pop(); };
  static void Print(std::ostream &s = std::cout) { _Mem().Print(s); }

  static void RaiseError(std::string error) {
    std::cout << error << std::flush;
#ifdef PYTHON_BINDINGS
    {
      using namespace pybind11;
      pybind11::print(error, "flush"_a = true);
	  throw std::runtime_error(error.c_str());
    }
#endif
#ifdef QI_ERROR_MPI_SUPPORT
    if (QI_MPI::Size() == 1) {
#endif
      std::cout << "\nPress [Enter] to exit..." << std::flush;
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      std::cin.clear();
      std::cin.get();
#ifdef QI_ERROR_MPI_SUPPORT
    }
    MPI_Abort(MPI_COMM_WORLD, -333);
#endif
    std::exit(-1);
  };

  static void DeathFunction(int signum) {
    std::cout << "�� �����, ������ ������ ��� �����: " << death_code << ".\n"
              << std::flush; // ���������� ��������
    // std::cout << "Thread id:" << getpid();
    Print();
    signal(signum, SIG_DFL); // ����������� �������
    exit(death_code); //����� �� ���������. ���� �� ������� �����, �� ����������
                      //����� ���������� ����������.
  };

  QI_Error(void);
  ~QI_Error(void);
};

#endif
