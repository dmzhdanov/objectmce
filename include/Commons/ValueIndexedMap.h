#pragma once

//#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index_container.hpp>

#include "ITERATOR_MACRO.h"

namespace mce {
using namespace ::boost;
using namespace ::boost::multi_index;
template <class KeyType, class ValueType, bool kRequireUniqueValues = false>
class ValueIndexedMap {
  template <class T>
  using ValuesOrderingType =
      std::conditional_t<kRequireUniqueValues, ordered_unique<T>,
                         ordered_non_unique<T>>;

public:
  using RecordType = std::pair<KeyType, ValueType>;
  using ContainerType = multi_index_container<
      RecordType,
      indexed_by<
          // sort by less<KeyType> on KeyType
          ordered_unique<member<RecordType, KeyType, &RecordType::first>>,
          // sort by std_pair::operator<
          // ordered_unique<identity<RecordType>>,
          // sort by less<second> on RecordType::second
          ValuesOrderingType<
              member<RecordType, ValueType, &RecordType::second>>>>;

  using KeyIndexType = typename ContainerType::template nth_index<0>::type;
  using ValueIndexType = typename ContainerType::template nth_index<1>::type;

  ValueIndexedMap();
  ~ValueIndexedMap();

  EQUIP_WITH_INPUT_ITERATOR(data_, ContainerType, RecordType)

  EQUIP_WITH_INSERT_ITERATOR(data_, ContainerType, RecordType, insert)

  const KeyIndexType &KeyIndex() { return data_.get<0>(); }
  const ValueIndexType &ValueIndex() { return data_.get<1>(); }

  std::pair<iterator, bool> insert(const RecordType &new_entry) {
    return data_.insert(new_entry);
  }
  std::pair<iterator, bool> insert(RecordType &&new_entry) {
    return data_.insert(new_entry);
  }
  template <class T> std::pair<iterator, bool> insert(T &&new_entry) {
    return data_.insert(new_entry);
  }

private:
  ContainerType data_;
};

} // namespace mce

#include "ValueIndexedMap.inl"
