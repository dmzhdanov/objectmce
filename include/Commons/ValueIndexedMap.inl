#include "ValueIndexedMap.h"

namespace mce {
#define LOCAL_MCE_VIM(... /*output typename*/)                                 \
  template <class KeyType, class ValueType, bool kRequireUniqueValues>                                    \
  ##__VA_ARGS__ ValueIndexedMap<KeyType, ValueType, kRequireUniqueValues>

LOCAL_MCE_VIM()::ValueIndexedMap() {}

LOCAL_MCE_VIM()::~ValueIndexedMap() {}

#undef LOCAL_MCE_VIM
} // namespace mce
