#include <chrono>

namespace mce {

using TimeVar = std::chrono::high_resolution_clock::time_point;

namespace utilities {

long long MeasureTime(TimeVar *_time, bool reset);

}
} // namespace mce
