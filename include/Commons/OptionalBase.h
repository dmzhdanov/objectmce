#pragma once

#include <type_traits>

namespace mce {
// code is adopted from
// https://codereview.stackexchange.com/questions/101541/optional-base-class-template-to-get-conditional-data-members
namespace block_adl {

template <class> struct EmptyBase {
  EmptyBase() = default;

  template <class... Args> constexpr EmptyBase(Args &&...) {}
};

template <bool condition, class T, class S = T>
using OptionalBase = std::conditional_t<condition, T, EmptyBase<S>>;

template <bool condition, template <class...> class RootBase,
          class NestedBase = EmptyBase<bool>, class... Args>
using NestedOptionalBase = std::conditional_t<
    condition,
    std::conditional_t<
        std::is_same<NestedBase, EmptyBase<bool>>::value,
        RootBase<Args...>, RootBase<Args..., NestedBase>>,
    NestedBase>;
} // namespace block_adl

// deriving from empty_base or optional_base will not make xstd an associated
// namespace this prevents ADL from finding overloads for e.g. begin/end/swap
// from namespace xstd
using block_adl::EmptyBase;
using block_adl::NestedOptionalBase;
using block_adl::OptionalBase;
} // namespace mce
