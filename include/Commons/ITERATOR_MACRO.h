#pragma once
////////////////////////////
//         iterator
////////////////////////////

namespace mce {
#define EQUIP_WITH_INPUT_ITERATOR(CONTAINER_NAME, CONTAINER_TYPE, VALUE_TYPE)  \
  /*private:*/                                                                 \
  template <bool is_const = false> class iterator_template {                   \
  private:                                                                     \
    using _ConstIteratorType = typename CONTAINER_TYPE::const_iterator;        \
    using _IteratorType = typename CONTAINER_TYPE::iterator;                   \
    /* deduce const qualifier from bool is_const parameter */                  \
    using InternalIterator =                                                   \
        std::conditional_t<is_const,                                           \
                                  _ConstIteratorType,                          \
                                  _IteratorType>;                              \
                                                                               \
  public:                                                                      \
    using iterator_category = std::forward_iterator_tag;                       \
    using value_type = VALUE_TYPE;                                             \
    using difference_type = std::ptrdiff_t;                                    \
    /* deduce const qualifier from bool is_const parameter */                  \
    using reference = typename std::conditional<is_const, VALUE_TYPE const &,  \
                                                VALUE_TYPE &>::type;           \
    using pointer = typename std::conditional<is_const, VALUE_TYPE const *,    \
                                              VALUE_TYPE *>::type;             \
                                                                               \
  public:                                                                      \
    template <bool T=is_const, typename = typename std::enable_if<T>::type>    \
    iterator_template(const iterator_template<false> &iterator)                \
        : it_(iterator.it_) {}                                                 \
    iterator_template(const iterator_template &iterator)                       \
        : it_(iterator.it_) {}                                                 \
    iterator_template(const InternalIterator &it) : it_(it) {}                 \
    iterator_template &operator++() {                                          \
      it_++;                                                                   \
      return *this;                                                            \
    }                                                                          \
    iterator_template operator++(int) {                                        \
      iterator_template retval = *this;                                        \
      ++(*this);                                                               \
      return retval;                                                           \
    }                                                                          \
    bool operator==(iterator_template other) const {                           \
      return it_ == other.it_;                                                 \
    }                                                                          \
    bool operator!=(iterator_template other) const {                           \
      return !(*this == other);                                                \
    }                                                                          \
    reference operator*() const { return *it_; }                               \
    pointer operator->() const { return &(*it_); };                            \
                                                                               \
  protected:                                                                   \
    InternalIterator it_;                                                      \
  };                                                                           \
                                                                               \
public:                                                                        \
  typedef iterator_template<false> iterator;                                   \
  typedef iterator_template<true> const_iterator;                              \
  iterator begin() { return iterator(CONTAINER_NAME.begin()); }                \
  iterator end() { return iterator(CONTAINER_NAME.end()); }                    \
  const_iterator begin() const {                                               \
    return const_iterator(CONTAINER_NAME.cbegin());                            \
  }                                                                            \
  const_iterator end() const { return const_iterator(CONTAINER_NAME.cend()); } \
  const_iterator cbegin() const {                                               \
    return const_iterator(CONTAINER_NAME.cbegin());                            \
  }                                                                            \
  const_iterator cend() const { return const_iterator(CONTAINER_NAME.cend()); }

#define EQUIP_WITH_INSERT_ITERATOR(CONTAINER_NAME, CONTAINER_TYPE, VALUE_TYPE, \
                                   INSERTER_FUNCTION)                          \
protected:                                                                     \
  class insert_iterator_template {                                             \
  protected:                                                                   \
    using iterator_category = std::forward_iterator_tag;                       \
    using InsertIteratorType = insert_iterator_template;                       \
    CONTAINER_TYPE *container_;                                                \
                                                                               \
  public:                                                                      \
    explicit insert_iterator_template(CONTAINER_TYPE &container)               \
        : container_(&container) {}                                            \
    InsertIteratorType &operator=(const VALUE_TYPE &value) {                   \
      container_->INSERTER_FUNCTION(value);                                    \
      return *this;                                                            \
    }                                                                          \
    InsertIteratorType &operator*() { return *this; }                          \
    InsertIteratorType &operator++() { return *this; }                         \
    InsertIteratorType operator++(int) { return *this; }                       \
  };                                                                           \
                                                                               \
public:                                                                        \
  insert_iterator_template insert_iterator() {                                 \
    return insert_iterator_template(CONTAINER_NAME);                           \
  };

} // namespace mce
