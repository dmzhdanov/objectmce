#include "text.h"

namespace mce {
namespace utilities {

template <class T0, int T1, int T2, int T3>
void EgMtExport(std::string filename,
                const Eigen::Matrix<T0, T1, T2, T3> &mat) {
  std::ofstream stream;
  stream.open(filename);
  stream << "{{";
  for (int i = 0; i < mat.rows(); i++) {
    for (int j = 0; j < mat.cols(); j++) {
      stream << util::num2mstr(mat(i, j))
             << ((j == mat.cols() - 1) ? "}" : ", ");
    };
    stream << ((i == mat.rows() - 1) ? "}" : ",\n{");
  };
  stream.close();
}; // namespace
   // utilitiestemplate<classT0,intT1,intT2,intT3>voidEgMtExport(std::stringfilename,constEigen::Matrix<T0,T1,T2,T3>&mat)

template <class _SparseMatrixType,
          typename = typename std::enable_if<
              std::is_base_of<Eigen::SparseMatrixBase<_SparseMatrixType>,
                              _SparseMatrixType>::value>::type>
void EgMtExport(std::string filename, const _SparseMatrixType &mat) {
  std::ofstream stream;
  stream.open(filename);
  stream << "SparseArray[";
  bool first = true;
  for (int i = 0; i < mat.outerSize(); i++) {
    for (typename _SparseMatrixType::InnerIterator it(mat, i); it; ++it) {
      stream << (first ? '{' : ',') << '{' << it.row() + 1 << ','
             << it.col() + 1 << "}->" << util::num2mstr(it.value());
      first = false;
    };
  };
  stream << "}, {" << mat.rows() << ',' << mat.cols() << "}]";
  stream.close();
};

} // namespace utilities
} // namespace mce
