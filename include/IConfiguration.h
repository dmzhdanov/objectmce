#pragma once

#include "Definitions.h"
#include "Wavefunction/IBasisFunction.h"
//#include "Wavefunction/Wavefunction.h"

namespace mce {

class Wavefunction;

class IConfiguration {
public:
  IConfiguration(){};
  virtual ~IConfiguration(){};

  virtual size_t NumberOfSpatialDOFs() const = 0;
  virtual size_t NumberOfPESs() const = 0;
  // virtual double Hbar() const = 0;
  virtual double GaussianWidth(SpatialDOF_id dof_id = 0) const = 0;
  virtual double GaussianQuadraticCoefficient(SpatialDOF_id dof_id = 0) const {
    return 1. / (2. * GaussianWidth() * GaussianWidth());
  };
  virtual double GaussianOverlapCutoff(
      SpatialDOF_id dof_id = 0) const = 0; // in units of GaussianWidth

  // funcitons
  virtual void CWiseAbs(std::shared_ptr<IBasisFunction> basis_function) const = 0;
  virtual double InfinityNorm(const Wavefunction &wavefunction) const = 0;
};

// IConfiguration::IConfiguration() {}
} // namespace mce
