#pragma once

#include "Operator_CCS.h"

namespace mce {

template <class PolicyType> class OperatorFactory {
public:
  OperatorFactory();
  ~OperatorFactory();
  Operator<PolicyType> CreateX(SpatialDOF_id dof_id) const;
  Operator<PolicyType> CreateP(SpatialDOF_id dof_id) const;
  Operator<PolicyType> CreateId(SpatialDOF_id dof_id) const;
  Operator<PolicyType> CreateExp(const Operator<PolicyType> &exponent) const;

private:
  Operator<PolicyType> CreateTaylorEntry(SpatialDOF_id dof_id,
                                         const MatrixXc_r &taylor_coefficients) const;
};

} // namespace mce

#include "OperatorFactory.inl"
