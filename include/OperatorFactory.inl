//#include "stdafx.h"

#include "Operator/OperatorEntryGaussian.h"
#include "OperatorFactory.h"

namespace mce {
	template <class PolicyType>
	OperatorFactory<PolicyType>::OperatorFactory()
	{
	}

	template <class PolicyType>
	OperatorFactory<PolicyType>::~OperatorFactory()
	{
	}

	template <class PolicyType>
	Operator<PolicyType> OperatorFactory<PolicyType>::CreateX(SpatialDOF_id dof_id) const
	{
		MatrixXc_r taylor_coefficients(1, 2);
		taylor_coefficients << 0, 1;
		return CreateTaylorEntry(dof_id, taylor_coefficients);
	};

	template <class PolicyType>
	Operator<PolicyType> OperatorFactory<PolicyType>::CreateP(SpatialDOF_id dof_id) const
	{
		MatrixXc_r taylor_coefficients(2, 1);
		taylor_coefficients << 0, 1;
		return CreateTaylorEntry(dof_id, taylor_coefficients);
	};
	
	template <class PolicyType>
	Operator<PolicyType> OperatorFactory<PolicyType>::CreateId(SpatialDOF_id dof_id) const
	{
		MatrixXc_r taylor_coefficients(1, 1);
		taylor_coefficients << 1;
		return CreateTaylorEntry(dof_id, taylor_coefficients);
	};

	template <class PolicyType>
	Operator<PolicyType> OperatorFactory<PolicyType>::CreateExp(const Operator<PolicyType>& exponent) const
	{
		Operator copy = exponent;
		if (copy.OperatorBase().NumberOfSummands() > 1)
		{
			QI_Error::RaiseError("[ERROR] OperatorFactory::CreateExp - Cannot exponentiate operators having > 1 summands.");
		}
		for (auto& summand : copy.OperatorBase())
		{
			for (auto &multiplicand : *summand)
			{
				auto p_multiplicand = std::static_pointer_cast<OperatorEntryGaussian<PolicyType>>(multiplicand.second);
				if (!p_multiplicand)
				{
					QI_Error::RaiseError("[ERROR] OperatorFactory::CreateExp - Cannot exponentiate entries other than OperatorEntryGaussian.");
				}
				if (p_multiplicand->exp_taylor_coefficients_.size() > 0)
				{
					QI_Error::RaiseError("[ERROR] OperatorFactory::CreateExp - Repeated exponentiation of OperatorEntryGaussian is not supported.");
				}
				if (p_multiplicand->taylor_coefficients_.rows() != 1)
				{
					QI_Error::RaiseError("[ERROR] OperatorFactory::CreateExp - Exponentiation of momenta is not supported.");
				}
				if (p_multiplicand->taylor_coefficients_.cols() > 3)
				{
					QI_Error::RaiseError("[ERROR] OperatorFactory::CreateExp - Exponentiation of powers higher than 2 is not supported.");
				}
				p_multiplicand->exp_taylor_coefficients_= p_multiplicand->taylor_coefficients_.block(0,0,1, p_multiplicand->taylor_coefficients_.cols());
				p_multiplicand->taylor_coefficients_ = MatrixXc_r(1,1);
				p_multiplicand->taylor_coefficients_ << 1.;
			}
		}
		return copy;
	}

	template <class PolicyType>
	Operator<PolicyType> OperatorFactory<PolicyType>::CreateTaylorEntry(SpatialDOF_id dof_id, const MatrixXc_r& taylor_coefficients) const
	{
		Operator<PolicyType> output;
		auto product = std::make_shared<internal::SpatialProduct<IOperatorEntry<PolicyType>>>();
		product->AddMultiplicand(dof_id, std::make_shared<OperatorEntryGaussian<PolicyType>>(taylor_coefficients));
		output.operator_base_.AddSpatialProduct(product);
		return output;
	};

}
