#pragma once

#include "Definitions.h"
#include "Wavefunction/PhaseSpacePoint.h"

namespace mce {
namespace utilities {
PhaseSpacePoint
PhaseSpacePointFromMatrix(const Eigen::Ref<const MatrixXd_r> &matrix);
}
} // namespace mce
