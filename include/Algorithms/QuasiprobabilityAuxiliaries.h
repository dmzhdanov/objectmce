#pragma once

#include "Definitions.h"

#include "Wavefunction/CanonicPair.h"

namespace mce {
namespace quasiprobability_auxiliaries {

// Interatively calculates the integrals of form
// $I_n=\int_{-\infty}^{\infty}x^n erf(a x + b) \exp(-\alpha x^2 + \beta x) dx$
// where p ranges from 0 to p_max,
// Result is written to the 0-th argument.
// Result is multiplied by exp(exponential_regularizing_multiplier)
void ErfGaussIntegral(
    VectorXc_c &output, std::complex<double> a, std::complex<double> b,
    double alpha, std::complex<double> beta, int p_max,
    std::complex<double> exponential_regularizing_multiplier = 0.);

// Calculates the integral
// $J(y)=\int_{-\infty}^{\infty}K_{\lambda}(z-y,(y+z)/2-X,P,a,b)\chi(p2,x2,z,\alpha2)dz$,
// where
// $K_{\lambda}(\lambda,\delta x,P,a,b) = \sqrt{\frac{\pi}{b}}e^{-a*\delta
// x^2-\frac{\lambda^2}{4*b*\hbar^2}-i\frac{\lambda P}{\hbar}}$ and
// $\chi(p2,x2,z,\alpha2){=}\sqrt[1/4]{\frac{2\alpha2}{2\pi}}e^{-\alpha2*(z-x)^2+i*p2*\frac{z-x}{\hbar}}$.
// Returns the tuple of coefficients $(\mu,p,x,\alpha)$, such that
// $J(y)=\mu\chi(p,x,y,\alpha)$.
std::tuple<std::complex<double>, double, double, double>
JIntegralCoefficients(double P, double X, double a, double b, double p2,
                      double x2, double alpha2);

std::tuple<std::complex<double>, CanonicPair, double>
JIntegralCoefficients(const CanonicPair &kernel_center,
                      const CanonicPair &kernel_width,
                      const CanonicPair &ket_center, double ket_alpha);

// Calculates integral
/* $\frac{1}{\pi  \hbar } \int_{-\infty }^{\infty }  \chi^*
 (p_1,x_1,y,\alpha_1) V(y) (\int_{-\infty }^{\infty }
 \frac{\psi(p_2,x_2,y,\alpha_2)
 K_{\lambda}(z-y,\frac{y+z}{2}-X,P,a,b)}{z-y}dz) dy$, */
// where
// $K_{\lambda}(\lambda,\delta x,P,a,b) = \sqrt{\frac{\pi}{b}}e^{-a*\delta
// x^2-\frac{\lambda^2}{4*b*\hbar^2}-i\frac{\lambda P}{\hbar}}$ and
// $\chi(p2,x2,z,\alpha2){=}\sqrt[1/4]{\frac{2\alpha2}{2\pi}}e^{-\alpha2*(z-x)^2+i*p2*\frac{z-x}{\hbar}}$,
// and
// V(y) is given by exp_taylor_coefficients and taylor_coefficients expansion
// coefficients, such that $V(y){=}sum_i taylor_coefficients_i y^i e^{sum_j
// exp_taylor_coefficients_j y^j}$
std::complex<double> GaussianWeightedMomentumFlow(
    double P, double X, double a, double b, double p1, double x1, double alpha1,
    double p2, double x2, double alpha2, const MatrixXc_r &taylor_coefficients,
    const VectorXc_r &exp_taylor_coefficients);

std::complex<double>
GaussianWeightedMomentumFlow(const CanonicPair &kernel_center,
                             const CanonicPair &kernel_width,
                             const CanonicPair &bra_center, double bra_alpha,
                             const CanonicPair &ket_center, double ket_alpha,
                             const MatrixXc_r &taylor_coefficients,
                             const VectorXc_r &exp_taylor_coefficients);

// Calculates integral
/* $\int_{-\infty}^{\infty}\int_{-\infty}^{\infty}dxd\lambda
(1/(2*\pi*\hbar))*\chi(-p1,x1,x-\lambda/2,\alpha1)*\chi(p2,x2,x+\lambda/2,\alpha2)
(-V(x-\lambda/2)) K_{\lambda}(\lambda,x-X,P,a,b,\hbar)$
*/
// here
/*
$K_{\lambda}(\lambda,\delta x,P,a,b) = \sqrt{\frac{\pi}{b}}e^{-a*\delta
 x^2-\frac{\lambda^2}{4*b*\hbar^2}-i\frac{\lambda P}{\hbar}}$
 */
// and
// $\chi(p2,x2,z,\alpha2){=}\sqrt[1/4]{\frac{2\alpha2}{2\pi}}e^{-\alpha2*(z-x)^2+i*p2*\frac{z-x}{\hbar}}$,
// the input parameters here are: P=kernel_center.P(), X=kernel_center.X(), p1 =
// bra_center.P(), x1 = bra_center.X(), p2 = ket_center.P(), x2 =
// ket_center.X(), alpha1=bra_alpha, alpha2= ket_alpha taylor_coefficients
// should represent the operator p/m (so, exp_taylor_coefficients should be
// empty)
std::complex<double> ComplementaryGaussianWeightedMomentumFlow(
    const CanonicPair &kernel_center, const CanonicPair &kernel_width,
    const CanonicPair &bra_center, double bra_alpha,
    const CanonicPair &ket_center, double ket_alpha,
    const MatrixXc_r &taylor_coefficients,
    const VectorXc_r &exp_taylor_coefficients);

// Calculates integral
/* \int_{-\infty}^{\infty}\int_{-\infty}^{\infty}dxd\lambda
(1/(2*\pi*\hbar))*\chi(-p1,x1,x-\lambda/2,\alpha1)*\chi(p2,x2,x+\lambda/2,\alpha2)*I*\hbar*\pder{K\lambda(\lambda,x-X,P,a,b,\hbar)}{\lambda}
*/
// the input parameters here are: P=kernel_center.P(), X=kernel_center.X(), p1 =
// bra_center.P(), x1 = bra_center.X(), p2 = ket_center.P(), x2 =
// ket_center.X(), alpha1=bra_alpha, alpha2= ket_alpha taylor_coefficients
// should represent the operator p/m (so, exp_taylor_coefficients should be
// empty)
std::complex<double>
GaussianWeightedPositionFlow(const CanonicPair &kernel_center,
                             const CanonicPair &kernel_width,
                             const CanonicPair &bra_center, double bra_alpha,
                             const CanonicPair &ket_center, double ket_alpha,
                             const MatrixXc_r &taylor_coefficients,
                             const VectorXc_r &exp_taylor_coefficients);

class HusimiFunctionMatrixElement {
public:
  HusimiFunctionMatrixElement(
      const CanonicPair &phase_space_point, const CanonicPair &bra,
      double bra_alpha, const CanonicPair &ket, double ket_alpha,
      const CanonicPair &kernel_gaussian_quadratic_coefficients_ab);

  std::complex<double> Value();
  std::complex<double>
  RightPolynomialOperatorValue(const MatrixXc_r &taylor_coefficients);

private:
  const CanonicPair phase_space_point_, bra_, ket_,
      kernel_gaussian_quadratic_coefficients_ab_;
  std::complex<double> bra_alpha_, ket_alpha_;
  std::complex<double> ddenom_;
  std::complex<double> nnom1_;
  std::complex<double> A_;
  std::complex<double> B_;
  std::complex<double> C_;
  std::complex<double> D_;
  std::complex<double> PP_, XX_;
};

// Calculates the $ij$ matrix element
// $\frak{G}[\int_{\infty}^{infty}(\frac{1}{2\pi\hbar}\psi_i(x-y/2) O(\hat
// p,\hat x+y/2)\psi_j(x+y/2)})e^{-ipy/\hbar}d y]$
std::complex<double> RightHusimiOperatorMatrixElement(
    const MatrixXc_r &taylor_coefficients,
    const VectorXc_r &exp_taylor_coefficients,
    const CanonicPair &phase_space_point, const CanonicPair &bra,
    double bra_alpha, const CanonicPair &ket, double ket_alpha,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab);

} // namespace quasiprobability_auxiliaries
} // namespace mce
