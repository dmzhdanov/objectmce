#pragma once

#include <vector>

#include "Evaluator/Evaluator_CCS.h"
#include "OperatorFactory.h"
#include "Operator_CCS.h"

namespace mce {
namespace ccs_system_utilities {
// momentum is in real part
std::complex<double> GetPilotWaveMomentum(Wavefunction state, size_t basis_idx,
                            const Operator<MCE_POLICY(CCS)> &momentum_operator,
                            const Operator<MCE_POLICY(CCS)> &smoothing_kernel);
std::complex<double> GetPilotWaveMomentumSquared(Wavefunction state, size_t basis_idx,
	const Operator<MCE_POLICY(CCS)> &momentum_operator,
	const Operator<MCE_POLICY(CCS)> &smoothing_kernel);
std::complex<double> GetPilotWaveMomentumDerivative(
	Wavefunction state, size_t basis_idx,
	const Operator<MCE_POLICY(CCS)> &hamiltonian,
	const Operator<MCE_POLICY(CCS)> &momentum_operator,
	const Operator<MCE_POLICY(CCS)> &smoothing_kernel);

std::complex<double> GetLocalAverageTimeDerivative(
	Wavefunction state,
	const Operator<MCE_POLICY(CCS)> &hamiltonian,
	const Operator<MCE_POLICY(CCS)> &observable,
	const Operator<MCE_POLICY(CCS)> &smoothing_kernel);

std::complex<double> GetMaterialTimeDerivative(Wavefunction state,
	const Operator<MCE_POLICY(CCS)> &hamiltonian,
	const Operator<MCE_POLICY(CCS)> &observable,
	const Operator<MCE_POLICY(CCS)> &momentum_operator,
	const Operator<MCE_POLICY(CCS)> &smoothing_kernel,
	double velocity);

//returns the squared pilot wave momentum and its material time derivative
std::pair<double,double> GetApproximateMaterialTimeDerivativeOfSquaredPilotWaveMomentum(Wavefunction state,
	const Operator<MCE_POLICY(CCS)> &hamiltonian,
	const Operator<MCE_POLICY(CCS)> &momentum_operator,
	const Operator<MCE_POLICY(CCS)> &identity_operator,
	const Operator<MCE_POLICY(CCS)> &smoothing_kernel,
	double velocity);

// velocity is in real part
std::complex<double> GetPilotWaveVelocity(Wavefunction state, size_t basis_idx,
	const Operator<MCE_POLICY(CCS)> &hamiltonian,
	const Operator<MCE_POLICY(CCS)> &position_operator,
	const Operator<MCE_POLICY(CCS)> &smoothing_kernel);

Operator<MCE_POLICY(CCS)> GetPilotSmoothingKernel(
    const Wavefunction &state, size_t basis_id,
    const OperatorFactory<MCE_POLICY(CCS)> &operator_factory,
    double smoothing_scale);

class PilotWaveQuantumForceEvaluator {
public:
  PilotWaveQuantumForceEvaluator(
      Wavefunction state, size_t basis_idx,
      const Operator<MCE_POLICY(CCS)> &hamiltonian,
      const OperatorFactory<MCE_POLICY(CCS)> &operator_factory,
      const Operator<MCE_POLICY(CCS)> &smoothing_kernel);
  ~PilotWaveQuantumForceEvaluator(){};
  double operator()(SpatialDOF_id dof_id) const;

private:
  Operator<MCE_POLICY(CCS)> Der(SpatialDOF_id dof_id,
                                const Operator<MCE_POLICY(CCS)> &O) const;
  Operator<MCE_POLICY(CCS)> Der2(SpatialDOF_id dof_id,
                                 const Operator<MCE_POLICY(CCS)> &O) const;

private:
  const Wavefunction state_;
  const size_t basis_idx_;
  Operator<MCE_POLICY(CCS)> hamiltonian_;
  OperatorFactory<MCE_POLICY(CCS)> operator_factory_;
  Operator<MCE_POLICY(CCS)> smoothing_kernel_;

  std::unique_ptr<Operator<MCE_POLICY(CCS)>> sigma_, s1_, K_;
  double _sigma_, _s1_, _s2_, _K_;
};

} // namespace ccs_system_utilities
} // namespace mce
