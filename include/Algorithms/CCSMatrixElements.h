#pragma once

#include "Definitions.h"
#include "Wavefunction/CanonicPair.h"

namespace mce {
namespace math {
// calculates the eigenvalue corresponding to given CCS ket vector
std::complex<double>
CCSEigenvalue(const CanonicPair &ket,
              const double gaussian_quadratic_coefficient_ket);

// calculates overlap matrix element $\scpr{bra}{ket}$ assuming that the
// gaussian widths given by 3rd and 4th parameters
std::complex<double> CCSOverlap(
    const CanonicPair &bra, const CanonicPair ket,
    const double gaussian_quadratic_coefficient_bra,
    const double gaussian_quadratic_coefficient_ket,
    std::complex<double> optional_exponent = 0 /*for regularization purposes*/);

// calculates overlap matrix element $\scpr{bra}{ket}$ assuming that the
// gaussian widths given by 3rd and 4th parameters
std::complex<double> CCSOverlap(
    const std::complex<double> &z_bra_conj, const std::complex<double> &z_ket,
    const double gaussian_quadratic_coefficient_bra,
    const double gaussian_quadratic_coefficient_ket,
    std::complex<double> optional_exponent = 0 /*for regularization purposes*/);

// returns multiplicative prefactor
std::complex<double>
CCSGaussianMultiplyInPlace(const VectorXc_r &exp_taylor_coefficients,
                           CanonicPair &state,
                           double &gaussian_quadratic_coefficient);

std::tuple<std::complex<double>, std::complex<double>>
CCSGaussianMultiplyInPlace2(const VectorXc_r &exp_taylor_coefficients,
                            CanonicPair &state,
                            double &gaussian_quadratic_coefficient);

MatrixXc_r_sp
GetCreationAnnihilationMatrix(const MatrixXc_r &taylor_coefficients,
                              const double gaussian_quadratic_coefficient_bra,
                              const double gaussian_quadratic_coefficient_ket);

std::complex<double>
GetGaussianMatrixElement(const CanonicPair &state1, double alpha1,
                         const CanonicPair &state2, double alpha2,
                         const MatrixXc_r &taylor_coefficients,
                         const VectorXc_r &exp_taylor_coefficients);

}; // namespace math
} // namespace mce
