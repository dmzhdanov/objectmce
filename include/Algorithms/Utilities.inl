#include "Utilities.h"
#include <set>

namespace mce {
namespace utilities {

template <class EigenMatrixType>
void ChopToFitNonZeros(EigenMatrixType &matrix) {

  if (matrix.size() == 0) {
    matrix.resize(
        matrix.RowsAtCompileTime == Eigen::Dynamic ? 0
                                                   : matrix.RowsAtCompileTime,
        matrix.ColsAtCompileTime == Eigen::Dynamic ? 0
                                                   : matrix.ColsAtCompileTime);
    return;
  }
  size_t rows = matrix.rows();
  size_t cols = matrix.cols();
  if (matrix.RowsAtCompileTime == Eigen::Dynamic) {
    while (rows > 0 && matrix.row(rows - 1).cwiseAbs().maxCoeff() < kTolerance)
      rows--;
  }
  if (rows > 0)
    while (cols > 0 && matrix.col(cols - 1).cwiseAbs().maxCoeff() < kTolerance)
      cols--;
  else {
    if (matrix.ColsAtCompileTime == Eigen::Dynamic)
      cols = 0;
  }
  matrix.conservativeResize(rows, cols);
}

template <class T>
VectorX<T> PowerSeries(const size_t length, const T multiplier,
                       T initial_value) {
  VectorX<T> output(length);
  if (length == 0)
    return output;
  output(0) = initial_value;
  for (size_t i = 1; i < length; i++) {
    initial_value *= multiplier;
    output(i) = initial_value;
  }
  return output;
};

// n*log(n) realisation without hash tables.
template <class ContainerType>
bool CheckNoDuplicates(const ContainerType &left, const ContainerType &right) {
  std::set<typename ContainerType::value_type> left_set;
  for (const auto &element : left) {
    left_set.insert(element);
  }
  // std::copy(left.begin(), left.end(),
  //          std::inserter(left_set, left_set.begin()));
  for (const auto &element : right) {
    if (std::find(left_set.begin(), left_set.end(), element) !=
        left_set.end()) {
      return false;
    };
  }
  return true;
}

template <class MultipliersIt, class CoefficientsIt>
void HermiteSeries(MultipliersIt m_first, MultipliersIt m_last,
                   CoefficientsIt c_first) {
  using Index = double;// decltype(*c_first);
  int max_order = int(m_last - m_first);
  std::vector<Index> hermite_coefficients(max_order + 1);
  std::fill(hermite_coefficients.begin(), hermite_coefficients.end(), 0);
  std::fill(c_first, c_first+ max_order, 0);
  hermite_coefficients[0] = 1;
  *c_first += (*m_first);
  m_first++;
  for (int i = 1; i < max_order; i++) {
	//*** updating hermite coefficients
	auto old_coeff = hermite_coefficients[0];
    hermite_coefficients[0] = -hermite_coefficients[1];
	*c_first += hermite_coefficients[0] * (*m_first);
	for (int j = 1; j < i; j++) {
	  auto old_coeff2 = hermite_coefficients[j];
      hermite_coefficients[j] = 2 * old_coeff/*hermite_coefficients[j - 1]*/ -
                                     (j + 1) * hermite_coefficients[j + 1];
	  old_coeff = old_coeff2;
	  *(c_first+j) += hermite_coefficients[j] * (*m_first);
    }
	hermite_coefficients[i] = 2 * old_coeff;
	*(c_first + i) += hermite_coefficients[i] * (*m_first);
	//*** end of updating Hermite coefficients
	m_first++;
  }
}

} // namespace utilities
} // namespace mce
