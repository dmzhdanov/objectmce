#pragma once

#include "Wavefunction/Wavefunction.h"

#include <boost/numeric/odeint.hpp>

// see
// https://www.boost.org/doc/libs/1_53_0/libs/numeric/odeint/doc/html/boost_numeric_odeint/odeint_in_detail/state_types__algebras_and_operations.html#boost_numeric_odeint.odeint_in_detail.state_types__algebras_and_operations.algebras_and_operations.vector_space_algebra

// used by ODEInt only for steppers with error control
namespace boost {
namespace numeric {
namespace odeint {
template <> struct vector_space_norm_inf<mce::Wavefunction> {
  typedef double result_type;
  double operator()(const mce::Wavefunction &state) const {
    using std::abs;
    using std::max;
    if (state.IsNull()) {
      return 0;
    }
    return state.Configuration().InfinityNorm(state);
    // return max(max(abs(p.x), abs(p.y)), abs(p.z));
  }
};
}
}
}
