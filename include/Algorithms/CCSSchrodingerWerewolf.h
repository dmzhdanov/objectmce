#pragma once

#include "Algorithms/CCSQuasiprobabilityFlowBundle.h"
#include "Algorithms/CCSQuasiprobabilityGaugeBundle.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityOperator_CCS.h"
#include "Algorithms/ISystem.h"
#include "Definitions.h"
#include "OperatorFactory.h"

namespace mce {
namespace algorithm {

class CCSWerewolfPolicy;

class CCSSchrodingerWerewolfSystem : public ISystem {
public:
  using PolicyType = MCE_POLICY(CCS);

  friend class CCSWerewolfPolicy;

  CCSSchrodingerWerewolfSystem(const Operator<PolicyType> &hamiltonian,
                               const Operator<PolicyType> &linear_solve_kernel,
                               OperatorFactory<PolicyType> &operator_factory);
  ~CCSSchrodingerWerewolfSystem() override{};

  void operator()(const Wavefunction &state, Wavefunction &pder_t_state,
                  const double t) const override;

  void SetSmoothingScale(double smoothing_scale); // in units of alpha
  void SetRegularizer(
      double
          reference_amplitude_max_regularizer); // for solving linear system
                                                // A.x = b => (A +
                                                // reference_amplitude_max_regularizer
                                                // I).x = b
  inline void SetMomentumMMARegularizer(double momentum_mma_regularizer) {
    momentum_mma_regularizer_ = momentum_mma_regularizer;
  }
  inline void SetAmplitudeMMARegularizer(double amplitude_mma_regularizer) {
    amplitude_mma_regularizer_ = amplitude_mma_regularizer;
  }
  inline void SetReprojectionRegularizer(double reprojection_regularizer) {
	  reprojection_regularizer_ = reprojection_regularizer;
  }

  void SetRegularizePhaseScale(double scale) {
    regularize_phase_scale_ = scale;
  }

  void SetGaugeApproximationOrder(unsigned gauge_approximation_order) {
    gauge_approximation_order_ = gauge_approximation_order;
  }

  inline void SetHusimiWidthFactors(double p_factor, double x_factor) {
    husimi_width_factors_.P(p_factor);
    husimi_width_factors_.X(x_factor);
  }

  // private:
  //  Operator<PolicyType> GetPilotSmoothingKernel(const Wavefunction &state,
  //                                               size_t basis_id, double
  //                                               optional_additional_scale =
  //                                               1) const;

private:
  const Operator<PolicyType> hamiltonian_;
  const Operator<PolicyType> linear_solve_kernel_;
  const OperatorFactory<PolicyType> operator_factory_;

  mutable std::shared_ptr<
      quasiprobability_auxiliaries::CCSQuasiprobabilityFlowBundle>
      quasiprobability_bundle_;

  mutable std::shared_ptr<
      quasiprobability_gauge_auxiliaries::CCSQuasiprobabilityGaugeBundle>
      quasiprobability_gauge_bundle_;

  double amplitude_mma_regularizer_;
  double momentum_mma_regularizer_;
  mutable double amplitude_max_regularizer_;
  double reference_amplitude_max_regularizer_;
  double reprojection_regularizer_;
  unsigned gauge_approximation_order_;

  CanonicPair husimi_width_factors_;

  double smoothing_scale_;

  double regularize_phase_scale_;
};
} // namespace algorithm
} // namespace mce
