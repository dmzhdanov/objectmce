#pragma once

#include "Definitions.h"

#include "Wavefunction/CanonicPair.h"

namespace mce {
namespace quasiprobability_gauge_auxiliaries {

#define DEFAULT_JBG2_ORDER -1

std::complex<double>
HusimiFunctionMatrixElement(const CanonicPair &phase_space_point,
                            const CanonicPair &bra, const CanonicPair &ket,
                            const CanonicPair
                                &kernel_gaussian_quadratic_coefficients_ab,
                            double wavefunction_gaussian_quadratic_coefficient /*must be the same for bra and ket*/);

std::complex<double>
HusimiFunctionXDerivativeMatrixElement(const CanonicPair &phase_space_point,
                                       const CanonicPair &bra,
                                       const CanonicPair &ket,
                                       const CanonicPair &
                                           kernel_gaussian_quadratic_coefficients_ab,
                                       double
                                           wavefunction_gaussian_quadratic_coefficient /*must be the same for bra and ket*/);

std::complex<double>
HusimiFunctionPDerivativeMatrixElement(const CanonicPair &phase_space_point,
                                       const CanonicPair &bra,
                                       const CanonicPair &ket,
                                       const CanonicPair &
                                           kernel_gaussian_quadratic_coefficients_ab,
                                       double
                                           wavefunction_gaussian_quadratic_coefficient /*must be the same for bra and ket*/);

std::complex<double> JBG2FunctionMatrixElement(
    const CanonicPair &phase_space_point, const CanonicPair &bra,
    const CanonicPair &ket,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab,
    double wavefunction_gaussian_quadratic_coefficient /*must be the same for
                                                          bra and ket*/
    ,
    unsigned int approximation_order = DEFAULT_JBG2_ORDER);

std::complex<double> JBG2FunctionPDerivativeMatrixElement(
    const CanonicPair &phase_space_point, const CanonicPair &bra,
    const CanonicPair &ket,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab,
    double wavefunction_gaussian_quadratic_coefficient /*must be the same for
                                                          bra and ket*/
    ,
	unsigned int approximation_order = DEFAULT_JBG2_ORDER);

std::complex<double>
GaugeMomentumFlowMatrixElement(double inverse_mass, double p_mean,
                               double p_mean_x_derivative,
                               const CanonicPair &phase_space_point,
                               const CanonicPair &bra, const CanonicPair &ket,
                               const CanonicPair
                                   &kernel_gaussian_quadratic_coefficients_ab,
                               double
                                   wavefunction_gaussian_quadratic_coefficient /*must be the same for bra and ket*/
	,
	unsigned int approximation_order = DEFAULT_JBG2_ORDER);

std::complex<double>
GaugePositionFlowMatrixElement(double inverse_mass, double p_mean,
                               const CanonicPair &phase_space_point,
                               const CanonicPair &bra, const CanonicPair &ket,
                               const CanonicPair
                                   &kernel_gaussian_quadratic_coefficients_ab,
                               double
                                   wavefunction_gaussian_quadratic_coefficient /*must be the same for bra and ket*/,
	unsigned int approximation_order = DEFAULT_JBG2_ORDER);

std::complex<double> GaugeMomentumFlowMatrixElement(
    const MatrixXc_r &taylor_coefficients,
    const MatrixXc_r &exp_taylor_coefficients, double p_mean,
    double p_mean_x_derivative, const CanonicPair &phase_space_point,
    const CanonicPair &bra,
    double alpha_bra, /*must be the same for bra and ket*/
    const CanonicPair &ket,
    double alpha_ket, /*must be the same for bra and ket*/
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab,
	unsigned int approximation_order = DEFAULT_JBG2_ORDER);

std::complex<double> GaugePositionFlowMatrixElement(
    const MatrixXc_r &taylor_coefficients,
    const MatrixXc_r &exp_taylor_coefficients, double p_mean,
    const CanonicPair &phase_space_point, const CanonicPair &bra,
    double alpha_bra /*gaussian quadratic coefficient, must be the same for bra
                        and ket*/
    ,
    const CanonicPair &ket,
    double alpha_ket /*gaussian quadratic coefficient, must be the same for bra
                        and ket*/
    ,
    const CanonicPair &kernel_gaussian_quadratic_coefficients_ab,
	unsigned int approximation_order = DEFAULT_JBG2_ORDER);

#undef DEFAULT_JBG2_ALGORITHM
} // namespace quasiprobability_gauge_auxiliaries
} // namespace mce
