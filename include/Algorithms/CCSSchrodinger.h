#pragma once

#include "Algorithms/ISystem.h"
#include "Definitions.h"
#include "OperatorFactory.h"

namespace mce {
namespace algorithm {
class CCSSchrodingerSystem : public ISystem {
public:
  using PolicyType = MCE_POLICY(CCS);

  CCSSchrodingerSystem(const Operator<PolicyType>& hamiltonian, const Operator<PolicyType>& linear_solve_kernel, OperatorFactory<PolicyType>& operator_factory);
  ~CCSSchrodingerSystem() override {};

  void operator()(const Wavefunction &state, Wavefunction &pder_t_state,
                  const double t) const override;

private:
  const Operator<PolicyType> hamiltonian_;
  const Operator<PolicyType> linear_solve_kernel_;
  const OperatorFactory<PolicyType> operator_factory_;

  mutable double amplitude_max_regularizer_;
};
} // namespace algorthm
} // namespace mce
