#pragma once

#include "Definitions.h"

#include "Evaluator/EvaluatorStrategyQuasiprobabilityGauge_CCS.h"
#include "Evaluator/Evaluator_CCS.h"
#include "OperatorFactory.h"
#include "Wavefunction/CanonicPair.h"

namespace mce {
namespace quasiprobability_gauge_auxiliaries {

class CCSQuasiprobabilityGaugeBundle {
public:
  using PolicyType = MCE_POLICY(CCS);
  using OperatorType = Operator<PolicyType>;
  using OperatorEntryType = std::shared_ptr<OperatorType>;
  using EvaluatorType = Evaluator<PolicyType>;
  using EvaluatorEntryType = std::shared_ptr<EvaluatorType>;
  using EvaluatorStrategyType =
      evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(CCS)>;
  using EvaluatorStrategyEntryType = std::shared_ptr<EvaluatorStrategyType>;

  CCSQuasiprobabilityGaugeBundle(
      const OperatorFactory<PolicyType> &operator_factory,
      const OperatorType &hamiltonian, SpatialDOF_id number_of_dofs,
      double pilot_smoothing_scale,
      const CanonicPair &width_factors = CanonicPair(1, 1),
      unsigned approximation_order = -1);

  void Initialize(Wavefunction wavefunction);
  void Reset();

  bool IsInitialized();

  double EvaluatePositionFlow(size_t center_basis_function_id,
                              SpatialDOF_id dof_id);

  // double EvaluatePositionFlowTest(size_t center_basis_function_id,
  // SpatialDOF_id dof_id);

  double EvaluateMomentumFlow(size_t center_basis_function_id,
                              SpatialDOF_id dof_id);

  double EvaluateHusimiFunction(size_t center_basis_function_id);

  EvaluatorStrategyEntryType Strategy() { return strategy_; }

private:
  const OperatorFactory<PolicyType> operator_factory_;
  const SpatialDOF_id number_of_dofs_;
  const double pilot_smoothing_scale_;

  OperatorEntryType identity_;
  std::vector<OperatorEntryType> velocities_;
  std::vector<OperatorEntryType> momenta_;
  EvaluatorStrategyEntryType strategy_;

  EvaluatorEntryType norm_ev_;
  std::vector<EvaluatorEntryType> velocities_pos_ev_;
  std::vector<EvaluatorEntryType> velocities_mom_ev_;

  Wavefunction wavefunction_; /*shallow copy of wavefunction*/
};

} // namespace quasiprobability_gauge_auxiliaries
} // namespace mce
