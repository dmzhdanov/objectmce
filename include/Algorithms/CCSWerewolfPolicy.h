#pragma once

#include "Algorithms/CCSPropagator.h"
#include "Algorithms/CCSSchrodingerWerewolf.h"

namespace mce {
namespace algorithm {
class CCSWerewolfPolicy {
public:
  using PolicyType = MCE_POLICY(CCS);
  CCSWerewolfPolicy(const CCSSchrodingerWerewolfSystem &system);
  ~CCSWerewolfPolicy();
  void operator()(const Wavefunction &state, const double t);
  CCSWerewolfPolicy &SetToGarashchuchishcheThreshold(double threshold);

private:
  double to_garashchuchishche_threshold_;

  const Operator<PolicyType> hamiltonian_;
  const OperatorFactory<PolicyType> operator_factory_;
  double smoothing_scale_;
};
} // namespace algorithm
} // namespace mce
