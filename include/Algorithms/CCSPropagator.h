#pragma once

#include "Algorithms/CCSSchrodinger.h"
#include "Algorithms/ISystem.h"
#include "Commons/Time.h"
#include "Definitions.h"
#include "Wavefunction/Wavefunction.h"

#include <boost/numeric/odeint.hpp>

namespace mce {
namespace algorithm {
class CCSPropagator {
public:
  using PolicyType = MCE_POLICY(CCS);
  using WerewolfPolicyType = std::function<void(Wavefunction &state, const double t)>;
  CCSPropagator();
  ~CCSPropagator();

  CCSPropagator& SetWerewolfPolicy(WerewolfPolicyType werewolf_policy);
  CCSPropagator& SetAdaptiveFlag(bool adaptive_flag);

  void Integrate(const ISystem &system, Wavefunction &wavefunction,
                 double start_time, double end_time, double delta_t,
                 double absolute_error_tolerance = 1.e-6,
                 double relative_error_tolerance = 1.e-6);

  void ObservedIntegrate(
      const ISystem &system, Wavefunction &wavefunction, double start_time,
      double end_time, double delta_t,
      const std::function<bool(Wavefunction &, const double, const size_t)> &observer,
      double observation_interval, double absolute_error_tolerance = 1.e-6,
      double relative_error_tolerance = 1.e-6);


private:
  WerewolfPolicyType werewolf_policy_;
  bool adaptive_flag_;
};
} // namespace algorithm
} // namespace mce
