#pragma once

#include "Definitions.h"
#include "OperatorFactory.h"
#include "Wavefunction/Wavefunction.h"

namespace mce {
namespace ccs_utilities {

using PolicyType = MCE_POLICY(CCS);

void Project(Wavefunction &target, const Wavefunction &source,
             const Operator<PolicyType> &kernel, bool fast = true, double epsilon_reg = 0);

void Filter(Wavefunction &target, double tolerance = kTolerance);

std::complex<double> Value(const Wavefunction &target, const VectorXc_r& point);

} // namespace ccs_utilities
} // namespace mce
