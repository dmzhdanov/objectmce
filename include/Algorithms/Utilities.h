#pragma once

#include "Definitions.h"

namespace mce {
namespace utilities {

template <class T>
VectorX<T> PowerSeries(const size_t length, const T multiplier,
                       T initial_value = 1);

MatrixXc_r DiagonalMatrix(ptrdiff_t index_of_diagonal, size_t size,
                          size_t size2 = 0);
MatrixXc_r RangeMatrix(ptrdiff_t index_of_diagonal, size_t size,
                       size_t size2 = 0);
MatrixXc_r TaylorXDifferenciate(const MatrixXc_r &taylor_coefficients);
MatrixXc_r TaylorXMultiply(const MatrixXc_r &left, const VectorXc_r &right);
MatrixXc_r TaylorXAdd(const MatrixXc_r &left, const MatrixXc_r &right);

template<class EigenMatrixType>
void ChopToFitNonZeros(EigenMatrixType& matrix);

bool CheckEqual(const MatrixXc_r& left, const MatrixXc_r&, double tolerance);

template<class ContainerType>
bool CheckNoDuplicates(const ContainerType& left, const ContainerType& right);

template <class MultipliersIt, class CoefficientsIt>
void HermiteSeries(MultipliersIt m_first, MultipliersIt m_last, CoefficientsIt c_first, CoefficientsIt tmp_first);



} // namespace utilities
}; // namespace mce

#include "Utilities.inl"