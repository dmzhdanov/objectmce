#pragma once

#include "Algorithms/ISystem.h"
#include "Definitions.h"
#include "OperatorFactory.h"

namespace mce {
namespace algorithm {
class CCSSchrodingerSystemPiloted : public ISystem {
public:
  using PolicyType = MCE_POLICY(CCS);

  CCSSchrodingerSystemPiloted(const Operator<PolicyType> &hamiltonian,
                             const Operator<PolicyType> &linear_solve_kernel,
                             OperatorFactory<PolicyType> &operator_factory);
  ~CCSSchrodingerSystemPiloted() override{};

  void operator()(const Wavefunction &state, Wavefunction &pder_t_state,
                  const double t) const override;

  void SetSmoothingScale(double smoothing_scale); // in units of alpha

private:
  Operator<PolicyType> GetPilotSmoothingKernel(const Wavefunction &state,
                                               size_t basis_id) const;

private:
  const Operator<PolicyType> hamiltonian_;
  const Operator<PolicyType> linear_solve_kernel_;
  const OperatorFactory<PolicyType> operator_factory_;

  mutable double amplitude_max_regularizer_;

  double smoothing_scale_;
};
} // namespace algorithm
} // namespace mce
