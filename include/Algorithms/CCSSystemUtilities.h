#pragma once

#include <vector>

#include "Evaluator/Evaluator_CCS.h"
#include "OperatorFactory.h"
#include "Operator_CCS.h"

namespace mce {
namespace ccs_system_utilities {

using PolicyType = MCE_POLICY(CCS);
using OperatorType = typename mce::Operator<MCE_POLICY(CCS)>;
using EvaluatorType = typename mce::Evaluator<MCE_POLICY(CCS)>;
using EvaluatorTypePtr =
    typename std::shared_ptr<mce::Evaluator<MCE_POLICY(CCS)>>;

class Derivative;

class DerivativeCreator {

public: // ???!!! needs to be private but then the code does not compile for
        // unclear reason.
  DerivativeCreator(const OperatorType &observable,
                    const OperatorFactory<PolicyType> &operator_factory);
  friend class Derivative;

public:
  void InitializeXTimesObservable(const IConfiguration &configuration);
  void InitializeObservableTimesX(const IConfiguration &configuration);

private:
  const OperatorFactory<PolicyType> operator_factory_;

public:
  const OperatorType observable_;
  std::vector<OperatorType> x_times_observable_;
  std::vector<OperatorType> observable_times_x_;
};

class DerivativeEvaluator {
  friend class Derivative;

private:
  DerivativeEvaluator(std::shared_ptr<DerivativeCreator> creator,
                      const BracketPair &bracket_pair);

public:
  std::complex<double> operator()(size_t bra_idx, size_t ket_idx);

  std::complex<double> DiffCenterX(SpatialDOF_id dof_id, size_t ket_idx);
  std::complex<double> DiffCenterP(SpatialDOF_id dof_id, size_t ket_idx);

  std::complex<double> DiffKetCenterX(SpatialDOF_id id, size_t bra_idx,
                                      size_t ket_idx);
  std::complex<double> DiffKetCenterP(SpatialDOF_id id, size_t bra_idx,
                                      size_t ket_idx);

private:
  void InitializeXTimesObservable();
  void InitializeObservableTimesX();

private:
  const std::shared_ptr<DerivativeCreator> creator_;
  const BracketPair bracket_pair_;
  const EvaluatorTypePtr observable_;
  std::vector<EvaluatorTypePtr> x_times_observable_;
  std::vector<EvaluatorTypePtr> observable_times_x_;
};

class Derivative {

public:
  Derivative(const OperatorType &observable,
             const OperatorFactory<PolicyType> &operator_factory);
  DerivativeEvaluator Evaluate(const BracketPair &bracket_pair);

  Derivative(const Derivative &other) = delete;
  Derivative(Derivative &&other) = delete;

private:
  const std::shared_ptr<DerivativeCreator> creator_;
};

// computes \scpr{state}{\der{}{t}{state}}, where partial derivatives of state
// with respect to its parameters are given in der_t_state
std::complex<double> WavefunctionDerivativeSelfProjection(
    const Wavefunction &state, const Wavefunction &der_t_state,
    OperatorFactory<PolicyType> operator_factory_);

std::complex<double> Covariance(const OperatorType &left_operator, const OperatorType &right_operator, const OperatorType &identity_operator, const BracketPair &bracket_pair, bool normalize = true);

} // namespace ccs_system_utilities
} // namespace mce
