#pragma once

#include "Definitions.h"

#include "Evaluator/EvaluatorStrategyQuasiprobabilityFlow_CCS.h"
#include "Evaluator/Evaluator_CCS.h"
#include "OperatorFactory.h"
#include "Wavefunction/CanonicPair.h"

namespace mce {
namespace quasiprobability_auxiliaries {

class CCSQuasiprobabilityFlowBundle {
public:
  using PolicyType = MCE_POLICY(CCS);
  using OperatorType = Operator<PolicyType>;
  using OperatorEntryType = std::shared_ptr<OperatorType>;
  using EvaluatorType = Evaluator<PolicyType>;
  using EvaluatorEntryType = std::shared_ptr<EvaluatorType>;
  using EvaluatorStrategyType =
      evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)>;
  using EvaluatorStrategyEntryType = std::shared_ptr<EvaluatorStrategyType>;

  CCSQuasiprobabilityFlowBundle(
      const OperatorFactory<PolicyType> &operator_factory,
      const OperatorType &hamiltonian, SpatialDOF_id number_of_dofs,
      const CanonicPair &width_factors = CanonicPair(1, 1));
  void Initialize(Wavefunction wavefunction);
  void Reset();

  bool IsInitialized();

  double EvaluatePositionFlow(size_t center_basis_function_id,
                              SpatialDOF_id dof_id);

  double EvaluateEffectiveMomentum(size_t center_basis_function_id,
                                   SpatialDOF_id dof_id);

  double EvaluateMomentumFlow(size_t center_basis_function_id,
                              SpatialDOF_id dof_id);

  double EvaluateEffectivePosition(
	  size_t center_basis_function_id, SpatialDOF_id dof_id);

  EvaluatorStrategyEntryType Strategy() { return strategy_; }

private:
  const OperatorFactory<PolicyType> operator_factory_;
  const SpatialDOF_id number_of_dofs_;

  OperatorEntryType potential_;
  OperatorEntryType identity_;
  std::vector<OperatorEntryType> auxiliary_harmonic_potentials_;
  std::vector<OperatorEntryType> velocities_;
  std::vector<OperatorEntryType> momenta_;
  EvaluatorEntryType potential_ev_;
  EvaluatorEntryType norm_ev_;
  std::vector<EvaluatorEntryType> auxiliary_harmonic_potentials_ev_;
  std::vector<EvaluatorEntryType> velocities_ev_;
  std::vector<EvaluatorEntryType> momenta_ev_;
  EvaluatorStrategyEntryType strategy_;
};

} // namespace quasiprobability_auxiliaries
} // namespace mce
