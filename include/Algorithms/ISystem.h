#pragma once

#include "Wavefunction/Wavefunction.h"

namespace mce {
namespace algorithm {
class ISystem {
public:
  ISystem(){};
  virtual ~ISystem(){};

  virtual void operator()(const Wavefunction &state, Wavefunction &pder_t_state,
                          const double t) const = 0;
};

} // namespace algorthm
} // namespace mce
