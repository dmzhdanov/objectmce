#pragma once

#include "Definitions.h"
#include "Operator/OperatorArithmeticCore.h"
#include "Evaluator/Evaluator_CCS.h"
#include "Evaluator/BracketPair.h"
//#include "Wavefunction.h"

namespace mce {

template <>
class Operator<MCE_POLICY(CCS)>
    : public OperatorArithmeticCore<Operator<MCE_POLICY(CCS)>,
                                    MCE_POLICY(CCS)> {
public:
  using PolicyType = MCE_POLICY(CCS);
  using OperatorEntryType = std::shared_ptr<IOperatorEntry<PolicyType>>;
  using EvaluatorEntryType = std::shared_ptr<IEvaluatorEntry<PolicyType>>;

  Evaluator<PolicyType> Evaluate(const BracketPair& bracket_pair) const {
	  return Evaluator<PolicyType>(OperatorBase(), bracket_pair);
  };
protected:
  /// public: // protected constructor can only be called from OperatorFactory
  template <typename> friend class OperatorFactory;
  Operator() : OperatorArithmeticCore<Operator<PolicyType>, PolicyType>(){};
  
};

} // namespace mce
