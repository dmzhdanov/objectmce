#pragma once
#include "stdafx.h"

#include "Operator/OperatorEntryGaussian.h"
//#include "OperatorEntrySum.h"
#include "Algorithms/Utilities.h"
#include "Commons/Text.h"

namespace mce {
template <class PolicyType>
OperatorEntryGaussian<PolicyType>::OperatorEntryGaussian(
    std::shared_ptr<StrategyType> strategy)
    : exp_taylor_coefficients_(), taylor_coefficients_(), strategy_(strategy) {}

template <class PolicyType>
OperatorEntryGaussian<PolicyType>::OperatorEntryGaussian(
    const MatrixXc_r &taylor_coefficients,
    const VectorXc_r &exp_taylor_coefficients,
    std::shared_ptr<StrategyType> strategy)
    : taylor_coefficients_(taylor_coefficients),
      exp_taylor_coefficients_(exp_taylor_coefficients), strategy_(strategy) {}

template <class PolicyType>
OperatorEntryGaussian<PolicyType>::~OperatorEntryGaussian() {}

template <class PolicyType>
bool OperatorEntryGaussian<PolicyType>::AddInPlace(MultiplicandType summand) {
  auto p_summand = std::static_pointer_cast<OperatorEntryGaussian>(summand);
  if (p_summand) {
    if (utilities::CheckEqual(p_summand->exp_taylor_coefficients_,
                              exp_taylor_coefficients_, kTolerance)) {
      taylor_coefficients_ = utilities::TaylorXAdd(
          taylor_coefficients_, p_summand->taylor_coefficients_);
      utilities::ChopToFitNonZeros(taylor_coefficients_);
      return true;
    }
  }
  return false;
};

template <class PolicyType>
MatrixXc_r
OperatorEntryGaussian<PolicyType>::PXNormalReorderRightPMultiplication(
    const MatrixXc_r &taylor_coefficients,
    const VectorXc_r &exp_taylor_coefficients, Eigen::Index p_power,
    std::function<void(Eigen::Index, const MatrixXc_r &)> functor) {
  auto RowLength = [&](Eigen::Index i) -> Eigen::Index {
    return taylor_coefficients.cols() +
           std::max(Eigen::Index(0), exp_taylor_coefficients.cols() - 2) * i;
  };
  MatrixXc_r output(taylor_coefficients.rows() + p_power,
                    std::max(Eigen::Index(0), RowLength(p_power)));
  output.setZero();
  MatrixXc_r tmp = output;
  output.block(0, 0, taylor_coefficients.rows(), RowLength(0)) =
      taylor_coefficients;
  VectorXc_r exp_diff_prefactor =
      utilities::TaylorXDifferenciate(exp_taylor_coefficients);
  auto shift_matrix = utilities::DiagonalMatrix(-1, output.rows());
  functor(0, output);
  for (MatrixXc_r::Index i = 0; i < p_power; ++i) {
    output = shift_matrix * output;
    for (MatrixXc_r::Index j = 0; j < taylor_coefficients.rows() + i; ++j) {

      MatrixXc_r taylor_series_differential(
          (utilities::TaylorXDifferenciate(
              output.block(j + 1, 0, 1, RowLength(i)))) *
          I * kHBar);
      output.block(j, 0, 1, taylor_series_differential.cols()) +=
          taylor_series_differential;
      MatrixXc_r exp_series_differential(
          (utilities::TaylorXMultiply(output.block(j + 1, 0, 1, RowLength(i)),
                                      exp_diff_prefactor)) *
          I * kHBar);
      output.block(j, 0, 1, exp_series_differential.cols()) +=
          exp_series_differential;
    }
    functor(i + 1, output);
  }
  return output;
};

template <class PolicyType>
bool OperatorEntryGaussian<PolicyType>::LeftMultiplyInPlace(
    MultiplicandType other) {
  auto p_other = std::static_pointer_cast<OperatorEntryGaussian>(other);
  if (p_other) {
    auto other_copy =
        std::static_pointer_cast<OperatorEntryGaussian<PolicyType>>(
            p_other->Clone());
    auto this_copy = std::static_pointer_cast<OperatorEntryGaussian>(Clone());
    other_copy->RightMultiplyInPlace(this_copy);
    *this = *other_copy;
    return true;
  }
  return false;
}

template <class PolicyType>
bool OperatorEntryGaussian<PolicyType>::RightMultiplyInPlace(
    MultiplicandType other) {
  auto p_other = std::static_pointer_cast<OperatorEntryGaussian>(other);
  if (p_other) {
	VectorXc_r output_exp_taylor_coefficients(0);
    MatrixXc_r output_taylor_coefficients(0, 0);
	if (p_other->taylor_coefficients_.size() > 0 && taylor_coefficients_.size() > 0)
	{
		output_exp_taylor_coefficients = utilities::TaylorXAdd(
			exp_taylor_coefficients_, p_other->exp_taylor_coefficients_);
		PXNormalReorderRightPMultiplication(
			taylor_coefficients_, exp_taylor_coefficients_,
			p_other->taylor_coefficients_.rows() - 1,
			[&](Eigen::Index index, const MatrixXc_r &m) {
			output_taylor_coefficients = utilities::TaylorXAdd(
				output_taylor_coefficients,
				utilities::TaylorXMultiply(
					m, p_other->taylor_coefficients_.row(index)));
		});
	}

    taylor_coefficients_ = output_taylor_coefficients;
    exp_taylor_coefficients_ = output_exp_taylor_coefficients;
    utilities::ChopToFitNonZeros(taylor_coefficients_);
    utilities::ChopToFitNonZeros(exp_taylor_coefficients_);
    return true;
  }
  return false;
}

template <class PolicyType>
bool OperatorEntryGaussian<PolicyType>::MultiplyInPlace(
    std::complex<double> multiplier) {
  if (taylor_coefficients_.size() > 0)
    taylor_coefficients_ *= multiplier;
  return true;
}

template <class PolicyType>
typename OperatorEntryGaussian<PolicyType>::MultiplicandType
OperatorEntryGaussian<PolicyType>::Clone() {
  return std::make_shared<OperatorEntryGaussian<PolicyType>>(*this);
}

template <class PolicyType>
void OperatorEntryGaussian<PolicyType>::Print(std::ostream &stream,
                                              SpatialDOF_id i) const {
  std::string x_symbol = (i == -1) ? "x" : "x[" + std::to_string(i) + "]";
  std::string p_symbol = (i == -1) ? "p" : "p[" + std::to_string(i) + "]";
  std::string out_brace;
  if (exp_taylor_coefficients_.size() != 0) {
    stream << "Exp[";
    stream << "(" << utilities::num2mstr(exp_taylor_coefficients_(0, 0)) << ")";
    for (Eigen::Index i = 1; i < exp_taylor_coefficients_.size(); i++) {
      if ((std::abs(exp_taylor_coefficients_(0, i)) >
           std::pow(10, -(int)utilities::kPrintPrecision))) {
        stream << " + (" << utilities::num2mstr(exp_taylor_coefficients_(0, i))
               << ") * " << x_symbol << "^" << i;
      }
    }
    stream << "] * (";
    out_brace = ")";
  }
  std::string p_power = "";
  std::string plus = "";
  if (taylor_coefficients_.size() == 0) {
    stream << "0)";
  } else {
    for (Eigen::Index j = 0; j < taylor_coefficients_.rows(); j++) {
      if (std::abs(taylor_coefficients_(j, 0)) >
          std::pow(10, -(int)utilities::kPrintPrecision)) {
        stream << plus << "(" << utilities::num2mstr(taylor_coefficients_(j, 0))
               << ")" << p_power;
      }
      for (Eigen::Index i = 1; i < taylor_coefficients_.cols(); i++) {
        if (std::abs(taylor_coefficients_(j, i)) >
            std::pow(10, -(int)utilities::kPrintPrecision)) {
          stream << " + (" << utilities::num2mstr(taylor_coefficients_(j, i))
                 << ")" << p_power << " * " << x_symbol
                 << (i > 1 ? "^" + std::to_string(i) : "");
        }
      }
      p_power = " * " + p_symbol + (j > 0 ? "^" + std::to_string(j + 1) : "");
      plus = " + ";
    }
  }
  stream << out_brace;
};

template <class PolicyType>
typename OperatorEntryGaussian<PolicyType>::StrategyType &
OperatorEntryGaussian<PolicyType>::Strategy() const {
  return *strategy_;
};

template <class PolicyType>
typename OperatorEntryGaussian<PolicyType>::EvaluatorEntryType
OperatorEntryGaussian<PolicyType>::Evaluator(std::any initializer_list) const {
  return std::make_shared<EvaluatorEntry<OperatorEntryGaussian<PolicyType>>>(
      initializer_list);
};

} // namespace mce
