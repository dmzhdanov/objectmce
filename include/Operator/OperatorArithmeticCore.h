#pragma once

#include "Definitions.h"
#include "IOperatorEntry.h"
#include "OperatorBase.h"

namespace mce {
// yes, here is a nice place to apply curiously recurring template pattern
// (CRTP)!
template <typename OperatorType, typename PolicyType>
class OperatorArithmeticCore
    : public boost::additive1<
          OperatorType,
          boost::additive2<
              OperatorType, std::complex<double>,
              boost::multiplicative<OperatorType,
                                    boost::multiplicative2<
                                        OperatorType, std::complex<double>>>>> {
public:
  OperatorArithmeticCore();
  ~OperatorArithmeticCore();
  OperatorArithmeticCore(const OperatorArithmeticCore &other);
  OperatorArithmeticCore(OperatorArithmeticCore &&other) = default;
  OperatorArithmeticCore &operator=(const OperatorArithmeticCore &other);
  OperatorArithmeticCore &operator=(OperatorArithmeticCore &&other) = default;

  // algebra
  OperatorType operator-() const;
  OperatorType &operator+=(const OperatorArithmeticCore &other);
  OperatorType &operator-=(const OperatorArithmeticCore &other);
  OperatorType &operator*=(const OperatorArithmeticCore &other);
  OperatorType &operator*=(const std::complex<double> &multiplier);

  // properties
  void Print(std::ostream &stream) const;
  const internal::OperatorBase<IOperatorEntry<PolicyType>> &
  OperatorBase() const {
    return operator_base_;
  }

private:
  inline OperatorType &derived() { return *static_cast<OperatorType *>(this); }
  inline const OperatorType &derived() const { return *static_cast<const OperatorType *>(this); }

protected:
  internal::OperatorBase<IOperatorEntry<PolicyType>> operator_base_;
};

template <class PolicyType> class Operator;

} // namespace mce

#include "OperatorArithmeticCore.inl"
