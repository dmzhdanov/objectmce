#pragma once

#include <boost/operators.hpp>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <type_traits>
#include <vector>

#include "Commons/ITERATOR_MACRO.h"
#include "Commons/OptionalBase.h"
#include "Definitions.h"
#include "Traits.h"

namespace mce {
namespace internal {

// **********************************************
// **********   class SpatialProduct   **********
// **********************************************

template <class OperatorEntryType> class SpatialProduct {
public:
  using SpatialProductType = std::shared_ptr<SpatialProduct>;
  using MultiplicandType = std::shared_ptr<OperatorEntryType>;
  using ContainerType = std::map<SpatialDOF_id, MultiplicandType>;
  using ContainerEntry = std::pair<const SpatialDOF_id, MultiplicandType>;

  // construction / destruction
  SpatialProduct(){};
  virtual ~SpatialProduct(){};
  template <class NewOperatorEntryType, class Function>
  std::shared_ptr<SpatialProduct<NewOperatorEntryType>>
  CloneTransformed(Function function) const;

  SpatialProductType Clone() const;
  static ContainerEntry CloneFunctor(const ContainerEntry &entry);

  // algebra
  void AddMultiplicand(SpatialDOF_id dof_id, MultiplicandType multiplicand);
  template <class T=bool>
  typename std::enable_if<
                Traits<OperatorEntryType>::kMultipliable, T>::type
  RightMultiplyInPlace(SpatialProductType other);
  template <class T=bool>
  typename std::enable_if<
                Traits<OperatorEntryType>::kMultipliable, T>::type
  MultiplyInPlace(std::complex<double> multiplier);

  // properties
  size_t Dimensionality() const;

  EQUIP_WITH_INPUT_ITERATOR(spatial_product_, ContainerType, ContainerEntry)

  EQUIP_WITH_INSERT_ITERATOR(spatial_product_, ContainerType, ContainerEntry,
                             insert)

  void Print(std::ostream &stream);

public:
  ContainerType spatial_product_;
};

// **********************************************
// ***********        class OperatorBase        **********
// **********************************************

template <class OperatorEntryType>
class OperatorBase
    : public NestedOptionalBase<
          Traits<OperatorEntryType>::kMultipliable, boost::multiplicative1,
          NestedOptionalBase<
              Traits<OperatorEntryType>::kSummable, boost::additive1,
              NestedOptionalBase<
                  Traits<OperatorEntryType>::kSummable, boost::additive2,
                  NestedOptionalBase<Traits<OperatorEntryType>::kMultipliable,
                                     boost::multiplicative2, EmptyBase<bool>,
                                     OperatorBase<OperatorEntryType>,
                                     std::complex<double>>,
                  OperatorBase<OperatorEntryType>, std::complex<double>>,
              OperatorBase<OperatorEntryType>>,
          OperatorBase<OperatorEntryType>>

/*
public boost::additive1<
  OperatorBase<OperatorEntryType>,
  boost::additive2<
      OperatorBase<OperatorEntryType>, std::complex<double>,
      boost::multiplicative<
          OperatorBase<OperatorEntryType>,
          boost::multiplicative2<OperatorBase<OperatorEntryType>,
                                 std::complex<double>>>>>//*/
//
{
public:
  using SpatialProductType = std::shared_ptr<SpatialProduct<OperatorEntryType>>;
  using PESType = std::shared_ptr<OperatorBase<OperatorEntryType>>;

  // construction / destruction
  OperatorBase()
      : pes_summands_(std::make_shared<std::vector<SpatialProductType>>()){};
  virtual ~OperatorBase(){};
  OperatorBase(const OperatorBase &other);
  OperatorBase(OperatorBase &&other) = default;
  OperatorBase &operator=(const OperatorBase &other);
  OperatorBase &operator=(OperatorBase &&other) = default;
  template <class NewOperatorEntryType, class Function>
  OperatorBase<NewOperatorEntryType> CloneTransformed(Function function) const;
  OperatorBase Clone() const;
  OperatorBase ShallowCopy() const;

  // algebra
  OperatorBase &AddSpatialProduct(SpatialProductType other);
  template <class T = OperatorBase>
  typename std::enable_if<Traits<OperatorEntryType>::kMultipliable, T>::type &
  RightMultiplyBySpatialProduct(SpatialProductType other);

  template <class T = OperatorBase>
  typename std::enable_if<Traits<OperatorEntryType>::kSummable &&
                              Traits<OperatorEntryType>::kMultipliable,
                          T>::type
  operator-() const;
  template <class T = OperatorBase>
  typename std::enable_if<Traits<OperatorEntryType>::kSummable, T>::type &
  operator+=(const OperatorBase &other);
  template <class T = OperatorBase>
  typename std::enable_if<Traits<OperatorEntryType>::kSummable &&
                              Traits<OperatorEntryType>::kMultipliable,
                          T>::type &
  operator-=(const OperatorBase &other);
  template <class T = OperatorBase>
  typename std::enable_if<Traits<OperatorEntryType>::kMultipliable, T>::type &
  operator*=(const OperatorBase &other);
  template <class T = OperatorBase>
  typename std::enable_if<Traits<OperatorEntryType>::kMultipliable, T>::type &
  operator*=(const std::complex<double> &other);

  // properties
  size_t NumberOfSummands() const;

  EQUIP_WITH_INPUT_ITERATOR((*pes_summands_), std::vector<SpatialProductType>,
                            SpatialProductType)

  EQUIP_WITH_INSERT_ITERATOR(*pes_summands_, std::vector<SpatialProductType>,
                             SpatialProductType, push_back)

  void Print(std::ostream &stream) const;

public:
  std::shared_ptr<std::vector<SpatialProductType>> pes_summands_;
};
} // namespace internal
} // namespace mce

#include "OperatorBase.inl"
