#pragma once

#include <any>
#include <memory>

#include "Definitions.h"
#include "Traits.h"

namespace mce {

template<class PolicyType>
class IEvaluatorEntry;

template</*class PolicyType, */class OperatorEntryType>
class EvaluatorEntry;

template<class PolicyType>
class IOperatorEntry {
public:
  using MultiplicandType = std::shared_ptr<IOperatorEntry>;
  using StrategyType = std::any;
  using EvaluatorEntryType = std::shared_ptr<IEvaluatorEntry<PolicyType>>;

  inline IOperatorEntry(){};
  virtual inline ~IOperatorEntry() = 0; // needs inline declaration below to be
                                        // properly compiled

  virtual const DOFTypes DofType() const = 0;

  virtual bool AddInPlace(MultiplicandType summand) = 0;
  virtual bool LeftMultiplyInPlace(MultiplicandType other) = 0;
  virtual bool RightMultiplyInPlace(MultiplicandType other) = 0;
  virtual bool MultiplyInPlace(std::complex<double> multiplier) = 0;
  virtual MultiplicandType Clone() = 0;

  virtual void Print(std::ostream &stream, SpatialDOF_id i = -1) const = 0;

  virtual StrategyType &Strategy() const = 0;
  virtual EvaluatorEntryType Evaluator(std::any initializer_list = nullptr) const { return nullptr; };
};
template<class PolicyType>
inline IOperatorEntry<PolicyType>::~IOperatorEntry(){};

namespace internal {
template<class PolicyType>
struct Traits<IOperatorEntry<PolicyType>> {
  enum { kSummable = true, kMultipliable = true };
};

} // namespace internal

} // namespace mce
