#pragma once

#include "IOperatorEntry.h"

namespace mce 
{
	// Some algebraic operations
	template<class OperatorEntryType>
	bool AddInPlace(OperatorEntryType& left, OperatorEntryType right);
	template<class OperatorEntryType>
	bool MultiplyInPlace(OperatorEntryType& left, OperatorEntryType right);
	template<class OperatorEntryType>
	bool MultiplyInPlace(OperatorEntryType& left, std::complex<double> multiplier);

};

#include "OperatorEntryAlgebra.inl"
