// do not include #include "stdafx.h" here!

#include "Operator/OperatorBase.h"
#include "Operator/OperatorEntryAlgebra.h"

namespace mce {
namespace internal {

template <class OperatorEntryType>
template <class NewOperatorEntryType, class Function>
std::shared_ptr<SpatialProduct<NewOperatorEntryType>>
SpatialProduct<OperatorEntryType>::CloneTransformed(Function function) const {
  using NewSpatialProductType =
      std::shared_ptr<SpatialProduct<NewOperatorEntryType>>;
  using NewMultiplicandType = std::shared_ptr<NewOperatorEntryType>;
  using NewContainerType = std::map<SpatialDOF_id, NewMultiplicandType>;
  auto new_spatial_product =
      std::make_shared<SpatialProduct<NewOperatorEntryType>>();
  std::transform(spatial_product_.cbegin(), spatial_product_.cend(),
                 new_spatial_product->insert_iterator(),
                 [&function](const auto &multiplicand_entry) {
                   return function(multiplicand_entry);
                 });
  return new_spatial_product;
};

template <class OperatorEntryType>
typename SpatialProduct<OperatorEntryType>::ContainerEntry
SpatialProduct<OperatorEntryType>::CloneFunctor(const ContainerEntry &entry) {
  return ContainerEntry(entry.first, entry.second->Clone());
};

template <class OperatorEntryType>
std::shared_ptr<SpatialProduct<OperatorEntryType>>
SpatialProduct<OperatorEntryType>::Clone() const {
  return CloneTransformed<OperatorEntryType>(&CloneFunctor);
};

template <class OperatorEntryType>
void SpatialProduct<OperatorEntryType>::AddMultiplicand(
    SpatialDOF_id dof_id, MultiplicandType multiplicand) {
  auto dof_check = spatial_product_.find(dof_id);
  if (dof_check == spatial_product_.end()) {
    spatial_product_.insert(ContainerEntry(dof_id, multiplicand));
  } else {
    QI_Error::RaiseError("[Error] operator_structure::DOFProduct - "
                         "redefinition of multiplicand");
  }
};

template <class OperatorEntryType>
template <class T>
typename std::enable_if<
                Traits<OperatorEntryType>::kMultipliable,T>::type
SpatialProduct<OperatorEntryType>::RightMultiplyInPlace(
    SpatialProductType other) {
  for (auto &multiplicand : *other) {
    auto &dof_id = multiplicand.first;
    auto dof_pointer = spatial_product_.find(dof_id);
    if (dof_pointer != spatial_product_.end()) {
      if (!mce::MultiplyInPlace(dof_pointer->second, multiplicand.second)) {
        return false;
      };
    } else {
      spatial_product_.insert(multiplicand);
    }
  }
  return true;
};

template <class OperatorEntryType>
template <class T>
typename std::enable_if<
                Traits<OperatorEntryType>::kMultipliable,T>::type
SpatialProduct<OperatorEntryType>::MultiplyInPlace(
    std::complex<double> multiplier) {
  for (auto &multiplicand : spatial_product_) {
    if (mce::MultiplyInPlace(
            multiplicand.second,
            multiplier)) { // if
                           // (multiplicand.second->MultiplyInPlace(multiplier)){
      return true;
    }
  }
  return false;
};

template <class OperatorEntryType>
size_t SpatialProduct<OperatorEntryType>::Dimensionality() const {
  return spatial_product_.size();
};

template <class OperatorEntryType>
void SpatialProduct<OperatorEntryType>::Print(std::ostream &stream) {
  std::string separator = "(";
  for (const auto &multiplicand : spatial_product_) {
    stream << separator;
    multiplicand.second->Print(stream, multiplicand.first);
    stream << ")";
    separator = " * (";
  }
}

//******************************************************************
//************************  OperatorBase  **************************
//******************************************************************

template <class OperatorEntryType>
OperatorBase<OperatorEntryType>::OperatorBase(
    const OperatorBase<OperatorEntryType> &other) {
  this->pes_summands_ = std::move(
      other.Clone().pes_summands_); // /*std::move(*/other.Clone();//);
};

template <class OperatorEntryType>
OperatorBase<OperatorEntryType> &OperatorBase<OperatorEntryType>::operator=(
    const OperatorBase<OperatorEntryType> &other) {
  this->pes_summands_ = std::move(other.Clone().pes_summands_);
  return *this;
};

template <class OperatorEntryType>
template <class NewOperatorEntryType, class Function>
OperatorBase<NewOperatorEntryType>
OperatorBase<OperatorEntryType>::CloneTransformed(Function function) const {
  OperatorBase<NewOperatorEntryType> clone;
  std::transform(
      pes_summands_->cbegin(), pes_summands_->cend(), clone.insert_iterator(),
      // std::back_inserter(*(clone.pes_summands_)),
      [&function](const auto &pes_summand) {
        //        using NewDOFProductType = typename
        //            std::shared_ptr<SpatialProduct<NewOperatorEntryType> >;
        return pes_summand->template CloneTransformed<NewOperatorEntryType>(
            function);
      });
  return clone;
}

template <class OperatorEntryType>
OperatorBase<OperatorEntryType> OperatorBase<OperatorEntryType>::Clone() const {
  return CloneTransformed<OperatorEntryType>(
      &SpatialProduct<OperatorEntryType>::CloneFunctor);
};

template <class OperatorEntryType>
OperatorBase<OperatorEntryType>
OperatorBase<OperatorEntryType>::ShallowCopy() const {
  OperatorBase<OperatorEntryType> copy;
  copy.pes_summands_ = pes_summands_;
  return copy;
};

template <class OperatorEntryType>
OperatorBase<OperatorEntryType> &
OperatorBase<OperatorEntryType>::AddSpatialProduct(SpatialProductType other) {
  if (other->Dimensionality() == 1) {
    for (auto &summand : *pes_summands_) {
      if ((summand->Dimensionality() == 1) &&
          (summand->begin()->first == other->begin()->first) &&
          (mce::AddInPlace(summand->begin()->second, other->begin()->second))) {
        return *this;
      }
    }
  }
  (*pes_summands_).push_back(other);
  return *this;
};

template <class OperatorEntryType>
template <class T>
typename std::enable_if<Traits<OperatorEntryType>::kSummable, T>::type &
OperatorBase<OperatorEntryType>::operator+=(
    const OperatorBase<OperatorEntryType> &other) {
  // ???!!! very conservative approach: first we make a copy of rhs to ensure
  // that there is no shared pointers with lhs
  auto other_tmp = other;
  for (auto summand : other_tmp) {
    AddSpatialProduct(summand);
  }
  return *this;
}

template <class OperatorEntryType>
template <class T>
typename std::enable_if<Traits<OperatorEntryType>::kSummable &&
	Traits<OperatorEntryType>::kMultipliable,
	T>::type
OperatorBase<OperatorEntryType>::operator-() const {
  auto result = *this;
  result *= -1;
  return result;
}

template <class OperatorEntryType>
template <class T>
typename std::enable_if<Traits<OperatorEntryType>::kSummable &&
                            Traits<OperatorEntryType>::kMultipliable,
                        T>::type &
OperatorBase<OperatorEntryType>::operator-=(
    const OperatorBase<OperatorEntryType> &other) {
  // ???!!! very conservative approach: first we make a copy of rhs to ensure
  // that there is no shared pointers with lhs
  auto other_tmp = (-1.) * other;
  for (auto summand : other_tmp) {
    AddSpatialProduct(summand);
  }
  return *this;
}

template <class OperatorEntryType>
template <class T>
typename std::enable_if<Traits<OperatorEntryType>::kMultipliable, T>::type &
OperatorBase<OperatorEntryType>::RightMultiplyBySpatialProduct(
    SpatialProductType other) {
  for (auto &summand : *pes_summands_) {
    if (!summand->RightMultiplyInPlace(other)) {
      QI_Error::RaiseError(
          "[ERROR] "
          "OperatorBase<OperatorEntryType>::RightMultiplyBySpatialProduct - "
          "multiplication failed");
    }
  };
  return *this;
};

template <class OperatorEntryType>
template <class T>
typename std::enable_if<Traits<OperatorEntryType>::kMultipliable, T>::type &
OperatorBase<OperatorEntryType>::operator*=(
    const OperatorBase<OperatorEntryType> &other) {
  OperatorBase<OperatorEntryType> product;
  for (const auto &summand : *(other.pes_summands_)) {
    product += (Clone().RightMultiplyBySpatialProduct(summand));
  };
  *this = product;
  return *this;
}

template <class OperatorEntryType>
template <class T>
typename std::enable_if<Traits<OperatorEntryType>::kMultipliable, T>::type &
OperatorBase<OperatorEntryType>::operator*=(
    const std::complex<double> &multiplier) {
  OperatorBase<OperatorEntryType> scaled_operator = *this;
  for (const auto &summand : scaled_operator) {
    summand->MultiplyInPlace(multiplier);
  };
  *this = scaled_operator;
  return *this;
};

template <class OperatorEntryType>
void OperatorBase<OperatorEntryType>::Print(std::ostream &stream) const {
  std::string separator = "(";
  for (const auto &summand : *pes_summands_) {
    stream << separator;
    summand->Print(stream);
    stream << ")";
    separator = " + (";
  }
}

template <class OperatorEntryType>
size_t OperatorBase<OperatorEntryType>::NumberOfSummands() const {
  return pes_summands_->size();
};

} // namespace internal
} // namespace mce
