#pragma once

#include "Definitions.h"
#include "IOperatorEntry.h"
#include <memory>

namespace mce {

template<class PolicyType>
class OperatorEntrySum : public IOperatorEntry<PolicyType> {
public:
  using typename IOperatorEntry<PolicyType>::StrategyType;
  using typename IOperatorEntry<PolicyType>::EvaluatorEntryType;
  using typename IOperatorEntry<PolicyType>::MultiplicandType;
  
  OperatorEntrySum(std::shared_ptr<StrategyType> strategy = nullptr);
  ~OperatorEntrySum() override;
  //OperatorEntrySum(const OperatorEntrySum&) = delete;
  //OperatorEntrySum(OperatorEntrySum&&) = delete;

  const DOFTypes DofType() const override { return summands_[0]->DofType(); }

  bool AddInPlace(MultiplicandType summand) override;
  bool LeftMultiplyInPlace(MultiplicandType other) override;
  bool RightMultiplyInPlace(MultiplicandType other) override;
  bool MultiplyInPlace(std::complex<double> multiplier) override;

  MultiplicandType Clone() override;

  void Print(std::ostream& stream, SpatialDOF_id i) const override;

  EvaluatorEntryType Evaluator(std::any initializer_list = nullptr) const override;
  
  StrategyType& Strategy() const override;

public:
  std::vector<MultiplicandType> summands_;

  std::shared_ptr<StrategyType> strategy_;
};

} // namespace mce
