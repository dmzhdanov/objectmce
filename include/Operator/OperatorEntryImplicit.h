#pragma once

#include "Definitions.h"
#include "IOperatorEntry.h"

namespace mce {

template <class PolicyType>
class OperatorEntryImplicit : public IOperatorEntry<PolicyType> {
public:
  using typename IOperatorEntry<PolicyType>::StrategyType;
  using typename IOperatorEntry<PolicyType>::EvaluatorEntryType;
  using typename IOperatorEntry<PolicyType>::MultiplicandType;
  
  OperatorEntryImplicit(std::shared_ptr<StrategyType> strategy = nullptr);
  ~OperatorEntryImplicit() override;
  // OperatorEntryImplicit(const OperatorEntryImplicit&) = delete;
  // OperatorEntryImplicit(OperatorEntryImplicit&&) = delete;

  const DOFTypes DofType() const override { return DOFTypes::Continuous; }

  bool AddInPlace(MultiplicandType summand) override;
  bool LeftMultiplyInPlace(MultiplicandType other) override;
  bool RightMultiplyInPlace(MultiplicandType other) override;
  bool MultiplyInPlace(std::complex<double> multiplier) override;

  typename IOperatorEntry<PolicyType>::MultiplicandType Clone() override;

  void Print(std::ostream &stream, SpatialDOF_id i) const override;

  EvaluatorEntryType
  Evaluator(std::any initializer_list = nullptr) const override;

  typename IOperatorEntry<PolicyType>::StrategyType &Strategy() const override;

public:
  std::shared_ptr<StrategyType> strategy_;
};

} // namespace mce

#include "OperatorEntryImplicit.inl"
