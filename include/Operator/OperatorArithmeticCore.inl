// do not include #include "stdafx.h" here!

#include "Operator/OperatorArithmeticCore.h"

namespace mce {

template <typename OperatorType, typename PolicyType>
OperatorArithmeticCore<OperatorType, PolicyType>::OperatorArithmeticCore()
    : operator_base_(){};

template <typename OperatorType, typename PolicyType>
OperatorArithmeticCore<OperatorType, PolicyType>::~OperatorArithmeticCore(){};

template <typename OperatorType, typename PolicyType>
OperatorArithmeticCore<OperatorType, PolicyType>::OperatorArithmeticCore(
    const OperatorArithmeticCore<OperatorType, PolicyType> &other)
    : operator_base_(other.operator_base_){};

template <typename OperatorType, typename PolicyType>
OperatorArithmeticCore<OperatorType, PolicyType> &
OperatorArithmeticCore<OperatorType, PolicyType>::
operator=(const OperatorArithmeticCore<OperatorType, PolicyType> &other) {
  operator_base_ = other.operator_base_;
  return derived();
};

template <typename OperatorType, typename PolicyType>
OperatorType OperatorArithmeticCore<OperatorType, PolicyType>::
operator-() const {
  OperatorType result(derived());
  result *= -1.;
  return result;
};

template <typename OperatorType, typename PolicyType>
OperatorType &OperatorArithmeticCore<OperatorType, PolicyType>::
operator+=(const OperatorArithmeticCore<OperatorType, PolicyType> &other) {
  operator_base_ += other.operator_base_;
  return derived();
};

template <typename OperatorType, typename PolicyType>
OperatorType &OperatorArithmeticCore<OperatorType, PolicyType>::
operator-=(const OperatorArithmeticCore<OperatorType, PolicyType> &other) {
  operator_base_ -= other.operator_base_;
  return derived();
};

template <typename OperatorType, typename PolicyType>
OperatorType &OperatorArithmeticCore<OperatorType, PolicyType>::
operator*=(const OperatorArithmeticCore<OperatorType, PolicyType> &other) {
  operator_base_ *= other.operator_base_;
  return derived();
};

template <typename OperatorType, typename PolicyType>
OperatorType &OperatorArithmeticCore<OperatorType, PolicyType>::
operator*=(const std::complex<double> &multiplier) {
  operator_base_ *= multiplier;
  return derived();
};

template <typename OperatorType, typename PolicyType>
void OperatorArithmeticCore<OperatorType, PolicyType>::Print(
    std::ostream &stream) const {
  operator_base_.Print(stream);
};

} // namespace mce
