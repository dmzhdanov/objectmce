#pragma once

#include "Definitions.h"
#include "IOperatorEntry.h"

namespace mce {

template<class PolicyType>
class OperatorEntryGaussian : public IOperatorEntry<PolicyType> {
public:
  using typename IOperatorEntry<PolicyType>::StrategyType;
  using typename IOperatorEntry<PolicyType>::EvaluatorEntryType;
  using typename IOperatorEntry<PolicyType>::MultiplicandType;
  
  explicit OperatorEntryGaussian(std::shared_ptr<StrategyType> strategy = nullptr);
  OperatorEntryGaussian(const MatrixXc_r& taylor_coefficients, const VectorXc_r& exp_taylor_coefficients = VectorXc_r(), std::shared_ptr<StrategyType> strategy = nullptr);
  ~OperatorEntryGaussian() override;

  const DOFTypes DofType() const override { return DOFTypes::Continuous; }

  bool AddInPlace(MultiplicandType summand) override;
  bool LeftMultiplyInPlace(MultiplicandType other) override;
  bool RightMultiplyInPlace(MultiplicandType other) override;
  bool MultiplyInPlace(std::complex<double> multiplier) override;

  MultiplicandType Clone() override;

  void Print(std::ostream& stream, SpatialDOF_id i ) const override;

  StrategyType& Strategy() const override;

  EvaluatorEntryType Evaluator(std::any initializer_list = nullptr) const override;

protected:
  MatrixXc_r PXNormalReorderRightPMultiplication(
      const MatrixXc_r &x_taylor_coefficients,
      const VectorXc_r &exp_taylor_coefficients, Eigen::Index p_power,
      std::function<void(Eigen::Index, const MatrixXc_r &)> functor =
          [](Eigen::Index, const MatrixXc_r &) -> void {});
public:
	MatrixXc_r taylor_coefficients_;
	VectorXc_r exp_taylor_coefficients_;

	std::shared_ptr<StrategyType> strategy_;
};

} // namespace mce

#include "OperatorEntryGaussian.inl"
