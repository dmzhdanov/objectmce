//#include "stdafx.h"

#include "Operator/OperatorEntryImplicit.h"

#include "Operator/OperatorEntryAlgebra.h"

namespace mce {

template <class PolicyType>
OperatorEntryImplicit<PolicyType>::OperatorEntryImplicit(
    std::shared_ptr<StrategyType> strategy)
    : strategy_(strategy) {}

template <class PolicyType>
OperatorEntryImplicit<PolicyType>::~OperatorEntryImplicit() {}

template <class PolicyType>
bool OperatorEntryImplicit<PolicyType>::AddInPlace(MultiplicandType summand) {
  return false;
};

template <class PolicyType>
bool OperatorEntryImplicit<PolicyType>::LeftMultiplyInPlace(
    MultiplicandType other) {
  return false;
};

template <class PolicyType>
bool OperatorEntryImplicit<PolicyType>::RightMultiplyInPlace(
    MultiplicandType other) {
  return false;
};

template <class PolicyType>
bool OperatorEntryImplicit<PolicyType>::MultiplyInPlace(
    std::complex<double> multiplier) {
  return false;
};

template <class PolicyType>
typename IOperatorEntry<PolicyType>::MultiplicandType
OperatorEntryImplicit<PolicyType>::Clone() {
  auto clone = std::make_shared<OperatorEntryImplicit>();
  clone->strategy_ = strategy_;
  return clone;
}

template <class PolicyType>
void OperatorEntryImplicit<PolicyType>::Print(std::ostream &stream,
                                              SpatialDOF_id i) const {
  stream << "(1)";
};

template <class PolicyType>
typename IOperatorEntry<PolicyType>::StrategyType &
OperatorEntryImplicit<PolicyType>::Strategy() const {
  return *strategy_;
};

template <class PolicyType>
typename OperatorEntryImplicit<PolicyType>::EvaluatorEntryType
OperatorEntryImplicit<PolicyType>::Evaluator(std::any initializer_list) const {
  return std::make_shared<EvaluatorEntry<OperatorEntryImplicit<PolicyType>>>(
      initializer_list);
};

} // namespace mce
