//#include "stdafx.h"

#include "Operator/OperatorEntryAlgebra.h"
#include "Operator/OperatorEntrySum.h"

namespace mce {

template <class PolicyType>
OperatorEntrySum<PolicyType>::OperatorEntrySum(
    std::shared_ptr<StrategyType> strategy)
    : strategy_(strategy) {}

template <class PolicyType> OperatorEntrySum<PolicyType>::~OperatorEntrySum() {}

template <class PolicyType>
bool OperatorEntrySum<PolicyType>::AddInPlace(MultiplicandType summand) {

  for (auto &current_summand : summands_) {
    if (mce::AddInPlace(current_summand, summand)) {
      return true;
    }
  };
  summands_.push_back(summand);
  return true;
};

template <class PolicyType>
bool OperatorEntrySum<PolicyType>::LeftMultiplyInPlace(MultiplicandType other) {
  auto copy = other->Clone();
  for (auto &current_summand : summands_) {
    if (!current_summand->LeftMultiplyInPlace(other)) {
      if (copy->RightMultiplyInPlace(current_summand)) {
        current_summand = copy;
        copy = other->Clone();
      } else {
        return false;
      };
    }
  };
  return true;
};

template <class PolicyType>
bool OperatorEntrySum<PolicyType>::RightMultiplyInPlace(
    MultiplicandType other) {
  auto copy = other->Clone();
  for (auto &current_summand : summands_) {
    if (!current_summand->RightMultiplyInPlace(other)) {
      if (copy->LeftMultiplyInPlace(current_summand)) {
        current_summand = copy;
        copy = other->Clone();
      } else {
        return false;
      };
    }
  };
  return true;
};

template <class PolicyType>
bool OperatorEntrySum<PolicyType>::MultiplyInPlace(
    std::complex<double> multiplier) {
  for (auto &current_summand : summands_) {
    if (!current_summand->MultiplyInPlace(multiplier)) {
      return false;
    }
  };
  return true;
};

template <class PolicyType>
IOperatorEntry<PolicyType>::MultiplicandType
OperatorEntrySum<PolicyType>::Clone() {
  auto clone = std::make_shared<OperatorEntrySum>();
  for (auto &summand : summands_) {
    clone->summands_.push_back((*summand).Clone());
  }
  clone->strategy_ = strategy_;
  return clone;
}

template <class PolicyType>
void OperatorEntrySum<PolicyType>::Print(std::ostream &stream,
                                         SpatialDOF_id i) const {
  if (summands_.size() == 1) {
    summands_[0]->Print(stream, i);
  } else {
    std::string separator = "(";
    for (auto &current_summand : summands_) {
      stream << separator;
      current_summand->Print(stream, i);
      separator = " + ";
    }
    stream << ")";
  }
};

template <class PolicyType>
IOperatorEntry<PolicyType>::StrategyType &
OperatorEntrySum<PolicyType>::Strategy() const {
  return *strategy_;
};

template <class PolicyType>
OperatorEntrySum<PolicyType>::EvaluatorEntryType
OperatorEntrySum<PolicyType>::Evaluator(
    std::any initializer_list = nullptr) const {
  return std::make_shared<EvaluatorEntry<OperatorEntrySum<PolicyType>>>(
      initializer_list);
};

} // namespace mce
