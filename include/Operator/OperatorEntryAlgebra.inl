//#include "stdafx.h"

#include "Operator/OperatorEntryAlgebra.h"

namespace mce {
template<class OperatorEntryType>
bool
AddInPlace(OperatorEntryType &left,
           OperatorEntryType right) {
  if (left->AddInPlace(right)) {
    return true;
  }
  auto right_copy = right->Clone();
  if (right_copy->AddInPlace(left)) {
	left = right_copy;
    return true;
  }
  return false;
};

template<class OperatorEntryType>
bool MultiplyInPlace(OperatorEntryType &left,
                     OperatorEntryType right) {
  if (left->RightMultiplyInPlace(right)) {
    return true;
  }
  auto right_copy = right->Clone();
  if (right_copy->LeftMultiplyInPlace(left)) {
    left = right_copy;
    return true;
  }
  return false;
};

template<class OperatorEntryType>
bool
MultiplyInPlace(OperatorEntryType &left,
                std::complex<double> multiplier){
	return (left->MultiplyInPlace(multiplier));
};

} // namespace mce
