#pragma once

namespace mce {
namespace internal {

template <class OperatorEntryType> struct Traits {
  enum { kSummable = false, kMultipliable = false };
};

} // namespace internal
} // namespace mce
