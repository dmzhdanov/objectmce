#pragma once
#include "IConfiguration.h"
#include "Wavefunction/Wavefunction.h"

namespace mce {
class WavefunctionFactory {
public:
  WavefunctionFactory(size_t num_pess = 0, size_t num_spatial_dofs = 1,
                      double gaussian_width = 1,
                      double gaussian_overlap_cutoff = 5);
  ~WavefunctionFactory();
  Wavefunction Create(size_t size = 0);
  Wavefunction CreateUniformSwarm(const PES_id pes_id,
                                  const PhaseSpacePoint &center,
                                  const PhaseSpacePoint &spread,
                                  const PhaseSpacePoint &step_size);

  Wavefunction CreateRandomSwarm(const PES_id pes_id,
	  const PhaseSpacePoint &center,
	  const PhaseSpacePoint &spread,
	  size_t basis_size, unsigned long random_seed = 0);

  Wavefunction CreateUniformSwarm2(const PES_id pes_id,
	  const Eigen::Ref<const MatrixXd_r> &center,
	  const Eigen::Ref<const MatrixXd_r> &spread,
	  const Eigen::Ref<const MatrixXd_r> &step_size);

  Wavefunction CreateRandomSwarm2(
	  const PES_id pes_id, const Eigen::Ref<const MatrixXd_r> &center,
	  const Eigen::Ref<const MatrixXd_r> &spread, size_t basis_size, unsigned long random_seed = 0);

private:
  std::shared_ptr<IConfiguration> configuration_;
};
} // namespace mce
