#pragma once
#include <complex>

namespace mce {

	class AmplitudePhasePair
	{
	public:
		AmplitudePhasePair(std::complex<double> amplitude = 0, double phase = 0);
		~AmplitudePhasePair();
		AmplitudePhasePair& SetZero() { amplitude_ = 0; phase_ = 0; return *this; };
		std::complex<double> Amplitude() const { return amplitude_; }
		double Phase() const { return phase_; }
		std::complex<double> Amplitude(const std::complex<double>& new_value) { amplitude_ = new_value;  return amplitude_; }
		double Phase(double new_value) { phase_ = new_value; return phase_; }

		bool CheckForNans() const;

		// algebra
		AmplitudePhasePair &CWisePlusEqual(const AmplitudePhasePair &other);
		AmplitudePhasePair &CWiseMultiplyEqual(const double multiplier);
		AmplitudePhasePair &CWiseDivideEqual(const AmplitudePhasePair &denominator);

		AmplitudePhasePair &CWisePlusEqual(const double epsilon);

		// return value
		std::complex<double> Value();
	public:
		std::complex<double> amplitude_;
		double phase_;
	};

	// binary operations
	// AmplitudePhasePair CWiseDivide(const AmplitudePhasePair &numerator, const AmplitudePhasePair &denominator);
}