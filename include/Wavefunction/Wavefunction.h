#pragma once

#include <memory>
#include <vector>

//#include <boost/numeric/odeint.hpp>
#include <boost/operators.hpp>

#include "Commons/ITERATOR_MACRO.h"
#include "EhrenfestBasisFunction.h"
#include "IBasisFunction.h"
#include "IConfiguration.h"
#include <iterator>

// namespace mce {
// class Wavefunction
//    : boost::additive1<
//          Wavefunction,
//          boost::additive2<Wavefunction, double,
//                           boost::multiplicative2<Wavefunction, double> >
//                           > {
namespace mce {
// class Wavefunction
//    : boost::additive1<
//          Wavefunction,
//          boost::dividable<Wavefunction,
//                           boost::multiplicative2<Wavefunction, double>>>

class Wavefunction
    : boost::additive1<
          Wavefunction,
          boost::dividable<
              Wavefunction,
              boost::additive2<Wavefunction, double,
                               boost::multiplicative2<Wavefunction, double>>>> {
public:
  // constructors/destructrors
  using ContainerEntryType = std::shared_ptr<IBasisFunction>;
  using ContainerType = std::vector<ContainerEntryType>;
  ~Wavefunction();
  Wavefunction(
      std::shared_ptr<IConfiguration> configuration = nullptr /*= default*/,
      size_t basis_size = 0);
  Wavefunction(const Wavefunction &other);
  Wavefunction(Wavefunction &&other) = default;

  Wavefunction Clone(bool copy_data = true) const;
  Wavefunction ShallowCopy() const;

  template <class T> std::shared_ptr<T> PushBack() {
    auto basis_function = std::make_shared<T>(
        configuration_->NumberOfSpatialDOFs(), configuration_->NumberOfPESs());
    Data().push_back(basis_function);
    return basis_function;
  };

  void PushBack(const std::shared_ptr<IBasisFunction> basis_function);

  std::shared_ptr<IBasisFunction> Erase(size_t index);
  const std::shared_ptr<IBasisFunction> &operator[](size_t index) const;
  std::shared_ptr<IBasisFunction> &operator[](size_t index);
  // std::shared_ptr<IBasisFunction> &At(size_t index) { return (*this)[index];
  // };

  Wavefunction &operator<<(
      Wavefunction &other); // moves contents of other and appends it to caller

  Wavefunction &SetZero();
  Wavefunction &SetNature(const BasisFunctionNature &nature);

  bool IsNull() const { return Data().size() == 0; }
  size_t Size() const { return Data().size(); }

  bool CheckForNans() const;

  void CheckCompatibilityWith(const Wavefunction &other);
  const IConfiguration &Configuration() const { return *configuration_; }

  // assignments
  Wavefunction &operator=(const Wavefunction &other);
  Wavefunction &operator=(Wavefunction &&other) = default;

  // algebra
  Wavefunction &operator+=(const Wavefunction &other);
  Wavefunction &operator-=(const Wavefunction &other);
  Wavefunction &operator*=(const double multiplier);

  // needed for error steppers only
  Wavefunction &operator/=(const Wavefunction &other);
  Wavefunction &operator+=(const double epsilon);

  EQUIP_WITH_INPUT_ITERATOR((*data_),
                            std::vector<std::shared_ptr<IBasisFunction>>,
                            std::shared_ptr<IBasisFunction>)

private:
  ContainerType &Data() const { return *data_; };

public:
  std::shared_ptr<ContainerType> data_;
  std::shared_ptr<IConfiguration> configuration_;
};
} // namespace mce

// used by ODEInt only for steppers with error control

namespace std {

// mce::Wavefunction operator/(const mce::Wavefunction &numerator, const
// mce::Wavefunction &denominator)
//{
//	auto copy = numerator;
//	return copy/=denominator;
//}

// mce::Wavefunction operator/(const mce::Wavefunction &numerator, const
// mce::Wavefunction &denominator)
//{
//	auto copy = numerator;
//	return copy/=denominator;
//}

// mce::Wavefunction operator+(const mce::Wavefunction &left, const
// mce::Wavefunction &right)
//{
//	auto copy = left;
//	return copy += right;
//}

inline mce::Wavefunction abs(const mce::Wavefunction &state) {
  mce::Wavefunction state_abs = state;
  for (auto &basis_function : state_abs) {
    state_abs.Configuration().CWiseAbs(basis_function);
  }
  return state_abs;
}
} // namespace std
