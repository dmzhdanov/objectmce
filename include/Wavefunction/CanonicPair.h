#pragma once
#include "Definitions.h"

namespace mce {
class CanonicPair {
public:
  CanonicPair();
  CanonicPair(double p, double x);
  CanonicPair(const CanonicPair &) = default;
  CanonicPair(CanonicPair &&) = default;
  CanonicPair &operator=(const CanonicPair &) = default;
 // double XVal() const { return x; } // obsolete!
 // double PVal() const { return p; } // obsolete! 
  inline double X() const { return x_; }
  inline double P() const { return p_; }
  inline double X(double new_value){ x_ = new_value;  return x_; }
  inline double P(double new_value){ p_ = new_value;  return p_; }
  inline CanonicPair &SetZero() { x_ = 0; p_ = 0; return *this; }

  // algebra  
  CanonicPair &operator+=(const CanonicPair &other); 
  CanonicPair &operator*=(const double multiplier); 
  CanonicPair &operator/=(const CanonicPair &denominator);
  // for error steppers :(
  CanonicPair &CWisePlusEqual(const double epsilon);

  bool CheckForNans() const;

  // properties
  void Print(std::ostream &stream) const;

public:
  double x_;
  double p_;
};


// binary operations
CanonicPair operator/(const CanonicPair &p1, const CanonicPair &p2);


} // namespace mce
