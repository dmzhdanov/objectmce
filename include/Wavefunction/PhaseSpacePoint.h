#pragma once
#include <vector>

#include "CanonicPair.h"
#include "Definitions.h"
#include "Commons/ITERATOR_MACRO.h"

namespace mce {
class PhaseSpacePoint {
public:
	PhaseSpacePoint(size_t num_dof = 0); // !!!??? check if default constuctor is really needed
  ~PhaseSpacePoint();

  void Initialize(size_t number_of_pess);

  CanonicPair &operator[](const SpatialDOF_id dof_no);
  const CanonicPair& operator[](const SpatialDOF_id dof_no) const;
  size_t NumberOfDOFs() const;
  bool IsNull() const;
  void CheckCompatibilityWith(const PhaseSpacePoint &other);

  PhaseSpacePoint& SetZero();

  bool CheckForNans() const;

  // algebra
  PhaseSpacePoint &CWisePlusEqual(const PhaseSpacePoint &other);
  PhaseSpacePoint &CWiseMultiplyEqual(const double multiplier);
  PhaseSpacePoint &CWiseDivideEqual(const PhaseSpacePoint &denominator);

  PhaseSpacePoint &CWisePlusEqual(const double epsilon);

  EQUIP_WITH_INPUT_ITERATOR(phase_variables_, std::vector<CanonicPair>, CanonicPair)

	  // properties
  void Print(std::ostream &stream) const;
private:
  std::vector<CanonicPair> phase_variables_;
};

//PhaseSpacePoint CWiseDivide(const PhaseSpacePoint &numerator, const PhaseSpacePoint &denominator);

} // namespace mce
