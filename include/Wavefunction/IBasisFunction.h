#pragma once
#include <memory>

#include "ElectronicWavefunction.h"
#include "PhaseSpacePoint.h"

namespace mce {

enum class BasisFunctionNature {
  Shalashilishche,
  LameBurghardtishche,
  Garashchuchishche,
  BlueShaloBohmishche,
  ShaloBohmishche,
  ShaloBohmishche_v2,
  Garahardtishche,
  QuasiProbabilishche,
  QuasiProbabilishche_v2,
  QuasiProbabilishche_v3,
  QuasiProbabilishche_v4
};

class IBasisFunction {
  //
public:
  IBasisFunction(const PhaseSpacePoint &gaussian_center,
                 const ElectronicWavefunction &electronic_wavefunction)
      : gaussian_center_(gaussian_center),
        electronic_wavefunction_(electronic_wavefunction){};

  IBasisFunction(size_t num_spatial_dofs = 0, size_t num_pess = 0)
      : gaussian_center_(num_spatial_dofs), electronic_wavefunction_(num_pess) {
  }

  IBasisFunction(const IBasisFunction &other) = delete;

  virtual std::shared_ptr<IBasisFunction> Clone(bool copy_data = true) = 0;
  virtual ~IBasisFunction(){};

  virtual CanonicPair &GetCenter(SpatialDOF_id id) = 0;
  virtual AmplitudePhasePair &GetPESAmplitude(PES_id id) = 0;
  virtual AmplitudePhasePair &GetMultiplier() = 0;

  virtual IBasisFunction &SetZero() = 0;

  virtual bool CheckForNans() const = 0;

  // algebra
  virtual IBasisFunction &CWisePlusEqual(const IBasisFunction &other) = 0;
  virtual IBasisFunction &CWiseMultiplyEqual(const double multiplier) = 0;
  virtual IBasisFunction &CWiseDivideEqual(const IBasisFunction &denom) = 0;

  virtual IBasisFunction &CWisePlusEqual(const double epsilon) = 0;

  virtual BasisFunctionNature Nature() const = 0;
  virtual BasisFunctionNature Nature(const BasisFunctionNature &nature) =  0;

public:
  PhaseSpacePoint gaussian_center_;
  ElectronicWavefunction electronic_wavefunction_;
  AmplitudePhasePair multiplier_;

  BasisFunctionNature nature_;
};

} // namespace mce
