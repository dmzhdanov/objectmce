#pragma once

#include "ElectronicWavefunction.h"
#include "IBasisFunction.h"
#include "PhaseSpacePoint.h"
#include <complex>

namespace mce {
class EhrenfestBasisFunction : public IBasisFunction {
public:
  EhrenfestBasisFunction(const PhaseSpacePoint &gaussian_center,
                         const ElectronicWavefunction &electronic_wavefunction)
      : IBasisFunction(gaussian_center, electronic_wavefunction) {
    nature_ = BasisFunctionNature::Shalashilishche;
  };
  ~EhrenfestBasisFunction();

public:
  EhrenfestBasisFunction(size_t num_spatial_dofs = 0, size_t num_pess = 0)
      : IBasisFunction(num_spatial_dofs, num_pess) {
    nature_ = BasisFunctionNature::Shalashilishche;
  }
  EhrenfestBasisFunction(const mce::EhrenfestBasisFunction &other);
  std::shared_ptr<IBasisFunction> Clone(bool copy_data = true) override;

  CanonicPair &GetCenter(SpatialDOF_id id) override {
    return gaussian_center_[id];
  };
  AmplitudePhasePair &GetPESAmplitude(PES_id id) override {
    return electronic_wavefunction_[id];
  };
  AmplitudePhasePair &GetMultiplier() override { return multiplier_; };

  BasisFunctionNature Nature() const override { return nature_; }

  BasisFunctionNature Nature(const BasisFunctionNature &nature) override {
    nature_ = nature;
    return nature_;
  }

  bool IsNull() const;
  void CheckCompatibilityWith(const IBasisFunction &other);

  IBasisFunction &SetZero() override;

  bool CheckForNans() const override;

  // algebra
  IBasisFunction &CWisePlusEqual(const IBasisFunction &other) override;
  IBasisFunction &CWiseMultiplyEqual(const double multiplier) override;
  IBasisFunction &CWiseDivideEqual(const IBasisFunction &denominator) override;

  IBasisFunction &CWisePlusEqual(const double epsilon) override;
};

// std::shared_ptr<IBasisFunction> CWiseDivide(const IBasisFunction &p1,
//                                   const IBasisFunction &p2);

} // namespace mce
