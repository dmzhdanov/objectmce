#pragma once
#include <vector>

#include "AmplitudePhasePair.h"
#include "Commons/ITERATOR_MACRO.h"
#include "Definitions.h"

namespace mce {
class ElectronicWavefunction {
public:
  ElectronicWavefunction(size_t num_states = 0)
      : coefficients_(num_states){}; //!!!??? check if the default constructor
                                     //! is really needed
  //~ElectronicWavefunction() {};

  void Initialize(size_t number_of_pess);

  size_t NumberOfPESs() const;
  bool IsNull() const;
  void CheckCompatibilityWith(const ElectronicWavefunction &other);

  bool CheckForNans() const;

  ElectronicWavefunction &SetZero();

  ElectronicWavefunction &CWisePlusEqual(const ElectronicWavefunction &other);
  ElectronicWavefunction &CWiseMultiplyEqual(const double multiplier);
  ElectronicWavefunction &CWiseDivideEqual(const ElectronicWavefunction &denominator);

  ElectronicWavefunction &CWisePlusEqual(const double epsilon);

  AmplitudePhasePair &operator[](PES_id id);
  const AmplitudePhasePair &operator[](PES_id id) const;

  EQUIP_WITH_INPUT_ITERATOR(coefficients_,
                            std::vector<AmplitudePhasePair>,
                            AmplitudePhasePair)

public:
  std::vector<AmplitudePhasePair> coefficients_;
};

// ElectronicWavefunction CWiseDivide(const ElectronicWavefunction &numerator,
//                                   const ElectronicWavefunction &denominator);
} // namespace mce
