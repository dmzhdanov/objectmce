#pragma once
#include <Eigen/Core> //defines BandMatrix
#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "Commons/QI_Error.h"
#include <string>
#include <type_traits>

namespace mce {
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
    MatrixXd_r;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>
MatrixXd_c;

typedef Eigen::Matrix<std::complex<double>, Eigen::Dynamic, Eigen::Dynamic,
                      Eigen::RowMajor>
    MatrixXc_r;
typedef Eigen::DiagonalMatrix<double, Eigen::Dynamic> MatrixXd_diag;
// Now can use alias templates!
template <class T>
using MatrixX_r =
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
typedef Eigen::Matrix<std::complex<double>, 1, Eigen::Dynamic, Eigen::RowMajor>
    VectorXc_r;
typedef Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1, Eigen::ColMajor>
    VectorXc_c;
typedef Eigen::Matrix<double, 1, Eigen::Dynamic, Eigen::RowMajor> VectorXd_r;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1,  Eigen::ColMajor> VectorXd_c;

template <class T> using VectorX = Eigen::Matrix<T, 1, Eigen::Dynamic>;

typedef Eigen::SparseMatrix<std::complex<double>, Eigen::RowMajor>
    MatrixXc_r_sp;

typedef size_t PES_id;
typedef size_t SpatialDOF_id;

enum class EvaluationPolicies { CCS = 1 };

template <EvaluationPolicies policy> class PolicyTraits {};

using DefaultEvaluationPolicy = PolicyTraits<EvaluationPolicies::CCS>;

#define MCE_POLICY(name) mce::PolicyTraits<mce::EvaluationPolicies::name>

// Global variables

extern const std::complex<double> I;
extern double kHBar;
extern double kTolerance;

enum DOFTypes { Continuous, Quantized };

}; // namespace mce
