#pragma once

#include "Definitions.h"
#include "IConfiguration.h"

namespace mce {
class Wavefunction;

class Configuration : public IConfiguration {
public:
  Configuration(size_t num_pess = 0, size_t num_spatial_dofs = 1,
                double gaussian_width = 1, double gaussian_overlap_cutoff = 5);
  //Configuration();
  ~Configuration();
  size_t NumberOfSpatialDOFs() const override { return num_spatial_dofs_; }
  size_t NumberOfPESs() const override { return num_pess_; }
  double GaussianWidth(SpatialDOF_id dof_id) const override {
    return gaussian_width_;
  }
  double GaussianOverlapCutoff(SpatialDOF_id dof_id = 0) const override {
    return gaussian_overlap_cutoff_;
  } // in units of gaussian width;

  void CWiseAbs(std::shared_ptr<IBasisFunction> basis_function) const override;
  double InfinityNorm(const Wavefunction &wavefunction) const override;

public:
  size_t num_pess_;
  size_t num_spatial_dofs_;
  double gaussian_width_;
  double gaussian_overlap_cutoff_;
};
} // namespace mce
