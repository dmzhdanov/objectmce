#include "PyMCE.h"

#include "Evaluator/Evaluator_CCS.h"
//#include "Wavefunction/Wavefunction.h"

namespace pymce {

void init_PyEvaluator(py::module &m) {
  using namespace mce;
  using PyEvaluator = Evaluator<MCE_POLICY(CCS)>;

  py::class_<PyEvaluator>(m, "Evaluator")
      // no constructor! - shouldn't be constructed manually
      .def("__call__", &PyEvaluator::operator())
      .def("Mean", &PyEvaluator::Mean);
}

} // namespace pymce
