#include "PyMCE.h"

#include "WavefunctionFactory.h"

namespace pymce {

void init_PyWavefunctionFactory(py::module &m) {
  using namespace mce;

  py::class_<WavefunctionFactory>(m, "WavefunctionFactory")
      .def(py::init<int, int, double, double>())
      .def("Create", &WavefunctionFactory::Create,
           "Creates zero-intitalized wavefunction of <size> Gaussians")
      .def("CreateUniformSwarm2", &WavefunctionFactory::CreateUniformSwarm2)
      .def("CreateRandomSwarm2", &WavefunctionFactory::CreateRandomSwarm2,
           py::arg("pes_id"), py::arg("center"), py::arg("spread"),
           py::arg("basis_size"), py::arg("random_seed") = 0);
}

} // namespace pymce
