#include "PyMCE.h"

#include "OperatorFactory.h"

namespace pymce {

void init_PyOperatorFactory(py::module &m) {
  using namespace mce;
  using PyOperatorFactory = OperatorFactory<MCE_POLICY(CCS)>;

  py::class_<PyOperatorFactory>(m, "OperatorFactory")
      .def(py::init<>())
      .def("CreateX", &PyOperatorFactory::CreateX)
      .def("CreateP", &PyOperatorFactory::CreateP)
      .def("CreateId", &PyOperatorFactory::CreateId)
      .def("CreateExp", &PyOperatorFactory::CreateExp);
}

} // namespace pymce
