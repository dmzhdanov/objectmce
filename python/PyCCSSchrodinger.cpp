#include "PyMCE.h"

#include "Algorithms/CCSSchrodinger.h"
#include "Algorithms/CCSSchrodingerPiloted.h"
#include "Algorithms/CCSSchrodingerWerewolf.h"
#include "Algorithms/CCSSchrodinger_v2.h"
#include "Algorithms/CCSSchrodinger_v3.h"

namespace pymce {

void init_PyCCSSchrodinger(py::module &m) {
  using namespace mce;
  using namespace algorithm;
  using PolicyType = MCE_POLICY(CCS);

  py::class_<ISystem> PyISystem(m, "ISystem");
  PyISystem.def("__call__", &ISystem::operator());

  py::class_<CCSSchrodingerSystem>(m, "CCSSchrodingerSystem", PyISystem)
      .def(py::init<const Operator<PolicyType> &, const Operator<PolicyType> &,
                    OperatorFactory<PolicyType> &>());

  py::class_<CCSSchrodingerSystem_v2>(m, "CCSSchrodingerSystem_v2", PyISystem)
      .def(py::init<const Operator<PolicyType> &, const Operator<PolicyType> &,
                    OperatorFactory<PolicyType> &>());

  py::class_<CCSSchrodingerSystem_v3>(m, "CCSSchrodingerSystem_v3", PyISystem)
      .def(py::init<const Operator<PolicyType> &, const Operator<PolicyType> &,
                    OperatorFactory<PolicyType> &>());

  py::class_<CCSSchrodingerSystemPiloted>(m, "CCSSchrodingerSystemPiloted",
                                          PyISystem)
      .def(py::init<const Operator<PolicyType> &, const Operator<PolicyType> &,
                    OperatorFactory<PolicyType> &>())
      .def("SetSmoothingScale",
           &CCSSchrodingerSystemPiloted::SetSmoothingScale);

  py::class_<CCSSchrodingerWerewolfSystem>(m, "CCSSchrodingerWerewolfSystem",
                                           PyISystem)
      .def(py::init<const Operator<PolicyType> &, const Operator<PolicyType> &,
                    OperatorFactory<PolicyType> &>())
      .def("SetSmoothingScale",
           &CCSSchrodingerWerewolfSystem::SetSmoothingScale)
      .def("SetRegularizer", &CCSSchrodingerWerewolfSystem::SetRegularizer)
      .def("SetMomentumMMARegularizer",
           &CCSSchrodingerWerewolfSystem::SetMomentumMMARegularizer)
      .def("SetAmplitudeMMARegularizer",
           &CCSSchrodingerWerewolfSystem::SetAmplitudeMMARegularizer)
      .def("SetReprojectionRegularizer",
           &CCSSchrodingerWerewolfSystem::SetReprojectionRegularizer)
      .def("SetRegularizePhaseScale",
           &CCSSchrodingerWerewolfSystem::SetRegularizePhaseScale)
      .def("SetHusimiWidthFactors",
           &CCSSchrodingerWerewolfSystem::SetHusimiWidthFactors)
      .def("SetGaugeApproximationOrder",
           &CCSSchrodingerWerewolfSystem::SetGaugeApproximationOrder);
}
} // namespace pymce
