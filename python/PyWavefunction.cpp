#include "PyMCE.h"

#include "Wavefunction/Wavefunction.h"

// PYBIND11_MAKE_OPAQUE(mce::Wavefunction)

namespace pymce {

void init_PyWavefunction(py::module &m) {
  using namespace mce;
  using PyScalar = std::complex<double>;

  py::enum_<BasisFunctionNature>(m, "BasisFunctionNature", py::arithmetic())
      .value("Shalashilishche", BasisFunctionNature::Shalashilishche)
      .value("LameBurghardtishche", BasisFunctionNature::LameBurghardtishche)
      .value("Garashchuchishche", BasisFunctionNature::Garashchuchishche)
      .value("BlueShaloBohmishche", BasisFunctionNature::BlueShaloBohmishche)
      .value("ShaloBohmishche", BasisFunctionNature::ShaloBohmishche)
      .value("ShaloBohmishche_v2", BasisFunctionNature::ShaloBohmishche_v2)
      .value("Garahardtishche", BasisFunctionNature::Garahardtishche)
      .value("QuasiProbabilishche", BasisFunctionNature::QuasiProbabilishche)
      .value("QuasiProbabilishche_v2",
             BasisFunctionNature::QuasiProbabilishche_v2)
	  .value("QuasiProbabilishche_v3",
		  BasisFunctionNature::QuasiProbabilishche_v3)
	  .value("QuasiProbabilishche_v4",
			  BasisFunctionNature::QuasiProbabilishche_v4);

  py::class_<CanonicPair>(m, "CanonicPair")
      .def(py::init<>())
      .def(py::init<CanonicPair const &>())
      .def("X", (double (CanonicPair::*)() const) & CanonicPair::X)
      .def("X", py::overload_cast<double>(&CanonicPair::X))
      .def("P", (double (CanonicPair::*)() const) & CanonicPair::P)
      .def("P", py::overload_cast<double>(&CanonicPair::P));

  py::class_<AmplitudePhasePair>(m, "AmplitudePhasePair")
      .def(py::init<>())
      .def(py::init<AmplitudePhasePair const &>())
      .def("Amplitude", (PyScalar(AmplitudePhasePair::*)() const) &
                            AmplitudePhasePair::Amplitude)
      .def("Amplitude",
           py::overload_cast<const PyScalar &>(&AmplitudePhasePair::Amplitude))
      .def("Phase",
           (double (AmplitudePhasePair::*)() const) & AmplitudePhasePair::Phase)
      .def("Phase",
           py::overload_cast<const double>(&AmplitudePhasePair::Phase));

  py::class_<IBasisFunction>(m, "EhrenfestBasisFunction")
      // .def(py::init<>()) // virtual class!
      // .def(py::init<IBasisFunction const &>())
      .def("GetCenter", &IBasisFunction::GetCenter,
           "Get (non-const) Gaussian center coordinates.",
           py::return_value_policy::reference)
      .def("GetMultiplier", &IBasisFunction::GetMultiplier,
           "Get (non-const) overall wavefunction multiplier.",
           py::return_value_policy::reference)
      .def("Nature", (BasisFunctionNature(IBasisFunction::*)() const) &
                         IBasisFunction::Nature)
      .def("Nature", py::overload_cast<const BasisFunctionNature &>(
                         &IBasisFunction::Nature));
  ;

  py::class_<Wavefunction>(m, "Wavefunction")
      .def(py::init<Wavefunction const &>())
      // .def("__assign__", py::overload_cast<const Wavefunction
      // &>(&Wavefunction::operator=)) // doesn't work because assignments in
      // python cannot be overloaded. See:
      // https://nedbatchelder.com/text/names.html
      .def(py::self + py::self)
      .def(py::self - py::self)
      .def(py::self += py::self)
      .def(py::self -= py::self)
      .def(py::self *= double())
      .def(double() * py::self)
      .def(py::self * double())
      .def("Configuration", &Wavefunction::Configuration, "",
           py::return_value_policy::reference)
      .def("Size", &Wavefunction::Size)
      .def("Clone", &Wavefunction::Clone)
      .def("SetNature", &Wavefunction::SetNature)
      .def("__lshift__", &Wavefunction::operator<<)
      .def("CheckCompatibilityWith", &Wavefunction::CheckCompatibilityWith)
      .def(
          "__getitem__",
          [](Wavefunction &op, size_t id) { return op[id].get(); },
          py::return_value_policy::reference) // ??? cannot handle smart
                                              // pointers as return type?
      // doesn't compile properly :( use plain-style iterators for now
      //.def(
      //    "__iter__",
      //    [](Wavefunction &w) { return py::make_iterator(w.begin(), w.end());
      //    }, py::keep_alive<0, 1>())
      ;
}

} // namespace pymce
