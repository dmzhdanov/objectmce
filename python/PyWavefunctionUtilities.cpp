#include "PyMCE.h"

#include "Algorithms/WavefunctionUtilities.h"

namespace pymce {

void init_PyWavefunctionUtilities(py::module &m) {
  using namespace mce;
  using namespace ccs_utilities;
  using PyScalar = std::complex<double>;

  m.def("Project", &Project, "Project wavefunction onto another basis.");
  m.def("Filter", &Filter, "Removes insignificant wavefunction basis entries.");
  m.def("Value", &Value, "Removes insignificant wavefunction basis entries.");
}

} // namespace pymce
