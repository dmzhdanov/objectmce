#include "PyMCE.h"

#include "Algorithms/CCSPropagator.h"
#include "Algorithms/CCSWerewolfPolicy.h"

namespace pymce {

void init_PyCCSPropagator(py::module &m) {
  using namespace mce;
  using namespace algorithm;
  using PolicyType = MCE_POLICY(CCS);

  py::class_<CCSWerewolfPolicy>(m, "CCSWerewolfPolicy")
	  .def(py::init<const CCSSchrodingerWerewolfSystem&>())
	  .def("__call__", &CCSWerewolfPolicy::operator())
	  .def("SetToGarashchuchishcheThreshold", &CCSWerewolfPolicy::SetToGarashchuchishcheThreshold);

  py::class_<CCSPropagator>(m, "CCSPropagator")
      .def(py::init<>())
      .def("Integrate", &CCSPropagator::Integrate)
	  .def("SetWerewolfPolicy", &CCSPropagator::SetWerewolfPolicy)
	  .def("SetAdaptiveFlag", &CCSPropagator::SetAdaptiveFlag)
      .def("ObservedIntegrate",
           [](CCSPropagator &propagator, const ISystem &system,
              Wavefunction &wavefunction, double start_time, double end_time,
              double delta_t,
              const std::function<bool(Wavefunction &, const double, const size_t)>
                  &observer,
              double observation_interval, double absolute_error_tolerance,
              double relative_error_tolerance) {
#ifdef PYTHON_BINDINGS
             pybind11::gil_scoped_release release;
#endif
             propagator.ObservedIntegrate(
                 system, wavefunction, start_time, end_time, delta_t, observer,
                 observation_interval, absolute_error_tolerance,
                 relative_error_tolerance);
#ifdef PYTHON_BINDINGS
             pybind11::gil_scoped_acquire acquire;
#endif
           });
}
} // namespace pymce
