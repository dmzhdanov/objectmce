
#include "PyMCE.h"

#include "OperatorFactory.h"
#include "Operator_CCS.h"
#include "Wavefunction/Wavefunction.h"

namespace py = pybind11;

namespace pymce {
	void init_PyOperator(py::module &);
	void init_PyOperatorFactory(py::module &);
	void init_PyConfiguration(py::module &);
	void init_PyWavefunction(py::module &);
	void init_PyWavefunctionFactory(py::module &);
	void init_PyEvaluator(py::module &);
	// extras
	void init_PyWavefunctionUtilities(py::module &);
	void init_PyCCSSchrodinger(py::module &);
	void init_PyCCSPropagator(py::module &);
	void init_PyEvaluatorStrategy(py::module &m);
}

PYBIND11_MODULE(PyMCE, m) {
	//py::class_<Pet>(m, "Pet")
	//	.def(py::init<const std::string &>())
	//	.def("setName", &Pet::setName)
	//	.def("getName", &Pet::getName);
	using namespace pymce;

	init_PyOperator(m);
	init_PyOperatorFactory(m);
	init_PyConfiguration(m);
	init_PyWavefunction(m);
	init_PyWavefunctionFactory(m);
	init_PyEvaluator(m);
	init_PyEvaluatorStrategy(m);
	// extras
	init_PyWavefunctionUtilities(m);
	init_PyCCSSchrodinger(m);
	init_PyCCSPropagator(m);
}
