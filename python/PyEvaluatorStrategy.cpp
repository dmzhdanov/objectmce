#include "PyMCE.h"

#include "Evaluator/EvaluatorStrategyQuasiprobabilityFlow_CCS.h"
#include "Evaluator/EvaluatorStrategyQuasiprobabilityGauge_CCS.h"
//#include "Wavefunction/Wavefunction.h"

namespace pymce {

	void init_PyEvaluatorStrategy(py::module &m) {
		using namespace mce;
		using PyEvaluatorStrategy = IEvaluatorStrategy;
		using PyEvaluatorStrategyQF = evaluator_strategy::QuaisiprobabilityFlowStrategy<MCE_POLICY(CCS)>;
		using PyFlowType = evaluator_strategy::QuasiprobabilityFlowType;

		py::class_<PyEvaluatorStrategy>(m, "EvaluatorStrategy")
			// no constructor! - shouldn't be constructed manually
			.def("Clone", &PyEvaluatorStrategy::Clone);

		py::enum_<PyFlowType>(m, "QuasiprobabilityFlowType", py::arithmetic())
			.value("Momentum", PyFlowType::Momentum)
			.value("Position", PyFlowType::Position);

		py::class_<PyEvaluatorStrategyQF>(m, "EvaluatorStrategyQuasiprobabilityFlow")
			// no constructor! - shouldn't be constructed manually
			.def(py::init<PyFlowType, CanonicPair,size_t,SpatialDOF_id>())
			.def("Clone", &PyEvaluatorStrategyQF::Clone)
			.def("ShallowCopy", &PyEvaluatorStrategyQF::ShallowCopy)
			.def("CenterBasisFunction", (size_t (PyEvaluatorStrategyQF::*)() const) & PyEvaluatorStrategyQF::CenterBasisFunction)
			.def("CenterBasisFunction", py::overload_cast<size_t>(&PyEvaluatorStrategyQF::CenterBasisFunction))
			.def("DofId", (SpatialDOF_id(PyEvaluatorStrategyQF::*)() const) & PyEvaluatorStrategyQF::DofId)
			.def("DofId", py::overload_cast<SpatialDOF_id>(&PyEvaluatorStrategyQF::DofId))
			.def("QuasiprobabilityFlowType", (PyFlowType(PyEvaluatorStrategyQF::*)() const) & PyEvaluatorStrategyQF::FlowType)
			.def("QuasiprobabilityFlowType", py::overload_cast<PyFlowType>(&PyEvaluatorStrategyQF::FlowType));

		/////////////////////////////////////////////
		/////////////////////////////////////////////

		using PyEvaluatorStrategyQG = evaluator_strategy::QuaisiprobabilityGaugeStrategy<MCE_POLICY(CCS)>;
		using PyGaugeType = evaluator_strategy::QuasiprobabilityGaugeType;

		py::enum_<PyGaugeType>(m, "QuasiprobabilityGaugeType", py::arithmetic())
			.value("Momentum", PyGaugeType::Momentum)
			.value("Position", PyGaugeType::Position);

		py::class_<PyEvaluatorStrategyQG>(m, "EvaluatorStrategyQuasiprobabilityGauge")
			// no constructor! - shouldn't be constructed manually
			.def(py::init<double, double, PyGaugeType, CanonicPair, size_t>())
			.def("Clone", &PyEvaluatorStrategyQG::Clone)
			.def("ShallowCopy", &PyEvaluatorStrategyQG::ShallowCopy)
			.def("CenterBasisFunction", (size_t(PyEvaluatorStrategyQG::*)() const) & PyEvaluatorStrategyQG::CenterBasisFunction)
			.def("CenterBasisFunction", py::overload_cast<size_t>(&PyEvaluatorStrategyQG::CenterBasisFunction))
			.def("QuasiprobabilityGaugeType", (PyGaugeType(PyEvaluatorStrategyQG::*)() const) & PyEvaluatorStrategyQG::FlowType)
			.def("QuasiprobabilityGaugeType", py::overload_cast<PyGaugeType>(&PyEvaluatorStrategyQG::FlowType))
			.def("MeanMomentum", (double(PyEvaluatorStrategyQG::*)() const) & PyEvaluatorStrategyQG::MeanMomentum)
			.def("MeanMomentum", py::overload_cast<double>(&PyEvaluatorStrategyQG::MeanMomentum))
			.def("MeanMomentumXDerivative", (double(PyEvaluatorStrategyQG::*)() const) & PyEvaluatorStrategyQG::MeanMomentumXDerivative)
			.def("MeanMomentumXDerivative", py::overload_cast<double>(&PyEvaluatorStrategyQG::MeanMomentumXDerivative));
	}

} // namespace pymce