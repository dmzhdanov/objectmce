#include "PyMCE.h"

#include "Operator_CCS.h"
//#include "Wavefunction/Wavefunction.h"

namespace pymce {

void init_PyOperator(py::module &m) {
  using namespace mce;
  using PyOperator = Operator<MCE_POLICY(CCS)>;
  using PyEvaluator = Evaluator<MCE_POLICY(CCS)>;
  using PyScalar = std::complex<double>;

  py::class_<PyOperator>(m, "Operator")
      // .def(py::init<const std::string &>()) //Private constructor
      .def(py::self + py::self)
      .def(py::self - py::self)
      .def(py::self * py::self)
      .def(py::self += py::self)
      .def(py::self -= py::self)
      .def(py::self *= py::self)
      .def(py::self *= PyScalar())
      .def(PyScalar() * py::self)
      .def(py::self * PyScalar())
      .def(
          "__pow__",
          [](const PyOperator &op, int exp) -> PyOperator {
            if (exp <= 0) {
              QI_Error::RaiseError("[ERROR] PyOperator-_pow_ - unfortunately, "
                                   "operator^k for k<=0 is not supported yet...");
              return op;
            }
            PyOperator out = op;
            for (int i = 1; i < exp; i++) {
              out *= op;
            }
            return out;
          },
          py::is_operator())
      .def(
          "__neg__", &PyOperator::operator-
/*          [](const PyOperator &op) -> PyOperator {
            PyOperator out = -op;
            return out;
          }*/,
          py::is_operator())
      .def("__repr__",
           [](const PyOperator &op) {
             std::stringstream s;
             op.Print(s);
             return s.str();
           })
      .def("Evaluate",
           [](const PyOperator &op, const Wavefunction &bra,
              const Wavefunction &ket) {
             return op.Evaluate({bra, ket});
           })
      .def("Evaluate", [](const PyOperator &op, const Wavefunction &bra) {
        return op.Evaluate({bra});
      });

} // namespace pymce

} // namespace pymce
