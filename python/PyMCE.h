#include <pybind11/complex.h>
#include <pybind11/eigen.h>
#include <pybind11/functional.h>
#include <pybind11/operators.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "Definitions.h"

namespace py = pybind11;

// struct Pet {
//	Pet(const std::string &name) : name(name) { }
//	void setName(const std::string &name_) { name = name_; }
//	const std::string &getName() const { return name; }
//
//	std::string name;
//};
