#include "PyMCE.h"

#include "IConfiguration.h"

namespace pymce {

	void init_PyConfiguration(py::module &m) {
		using namespace mce;
		using PyScalar = std::complex<double>;

		py::class_<IConfiguration>(m, "Configuration")
			// .def(py::init<>())  // Abstract class!
			.def("NumberOfSpatialDOFs", &IConfiguration::NumberOfSpatialDOFs)
			.def("GaussianWidth", &IConfiguration::GaussianWidth)
			.def("GaussianQuadraticCoefficient", &IConfiguration::GaussianQuadraticCoefficient)
			.def("GaussianOverlapCutoff", &IConfiguration::GaussianOverlapCutoff)
			.def("NumberOfPESs", &IConfiguration::NumberOfPESs);
	}
}